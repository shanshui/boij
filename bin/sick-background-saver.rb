#!/usr/bin/env ruby
# background_saver.rb

=begin

13/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

Saves tye currently operative parameters and background data
for the Sick camera

$Id: background_saver.rb 3468 2012-03-20 13:56:50Z karl $

=end

require 'getoptlong'
require 'sick/background_saver'

dname=nil
srange=nil
angres=nil

opts=GetoptLong::new(['--dname','-d',GetoptLong::REQUIRED_ARGUMENT],
                     ['--scanrange','-s',GetoptLong::REQUIRED_ARGUMENT],
                     ['--angres','-a',GetoptLong::REQUIRED_ARGUMENT])

opts.each do |opt,arg|
  case opt
  when '--dname'
    dname=arg
  when '--scanrange'
    srange=arg.to_i
    raise "Bad scanrange (#{srange}, must be either 100 or 180)" if(srange!=100 && srange!=180)
  when '--angres'
    angres=arg.to_i
    raise "Bad ang res (#{angres}, must be 25 or 50)" if(angres!=25 && angres!=50)
  else
    raise "Usage: #{$0} -d device_path -s scanrange -a angres (#{opts.error_message()})" unless(dname && srange && angres)
  end
end

Backgsa::new(dname,srange,angres).runme()
