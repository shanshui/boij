require 'yaml'
require 'date'

class Hours_config
  attr_accessor(:enabled,:schedule)

  def initialize()
    set_defaults()
  end

  def set_defaults()
    @enabled = false if @enabled.nil?
    @schedule = {
      "Mon" => { "shutdown" => 'at 19:00', "wakeup" => 'Tue 10:00' },
      "Tue" => { "shutdown" => 'at 19:00', "wakeup" => 'Wed 10:00' },
      "Wed" => { "shutdown" => 'at 19:00', "wakeup" => 'Thu 10:00' },
      "Thu" => { "shutdown" => 'at 21:00', "wakeup" => 'Fri 10:00' },
      "Fri" => { "shutdown" => 'at 21:00', "wakeup" => 'Sat 10:00' },
      "Sat" => { "shutdown" => 'at 19:00', "wakeup" => 'Sun 10:00' },
      "Sun" => { "shutdown" => 'at 19:00', "wakeup" => 'Tue 10:00' }
    } if @schedule.nil?
  end

  def Hours_config::load
    begin
      cfg = YAML::load(File::read(HOURSCONF))
    rescue
      cfg = Hours_config.new
    end
    cfg.set_defaults()
    cfg
  end

  def save
    File::open(HOURSCONF,'wb') do |f|
      f.write(YAML::dump(self))
    end
  end

  def today
    day = DateTime.now.strftime("%a")
    @schedule[day]
  end

  def shutdown
    d = today
    if d
      s = d["shutdown"]
      if s
        s = s.gsub(/[^0-9:]/, '')
      end
      s
    end
  end

  def wakeup
    d = today
    if d
      d["wakeup"]
    end
  end

end
