#!/usr/bin/env ruby
# daemoner.rb

=begin

29/12/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

The logic that handles one run

$Id: daemoner.rb 2629 2010-11-11 12:30:36Z karl $

=end

require 'boij.rb'
require 'engine3'
require 'moving_people_master'
require 'lumin_hound'

Fluid_logfile::new('daemoner')

def interr
  $eps.reverse.each do |ep|
    loggo("Expiring pid #{ep.pid} (#{ep.cls.name})")
    ep.expire()
    sleep(2)
  end
  sleep(3)
  loggo('Terminating.')
  exit(0)
end

daemons=[[Engine,nil],5,[Moving_people_master,[1,true]],[Lumin_hound,nil]]

$eps=[]
daemons.each do |d|
  if(d.is_a?(Numeric))
    sleep(d)
  else
    $eps.push(External_proc::new(d[0],d[1]))
  end
end

Signal::trap('INT') do
  exit(0)
end
Signal::trap('HUP') do
  exit(0)
end

at_exit do
  interr()
end

loop do
  sleep(20)
  $eps.each do |ep|
    v=ep.is_dead?()
    if(v)
      loggo("#{ep.cls.to_s}/#{ep.pid} died! Must die to be reborn.")
      exit(0)
    end
  end
end
