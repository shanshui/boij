#!/usr/bin/env ruby

=begin

18/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The viewing and tuning of motion detection
Directly from Valencia and Zwolle

$Id: filtered_test.rb 1506 2008-02-29 15:16:45Z karl $

=end

require 'boij.so'
require 'gtk2'

class Ftest
  NTHRESH=5
  THRESHLBLS=["When contrib to eval","When clipping eval","When grouping (direct)",
    "When grouping (recurse)","Group acceptance"]
  DOTSIZE=20

  def initialize(controlflag)
    @cf=controlflag
    @sva=Boij_sharedvideoarea::new(nil)
    if(@cf)
      @curthresh=@sva.get_threshold()
      @cur_grid,@togx,@togy=@sva.get_grid()
      @cur_gridpos=[0,0]
      @max_gridpos=[Boij_sharedvideoarea::GRID_X,Boij_sharedvideoarea::GRID_Y]
    end
  end

  def runme
    Gtk::init()
    window=Gtk::Window.new("Boijmans filtering meditator")
    main_hbox=Gtk::HBox.new(false,5)
    window.add(main_hbox)

    #
    # The colours
    #
    
    @backg_col=Gdk::Color::new(0,0,80*256)
    @grid_col=Gdk::Color::new(255*256,255*256,0)
    @basegrid_col=Gdk::Color::new(255*256,127*256,0)
    @colours=[]
    Boij_sharedvideoarea::MAX_MONITORED.times() do |j|
      col=Gdk::Color::new(rand(256*256),rand(256*256),rand(256*256))
      @colours.push(col)
    end
    
    #
    # The drawing area
    #

    @da=Gtk::DrawingArea.new
    @da.set_property('can-focus',TRUE)
    if(@cf)
      @da.set_size_request(IMGSIZE[0]+Boij_sharedvideoarea::MAX_GRID_OFFSET*2,IMGSIZE[1]+Boij_sharedvideoarea::MAX_GRID_OFFSET*2)

      @da.add_events(Gdk::Event::KEY_PRESS_MASK)
      @da.signal_connect("key_press_event") do |widget, event|
	f=event.state.to_i()
	shift=(event.state.to_i()&1)>0
	ctrl=(event.state.to_i()&4)>0
	
	case event.keyval
	when Gdk::Keyval::GDK_Down
	  if(ctrl)
	    update_current_gridpos([0,shift ? 10 : 1])
	  else
	    change_current_gridpos([0,1])
	  end
	when Gdk::Keyval::GDK_Up
	  if(ctrl)
	    update_current_gridpos([0,shift ? -10 : -1])
	  else
	    change_current_gridpos([0,-1])
	  end
	when Gdk::Keyval::GDK_Left
	  if(ctrl)
	    update_current_gridpos([shift ? -10 : -1,0])
	  else
	    change_current_gridpos([-1,0])
	  end
	when Gdk::Keyval::GDK_Right
	  if(ctrl)
	    update_current_gridpos([shift ? 10 : 1,0])
	  else
	    change_current_gridpos([1,0])
	  end
	end
	true
      end

      gbox=Gtk::HBox::new(false,5)
      
      b=Gtk::Button.new('Save grid')
      b.signal_connect('clicked') do
	save_grid()
      end
      gbox.pack_start(b,false,false)
      b=Gtk::Button.new('Reset grid')
      b.signal_connect('clicked') do
	reset_grid()
      end
      gbox.pack_start(b,false,false)
      
      @gadj_x=Gtk::Adjustment.new(0,-100,100,1,10,0)
      s=Gtk::SpinButton.new(@gadj_x,0,0)
      gbox.pack_start(Gtk::Label::new('Adj X'),false,false)
      gbox.pack_start(s,false,false)
      
      @gadj_y=Gtk::Adjustment.new(0,-100,100,1,10,0)
      s=Gtk::SpinButton.new(@gadj_y,0,0)
      gbox.pack_start(Gtk::Label::new('Adj Y'),false,false)
      gbox.pack_start(s,false,false)
      
      fgr=Gtk::Button.new('Fix greyness')
      fgr.signal_connect('clicked') do
	fix_greyness()
      end
      
      closebtn=Gtk::Button.new('Close')
      closebtn.signal_connect('clicked') do
	Gtk::main_quit()
      end
      
      box_th=Gtk::VBox.new(false,5)
      frame_th=Gtk::Frame.new('Threshold').add(box_th)
      
      @da.signal_connect('expose_event') do |w,e|
	repaint(w)
      end

      main_hbox.pack_start(Gtk::VBox.new(false,5).pack_start(@da,false,false).
			   pack_start(Gtk::HBox.new(false,5).pack_start(gbox,false,false).pack_start(fgr,false,false).
				      pack_start(closebtn,false,true)),
			   true,true).pack_start(frame_th,false,false)
    else
      main_hbox.pack_start(Gtk::VBox.new(false,5).pack_start(@da,false,false))
      @da.set_size_request(IMGSIZE[0],IMGSIZE[1])
      @da.signal_connect('expose_event') do |w,e|
	simple_repaint(w)
      end
    end
    
    if(@cf)
      NTHRESH.times do |i|
	adj=Gtk::Adjustment.new(@curthresh[i],0,100,1,10,0)
	threshc=Gtk::SpinButton.new(adj,0,0)
	threshc.signal_connect('value-changed') do |spb|
	  update_thresh(i,spb.value_as_int)
	end
	box_th.pack_start(Gtk::Frame.new(THRESHLBLS[i]).add(threshc))
      end

      toggle_x=Gtk::CheckButton::new('Toggle X')
      toggle_x.set_active(@togx)
      toggle_x.signal_connect('toggled') do |cb|
	@togx=cb.active?()
      end
      toggle_y=Gtk::CheckButton::new('Toggle Y')
      toggle_y.set_active(@togy)
      toggle_y.signal_connect('toggled') do |cb|
	@togy=cb.active?()
      end
      
      box_th.pack_start(Gtk::HButtonBox::new().pack_start(toggle_x,false,false).pack_start(toggle_y,false,false))
    end
    
    window.signal_connect('destroy') do
      Gtk::main_quit()
    end
    
    window.show_all()
    
    Gtk::timeout_add(40) do
      timeout_func()
      true
    end

    Gtk::main()
  end
  
  def set_thresh
    @sva.set_threshold(@curthresh)
  end
  
  def update_thresh(pos,val)
    @curthresh[pos]=val
    set_thresh()
  end
  
  #def report_new_speed(newx,newy)
  #  $lbl1.set_text(sprintf("Speed: %2d,%2d\n",newx,newy)) if($lbl1)
  #end
  #
  #def report_capture(head)
  #  if(head.spots && head.spots.length()>0)
  #    $lbl2.set_text("#{head.spots.length()} spots. Strongest at #{head.spots[0][0]}, #{head.spots[0][1]} (#{head.spots[0][2]})")
  #  else
  #    $lbl2.set_text("Returning home.")
  #  end
  #  $da.queue_draw()
  #end
  
  def repaint(w)
    return if(!@rpix ||
	      w.allocation.width<IMGSIZE[0]+Boij_sharedvideoarea::MAX_GRID_OFFSET*2 ||
	      w.allocation.height<IMGSIZE[1]+Boij_sharedvideoarea::MAX_GRID_OFFSET*2)
    
    gc=Gdk::GC::new(w.window)
    
    gc.set_rgb_fg_color(@backg_col)
    w.window.draw_rectangle(gc,true,0,0,w.allocation.width,w.allocation.height) # clean
    
    pixbuf=Gdk::Pixbuf::new(@rpix,Gdk::Pixbuf::COLORSPACE_RGB,
			    false,8,IMGSIZE[0],IMGSIZE[1],
			    IMGSIZE[0]*3)
    w.window.draw_pixbuf(gc,pixbuf,0,0,Boij_sharedvideoarea::MAX_GRID_OFFSET,Boij_sharedvideoarea::MAX_GRID_OFFSET,
			 IMGSIZE[0],IMGSIZE[1],Gdk::RGB::DITHER_NORMAL,0,0)
    
    #
    # The grid
    #

    xoffs=Boij_sharedvideoarea::MAX_GRID_OFFSET+@gadj_x.value()
    yoffs=Boij_sharedvideoarea::MAX_GRID_OFFSET+@gadj_y.value()
    
    gc.set_line_attributes(2,Gdk::GC::LINE_SOLID,Gdk::GC::CAP_BUTT,Gdk::GC::JOIN_ROUND)
    gc.set_rgb_fg_color(@grid_col)
    brx=IMGSIZE[0]/Boij_sharedvideoarea::GRID_X
    bry=IMGSIZE[1]/Boij_sharedvideoarea::GRID_Y
    
    coo=[]  
    (Boij_sharedvideoarea::GRID_Y+1).times do |y|
      coo[y]=[]
      (Boij_sharedvideoarea::GRID_X+1).times do |x|
	coo[y].push([xoffs+brx*x+@cur_grid[y][x][0],yoffs+bry*y+@cur_grid[y][x][1]])
      end
      w.window.draw_lines(gc,coo[y])
    end
    (Boij_sharedvideoarea::GRID_X+1).times do |x|
      l=[]
      (Boij_sharedvideoarea::GRID_Y+1).times do |y|
	l.push(coo[y][x])
      end
      w.window.draw_lines(gc,l)
    end

    gc.set_rgb_fg_color(@basegrid_col)
    (Boij_sharedvideoarea::GRID_X+1).times do |x|
      w.window.draw_line(gc,Boij_sharedvideoarea::MAX_GRID_OFFSET+brx*x,Boij_sharedvideoarea::MAX_GRID_OFFSET,
			 Boij_sharedvideoarea::MAX_GRID_OFFSET+brx*x,Boij_sharedvideoarea::MAX_GRID_OFFSET+IMGSIZE[1])
    end
    (Boij_sharedvideoarea::GRID_Y+1).times do |y|
      w.window.draw_line(gc,Boij_sharedvideoarea::MAX_GRID_OFFSET,Boij_sharedvideoarea::MAX_GRID_OFFSET+bry*y,
			 Boij_sharedvideoarea::MAX_GRID_OFFSET+IMGSIZE[0],Boij_sharedvideoarea::MAX_GRID_OFFSET+bry*y)
    end
    
    gc.set_rgb_fg_color(@backg_col)
    w.window.draw_arc(gc,true,coo[@cur_gridpos[1]][@cur_gridpos[0]][0]-DOTSIZE/2,coo[@cur_gridpos[1]][@cur_gridpos[0]][1]-DOTSIZE/2,
		      DOTSIZE,DOTSIZE,0,360*64)    
    
    gc.set_line_attributes(3,Gdk::GC::LINE_SOLID,Gdk::GC::CAP_BUTT,Gdk::GC::JOIN_ROUND)
    
    if(@clients && @clients.length()>0)
      @clients.each_with_index do |sp,i|
	gc.set_rgb_fg_color(@colours[sp[0]])
	
	size=5+(sp[3][6]/10000.0)*20
	#      STDERR.printf("(((%d,%d)))",i,size)
	#        pp [sp,sp[0],sp[1],sp[2]-sp[0],sp[3]-sp[1]]
	w.window.draw_rectangle(gc,false,Boij_sharedvideoarea::MAX_GRID_OFFSET+sp[3][2],
				Boij_sharedvideoarea::MAX_GRID_OFFSET+sp[3][3],
				sp[3][4]-sp[3][2],sp[3][5]-sp[3][3])
	w.window.draw_arc(gc,true,Boij_sharedvideoarea::MAX_GRID_OFFSET+sp[3][0]-size/2,
			  Boij_sharedvideoarea::MAX_GRID_OFFSET+sp[3][1]-size/2,size,size,0,360*64)
      end
    end
  end

  def simple_repaint(w)
    return if(!@rpix || w.allocation.width<IMGSIZE[0] || w.allocation.height<IMGSIZE[1])
    
    gc=Gdk::GC::new(w.window)
    
    pixbuf=Gdk::Pixbuf::new(@rpix,Gdk::Pixbuf::COLORSPACE_RGB,
			    false,8,IMGSIZE[0],IMGSIZE[1],
			    IMGSIZE[0]*3)
    w.window.draw_pixbuf(gc,pixbuf,0,0,0,0,
			 IMGSIZE[0],IMGSIZE[1],Gdk::RGB::DITHER_NORMAL,0,0)
    
    #
    # The grid
    #
    
    gc.set_line_attributes(2,Gdk::GC::LINE_SOLID,Gdk::GC::CAP_BUTT,Gdk::GC::JOIN_ROUND)
    gc.set_rgb_fg_color(@grid_col)
    brx=IMGSIZE[0]/Boij_sharedvideoarea::GRID_X
    bry=IMGSIZE[1]/Boij_sharedvideoarea::GRID_Y

    (Boij_sharedvideoarea::GRID_X+1).times do |x|
      w.window.draw_line(gc,brx*x,0,brx*x,IMGSIZE[1])
    end
    (Boij_sharedvideoarea::GRID_Y+1).times do |y|
      w.window.draw_line(gc,0,bry*y,IMGSIZE[0],bry*y)
    end
    
    gc.set_line_attributes(3,Gdk::GC::LINE_SOLID,Gdk::GC::CAP_BUTT,Gdk::GC::JOIN_ROUND)
    
    if(@clients && @clients.length()>0)
      @clients.each_with_index do |sp,i|
	gc.set_rgb_fg_color(@colours[sp[0]])
	
	size=5+(sp[3][6]/10000.0)*20
	#      STDERR.printf("(((%d,%d)))",i,size)
	#        pp [sp,sp[0],sp[1],sp[2]-sp[0],sp[3]-sp[1]]
	w.window.draw_rectangle(gc,false,sp[3][2],sp[3][3],
				sp[3][4]-sp[3][2],sp[3][5]-sp[3][3])
	w.window.draw_arc(gc,true,sp[3][0]-size/2,sp[3][1]-size/2,size,size,0,360*64)
      end
    end
  end

  def timeout_func
    pix=@sva.get_frame()
    if(pix)
      @rpix=Boij_sharedvideoarea::triplicate(pix)
      @da.queue_draw()
      
      #    @lbl1.set_text(sprintf("Pan %d tilt %d\n",cpos[1]*360,cpos[0]*360)) if(@lbl1)
      nc=@sva.get_clients(true)
      @clients=nc if(nc)
    end
  end

  def change_current_gridpos(var)
    var.each_with_index do |v,i|
      @cur_gridpos[i]+=v
      @cur_gridpos[i]=0 if(@cur_gridpos[i]<0)
      @cur_gridpos[i]=@max_gridpos[i] if(@cur_gridpos[i]>@max_gridpos[i])
    end
  end
  
  def update_current_gridpos(var)
    a=@cur_grid[@cur_gridpos[1]][@cur_gridpos[0]]
    var.each_with_index do |v,i|
      a[i]+=v
      a[i]=-Boij_sharedvideoarea::MAX_GRID_OFFSET+1 if(a[i]<=-Boij_sharedvideoarea::MAX_GRID_OFFSET)
      a[i]=Boij_sharedvideoarea::MAX_GRID_OFFSET-1 if(a[i]>=Boij_sharedvideoarea::MAX_GRID_OFFSET)
    end
    @cur_grid[@cur_gridpos[1]][@cur_gridpos[0]]=a
  end

  def save_grid
    gx=@gadj_x.value()
    @gadj_x.value=0
    gy=@gadj_y.value()
    @gadj_y.value=0
    
    File::open(GRIDFILE,'w') do |f|
      (Boij_sharedvideoarea::GRID_Y+1).times do |i|
	(Boij_sharedvideoarea::GRID_X+1).times do |j|
	  @cur_grid[i][j][0]+=gx
	  @cur_grid[i][j][1]+=gy
	  f.write(@cur_grid[i][j].pack('ss'))
	end
      end
      f.write([@togx ? 1 : 0,@togy ? 1 : 0].pack('ss'))
    end

    @sva.set_grid(@cur_grid,@togx,@togy)
  end

  def reset_grid
    @cur_grid=[]
    (Boij_sharedvideoarea::GRID_Y+1).times do |i|
      g=[]
      (Boij_sharedvideoarea::GRID_X+1).times do |i|
	g.push([0,0])
      end
      @cur_grid.push(g)
    end
  end

  def fix_greyness
    v=@sva.background_level()
    nv=(15+v*75).to_i()
    STDERR.printf("ORA E' %f richiede %d\n",v,nv)
  end
end

ft=Ftest::new(ARGV.length()<=0 ? true : false)

begin
  ft.runme()
rescue
  pp ["FT crash",$!,$!.backtrace()]
end
