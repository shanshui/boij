#!/usr/bin/env ruby
# playground_faker.rb

=begin

4/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This is the faker of the playground - a SDL window where we may have persons
appearing or disappearing, moving, etc

$Id: playground_faker.rb 2626 2010-11-10 14:48:16Z karl $

=end

require 'boij'

require 'RUDL'
include RUDL
include Constant

class Ball
  RADIUS=10
  BALLC=[[255,0,0],[255,128,0],[255,255,0],[0,255,0],[80,80,255],[40,0,128],[200,0,200]]
  TIMEFACTOR=5.0
  @@cnt=0

  attr_accessor(:pos)
  attr_reader(:col)

  def initialize(pos)
    @pos=pos
    @col=BALLC[@@cnt%BALLC.length()]
    @moveto=nil

    @@cnt+=1
  end

  def fire_moveto(pos,time)
    @movefrom=@pos
    @moveto=pos
    @movedist=[@moveto[0]-@movefrom[0],@moveto[1]-@movefrom[1]]
    @movefrom_time=Time::now()
    @moveto_time=@movefrom_time+time*TIMEFACTOR
    @move_len=@moveto_time-@movefrom_time
  end

  def moveto
    return false if(!@moveto)

    now=Time::now()
    if(now>=@moveto_time)
      @pos=@moveto
      @moveto=nil
    else
      fact=(now-@movefrom_time)/@move_len
      @pos=[@movefrom[0]+@movedist[0]*fact,@movefrom[1]+@movedist[1]*fact]
    end

    true
  end
end

class Faker
  attr_accessor(:chgflg)

  MM_PER_PIXEL=10
  BACKG=[0,0,0]
  MINDIST=Ball::RADIUS*2

  def initialize
    @localsize=PLAYGROUND_SIZE.collect do |v|
      v/MM_PER_PIXEL
    end
    @w=DisplaySurface.new(@localsize)
    @balls=Array::new(Boij_sharedvideoarea::MAX_MONITORED)

    @networker=Server_networker::new(self)
    @chgflg=false
  end

  def runme
    @selected=nil
    @done=false
    while(!@done)
      @chgflg=false
      while(true)
	event=EventQueue.poll()
	break if(!event)
	case event
	  #      when MouseMotionEvent
	  #        if(@selected && @selected[0])
	  #          @selected[0].pos=event.pos
	  #          @chgflg=true
	  #        end
	when MouseButtonDownEvent
	  case event.button
	  when 1
	    @selected=find_closest(event.pos).push(Time::now())
	    @chgflg=true
	  when 2
	    Boij_sharedvideoarea::MAX_MONITORED.times do |b|
	      if(!@balls[b])
		@balls[b]=Ball::new(event.pos)
		@chgflg=true
		break
	      end
	    end
	  when 3
	    t=find_closest(event.pos)
	    if(t[0])
	      @balls[@balls.index(t[0])]=nil
	      @chgflg=true
	    end
	  end
	when MouseButtonUpEvent
	  if(event.button==1 && @selected && @selected[0])
	    @selected[0].fire_moveto(event.pos,Time::now()-@selected[2])
	    @selected=nil
	    @chgflg=true
	  end
	when KeyDownEvent
	  @done=true if(event.key==K_ESCAPE)
	when QuitEvent
	  @done=true
	end
      end
      @balls.each() do |b|
	@chgflg=true if(b && b.moveto())
      end

      if(@chgflg)
        paint_screen()
        pkt=@balls.collect do |b|
          b ? [b.pos[0].to_f()/@localsize[0],
               b.pos[1].to_f()/@localsize[1]] : nil
        end
        @networker.send_pkt(pkt)
      end
      sleep(0.01)
    end
    @w.destroy()
    @networker.done=true
  end

  def find_closest(pos)
    mindist=MINDIST
    clb=nil
    @balls.each do |b|
      next if(!b)
      xd=b.pos[0]-pos[0]
      yd=b.pos[1]-pos[1]
      dist=Math::sqrt(xd*xd+yd*yd)
      if(dist<mindist)
        clb=b
        mindist=dist
      end
    end
    [clb,mindist]
  end

  def paint_screen
    @w.fill(BACKG)
    @balls.each do |b|
      next if(!b)
      @w.fill([255,255,255],[b.pos[0]-Ball::RADIUS,b.pos[1]-Ball::RADIUS,Ball::RADIUS*2+1,Ball::RADIUS*2+1]) if(@selected && @selected[0]==b)
      @w.filled_circle(b.pos,Ball::RADIUS,b.col)
    end
    @w.update
  end
end

lf=Fluid_logfile::new('faker')

fo=Faker::new()
begin
  fo.runme()
rescue
  pp ["Faker crash",$!,$!.backtrace()]
end
