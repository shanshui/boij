require 'yaml'

class Player_config
  attr_accessor(:playground_size,:gen_new_image,:choose_new_image,:space_comp_fact,
                :min_zoom,:max_zoom,:zoom_alg,:zoom_alg_n,
                :min_sound_rate,:max_sound_rate,
                :min_volume,:max_volume,
                :audio_exhaust_rate,
                :min_dist,
                :new_image_travel,
                :image_height,
                :record_cmd)

  PLAYGROUND_SIZE=[5790,6530] # in mm
  GENERATE_NEW_IMAGE=200 #300 #1500 # in mm
  CHOOSE_NEW_IMAGE=10000 #5000 #3500 # in mm
  SPACE_COMPRESSION_FACT=6.5#8.0

  ZOOM_ALGS = ['lin','sin','cos']
  ZOOM_ALG = 'lin'
  ZOOM_ALG_N = 1 # number of sin/cos curves

  # LOWERING MIN MAKES THE MAX ZOOM PIXELS LARGER
  # INCREASING MAX MAKES THE MIN ZOOM PIXELS SMALLER
  # Sorry for the inconvenience...
  #
  MIN_ZOOM=-0.6 #-1.0 #0.001 #0.2 # 0.2 #0.3 #0.85
  MAX_ZOOM=2.0 #3.0 #1.02 #0.8 #2.0 #1.5 #3.5

  MIN_SOUND_RATE=0.002#1.0#0.08#0.4
  MAX_SOUND_RATE=0.1#1.0#0.6#1.8

  MIN_VOLUME=0.8 #1.0#1.0#0.08#0.4
  MAX_VOLUME=0.5 #0.7#1.0#0.6#1.8

  AUDIO_EXHAUST_RATE=0.8 #0.995

  MIN_DIST=0.005 #0.02 #0.03
  NEW_IMAGE_TRAVEL=2.0

  IMAGE_HEIGHT=1200

  RECORD_CMD = "ffmpeg -framerate 25 -f x11grab -i '' -f alsa -ac 2 -i hw:0 -c:v libx264rgb -crf 0 -preset ultrafast"
  # RECORD_POSTPROC_CMD = "ffmpeg -i output.mkv -c:v libx264rgb -crf 0 -preset veryslow output-smaller.mkv"

  #IMGSTART_MIN=7.0
  #IMGSTART_MAX=25.0
  #IMGSTART_DIFF=IMGSTART_MAX-IMGSTART_MIN

  #MIN_PCT_TO_USE=0.85 #0.1 #0.99 #0.5
  #MAX_PCT_TO_USE=0.99

  def initialize()
    set_defaults()
  end

  def set_defaults()
    @playground_size = PLAYGROUND_SIZE if @playground_size.nil?
    @generate_new_image = GENERATE_NEW_IMAGE if @generate_new_image.nil?
    @choose_new_image = CHOOSE_NEW_IMAGE if @choose_new_image.nil?
    @space_compression_fact = SPACE_COMPRESSION_FACT if @space_compression_fact.nil?
    @min_zoom = MIN_ZOOM if @min_zoom.nil?
    @max_zoom = MAX_ZOOM if @max_zoom.nil?
    @zoom_alg = ZOOM_ALG if @zoom_alg.nil?
    @zoom_alg_n = ZOOM_ALG_N if @zoom_alg_n.nil?
    @min_sound_rate = MIN_SOUND_RATE if @min_sound_rate.nil?
    @max_sound_rate = MAX_SOUND_RATE if @max_sound_rate.nil?
    @min_volume = MIN_VOLUME if @min_volume.nil?
    @max_volume = MAX_VOLUME if @max_volume.nil?
    @audio_exhaust_rate = AUDIO_EXHAUST_RATE if @audio_exhaust_rate.nil?
    @min_dist = MIN_DIST if @min_dist.nil?
    @new_image_travel = NEW_IMAGE_TRAVEL if @new_image_travel.nil?
    @image_height = IMAGE_HEIGHT if @image_height.nil?
    @record_cmd = RECORD_CMD if @record_cmd.nil?
  end

  def Player_config::load
    begin
      cfg = YAML::load(File::read(PLAYERCONF))
    rescue
      cfg = Player_config.new
    end
    cfg.set_defaults()
    cfg
  end

  def save
    File::open(PLAYERCONF,'wb') do |f|
      f.write(YAML::dump(self))
    end
  end

  def diff_zoom
    @max_zoom - @min_zoom
  end

  def diff_sound_rate
    @max_sound_rate - @min_sound_rate
  end

  def diff_volume
    @max_volume - @min_volume
  end

end
