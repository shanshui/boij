#!/usr/bin/env ruby
# raalte_stopper.rb

=begin

17/03/2011 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The logic that stops the process in Raalte

$Id: raalte_stopper.rb 2836 2011-03-17 09:18:04Z karl $

=end

require 'boij'

Fluid_logfile::killpid('runner')
