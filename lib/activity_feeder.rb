#!/usr/bin/env ruby
# activity_feeder.rb

=begin

12/07/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Reads from activity_handler drb socket, and feeds person motion

$Id: activity_feeder.rb 2621 2010-11-08 14:07:04Z karl $

=end

require 'boij'
require 'activity_handler'

class Player
  attr_reader(:prog)
  attr_accessor(:x,:y,:active,:used)

  def initialize(prog)
    @prog=prog
    @active=false
  end

  def distance(x,y)
    dx=@x-x
    dy=@y-y
    Math::sqrt(dx*dx+dy*dy)
  end
end

class Activity_feeder
  SLEEP_TIME=0.2
  THRESH=2.0
  MIN_DIST=1.1
  MAX_WIDTH=5.0

  def initialize
    @ply=[]
    Boij_sharedvideoarea::MAX_MONITORED.times do |i|
      @ply.push(Player::new(i))
    end

    DRb::start_service()
    @ah=DRbObject::new(nil,drb_uri(DRB_PORT_ACTIVITY))
    @x,@y=@ah.dimensions
    @cont=Contour::new(@x,@y)

    @eng=DRbObject::new(nil,drb_uri(DRB_PORT_IMG))
  end

  def runme
    loop do
      a=@ah.values()
      cdots=@cont.calc(a.flatten,[THRESH])[0]
      centers=@cont.find_centers(cdots,MIN_DIST)

      #
      # Use size of blob as Y
      #

      centers.each do |a|
        a.push(1.0-([a[3],MAX_WIDTH].min())/MAX_WIDTH)
      end

      @ply.each do |p|
        p.used=false
      end
      nearby=[]
      centers.each do |c|
        @ply.each do |p|
          if(p.active)
            dist=p.distance(c[0],c[5])
            nearby.push([c,p,dist]) if(dist<=MIN_DIST)
          end
        end
      end
      nearby.sort do |a,b|
        b[2]<=>a[2]
      end.each do |cnt,ply,dist|
        next if(ply.used || !centers.include?(cnt))
        ply.x=cnt[0]
        ply.y=cnt[5]
        ply.used=true
        centers.delete(cnt)
      end
      @ply.each do |p|
        p.active=false if(p.active && !p.used)
      end
      centers.each do |x,y|
        @ply.each do |p|
          if(!p.active)
            p.active=true
            p.x=x
            p.y=y
            break
          end
        end
      end
      pkt=@ply.collect do |p|
        p.active ? [p.x/@x,p.y/@y] : nil
      end
      @eng.new_positions(pkt)

      sleep(SLEEP_TIME)
    end
  end
end

Activity_feeder::new().runme()
