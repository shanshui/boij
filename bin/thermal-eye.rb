#!/usr/bin/env ruby

=begin

12/12/2005 c.e. prelz AS FLUIDO <fluido@fluido.as>
Project vaLENcia

Talking to the thermaleye

$Id: thermaleye.rb 1494 2008-02-12 15:02:20Z karl $

=end

require 'thread'
Thread.abort_on_exception=true

class Thermaleye
  attr_reader(:unit)

  DEVICE="/dev/ttyUSB0"

  def initialize
    @unit=File::open(DEVICE,File::RDWR)

    @read_thread=Thread::new do
      collector()
    end

  end

  def short_to_str(sh)
    return sprintf("%c%c",sh&0xff,sh>>8)
  end

  def send_command(cmd,data) # data is an array of ints (shorts)
    cts=short_to_str(2)+short_to_str(cmd)
    sum=-2-cmd
    data.each do |sh|
      cts+=short_to_str(sh)
      sum-=sh
    end
    cts+=short_to_str(sum&0xffff)
    
    @unit.syswrite(cts)
    @unit.flush()
    Thread::pass
  end

  def collector
    p 'Start listening'
    loop do
      p 'Befsel'
      val=IO::select([@unit],nil,nil,0.1)
      if(val)
	c=@unit.readchar()
	p ["READ",c,c[0]]
      end
      Thread::pass      
    end
    p 'Morto'
  end
end

t=Thermaleye::new()
t.send_command(1,[])
sleep(10)

