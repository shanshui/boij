# boij_xserver.rb

=begin

16/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Thread that runs the X server

$Id: boij_xserver.rb 4400 2015-01-24 17:55:17Z karl $

=end

class Boij_xserver
  CONF_FILE='/etc/X11/boij.conf'
  
  attr_reader(:pid)

  def initialize(x,y)
    conff=<<EOF
Section "ServerLayout"
  Identifier "layout_1"
  InputDevice "kbd" "CoreKeyboard"
  InputDevice "mouse" "CorePointer"
  Option "Xinerama" "#{N_SCREENS>1 ? 1 : 0}"
EndSection

#Section "Files"
#  RgbPath "/usr/X11R6/lib/X11/rgb"
#EndSection

Section "Module"
  Load "dbe"
  Load "extmod"
  Load "type1"
  Load "freetype"
  Load "glx"
EndSection

Section "InputDevice"
  Identifier  "kbd"
  Driver "kbd"
  Option "CoreKeyboard"
  Option "XkbRules" "xorg"
  Option "XkbModel" "pc104"
  Option "XkbLayout" "us"
EndSection

Section "InputDevice"
  Identifier "mouse"
  Driver "mouse"
  Option "CorePointer"
  Option "Device" "/dev/input/mice"
  Option "Protocol" "ImPS/2"
  Option "Emulate3Buttons" "true"
EndSection

Section "Monitor"
  Identifier "monitor_1"
  VendorName "Unknown"
  HorizSync 26.0 - 68.0
  VertRefresh 49.0 - 61.0
  Option "DPMS"
EndSection
EOF
    if(N_SCREENS>1)
      conff+=<<EOF
Section "Monitor"
  Identifier "monitor_2"
  VendorName "Unknown"
  HorizSync 26.0 - 68.0
  VertRefresh 49.0 - 61.0
  Option "DPMS"
EndSection
EOF
      if(N_SCREENS>2)
        conff+=<<EOF
Section "Monitor"
  Identifier "monitor_3"
  VendorName "Unknown"
  HorizSync 26.0 - 68.0
  VertRefresh 49.0 - 61.0
  Option "DPMS"
EndSection
EOF
      end
    end
    conff+=<<EOF
Section "Device"
  Identifier "card_1"
  Driver "nvidia"
  VendorName "NVIDIA Corporation"
  BoardName "GeForce 210"
#  BusID "PCI:2:0:0"
EndSection
Section "Screen"
  Identifier "screen_1"
  Device "card_1"
  Monitor "monitor_1"
  DefaultDepth 24
EOF
    if(N_SCREENS==2)
      conff+=<<EOF
  Option "TwinView " "on"
  Option "TwinViewOrientation" "RightOf"
  Option "MetaModes" "#{x}x#{y},#{x}x#{y}+#{x}+0"
EOF
    end
    conff+=<<EOF
  SubSection "Display"
    Depth 24
    Modes "#{x}x#{y}"
  EndSubSection
EndSection

Section "ServerFlags"
  Option "AllowMouseOpenFail" "on"
  Option "BlankTime " "0"
  Option "StandbyTime " "0"
  Option "SuspendTime " "0"
  Option "OffTime " "0"
  Option "NoPM" "on"
#
#  Option "DontVTSwitch" "on"
#  Option "DontZap" "on"
  Option "DontVTSwitch" "off"
  Option "DontZap" "off"
  Option "DontZoom" "on"
EndSection

Section "DRI"
  Mode 0666
EndSection

EOF
#    File::open(CONF_FILE,'w') do |f|
#      f.puts(conff)
#    end
    
    pd=Kernel.fork()
    if(!pd) # son
#      cmd="/usr/bin/Xorg -retro -logverbose 0xffff -config #{CONF_FILE.split('/')[-1]}"
#      cmd="/usr/bin/Xorg -retro -config #{CONF_FILE.split('/')[-1]}"
      cmd="/usr/bin/Xorg"
      loggo("about to execute <#{cmd}>")
      Kernel.exec(cmd)
      exit(0)
    end
    
    loggo("X started (pid=#{pd})")
    @pid=pd
  end

  def reap
    p ["Reaping ",@pid]
    Process::kill("TERM",@pid)
    p ["Waiting ",@pid]
    Process::waitpid(@pid)
#    p ["Blanking ",@pid]
#      system("sudo /usr/share/lounge/bin/screen_off.rb")
    p ["Done ",@pid]
  end
end
