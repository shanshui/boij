#!/usr/bin/env ruby

require 'yaml'

puts(YAML::dump(Marshal::restore(File::read(ARGV[0]))))
