# image_dbase.rb

=begin

02/07/2010 c.e.prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

small sqlite database with existing images

$Id: image_dbase.rb 2702 2011-01-21 16:27:57Z karl $

=end

require 'boij'
require 'sqlite3'

class Wfolder
  attr_reader(:db,:id,:name,:md5,:parent,:sons,:used,:local_images)
  attr_accessor(:n_images)
  @@folders={}

  def initialize(db,name,used,parent)
    @db,@name,@used,@parent=db,name,used,parent
    @sons={}
    @parent.sons[@name]=self if(@parent)
    @local_images=[]
    @n_images=0
  end

  def set_id(id)
    @id=id
    @md5=Digest::MD5::hexdigest(@id.to_s+'_'+@name)
    @@folders[@md5]=self
  end

  def set_images(arr)
    @local_images=arr
    @n_images=@local_images.length
    p=@parent
    while(p)
      p.n_images+=@local_images.length
      p=p.parent
    end
  end

  def complete_name
    cn=@name
    p=@parent
    while(p)
      cn=p.name+'/'+cn
      p=p.parent
    end
    cn
  end

  def delete_me!
    Wfolder::recurse_delete_folder_and_images(self)
  end

  def set_active!(value)
    @used=value
    @db.set_folder_active(@id,value)
  end

  def Wfolder::recurse_delete_folder_and_images(fol)
    fol.sons.values.each do |son|
      Wfolder::recurse_delete_folder_and_images(son)
    end
    fol.db.delete_folder(fol.id)
    @@folders.delete(@md5)
  end

  def Wfolder::load(db)
    @@folders={}
    rootfolders={}
    images={}

    db.folders().each do |name,id,use|
      parts=name.split('/')
      lastpart=parts[-1]
      if(rootfolders[parts[0]])
        parent=rootfolders[parts[0]]
      else
        parent=Wfolder::new(db,parts[0],use,nil)
        rootfolders[parts[0]]=parent
      end
      parts[1..-1].each do |lbl|
        newparent=(parent.sons[lbl]) ? parent.sons[lbl] : Wfolder::new(db,lbl,use,parent)
        parent=newparent
      end
      parent.set_id(id)
    end

    @@folders.each do |k,f|
      if(!f.id)
        cn=f.complete_name()
        new_id=db.get_folder(cn,true)
        loggo("Had to create folder #{cn} (gets #{new_id})")
        f.set_id(new_id)
      end

      iof=db.images_of_folder(f.id)
      loggo("Folder #{f.name} has #{iof.length} images")
      f.set_images(iof.map do |id,md5,horizon|
                     i=Wimage::new(db,id,md5,horizon,f)
                     images[md5]=i
                     i
                   end)
    end

    [rootfolders,@@folders,images]
  end
end

class Wimage
  attr_reader(:db,:id,:md5,:horizon,:folder)

  def initialize(db,id,md5,horizon,folder)
    @db,@id,@md5,@horizon,@folder=db,id,md5,horizon,folder
  end

  def delete_me!
    @db.delete_image(@id)
    @folder.local_images.delete(self)
  end

  def update_horizon(new_val)
    @horizon=new_val
    @db.db.execute('update image set horizon=? where id=?',[new_val,@id])
  end

  def move_to_folder(f)
    @folder.local_images.delete(self)
    f.local_images.push(self)
    @folder=f
    @db.db.execute('update image set folder=? where id=?',[@folder.id,@id])
  end
end

class Wuser
  attr_reader(:id,:nick)

  def initialize(id,nick)
    @id,@nick=id,nick
  end
end

class Image_dbase
  attr_reader(:db)

  DBASE=BASE+'images.db'

  def initialize
    FileUtils::mkdir_p(BASE)
    exf=File::exist?(DBASE)
    @db=SQLite3::Database.new(DBASE)
    @db.type_translation=false
    @db.busy_timeout(1000)

    createdb() if(!exf)
  end

  def folders
    @db.execute('select name,id,use from folder order by name').map do |row|
      [row[0],row[1].to_i,row[2].to_i==1 ? true : false]
    end
  end

  def get_folder(name,create=false)
    if(name.end_with?('/'))
      loggo("Warning: get_folder called with trailing slash! (#{name})")
      name=name[0...-1]
    end

    rows=@db.execute('select id from folder where name=?',name)
    return rows[0][0].to_i if(rows.length>0)
    return false if(!create)
    @db.execute('insert into folder (name) values (?)',name)
    @db.last_insert_row_id()
  end

  def delete_folder(id)
    @db.execute('delete from image where folder=?',id)
    @db.execute('delete from folder where id=?',id)
  end

  def set_folder_active(id,value)
    @db.execute('update folder set use=? where id=?',[value ? 1 : 0,id])
  end

  def image_with_md5(md5)
    rows=@db.execute('select id from image where md5=?',md5)
    return false if(rows.length<1)
    rows[0][0].to_i
  end

  def insert_image(md5,folder_id,horizon)
    if(image_with_md5(md5))
      loggo("Image already exists!")
      return false
    end
    @db.execute('insert into image (md5,folder,horizon) values (?,?,?)',[md5,folder_id,horizon])
    @db.last_insert_row_id()
  end

  def images_of_folder(folder_id)
    @db.execute('select id,md5,horizon from image where folder=?',folder_id).map do |row|
      [row[0].to_i,row[1],row[2].to_i]
    end
  end

  def delete_image(id)
    @db.execute('delete from image where id=?',id)
  end

  def verify_user(nick,passwd)
    rows=@db.execute('select id,passwd from adm where nick=?',nick)
    return nil if(rows.length<1)
    return false if(Digest::MD5::hexdigest(passwd)!=rows[0][1])
    Wuser::new(rows[0][0].to_i,nick)
  end

  def create_user(nick,passwd)
    @db.execute('insert into adm (nick,passwd) values (?,?)',[nick,Digest::MD5::hexdigest(passwd)])
    @db.last_insert_row_id()
  end

  def createdb
    @db.transaction do
      @db.execute("create table folder (
id integer primary key autoincrement,
name text unique not null,
use integer not null default 1);")
      @db.execute("create table image (
id integer primary key autoincrement,
md5 text unique not null,
folder integer references folder(id),
horizon integer not null);")
      @db.execute("create table adm (
id integer primary key autoincrement,
nick text unique not null,
passwd text unique not null);")
    end
    get_folder('default',true)
  end
end
