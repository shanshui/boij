/* contour.c */

/*
 * 09/07/2010 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * Contours from regular grid
 *
 * $Id: contour.c 2412 2010-07-13 09:39:46Z karl $
 */

#include <ruby.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include "boij.h"

VALUE cls_contour;

typedef struct
{
  float x[2],y[2],*val[2];
} segment_stc;

typedef struct
{
  float x,y;
  struct group *group;
} dot_stc;

typedef struct group
{
  int prog;
  dot_stc **dots;  
  int ndots;
} group_stc;

typedef struct
{
  dot_stc *d1,*d2;
  float dist;
} dotdist_stc;
  
typedef struct
{
  int x,y,n_seg;
  float *val_arr,*cval_arr;
  segment_stc *segments;
} contour_stc;

static void free_contour(void *p);

static float intersect(float v1,float v2, float vint);
static float distance(dot_stc *d1,dot_stc *d2);
static int dotdist_compare(const void *a, const void *b);

static float zero=0.0;

VALUE new_contour(VALUE self,VALUE v_x,VALUE v_y)
{
  contour_stc *c=ALLOC(contour_stc);
  int i,x,y,cnt;
  
  c->x=FIX2INT(v_x);
  c->y=FIX2INT(v_y);

  c->val_arr=malloc(sizeof(float)*c->x*c->y);
  c->cval_arr=malloc(sizeof(float)*(c->x-1)*(c->y-1));
  c->n_seg=(c->x-1)*(c->y-1)*6+(c->x-1)*5+(c->y-1)*5+4;  
  c->segments=malloc(sizeof(segment_stc)*c->n_seg);

  segment_stc *s;
  
  for(s=c->segments,y=0;y<c->y-1;y++)
    for(x=0;x<c->x-1;x++,cnt+=4)
    {
      s->x[0]=(float)x;
      s->y[0]=(float)y;
      s->val[0]=c->val_arr+y*c->x+x;
      s->x[1]=(float)x;
      s->y[1]=y+1.0;
      s->val[1]=c->val_arr+(y+1)*c->x+x;
      s++;

      s->x[0]=(float)x;
      s->y[0]=(float)y;
      s->val[0]=c->val_arr+y*c->x+x;
      s->x[1]=x+1.0;
      s->y[1]=(float)y;
      s->val[1]=c->val_arr+y*c->x+x+1;
      s++;
      
      s->x[0]=(float)x;
      s->y[0]=(float)y;
      s->val[0]=c->val_arr+y*c->x+x;
      s->x[1]=x+0.5;
      s->y[1]=y+0.5;
      s->val[1]=c->cval_arr+y*(c->x-1)+x;
      s++;
      
      s->x[0]=x+1.0;
      s->y[0]=(float)y;
      s->val[0]=c->val_arr+y*c->x+x+1;
      s->x[1]=x+0.5;
      s->y[1]=y+0.5;
      s->val[1]=c->cval_arr+y*(c->x-1)+x;
      s++;
      
      s->x[0]=x+1.0;
      s->y[0]=y+1.0;
      s->val[0]=c->val_arr+(y+1)*c->x+x+1;
      s->x[1]=x+0.5;
      s->y[1]=y+0.5;
      s->val[1]=c->cval_arr+y*(c->x-1)+x;
      s++;
      
      s->x[0]=(float)x;
      s->y[0]=y+1.0;
      s->val[0]=c->val_arr+(y+1)*c->x+x;
      s->x[1]=x+0.5;
      s->y[1]=y+0.5;
      s->val[1]=c->cval_arr+y*(c->x-1)+x;
      s++;      
    }  
  
/*
 * add border segments
 */
  
  for(i=0;i<c->x-1;i++)
  {
    s->x[0]=(float)i;
    s->y[0]=0.0;
    s->val[0]=c->val_arr+i;
    s->x[1]=i+0.5;
    s->y[1]=-0.5;
    s->val[1]=&zero;
    s++;      

    s->x[0]=i+1.0;
    s->y[0]=0.0;
    s->val[0]=c->val_arr+i+1;
    s->x[1]=i+0.5;
    s->y[1]=-0.5;
    s->val[1]=&zero;
    s++;      

    s->x[0]=(float)i;
    s->y[0]=c->y-1.0;
    s->val[0]=c->val_arr+(c->y-1)*c->x+i;
    s->x[1]=i+1.0;
    s->y[1]=c->y-1.0;
    s->val[1]=c->val_arr+(c->y-1)*c->x+i+1;
    s++;      

    s->x[0]=(float)i;
    s->y[0]=c->y-1.0;
    s->val[0]=c->val_arr+(c->y-1)*c->x+i;
    s->x[1]=i+0.5;
    s->y[1]=c->y-0.5;
    s->val[1]=&zero;
    s++;      

    s->x[0]=i+1.0;
    s->y[0]=c->y-1.0;
    s->val[0]=c->val_arr+(c->y-1)*c->x+i+1;
    s->x[1]=i+0.5;
    s->y[1]=c->y-0.5;
    s->val[1]=&zero;
    s++;      
  }
  
  for(i=0;i<c->y-1;i++)
  {
    s->x[0]=0.0;
    s->y[0]=(float)i;
    s->val[0]=c->val_arr+i*c->x;
    s->x[1]=-0.5;
    s->y[1]=i+0.5;
    s->val[1]=&zero;
    s++;      

    s->x[0]=0.0;
    s->y[0]=i+1.0;
    s->val[0]=c->val_arr+(i+1)*c->x;
    s->x[1]=-0.5;
    s->y[1]=i+0.5;
    s->val[1]=&zero;
    s++;      

    s->x[0]=c->x-1.0;
    s->y[0]=(float)i;
    s->val[0]=c->val_arr+(i+1)*c->x-1;
    s->x[1]=c->x-1.0;
    s->y[1]=i+1.0;
    s->val[1]=c->val_arr+(i+2)*c->x-1;
    s++;      

    s->x[0]=c->x-1.0;
    s->y[0]=(float)i;
    s->val[0]=c->val_arr+(i+1)*c->x-1;
    s->x[1]=c->x-0.5;
    s->y[1]=i+0.5;
    s->val[1]=&zero;
    s++;      

    s->x[0]=c->x-1.0;
    s->y[0]=i+1.0;
    s->val[0]=c->val_arr+(i+2)*c->x-1;
    s->x[1]=c->x-0.5;
    s->y[1]=i+0.5;
    s->val[1]=&zero;
    s++;      
  }

/*
 * At last the four corners
 */
  
  s->x[0]=0.0;
  s->y[0]=0.0;
  s->val[0]=c->val_arr;
  s->x[1]=-0.5;
  s->y[1]=-0.5;
  s->val[1]=&zero;
  s++;      

  s->x[0]=0.0;
  s->y[0]=c->y-1.0;
  s->val[0]=c->val_arr+c->x-1;
  s->x[1]=-0.5;
  s->y[1]=c->y-0.5;
  s->val[1]=&zero;
  s++;      

  s->x[0]=c->x-1.0;
  s->y[0]=c->y-1.0;
  s->val[0]=c->val_arr+(c->y*c->x-1);
  s->x[1]=c->x-0.5;
  s->y[1]=c->y-0.5;
  s->val[1]=&zero;
  s++;      

  s->x[0]=c->x-1.0;
  s->y[0]=0.0;
  s->val[0]=c->val_arr+c->x-1;
  s->x[1]=c->x-0.5;
  s->y[1]=-0.5;
  s->val[1]=&zero;
  s++;

  return Data_Wrap_Struct(cls_contour,NULL,free_contour,c);
}

static void free_contour(void *p)
{
  contour_stc *c=(contour_stc *)p;

  free(c->val_arr);
  free(c->cval_arr);
  free(c->segments);

  free(p);
}

VALUE contour_calc(VALUE self,VALUE varr,VALUE vlevels)
{
  contour_stc *c;
  Data_Get_Struct(self,contour_stc,c);

  int i,j,x,y,nlevels;
  float *row1,*row2,*f,v1,v2,vi;
  segment_stc *s;

  nlevels=RARRAY_LEN(vlevels);
  float levels[nlevels];
  VALUE v,level_arrays[nlevels];

  for(i=0;i<nlevels;i++)
  {
    levels[i]=NUM2DBL(rb_ary_entry(vlevels,i));
    level_arrays[i]=rb_ary_new();
  }  
  
  for(i=0;i<c->x*c->y;i++)
    c->val_arr[i]=NUM2DBL(rb_ary_entry(varr,i));

  for(f=c->cval_arr,y=0;y<c->y-1;y++)
  {
    row1=c->val_arr+(y*c->x);
    row2=row1+c->x;

    for(x=0;x<c->x-1;x++,f++)
      *f=(row1[x]+row1[x+1]+row2[x]+row2[x+1])/4.0;
  }

/*
  fprintf(stderr,"In array\n");
  for(f=c->val_arr,y=0;y<c->y;y++)
  {
    for(x=0;x<c->x;x++,f++)
      fprintf(stderr,"%5.2f ",*f);
    fputc('\n',stderr);
  }
  fprintf(stderr,"Intermediate\n");
  for(f=c->cval_arr,y=0;y<c->y-1;y++)
  {
    for(x=0;x<c->x-1;x++,f++)
      fprintf(stderr,"%5.2f ",*f);
    fputc('\n',stderr);
  }
*/

  for(i=0,s=c->segments;i<c->n_seg;i++,s++)
  {    
    v1=*(s->val[0]);
    v2=*(s->val[1]);

    for(j=0;j<nlevels;j++)
    {
      vi=intersect(v1,v2,levels[j]);
      if(vi>0.0 && vi<1.0)
      {
	v=rb_ary_new();
	rb_ary_push(v,rb_float_new(s->x[0]+(s->x[1]-s->x[0])*vi));
	rb_ary_push(v,rb_float_new(s->y[0]+(s->y[1]-s->y[0])*vi));
	rb_ary_push(level_arrays[j],v);
      }
    }
  }
  
  v=rb_ary_new();
  for(i=0;i<nlevels;i++)    
    rb_ary_push(v,level_arrays[i]);

  return v;
}

VALUE contour_find_centers(VALUE self,VALUE varr,VALUE vmindist)
{
  contour_stc *c;
  Data_Get_Struct(self,contour_stc,c);

  int n_v=RARRAY_LEN(varr),i,j,ngroups=0,ndists=0;
  dot_stc *dots=malloc(sizeof(dot_stc)*n_v),*d;
  VALUE v,to_ret;
  float f,xmin,xmax,xsum,ymin,ymax,ysum,mindist=NUM2DBL(vmindist);
  group_stc **groups=NULL;
  dotdist_stc **dists=NULL;
  
  for(i=0;i<n_v;i++)
  {
    v=rb_ary_entry(varr,i);
    dots[i].x=NUM2DBL(rb_ary_entry(v,0));
    dots[i].y=NUM2DBL(rb_ary_entry(v,1));
    dots[i].group=NULL;
  }
  for(i=0;i<n_v-1;i++)
    for(j=i+1;j<n_v;j++)
    {
      f=distance(dots+i,dots+j);
      if(f<=mindist)
      {
	dists=realloc(dists,sizeof(dotdist_stc *)*(ndists+1));
	dists[ndists]=malloc(sizeof(dotdist_stc));
	dists[ndists]->d1=dots+i;
	dists[ndists]->d2=dots+j;
	dists[ndists]->dist=f;
	ndists++;
      }
    }
  to_ret=rb_ary_new();

  if(ndists>0)
  {
    dotdist_stc **dd;
    group_stc *g,**gd,*g1,*g2;
    
    qsort(dists,ndists,sizeof(dotdist_stc *),dotdist_compare);
    for(dd=dists,i=0;i<ndists;i++,dd++)
    {
      g1=(*dd)->d1->group;
      g2=(*dd)->d2->group;
      
//      fprintf(stderr,"[%.2f,%.2f] (G%d) and [%.2f,%.2f] (G%d) have distance %f\n",
//	      (*dd)->d1->x,(*dd)->d1->y,g1 ? g1->prog : -1,
//	      (*dd)->d2->x,(*dd)->d2->y,g2 ? g2->prog : -1,(*dd)->dist);
      
      if(g1 && g2)
      {
	if(g1->prog!=g2->prog)
	{
	  
/*
 * Must merge groups
 */

	  g1->dots=realloc(g1->dots,sizeof(dot_stc *)*(g1->ndots+g2->ndots));
	  for(j=0;j<g2->ndots;j++)
	  {
	    g2->dots[j]->group=g1;
	    g1->dots[g1->ndots+j]=g2->dots[j];
	  }
	  
	  g1->ndots+=g2->ndots;
	  free(g2->dots);
	  g2->dots=NULL;
	  g2->ndots=0;
	}
	
	continue;	
      }

      if(!g1 && !g2)
      {

/*
 * Both absent - create a new group
 */
      
	groups=realloc(groups,sizeof(group_stc *)*(ngroups+1));
	groups[ngroups]=malloc(sizeof(group_stc));
	groups[ngroups]->dots=NULL;
	groups[ngroups]->ndots=0;
	groups[ngroups]->prog=ngroups;
	g=groups[ngroups];	
	ngroups++;
      }
      else if(g1)
	g=g1;
      else
	g=g2;
      
      if(!g1)
      {
	g->dots=realloc(g->dots,sizeof(dot_stc *)*(g->ndots+1));
	g->dots[g->ndots]=(*dd)->d1;
	g->ndots++;
	(*dd)->d1->group=g;
      }
      
      if(!g2)
      {
	g->dots=realloc(g->dots,sizeof(dot_stc *)*(g->ndots+1));
	g->dots[g->ndots]=(*dd)->d2;
	g->ndots++;
	(*dd)->d2->group=g;
      }
      free(*dd);
    }
    free(dists);

    for(gd=groups,i=0;i<ngroups;i++,gd++)
    {
      if((*gd)->ndots>0)
      {
	for(xmin=c->x,xmax=0.0,xsum=0.0,ymin=c->y,ymax=0.0,ysum=0.0,j=0;j<(*gd)->ndots;j++)
	{
	  d=(*gd)->dots[j];	  
	  if(d->x<xmin)
	    xmin=d->x;
	  if(d->x>xmax)
	    xmax=d->x;
	  if(d->y<ymin)
	    ymin=d->y;
	  if(d->x>xmax)
	    ymax=d->y;
	  
	  xsum+=(*gd)->dots[j]->x;
 	  ysum+=(*gd)->dots[j]->y;
	}
	
	v=rb_ary_new();
	rb_ary_push(v,rb_float_new(xsum/(*gd)->ndots));
	rb_ary_push(v,rb_float_new(ysum/(*gd)->ndots));
	rb_ary_push(v,INT2FIX((*gd)->ndots));
	rb_ary_push(v,rb_float_new(xmax-xmin));
	rb_ary_push(v,rb_float_new(ymax-ymin));
	rb_ary_push(to_ret,v);    
	
	free((*gd)->dots);
      }      
      free(*gd);
    }
    free(groups);
  }
  free(dots);

  return to_ret;  
}
  
static float intersect(float v1,float v2, float vint)
{
  if(v1<v2)
  {
    if(vint<=v1)
      return 0.0;
    if(vint>=v2)
      return 1.0;
    return (vint-v1)/(v2-v1);
  }
  if(vint<=v2)
    return 1.0;
  if(vint>=v1)
    return 0.0;
  return 1.0-((vint-v2)/(v1-v2));
}

static float distance(dot_stc *d1,dot_stc *d2)
{
  float l1=d1->x-d2->x;
  float l2=d1->y-d2->y;
  
  return sqrtf(l1*l1+l2*l2);
}

static int dotdist_compare(const void *a, const void *b)
{
  dotdist_stc *dd1=*((dotdist_stc **)a);
  dotdist_stc *dd2=*((dotdist_stc **)b);
  
  if(dd1->dist<dd2->dist)
    return -1;
  if(dd1->dist>dd2->dist)
    return 1;
  return 0;
}
