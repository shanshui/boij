/* audiotoy.c */
/*
 * 10/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 *
 * Boij sound, almost same as for Valencia
 *
 * $Id: audiotoy.c 3879 2013-01-26 10:59:09Z karl $
 */

#define __USE_GNU

#include <ruby.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <pthread.h>
#include <inttypes.h>

#include <alsa/asoundlib.h>

#include "boij.h"

extern char *strcasestr (__const char *__haystack, __const char *__needle)
  __THROW __attribute_pure__ __nonnull ((1, 2));

//#define CHANNELS 6
//#define ALSA_DEVICE "plug:surround51:0"
//#define CHANNELS 4
//#define ALSA_DEVICE "plug:surround40:0"
//#define ALSA_DEVICE "four"
#define CHANNELS 2
#define ALSA_DEVICE "hw:0"
//#define ALSA_DEVICE "plughw:1,9"
#define ALSA_FORMAT SND_PCM_FORMAT_S16
#define ALSA_BUFFER_TIME 50000
#define ALSA_PERIOD_TIME 10000

#define SRATE 44100

#define SOX_ATTENUATION 0.9
#define SOX_COMMAND "sox -v %f  %s -c 1 -r %d -b 16 -e signed-integer -t \".raw\"  -"

typedef struct
{
  char *name;
  __s16 *samples;
  unsigned long n_samples;
} chunk_stc;

typedef struct
{
  chunk_stc *chunk;
  float restart_sample,cur_sample,rate,volume[CHANNELS],volume_factor_apply;
  unsigned char loop,active;
} chunk_to_use_stc;

typedef struct
{
  snd_pcm_t *alsa_handle;
  snd_pcm_hw_params_t *alsa_hwparams;
  snd_pcm_sw_params_t *alsa_swparams;
  snd_pcm_uframes_t alsa_buffer_size;
  snd_pcm_uframes_t alsa_period_size;
  unsigned int alsa_srate,blocksize;
  int16_t *alsa_frames;
  __s32 *accum_buffers;
  chunk_stc **chunks;
  chunk_to_use_stc **ctu;
  int n_chunks,n_ctu;
  uint8_t stop_thread;
  pthread_t playback_thr;
  pthread_mutex_t mtx;
  signed char whichloudspeaker;
} audiotoy_stc;

VALUE cls_audiotoy;

static void free_audiotoy(void *p);

static int first_available_chunk(audiotoy_stc *a);
static int first_available_ctu(audiotoy_stc *a);
static void *pback(void *arg);
static int xrun_recovery(snd_pcm_t *handle,int err);
static void generate_audioblock(audiotoy_stc *a);

VALUE new_audiotoy(VALUE self)
{
  audiotoy_stc *as=ALLOC(audiotoy_stc);

/*
 * Open the alsa output
 */

  snd_pcm_hw_params_alloca(&as->alsa_hwparams);
  snd_pcm_sw_params_alloca(&as->alsa_swparams);

  int err;

  if((err=snd_pcm_open(&as->alsa_handle,ALSA_DEVICE,SND_PCM_STREAM_PLAYBACK,SND_PCM_NONBLOCK))<0)
    rb_raise(rb_eRuntimeError,"%s: playback open error: %s\n",__func__,snd_strerror(err));

  if((err=snd_pcm_hw_params_any(as->alsa_handle,as->alsa_hwparams))<0)
    rb_raise(rb_eRuntimeError,"%s: broken configuration for playback: no configurations available: %s\n",
	     __func__,snd_strerror(err));

  if((err=snd_pcm_hw_params_set_access(as->alsa_handle,as->alsa_hwparams,
				       SND_PCM_ACCESS_RW_INTERLEAVED))<0)
    rb_raise(rb_eRuntimeError,"%s: access type not available: %s\n",__func__,snd_strerror(err));

  if((err=snd_pcm_hw_params_set_format(as->alsa_handle,as->alsa_hwparams,ALSA_FORMAT))<0)
    rb_raise(rb_eRuntimeError,"%s: sample format not available: %s\n",__func__,snd_strerror(err));

  if((err=snd_pcm_hw_params_set_channels(as->alsa_handle,as->alsa_hwparams,CHANNELS))<0)
    rb_raise(rb_eRuntimeError,"%s: setting channels failed: %s\n",__func__,snd_strerror(err));

//  if((err=snd_pcm_hw_params_set_rate_resample(as->alsa_handle,as->alsa_hwparams,1))<0)
//    rb_raise(rb_eRuntimeError,"%s: resampling setup failed: %s\n",__func__,snd_strerror(err));

  as->alsa_srate=SRATE;

  int dir=0;

  if((err=snd_pcm_hw_params_set_rate_near(as->alsa_handle,as->alsa_hwparams,&as->alsa_srate,&dir))<0)
    rb_raise(rb_eRuntimeError,"%s: rate not available: %s\n",__func__,snd_strerror(err));

  loggo("%s: rate is %d (dir=%d)",__func__,as->alsa_srate,dir);

  unsigned int newval=ALSA_PERIOD_TIME;

  if((err=snd_pcm_hw_params_set_period_time_near(as->alsa_handle,as->alsa_hwparams,&newval,&dir))<0)
    rb_raise(rb_eRuntimeError,"%s: period time not available: %s\n",__func__,snd_strerror(err));
  loggo("setting period time to %d. Got %d (dir=%d)",ALSA_PERIOD_TIME,newval,dir);

  if((err=snd_pcm_hw_params_get_period_size(as->alsa_hwparams,&as->alsa_period_size,&dir))<0)
    rb_raise(rb_eRuntimeError,"%s: error getting buffer size: %s\n",__func__,snd_strerror(err));

  newval=ALSA_BUFFER_TIME;
  if((err=snd_pcm_hw_params_set_buffer_time_near(as->alsa_handle,as->alsa_hwparams,&newval,&dir))<0)
    rb_raise(rb_eRuntimeError,"%s: buffer time not available: %s\n",__func__,snd_strerror(err));
  loggo("setting buffer time to %d. Got %d (dir=%d)",ALSA_BUFFER_TIME,newval,dir);

  if((err=snd_pcm_hw_params_get_buffer_size(as->alsa_hwparams,&as->alsa_buffer_size))<0)
    rb_raise(rb_eRuntimeError,"%s: error getting buffer size: %s\n",__func__,snd_strerror(err));

  loggo("%s: Period size is %d, Buffer size is %d",__func__,(int)as->alsa_period_size,(int)as->alsa_buffer_size);

  if((err=snd_pcm_hw_params(as->alsa_handle,as->alsa_hwparams))<0)
    rb_raise(rb_eRuntimeError,"%s: unable to set hw params for playback: %s\n",__func__,snd_strerror(err));

/*
 * Now sw params
 */

  if((err=snd_pcm_sw_params_current(as->alsa_handle,as->alsa_swparams))<0)
    rb_raise(rb_eRuntimeError,"%s: unable to determine current swparams for playback: %s\n",
	     __func__,snd_strerror(err));

  if((err=snd_pcm_sw_params_set_start_threshold(as->alsa_handle,as->alsa_swparams,
						(as->alsa_buffer_size/as->alsa_period_size)*
						as->alsa_period_size))<0)
    rb_raise(rb_eRuntimeError,"%s: unable to set start threshold mode for playback: %s\n",
	     __func__,snd_strerror(err));

  if((err=snd_pcm_sw_params_set_avail_min(as->alsa_handle,as->alsa_swparams,as->alsa_period_size))<0)
    rb_raise(rb_eRuntimeError,"%s: unable to set avail min for playback: %s\n",__func__,snd_strerror(err));

//  if((err=snd_pcm_sw_params_set_xfer_align(as->alsa_handle,as->alsa_swparams,1))<0)
//    rb_raise(rb_eRuntimeError,"%s: unable to set transfer align for playback: %s\n",
//	     __func__,snd_strerror(err));

  if((err=snd_pcm_sw_params(as->alsa_handle,as->alsa_swparams))<0)
    rb_raise(rb_eRuntimeError,"%s: unable to set sw params for playback: %s\n",__func__,snd_strerror(err));

  as->blocksize=(as->alsa_period_size*CHANNELS*snd_pcm_format_width(ALSA_FORMAT))/8;
  as->alsa_frames=malloc(as->blocksize);

  as->accum_buffers=malloc(sizeof(__s32)*as->alsa_period_size*CHANNELS);

  as->chunks=NULL;
  as->n_chunks=0;
  as->ctu=NULL;
  as->n_ctu=0;

  as->whichloudspeaker=-1;

/*
 * Start the playback thread
 */

  as->stop_thread = 0;
  int ret=pthread_mutex_init(&as->mtx,NULL);
  if(ret)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,ret,strerror(errno));
  ret=pthread_create(&as->playback_thr,NULL,pback,as);
  if(ret)
    loggo("%s: error #%d starting playback thread",__func__,ret);

  return Data_Wrap_Struct(cls_audiotoy,NULL,free_audiotoy,as);
}

static void free_audiotoy(void *p)
{
  audiotoy_stc *as=(audiotoy_stc *)p;

  if (as->stop_thread != 1) {
    fputs("Stopping playback thread\n", stderr);
    as->stop_thread = 1;
    pthread_cancel(as->playback_thr);
    pthread_join(as->playback_thr,NULL);
    fputs("Stopped\n",stderr);
  }

  if (as->alsa_handle) {
    snd_pcm_drop(as->alsa_handle);
    snd_pcm_close(as->alsa_handle);
    as->alsa_handle = NULL;
  }

  free(as->alsa_frames);
  free(as->accum_buffers);
  free(as);
}

#define LF_BSIZE (1024*64)

VALUE audiotoy_loadfile(VALUE self,VALUE vname,VALUE vattn)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  char *name=RSTRING_PTR(vname),cmd[512];
  float attn=NUM2DBL(vattn);
  int avchunk=first_available_chunk(a);

  a->chunks[avchunk]->name=realloc(a->chunks[avchunk]->name,strlen(name)+1);
  strcpy(a->chunks[avchunk]->name,name);

  sprintf(cmd,SOX_COMMAND,attn,name,SRATE);
  FILE *pop=popen(cmd,"r");
  char bfr[LF_BSIZE];
  int sread;
  loggo("%s: Loading <%s> to #%d\nCommand: %s",__func__,name,avchunk,cmd);

  while((sread=fread(bfr,1,LF_BSIZE,pop))>0)
  {
    a->chunks[avchunk]->samples=realloc(a->chunks[avchunk]->samples,
				       a->chunks[avchunk]->n_samples+sread);
    memcpy((char *)a->chunks[avchunk]->samples+a->chunks[avchunk]->n_samples,bfr,sread);
    a->chunks[avchunk]->n_samples+=sread;
  }
  pclose(pop);

  a->chunks[avchunk]->n_samples>>=1; /* we have 16bit samples */

  loggo("converted to %lu samples",a->chunks[avchunk]->n_samples);

  return INT2FIX(avchunk);
}

VALUE audiotoy_unloadfile(VALUE self,VALUE v)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int avchunk=FIX2INT(v),i;

  for(i=0;i<a->n_ctu;i++)
    if(a->ctu[i]->chunk==a->chunks[avchunk])
      a->ctu[i]->chunk=NULL;

  free(a->chunks[avchunk]->samples);
  a->chunks[avchunk]->n_samples=0;

  return self;
}

VALUE audiotoy_addchunk(VALUE self,VALUE v,VALUE loop,VALUE active,VALUE factor)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  pthread_mutex_lock(&a->mtx);

  int avchunk=FIX2INT(v),i;
  int ctu=first_available_ctu(a);

  a->ctu[ctu]->rate=0.0;
  if(loop==Qfalse)
  {
    a->ctu[ctu]->cur_sample=0.0;
    a->ctu[ctu]->loop=0;
  }
  else
  {
    a->ctu[ctu]->loop=1;
    if(loop==Qtrue)
      a->ctu[ctu]->restart_sample=0.0;
    else
      a->ctu[ctu]->restart_sample=a->chunks[avchunk]->n_samples*NUM2DBL(loop);
    a->ctu[ctu]->cur_sample=a->ctu[ctu]->restart_sample;
  }

  a->ctu[ctu]->active=(active==Qtrue) ? 1 : 0;
  a->ctu[ctu]->volume_factor_apply=NUM2DBL(factor);

  for(i=0;i<CHANNELS;i++)
    a->ctu[ctu]->volume[i]=0.0;

  a->ctu[ctu]->chunk=a->chunks[avchunk]; /* this makes the ctu alive */

  pthread_mutex_unlock(&a->mtx);

  return INT2FIX(ctu);
}

VALUE audiotoy_delchunk(VALUE self,VALUE v)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int ctu=FIX2INT(v);

  if(ctu<a->n_ctu && a->ctu[ctu]->chunk!=NULL)
  {
    pthread_mutex_lock(&a->mtx);
    a->ctu[ctu]->chunk=NULL;
    pthread_mutex_unlock(&a->mtx);
    return Qtrue;
  }
  return Qfalse;
}

VALUE audiotoy_firechunk(VALUE self,VALUE v,VALUE pos)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int ctu=FIX2INT(v);

  if(ctu>a->n_ctu || a->ctu[ctu]->chunk==NULL)
    return Qfalse;

  a->ctu[ctu]->cur_sample=NUM2DBL(pos);
  a->ctu[ctu]->active=1;

  return Qtrue;
}

VALUE audiotoy_setrate(VALUE self,VALUE v,VALUE rate)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int ctu=FIX2INT(v);

  if(ctu>a->n_ctu || a->ctu[ctu]->chunk==NULL)
    return Qfalse;

  a->ctu[ctu]->rate=NUM2DBL(rate);

  return Qtrue;
}

VALUE audiotoy_setvolume(VALUE self,VALUE v,VALUE vol)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int ctu=FIX2INT(v);

  if(ctu>a->n_ctu || a->ctu[ctu]->chunk==NULL)
    return Qfalse;

  int i;

  for(i=0;i<CHANNELS;i++)
    a->ctu[ctu]->volume[i]=NUM2DBL(rb_ary_entry(vol,i));

  return self;
}


VALUE audiotoy_setvolume51(VALUE self,VALUE v,VALUE vx,VALUE vy)
{
#if CHANNELS==6
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int ctu=FIX2INT(v);
  float x=NUM2DBL(vx);
  float y=NUM2DBL(vy);

  x-=0.5;
  if(x<-0.25)
    x=-0.25;
  else if(x>0.25)
    x=0.25;
  y-=0.5;
  y/=2.0;

  a->ctu[ctu]->volume[0]=-x+y+0.5;
  a->ctu[ctu]->volume[1]=x+y+0.5;
  a->ctu[ctu]->volume[2]=-x-y+0.5;
  a->ctu[ctu]->volume[3]=x-y+0.5;
  a->ctu[ctu]->volume[4]=0.5;
  a->ctu[ctu]->volume[5]=y+y+0.5;

#endif
  return self;
}

VALUE audiotoy_nchunks(VALUE self)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  return INT2FIX(a->n_chunks);
}

VALUE audiotoy_chunkfromname(VALUE self,VALUE v)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);
  char *name=RSTRING_PTR(v);
  int i;

  for(i=0;i<a->n_chunks;i++)
    if(a->chunks[i]->samples!=NULL && strcasestr(a->chunks[i]->name,name)!=NULL)
      return INT2FIX(i);

  return Qnil;
}

VALUE audiotoy_chunkname(VALUE self,VALUE v)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  int ctu=FIX2INT(v);

  if(a->ctu[ctu]->chunk==NULL)
    return Qnil;

  return rb_str_new2(a->ctu[ctu]->chunk->name);
}

VALUE audiotoy_fixloud(VALUE self,VALUE v)
{
  audiotoy_stc *a;
  Data_Get_Struct(self,audiotoy_stc,a);

  if(v==Qnil)
  {
    a->whichloudspeaker=-1;
    loggo("Whichlo disabled.");
  }
  else
  {
    a->whichloudspeaker=FIX2INT(v);
    loggo("Whichlo set to %d.",a->whichloudspeaker);
  }

  return self;
}

/*
 * LOCAL
 */

static int first_available_chunk(audiotoy_stc *a)
{
  int i;

  for(i=0;i<a->n_chunks;i++)
    if(a->chunks[i]->samples==NULL)
      return i;

  i=a->n_chunks++;

  a->chunks=realloc(a->chunks,sizeof(chunk_stc *)*(i+1));
  a->chunks[i]=malloc(sizeof(chunk_stc));
  bzero(a->chunks[i],sizeof(chunk_stc));

  return i;
}

static int first_available_ctu(audiotoy_stc *a)
{
  int i;

  for(i=0;i<a->n_ctu;i++)
    if(a->ctu[i]->chunk==NULL)
      return i;

  i=a->n_ctu++;

  a->ctu=realloc(a->ctu,sizeof(chunk_to_use_stc *)*(i+1));
  a->ctu[i]=malloc(sizeof(chunk_to_use_stc));
  bzero(a->ctu[i],sizeof(chunk_to_use_stc));

  return i;
}

static void *pback(void *arg)
{
  audiotoy_stc *a=(audiotoy_stc *)arg;
  int bytes_per_frame=snd_pcm_frames_to_bytes(a->alsa_handle,1);
  uint8_t *ptr;
  int n_read,to_send;

  while(!a->stop_thread)
  {
    generate_audioblock(a);
    for(ptr=(uint8_t *)a->alsa_frames,to_send=a->alsa_period_size;to_send>0;)
    {
      n_read=snd_pcm_writei(a->alsa_handle,ptr,to_send);

      if(n_read==-EAGAIN)
        continue;

      if(n_read<0)
      {
//        loggo("%s: Write error: %d,%s\n",__func__,n_read,snd_strerror(n_read));

        if(xrun_recovery(a->alsa_handle,n_read)<0)
	  rb_raise(rb_eRuntimeError,"%s: xrun recovery failed",__func__);

        break;	/* skip one period */
      }

      ptr+=(n_read*bytes_per_frame);
      to_send-=n_read;
    }
  }
  if (a->alsa_handle) {
    snd_pcm_drop(a->alsa_handle);
    snd_pcm_close(a->alsa_handle);
    a->alsa_handle = NULL;
  }
  return NULL;
}

/*
 *   Underrun and suspend recovery
 */

static int xrun_recovery(snd_pcm_t *handle,int err)
{
  if(err==-EPIPE) /* under-run */
  {
    err=snd_pcm_prepare(handle);
    if(err<0)
      loggo("%s: Can't recovery from underrun, prepare failed: %s\n",__func__,snd_strerror(err));
    return 0;
  }
  else if(err==-ESTRPIPE)
  {
    while((err=snd_pcm_resume(handle))==-EAGAIN)
      sleep(1);	/* wait until the suspend flag is released */

    if(err<0)
    {
      err=snd_pcm_prepare(handle);
      if(err<0)
        loggo("%s: Can't recovery from suspend, prepare failed: %s\n",__func__,snd_strerror(err));
    }

    return 0;
  }

  return err;
}

static void generate_audioblock(audiotoy_stc *a)
{
  int i,j,k,cs;
  __s16 *sp,lsp;
  __s32 *accp;
  chunk_to_use_stc *ctu;

  bzero(a->accum_buffers,sizeof(__s32)*a->alsa_period_size*CHANNELS);

  pthread_mutex_lock(&a->mtx);
  for(i=0;i<a->n_ctu;i++)
  {
    ctu=a->ctu[i];

    if(ctu->chunk!=NULL && ctu->active)
    {
      for(accp=a->accum_buffers,j=0;j<a->alsa_period_size;j++)
      {
	lsp=ctu->chunk->samples[(int)ctu->cur_sample];
	for(k=0;k<CHANNELS;k++)
	{
	  if(a->whichloudspeaker==k)
	    (*accp)+=lsp;
	  else if(a->whichloudspeaker<0 && ctu->volume[k]>0.0)
	    (*accp)+=lsp*ctu->volume[k];
	  accp++;
	}

	ctu->cur_sample+=ctu->rate;
	cs=(int)ctu->cur_sample;

	if(cs<ctu->restart_sample && ctu->rate<0.0)
	{
	  if(!ctu->loop)
	  {
	    ctu->active=0;
	    break;
	  }
	  ctu->cur_sample=(float)(ctu->chunk->n_samples-1);
	  for(k=0;k<CHANNELS;k++)
	    ctu->volume[k]*=ctu->volume_factor_apply;
	}
	else if(cs>=ctu->chunk->n_samples && ctu->rate>0.0)
	{
	  if(!ctu->loop)
	  {
	    ctu->active=0;
	    break;
	  }
	  ctu->cur_sample=ctu->restart_sample;
	  for(k=0;k<CHANNELS;k++)
	    ctu->volume[k]*=ctu->volume_factor_apply;
	}
      }
    }
  }
  for(sp=a->alsa_frames,accp=a->accum_buffers,i=0;i<a->alsa_period_size*CHANNELS;i++,sp++,accp++)
  {
    if((*accp)<-0x7fff)
      *sp=-0x7fff;
    else if((*accp)>0x7fff)
      *sp=0x7fff;
    else
      *sp=*accp;
  }

  pthread_mutex_unlock(&a->mtx);
}
