#!/usr/bin/env ruby
# thermalresetter.rb

=begin

03/08/2007 c.e. prelz AS FLUIDO <fluido@fluido.as>
Project //GLOW/IN/THE/DARK//

Just a series of commands that are required to configure a thermaleye to zwolle usage
(based on one case - setting this one camera - will it work on other cases?!?)

$Id: thermalresetter.rb 1494 2008-02-12 15:02:20Z karl $

=end

require 'pp'
require 'thermal'

=begin

answer to 1 (configuration data)

0: <0002> -> 2
1: <0001> -> 1
2: <0000> -> 0
3: <002b> -> 43			New camera 41
4: <5928> -> 22824		New camera 30810
5: <f860> -> 63584		New camera 63638
6: <f5d6> -> 62934		New camera 6710
7: <0000> -> 0			New camera 6
8: <0750> -> 1872		New camera 17718
9: <beef> -> 48879
10: <c01d> -> 49181
11: <0011> -> 17
12: <febf> -> 65215
13: <febf> -> 65215
14: <2795> -> 10133
15: <2795> -> 10133
16: <2b07> -> 11015
17: <048a> -> 1162
18: <92b6> -> 37558
19: <0003> -> 3
20: <0003> -> 3
21: <0004> -> 4
22: <0004> -> 4
23: <0009> -> 9
24: <0003> -> 3
25: <1116> -> 4374
26: <2006> -> 8198
27: <0624> -> 1572
28: <2006> -> 8198
29: <0603> -> 1539
30: <c5b5> -> 50613


0: <0002> -> 2
1: <0001> -> 1
2: <0000> -> 0
3: <0029> -> 41
4: <785a> -> 30810
5: <f896> -> 63638
6: <1a36> -> 6710
7: <0006> -> 6
8: <4536> -> 17718
9: <beef> -> 48879
10: <f941> -> 63809
11: <0011> -> 17
12: <02e5> -> 741
13: <02e3> -> 739
14: <2795> -> 10133
15: <2795> -> 10133
16: <2b01> -> 11009
17: <049f> -> 1183
18: <0702> -> 1794
19: <0003> -> 3
20: <0003> -> 3
21: <0004> -> 4
22: <0004> -> 4
23: <000a> -> 10
24: <0001> -> 1
25: <0727> -> 1831
26: <2007> -> 8199
27: <0624> -> 1572
28: <2006> -> 8198
29: <0603> -> 1539
30: <9829> -> 38953



answer to 1111 (colorization data)

0: <0002> -> 2
1: <0457> -> 1111
2: <0000> -> 0
3: <0000> -> 0
4: <001e> -> 30
5: <0064> -> 100
6: <00fa> -> 250
7: <0230> -> 560
8: <00ff> -> 255
9: <00ff> -> 255
10: <0000> -> 0
11: <00ff> -> 255
12: <0099> -> 153
13: <0033> -> 51
14: <00ff> -> 255
15: <0000> -> 0
16: <0000> -> 0
17: <f333> -> 62259

answer to 1140 (user control)

0: <0002> -> 2
1: <0474> -> 1140
2: <0000> -> 0
3: <0001> -> 1
4: <0000> -> 0
5: <0001> -> 1
6: <0048> -> 72
7: <004f> -> 79
8: <0054> -> 84
9: <0000> -> 0
10: <0064> -> 100
11: <0001> -> 1
12: <7e10> -> 32272
13: <0000> -> 0
14: <0000> -> 0
15: <0000> -> 0
16: <0001> -> 1
17: <0001> -> 1
18: <0001> -> 1
19: <7c25> -> 31781

answer to 1141 (power user)

0: <0002> -> 2
1: <0475> -> 1141
2: <0000> -> 0
3: <0004> -> 4
4: <0000> -> 0
5: <0000> -> 0
6: <0000> -> 0
7: <0000> -> 0
8: <0000> -> 0
9: <0004> -> 4
10: <0000> -> 0
11: <0000> -> 0
12: <0000> -> 0
13: <0000> -> 0
14: <fb81> -> 64385

answer to 1154 (ezoom factors)

0: <0002> -> 2
1: <0482> -> 1154
2: <0000> -> 0
3: <0000> -> 0
4: <0000> -> 0
5: <0000> -> 0
6: <0001> -> 1
7: <0000> -> 0
8: <0001> -> 1
9: <0000> -> 0
10: <0000> -> 0
11: <0000> -> 0
12: <0000> -> 0
13: <0000> -> 0
14: <0000> -> 0
15: <0000> -> 0
16: <0000> -> 0
17: <0000> -> 0
18: <0000> -> 0
19: <0000> -> 0
20: <0000> -> 0
21: <0000> -> 0
22: <0000> -> 0
23: <0000> -> 0
24: <0000> -> 0
25: <0000> -> 0
26: <0001> -> 1
27: <fb79> -> 64377

answer to 1154 (polarity)

3: <0001> -> 1

answer to 1156 (gain and level)

0: <0002> -> 2
1: <0484> -> 1156
2: <0000> -> 0
3: <0000> -> 0
4: <0003> -> 3
5: <18f8> -> 6392
6: <f896> -> 63638
7: <0001> -> 1
8: <0001> -> 1
9: <2710> -> 10000
10: <7fff> -> 32767
11: <0000> -> 0
12: <e890> -> 59536
13: <0000> -> 0
14: <4c08> -> 19464
15: <0e40> -> 3648

answer to 1106 (data access point)

3: <0000> -> 0

answer to 1107 (data access mode)

3: <0000> -> 0

=end

cmds=[
#      [139,[1]],
#      [140,[0,0,0,0x41,0x73,0x66,0,90,1,0x7e10,0,0,0,1,1,1]],
#      [141,[4,0,0,0,0,0,4,0,0,0,0]],
      [156,[0,0,GAIN_VALUE,BIAS_VALUE,0,0,0,0,0,0,0,0]],
      [156,[1,0,GAIN_VALUE,BIAS_VALUE,0,0,0,0,0,0,0,0]]
     ]
#1.upto(14) do |v|
#  cmds.push([138,[v,0,0,0,0,0,0,0,0]])
end

th=Thermal.new('/dev/ttyUSB0')
cmds.each do |c|
  STDERR.printf("Sending cmd %d\n",c[0])
  th.send_command(*c)
  rd=nil
  loop do
    rd=th.read()
    break if(rd)
  end
  
  rd.unpack('v*').each_with_index do |v,i|
    STDERR.printf("%d: <%.4x> -> %d\n",i,v,v)
  end
  sleep(0.3)
end
