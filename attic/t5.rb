MAX_TANV=0.99
MAX_TANRES=1.0/Math::tan(MAX_TANV)

def tanvalue(v)
  1.0-(Math::tan((1.0-v)*MAX_TANV)*MAX_TANRES)
end

0.0.step(1.0,0.05) do |v|
  printf("%f -> %f\n",v,tanvalue(v))
end
