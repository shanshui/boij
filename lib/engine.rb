#!/usr/bin/env ruby
# engine.rb

=begin

7/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

The engine that runs it all

$Id: engine.rb 1493 2008-02-12 14:57:49Z karl $

=end

require 'boij'
require 'boij_xserver'

require "RUDL"
include RUDL
include Constant

FULLSCREEN_FLAG=true#false
EXIT_WITH_ESC=!FULLSCREEN_FLAG
AUDIO_FLAG=true

OPEN_SOUNDDIR=BASE+"sounds/"
SOUNDFILE_ATTN=0.5

SNAPDIR=BASE+'snaps/'

class Engine
  BLACKFILL=Boij_sharedvideoarea::BLACKFILL_EXTEND
  #BLACKFILL=Boij_sharedvideoarea::BLACKFILL_MIRROR

  attr_reader(:audiotoy,:sounds,:blackfill)
  
  def initialize
    @blackfill=BLACKFILL
    
    if(FULLSCREEN_FLAG)
      @xsv=Boij_xserver::new(*SCREEN_SIZE)
      ENV['DISPLAY']=':0'
    else
      @xsv=nil
    end

    @pictures=[]
    Dir::foreach(PICTURE_BASE) do |f|
      next if(f[-3,3]!='.bo')
      @pictures.push(Picture::new(f.split('.')[0].to_i()).convert(@blackfill))
    end

    @pictures.sort!() do |a,b|
      a.prog<=>b.prog
    end

    initarr=[]
    @pictures.each_with_index do |pn,i|
      pn.prog=i
      initarr.push([*pn.size,pn.bottom_offset,pn.pixels])
    end

    @b_c=Boij_columns::new(WINDOW_SIZE,initarr)

    @sounds=[]
    if(AUDIO_FLAG)
      @audiotoy=Audiotoy::new()
      @sounds=[]
      Dir::entries(OPEN_SOUNDDIR).each do |f|
	next if(f[-4,4]!='.wav')
	pp f
	@sounds.push(@audiotoy.load_file(OPEN_SOUNDDIR+f,SOUNDFILE_ATTN))
      end  
    end
    
    @players=[]
    Boij_sharedvideoarea::MAX_MONITORED.times do |i|
      @players.push(Player::new(self,i))
    end
    
    @cn=Client_networker::new(method('tracker_callback').to_proc())
    @old_positions=nil
    @updatepos_flag=false
    @orphaned_images=[]
    @latest_pixels=nil

    @done=false
    Signal::trap('INT') do
      @done=true
    end
    Signal::trap('TERM') do
      @done=true
    end
  end
  
  def tracker_callback(packet)
    @positions=packet
    @updatepos_flag=true
  end
  
  def runme
    if(FULLSCREEN_FLAG)
      @d=DisplaySurface.new(WINDOW_SIZE,HWSURFACE|DOUBLEBUF|FULLSCREEN,24)
      Mouse.set_cursor([0,0],
                       [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],
                        [0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],
                       [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],
                        [0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]])
      
      Mouse.visible=false # hide the cursor
    else      
      @d=DisplaySurface.new(WINDOW_SIZE,HWSURFACE|DOUBLEBUF,24)
    end
    
    nxt_refresh=Time::now()
    until(@done)
      for event in EventQueue.get
	case event
        when QuitEvent
          @done=true
	when KeyDownEvent
	  case event.key
	  when K_ESCAPE
	    @done=true if EXIT_WITH_ESC
	  when 115
	    take_snap()
	  end
	end
      end

      if(@updatepos_flag)
        @updatepos_flag=false
        update_positions()
      end
      
      now=Time::now()
      imgs=[]+@orphaned_images
      @players.each do |p|
        if(p.arrival)
          p.start_new_image_if_needed(now)
          imgs+=p.owned_pictures
        end        
      end

      imgs.sort!() do |a,b|
        a.start_time<=>b.start_time
      end

      now=Time::now()

      to_send=[]
      imgs.each_with_index do |img,i|
        img.update_width(now)
        to_send.push([i,img.start_position,img.width.to_i(),img.picture.prog,
                      img.left_or_right,img.begf,img.size_to_use]) if(img.width.to_i()>0)
      end

      if(to_send.length()>0)
        @latest_pixels,discardables=@b_c.compute_screen(to_send)
        
        discardables.each() do |i|
#          loggo("Discardo #{i}")
          img=imgs[i]
	  
	  @audiotoy.del_chunk(img.achunk)
	  
          if(!img.owner)
            @orphaned_images.delete(img)
          else
            img.owner.owned_pictures.delete(img)
          end
        end
      end
#      if(Time::now()>=nxt_refresh)
#        nxt_refresh+=REFRESH_INTERVAL
        refresh_screen()
#      else
#        sleep(0.01)
#      end
    end

    @d.destroy()
    @cn.done=true
    @xsv.reap() if(@xsv)
  end

  def update_positions
    return if(!@positions)

    cpos=[]+@positions
    cpos.each_with_index() do |p,i|
      if(!p)
        if(@players[i].arrival)
          loggo("Player #{i+1} left.")
          @orphaned_images+=@players[i].give_owned_pictures_and_leave()
        end
      else
        if(!@players[i].arrival)
          loggo("Player #{i+1} arrived.")
          @players[i].arrival=Time::now()
#        else
#          xd=(@players[i].position[0]-p[0])*PLAYGROUND_SIZE[0]
#          yd=(@players[i].position[1]-p[1])*PLAYGROUND_SIZE[1]*SPACE_COMPRESSION_FACT
#          @players[i].traveled_distance+=Math::sqrt(xd*xd+yd*yd)
        end
        @players[i].position=p
      end
    end
  end
  
  def refresh_screen
    return if(!@latest_pixels)
    @d.pixels=@latest_pixels
    @d.flip()
  end

  def random_picture
    @pictures[rand(@pictures.length())]
  end

  def take_snap
    return if(!@latest_pixels)
    
    cmd='cjpeg > '+SNAPDIR+'s.'+Time::now().stamp()+'.jpg'
    IO::popen(cmd,'w') do |f|
      f.printf("P6\n%d %d\n255\n",*WINDOW_SIZE)
      f.print(Boij_sharedvideoarea::pixel_flip(@latest_pixels))
    end
  end
end

if($0==__FILE__)
#  lf=Fluid_logfile::new('engine')
  
  eo=Engine::new()
  begin
    eo.runme()
  rescue
    pp ["Engine crash",$!,$!.backtrace()]
  end
end


