#!/usr/bin/env ruby
# horizo.rb

=begin

9/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Small utility to select horizon of image

$Id: horizo.rb 1493 2008-02-12 14:57:49Z karl $

=end

IB=[1280,1024]
IR=IB[0].to_f()/IB[1]

require 'boij.so'

require "RUDL"
include RUDL
include Constant

raise "Usage: $0 <image path>" if(ARGV.length()<1)
ARGV.each do |path|
  img=Magick::Image::read(path)[0]
  size=[img.columns(),img.rows()]
  ir=size[0].to_f()/size[1]
  
  if(ir>IR)
    x=IB[0]
    y=(x/ir).to_i()
  else
    y=IB[1]
    x=(y*ir).to_i()
  end
  
  w=DisplaySurface.new([x,y],HWSURFACE|DOUBLEBUF|NOFRAME,24)
  img.resize!(x,y)
  fact=size[0].to_f()/x
  
  w.pixels=img.export_pixels_to_str(0,0,x,y,'BGR')
  w.update
  
  while(true)
    event=EventQueue.wait()
    case event
    when MouseMotionEvent
      latest_y=(event.pos[1]*fact).to_i()
      STDERR.print("\r-->#{latest_y}<---       ")
    when KeyDownEvent
      break if(event.key==K_ESCAPE)
      if(event.key==97)
        STDOUT.printf("%s#%d\n",path,latest_y)
        break
      end
    when QuitEvent
      break
    end
  end
  w.destroy()
end  

