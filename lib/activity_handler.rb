#!/usr/bin/env ruby
# activity_handler.rb

=begin

09/07/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Receives events from the sensor grid, computes the force field, and makes it available
to clients

$Id: activity_handler.rb 2411 2010-07-12 18:12:22Z karl $

=end

require 'boij'

class Grid_node
  attr_reader(:x,:y,:key)
  attr_accessor(:life)

  STRAIGHT_WEIGHT=0.5
  DIAG_WEIGHT=0.2

  def initialize(x,y)
    @x,@y=x,y
    @key=Grid_node::keyval(@x,@y)
    @neighbours=[]
    @life=0.0
  end

  def add_neighbour(node,weight)
    @neighbours.push([node,weight])
  end

  def add_event(v)
    @life+=v
    @neighbours.each do |node,weight|
      node.life+=v*weight
    end
  end

  def Grid_node::keyval(x,y)
    (x<< 8)|y
  end

  def Grid_node::get_array(x,y)
    hash={}
    x.times do |xi|
      y.times do |yi|
        n=Grid_node::new(xi,yi)
        hash[n.key]=n
      end
    end

    hash.values.each do |node|
      node.add_neighbour(hash[Grid_node::keyval(node.x-1,node.y)],STRAIGHT_WEIGHT) if(node.x>0)
      node.add_neighbour(hash[Grid_node::keyval(node.x-1,node.y+1)],DIAG_WEIGHT) if(node.x>0 && node.y<y-1)
      node.add_neighbour(hash[Grid_node::keyval(node.x,node.y+1)],STRAIGHT_WEIGHT) if(node.y<y-1)
      node.add_neighbour(hash[Grid_node::keyval(node.x+1,node.y+1)],DIAG_WEIGHT) if(node.x<x-1 && node.y<y-1)
      node.add_neighbour(hash[Grid_node::keyval(node.x+1,node.y)],STRAIGHT_WEIGHT) if(node.x<x-1)
      node.add_neighbour(hash[Grid_node::keyval(node.x+1,node.y-1)],DIAG_WEIGHT) if(node.x<x-1 && node.y>0)
      node.add_neighbour(hash[Grid_node::keyval(node.x,node.y-1)],STRAIGHT_WEIGHT) if(node.y>0)
      node.add_neighbour(hash[Grid_node::keyval(node.x-1,node.y-1)],DIAG_WEIGHT) if(node.x>0 && node.y>0)
    end

    hash
  end
end

class Activity_handler
  LOOPSLEEP=0.12
  ATTENUATION_PER_SECOND=0.15

  def initialize(x,y)
    Fluid_logfile::new('activity_handler')

    @x,@y=x,y
    @nodes=Grid_node::get_array(@x,@y)

    @clients={}

    DRb::start_service(drb_uri(DRB_PORT_ACTIVITY),self)
  end

  def runme
    @done=false
    Signal::trap('INT') do
      @done=true
    end
    Signal::trap('TERM') do
      @done=true
    end
    Signal::trap('USR1') do
      puts("Dumping at #{Time::now.stamp()}")

      a=values()
      @y.times do |y|
        @x.times do |x|
          printf("%7.4f ",a[y][x])
        end
        putc("\n")
      end
    end

    last_attenuation_time=Time::now
    until(@done)
      now=Time::now
      attval=1.0-(ATTENUATION_PER_SECOND*(now-last_attenuation_time))
      last_attenuation_time=now
      @nodes.values.each do |node|
        node.life*=attval
      end
      sleep(LOOPSLEEP)
    end
  end

  def dimensions
    [@x,@y]
  end

  def values
    ret=[]
    @y.times do
      ret.push([])
    end
    @nodes.values.each do |n|
      ret[n.y][n.x]=n.life
    end
    ret
  end

  def tickle(x,y,v)
    @nodes[Grid_node::keyval(x,y)].add_event(v)
  end

  def Activity_handler::remote_connect
    DRb::start_service()
    DRbObject::new(nil,drb_uri(DRB_PORT_ACTIVITY))
  end
end

if($0==__FILE__)
  raise "Usage: #{$0} x y" if(ARGV.length!=2)
  ah=Activity_handler::new(ARGV[0].to_i,ARGV[1].to_i)
  begin
    ah.runme()
  rescue
    loggo("Activity handler crash: #{$!}, #{$!.backtrace()}")
  end
end
