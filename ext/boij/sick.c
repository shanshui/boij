/* sick.c */

/*
 * 12/3/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * Talking to the Sick LMS200 sensor
 *
 * $Id: sick.c 4105 2014-01-08 13:51:03Z karl $
 */

#include "boij.h"
#include <LMS1xx.h>

#include <pthread.h>
#include <ctype.h>
#include <sys/timeb.h>
#include <linux/types.h>

#define MAX_Q_SIZE 1 // max mesaurement queue size
#define MAX_DIST 20000 // max measurable distance in mm

#define DEG_TO_RAD (M_PI/180.0)

typedef struct
{
  int len;
  uint8_t cmd[];
} cmd_stc;

typedef struct sick_cmd
{
  scanData data;
  struct sick_cmd *next;
} sick_cmd_stc;

typedef struct
{
  float x,y;
  int cnt,assigned;
  struct timeval last_present;
} loc_stc;

typedef struct
{
  int conn,n_samples,maxobj,minwgt;
  LMS1xx *unit;
  pthread_t reader_thr;
  pthread_mutex_t mtx;
  uint8_t thread_running;
  uint16_t *background,tolerance;
  int databfr_read,databfr_write;
  loc_stc *locations,*tracked_objects;
  sick_cmd_stc *cmd_queue;
  float maxdist,*sintable,*costable,minx,maxx,diffx,miny,maxy,diffy;
  suseconds_t max_disappeared;
} sick_stc;

VALUE cls_sick;

static void free_sick(void *p);
static inline float dist(loc_stc *ls1,loc_stc *ls2);
static inline void merge(loc_stc *ls1,loc_stc *ls2);
static void *receiver(void *arg);
static sick_cmd_stc **last_in_queue(sick_stc *s);
static void push_sick_data(sick_stc *s,sick_cmd_stc *new_data);
static sick_cmd_stc *pull_sick_data(sick_stc *s);
static sick_cmd_stc *new_sick_cmd(void);
static void free_sick_cmd(sick_cmd_stc *ss);
static inline suseconds_t get_timediff(struct timeval *t1,struct timeval *t2);
static void close_unit(sick_stc *s);

// VALUE v_startang,VALUE v_stopang,
VALUE new_sick(VALUE self,VALUE v_addr,VALUE v_port,VALUE v_scanrange,VALUE v_angres,VALUE v_background,VALUE v_ranges,VALUE v_tolerance,VALUE v_minwgt,
	       VALUE v_maxobj,VALUE v_maxdist,VALUE v_maxdisappeared)
{
  sick_stc *s;
  VALUE sdata=Data_Make_Struct(cls_sick,sick_stc,NULL,free_sick,s);

  char *addr=RSTRING_PTR(v_addr);
  int port=FIX2INT(v_port);
  int angres=FIX2INT(v_angres);
  int scanrange=FIX2INT(v_scanrange);
  int startang=0;
  int stopang=180;

  if (port == 0)
      port = 2111;

  if (scanrange == 100) {
      startang=40;
      stopang=140;
  }
  //int startang = FIX2INT(v_startang);
  //int stopang = FIX2INT(v_startang);
  //int scanrange = stopang - startang;

  if (angres == 0)
      angres = 25;

  loggo ("new_sick: addr=%s, port=%d, angres=%d, scanrange=%d, startang=%d, stopang=%d, background=%p",
         addr, port, angres, scanrange, startang, stopang, v_background);

  s->n_samples = scanrange / (angres/100.) + 1;
  loggo("No. of samples expected: %d",s->n_samples);

  if(v_background!=Qnil)
  {
    loggo ("Setting background");
    int l=RARRAY_LEN(v_background);
    if(l<s->n_samples || l>s->n_samples+2)
      rb_raise(rb_eArgError,"%s: expected bg length %d, got %d",__func__,s->n_samples,l);

    s->minx=NUM2DBL(rb_ary_entry(v_ranges,0));
    s->maxx=NUM2DBL(rb_ary_entry(v_ranges,1));
    s->diffx=s->maxx-s->minx;
    s->miny=NUM2DBL(rb_ary_entry(v_ranges,2));
    s->maxy=NUM2DBL(rb_ary_entry(v_ranges,3));
    s->diffy=s->maxy-s->miny;

    s->tolerance=FIX2INT(v_tolerance);

    s->background=malloc(sizeof(uint16_t)*l);
    for(int i=0;i<l;i++)
      s->background[i]=FIX2INT(rb_ary_entry(v_background,i))-s->tolerance;

    s->maxobj=FIX2INT(v_maxobj);
    s->minwgt=FIX2INT(v_minwgt);
    s->maxdist=NUM2DBL(v_maxdist);
    s->max_disappeared=FIX2INT(v_maxdisappeared);
    s->locations=malloc(sizeof(loc_stc)*s->n_samples);
    s->tracked_objects=malloc(sizeof(loc_stc)*s->maxobj);
    for(int i=0;i<s->maxobj;i++)
      s->tracked_objects[i].cnt=-1;

    s->sintable=malloc(sizeof(float)*s->n_samples);
    s->costable=malloc(sizeof(float)*s->n_samples);

    float angcur=0.0;
    if(scanrange==100)
      angcur=40*DEG_TO_RAD;
    float incr=DEG_TO_RAD;
    if(angres==25)
      incr/=4.0;
    else if(angres==50)
      incr/=2.0;

    for(int i=0;i<s->n_samples;i++)
    {
      s->sintable[i]=sinf(angcur);
      s->costable[i]=-cosf(angcur);
      angcur+=incr;
    }
  }
  else
    s->background=NULL;

  s->cmd_queue=NULL;

  scanCfg c, c0;
  c.scanningFrequency = 25*100;
  c.angleResolution = angres*100;
  c.startAngle = -45*10000;
  c.stopAngle  = 225*10000;

  scanOutputRange r, r0;
  r.angleResolution = angres*100;
  r.startAngle = startang*10000;
  r.stopAngle = stopang*10000;

  scanDataCfg cc;
  cc.deviceName = 0;
  cc.encoder = 0;
  cc.outputChannel = 1;
  cc.remission = 0;
  cc.resolution = 0;
  cc.position = 1;
  cc.timestamp = 0;
  cc.outputInterval = 1;

  loggo ("Connecting to unit");

  s->unit = LMS1xx_connect(addr,port);
  if (s->unit == NULL || LMS1xx_isConnected(s->unit) == 0)
    rb_raise(rb_eArgError,"%s: could not open %s: %s",__func__,addr,strerror(errno));
  if (LMS1xx_login(s->unit) < 0)
    rb_raise(rb_eArgError,"%s: LMS1xx_login error: %s",__func__,strerror(errno));

  if (LMS1xx_getScanCfg(s->unit, &c0) < 0)
    rb_raise(rb_eArgError,"%s: LMS1xx_getScanCfg error: %s",__func__,strerror(errno));

  int cfg_chg = 0;

  if (c.scanningFrequency != c0.scanningFrequency
      || c.angleResolution != c0.angleResolution) {
    loggo ("Setting scan configuration");
    cfg_chg = 1;
    if (LMS1xx_setScanCfg(s->unit, c) < 0)
       rb_raise(rb_eArgError,"%s: LMS1xx_setScanCfg error: %s",__func__,strerror(errno));
  }

  if (LMS1xx_getScanOutputRange(s->unit, &r0) < 0)
    rb_raise(rb_eArgError,"%s: LMS1xx_getScanOutputRange error: %s",__func__,strerror(errno));

  if (1 || r.angleResolution != r0.angleResolution
      || r.startAngle != r0.startAngle
      || r.stopAngle != r0.stopAngle) {
      loggo ("Setting scan output configuration");
      cfg_chg = 1;
      if (LMS1xx_setScanDataCfg(s->unit, cc) < 0)
          rb_raise(rb_eArgError,"%s: LMS1xx_setScanDataCfg error: %s",__func__,strerror(errno));
      if (LMS1xx_setScanOutputRange(s->unit, r) < 0)
          rb_raise(rb_eArgError,"%s: LMS1xx_setScanOutputRange error: %s",__func__,strerror(errno));
  }

  if (cfg_chg != 0) {
    loggo ("Saving configuration");
    if (LMS1xx_saveConfig(s->unit) < 0)
      rb_raise(rb_eArgError,"%s: LMS1xx_saveConfig error: %s",__func__,strerror(errno));
  }

  if (LMS1xx_startMeas(s->unit) < 0)
    rb_raise(rb_eArgError,"%s: LMS1xx_startMeas error: %s",__func__,strerror(errno));

  do {
    int status = LMS1xx_queryStatus(s->unit);
    loggo("Waiting for sensor to be ready. status=%d", status);
    if (-1 == status) // error
      rb_raise(rb_eArgError,"%s: LMS1xx_queryStatus error: %s",__func__,strerror(errno));
    if (7 == status) // ready
      break;
    usleep(1000000);
  } while (1);

  if (LMS1xx_scanContinous(s->unit, 1) < 0)
    rb_raise(rb_eArgError,"%s: LMS1xx_scanContinous error: %s",__func__,strerror(errno));

/*
 * Start reader thread
 */

  int ret=pthread_mutex_init(&s->mtx,NULL);
  if(ret)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,ret,strerror(errno));

  ret=pthread_create(&s->reader_thr,NULL,receiver,s);
  if(ret)
    rb_raise(rb_eArgError,"%s: error #%d starting background read thread (%s)",__func__,ret,strerror(errno));
  s->thread_running = 1;

  loggo ("Sensor ready.");
  return sdata;
}

static void free_sick(void *p)
{
  sick_stc *s=(sick_stc *)p;

  close_unit(s);

  if(s->background)
  {
    free(s->background);
    free(s->locations);
    free(s->tracked_objects);
    free(s->sintable);
    free(s->costable);
  }

  free(s);
}

VALUE sick_msg(VALUE self)
{
  sick_stc *s;
  Data_Get_Struct(self,sick_stc,s);

  sick_cmd_stc *ss=pull_sick_data(s);
  if(ss==NULL)
    return Qfalse;

  VALUE data = rb_ary_new();
  VALUE dist1 = rb_ary_new();

  for (int i=0; i<s->n_samples; i++)
    rb_ary_push(dist1, INT2FIX(ss->data.dist1[i]));

  rb_ary_push(data,dist1);

  free_sick_cmd(ss);
  return data;
}

VALUE sick_tracker(VALUE self,VALUE f_gensum)
{
  sick_stc *s;
  Data_Get_Struct(self,sick_stc,s);
  uint8_t gensum=(f_gensum==Qtrue) ? 1 : 0;

  sick_cmd_stc *ss;
  ss=pull_sick_data(s);
  if(ss==NULL)
    return Qnil;

  uint16_t *uptr=&ss->data.dist1;
  int i,j,cnt;
  float f,fx,fy;
  VALUE lw=Qnil,lz;

  if(gensum)
    lw=rb_ary_new();

  for(i=0,cnt=0;i<s->n_samples;i++)
    if(s->background != NULL && uptr[i] < s->background[i])
    {
      f=(float)uptr[i]/MAX_DIST;

      fx=((s->costable[i]*f)+1.0)/2.0;
      if(fx>=s->minx && fx<=s->maxx)
      {
	fy=s->sintable[i]*f;
	if(fy>=s->miny && fy<=s->maxy)
	{
          if(s->diffx != 0)
	    s->locations[cnt].x=(fx-s->minx)/s->diffx;
          if(s->diffy != 0)
	    s->locations[cnt].y=(fy-s->miny)/s->diffy;

	  if(gensum)
	  {
	    lz=rb_ary_new();
	    rb_ary_push(lz,rb_float_new(s->locations[cnt].x));
	    rb_ary_push(lz,rb_float_new(s->locations[cnt].y));
	    rb_ary_push(lw,lz);
	  }

	  s->locations[cnt].cnt=1;
	  s->locations[cnt].assigned=0;
	  cnt++;
	}
      }
    }

  free_sick_cmd(ss);

/*
 * Now, the regrouping
 */

  loc_stc *ls1=NULL,*ls2=NULL;
  float min_dist,d;

  while(1)
  {
    for(min_dist=s->maxdist,i=0;i<cnt;i++)
      if(s->locations[i].assigned==0)
	for(j=i+1;j<cnt;j++)
	  if(s->locations[j].assigned==0)
	  {
	    d=dist(&s->locations[i],&s->locations[j]);
	    if(d<min_dist)
	    {
	      ls1=&s->locations[i];
	      ls2=&s->locations[j];
	      min_dist=d;
	    }
	  }
    if(min_dist>=s->maxdist)
      break;
    merge(ls1,ls2);
  }

/*
 * Remove the small groups
 */

  for(i=0;i<cnt;i++)
    if(s->locations[i].assigned==0)
      if(s->locations[i].cnt<s->minwgt)
	s->locations[i].assigned=1;

/*
 * Assign to possible locations
 */

  struct timeval now;
  suseconds_t timediff;

  gettimeofday(&now,NULL);

  for(i=0;i<s->maxobj;i++)
    s->tracked_objects[i].assigned=0;

  while(1)
  {
    for(min_dist=s->maxdist,i=0;i<cnt;i++)
      if(s->locations[i].assigned==0)
	for(j=0;j<s->maxobj;j++)
	  if(s->tracked_objects[j].cnt>=0 &&s->tracked_objects[j].assigned==0)
	  {
	    d=dist(&s->locations[i],&s->tracked_objects[j]);
	    if(d<min_dist)
	    {
	      ls1=&s->locations[i];
	      ls2=&s->tracked_objects[j];
	      min_dist=d;
	    }
	  }
    if(min_dist>=s->maxdist)
      break;
    ls2->x=ls1->x;
    ls2->y=ls1->y;
    ls1->assigned=1;
    ls2->assigned=1;
    ls2->last_present=now;
  }

/*
 * Anything new to assign?
 */

  for(i=0;i<cnt;i++)
    if(s->locations[i].assigned==0)
    {
      for(j=0;j<s->maxobj;j++)
	if(s->tracked_objects[j].cnt<0)
	{
	  s->tracked_objects[j].cnt=j;
	  s->tracked_objects[j].x=s->locations[i].x;
	  s->tracked_objects[j].y=s->locations[i].y;
	  s->tracked_objects[j].assigned=1;
	  s->tracked_objects[j].last_present=now;
	  break;
	}
      if(j>=s->maxobj) // no more available
	break;
    }

/*
 * If long enough time passes since not found, dead
 * Done after because the Ruby code must make one expire before the new one is born
 */

  for(i=0;i<s->maxobj;i++)
    if(s->tracked_objects[i].cnt>=0 && s->tracked_objects[i].assigned==0)
    {
      timediff=get_timediff(&now,&s->tracked_objects[i].last_present);
      if(timediff>=s->max_disappeared)
	s->tracked_objects[i].cnt=-1;
    }

  VALUE to_ret=rb_hash_new(),v;

  for(i=0;i<s->maxobj;i++)
    if(s->tracked_objects[i].cnt>=0)
    {
      v=rb_ary_new();
      rb_ary_push(v,rb_float_new(s->tracked_objects[i].x));
      rb_ary_push(v,rb_float_new(s->tracked_objects[i].y));
      rb_hash_aset(to_ret,INT2FIX(i),v);
    }

  if(gensum)
    rb_hash_aset(to_ret,Qfalse,lw);

  return to_ret;
}

VALUE sick_close(VALUE self)
{
  sick_stc *s;
  Data_Get_Struct(self,sick_stc,s);

  close_unit(s);

  return self;
}

VALUE sick_mapped_background(VALUE self)
{
  sick_stc *s;
  Data_Get_Struct(self,sick_stc,s);
  VALUE to_ret=rb_ary_new(),v;
  int i;
  float f,fx,fy;

  for(i=0;i<s->n_samples;i++)
  {
    f=(float)s->background[i]/MAX_DIST;
    fx=(s->costable[i]*f+1.0)/2.0;
    fy=s->sintable[i]*f;

    //if(fx<s->minx || fx>s->maxx || fy<s->miny || fx>s->maxy)
    //  rb_ary_push(to_ret,Qnil);
    //else
    {
      v=rb_ary_new();
      rb_ary_push(v,rb_float_new((fx-s->minx)/s->diffx));
      rb_ary_push(v,rb_float_new((fy-s->miny)/s->diffy));
      rb_ary_push(to_ret,v);
    }
  }

  return to_ret;
}

VALUE sick_update_vals(VALUE self,VALUE v_ranges,VALUE v_minwgt,VALUE v_maxdist,VALUE v_maxdisappeared)
{
  sick_stc *s;
  Data_Get_Struct(self,sick_stc,s);

  s->minx=NUM2DBL(rb_ary_entry(v_ranges,0));
  s->maxx=NUM2DBL(rb_ary_entry(v_ranges,1));
  s->diffx=s->maxx-s->minx;
  s->miny=NUM2DBL(rb_ary_entry(v_ranges,2));
  s->maxy=NUM2DBL(rb_ary_entry(v_ranges,3));
  s->diffy=s->maxy-s->miny;

  s->minwgt=FIX2INT(v_minwgt);
  s->maxdist=NUM2DBL(v_maxdist);
  s->max_disappeared=FIX2INT(v_maxdisappeared);

  return self;
}

static inline float dist(loc_stc *ls1,loc_stc *ls2)
{
  float xd=ls1->x-ls2->x;
  float yd=ls1->y-ls2->y;

  return sqrtf(xd*xd+yd*yd);
}

static inline void merge(loc_stc *ls1,loc_stc *ls2)
{
  float xd=ls2->x-ls1->x;
  float yd=ls2->y-ls1->y;
  float w2=(float)ls2->cnt/(ls1->cnt+ls2->cnt);
//  fprintf(stderr,"%.4f/%.4f (%d) and %.4f/%.4f (%d) give ",ls1->x,ls1->y,ls1->cnt,ls2->x,ls2->y,ls2->cnt);

  ls1->x+=xd*w2;
  ls1->y+=yd*w2;
  ls1->cnt+=ls2->cnt;
  ls2->assigned=1;

//  fprintf(stderr,"%.4f/%.4f (%d)\n",ls1->x,ls1->y,ls1->cnt);
}

static void *receiver(void *arg)
{
  sick_stc *s=(sick_stc *)arg;

  while(s->thread_running)
  {
    sick_cmd_stc *ss = new_sick_cmd();
    int ret = LMS1xx_getScanData(s->unit, &ss->data);
    if (0 < ret) {
      push_sick_data(s,ss);
    } else if (ret < 0) {
      rb_raise(rb_eArgError,"%s: Read error: %s",__func__,strerror(errno));
      break;
    }
  }

  return NULL;
}

static sick_cmd_stc **last_in_queue(sick_stc *s)
{
  if(s->cmd_queue==NULL)
    return &s->cmd_queue;

  sick_cmd_stc *to_ret;

  for(to_ret=s->cmd_queue;to_ret->next;to_ret=to_ret->next)
    ;

  return &(to_ret->next);
}

static void push_sick_data(sick_stc *s,sick_cmd_stc *new_data)
{
  pthread_mutex_lock(&s->mtx);

#if MAX_Q_SIZE > 1
  sick_cmd_stc **to_attach=last_in_queue(s);
  new_data->next=NULL;
  *(to_attach)=new_data;
#else
  if (s->cmd_queue != NULL)
    free_sick_cmd(s->cmd_queue);
  s->cmd_queue = new_data;
#endif

  pthread_mutex_unlock(&s->mtx);
}

static sick_cmd_stc *pull_sick_data(sick_stc *s)
{
  if(s->cmd_queue==NULL)
    return NULL;

  sick_cmd_stc *to_ret;

  pthread_mutex_lock(&s->mtx);

  to_ret=s->cmd_queue;
  s->cmd_queue=to_ret->next;
  to_ret->next=NULL;

  pthread_mutex_unlock(&s->mtx);

  return to_ret;
}

static sick_cmd_stc *new_sick_cmd(void)
{
  sick_cmd_stc *new_cmd=malloc(sizeof(sick_cmd_stc));
  bzero(new_cmd,sizeof(sick_cmd_stc));
  return new_cmd;
}

static void free_sick_cmd(sick_cmd_stc *ss)
{
  free(ss);
}

static inline suseconds_t get_timediff(struct timeval *t1,struct timeval *t2)
{
  return (t1->tv_sec-t2->tv_sec)*1000000L+(t1->tv_usec-t2->tv_usec);
}

static void close_unit(sick_stc *s)
{
  if (s->thread_running) {
    fputs("Stopping reader thread\n", stderr);
    s->thread_running = 0;
    pthread_cancel(s->reader_thr);
    pthread_join(s->reader_thr,NULL);
    fputs("Stopped\n",stderr);
  }

  if (s->unit != NULL) {
      LMS1xx_scanContinous(s->unit, 0);
      LMS1xx_stopMeas(s->unit);
      LMS1xx_disconnect(s->unit);
      s->unit = NULL;
  }
}
