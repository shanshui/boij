# quadrilat.rb

=begin

15/12/2008 c.e prelz AS FLUIDO fluido@fluido.as
project .:laspal:.

One quadrilateral within the field of capture of the thermcam

$Id: quadrilat.rb 2626 2010-11-10 14:48:16Z karl $

=end

class Quadrilat_dot
  attr_reader(:label,:qlat)
  attr_accessor(:x,:y)

  def initialize(qlat,label,x,y)
    @qlat,@label,@x,@y=qlat,label,x,y
  end

  def dist_from(x,y)
    xd=self.x-x
    yd=self.y-y
    Math::sqrt(xd*xd+yd*yd)
  end

  def update(dot)
    @x=dot.x
    @y=dot.y
  end

  def coords
    [@x,@y]
  end
  
  def to_s
    "#{@x},#{@y}"
  end

#  def scaled_dot
#    [@x/@qlat.areasize[0].to_f(),@y/@qlat.areasize[1].to_f()]
#  end
end

class Quadrilat
  attr_reader(:progr,:areasize,:ul,:ur,:lr,:ll,:dots)

  def initialize(progr)
    @progr=progr
    
    @ul=Quadrilat_dot::new(self,'UL',0.0,0.0)
    @ur=Quadrilat_dot::new(self,'UR',1.0,0.0)
    @lr=Quadrilat_dot::new(self,'LR',1.0,1.0)
    @ll=Quadrilat_dot::new(self,'LL',0.0,1.0)
    @dots=[@ul,@ur,@lr,@ll]
  end

  def update_dots(ql)
    @ul.update(ql.ul)
    @ur.update(ql.ur)
    @lr.update(ql.lr)
    @ll.update(ql.ll)
  end

  def to_s
    @ul.to_s+','+@ur.to_s+','+@lr.to_s+','+@ll.to_s
  end
end
