require 'boij.rb'
require 'sdl'

SQS=45
MAX=5.0
SLEEP_TIME=0.1
THRESH=2.5
MIN_DIST=1.1

SQL1=10
SQL2=5
SQCOL1=[255,0,0]
SQCOL2=[0,0,255]

require 'activity_handler'
DRb::start_service()
ah=DRbObject::new(nil,drb_uri(DRB_PORT_ACTIVITY))
x,y=ah.dimensions
cont=Contour::new(x,y)
scs=[SQS*x,SQS*y]
sqcoord=[]
y.times do |yi|
  a=[]
  x.times do |xi|
    a.push([xi*SQS,yi*SQS])
  end
  sqcoord.push(a)
end

SDL::init(SDL::INIT_VIDEO)
screen=SDL::Screen::open(*scs,32,SDL::HWSURFACE|SDL::DOUBLEBUF)
loop do
  loop do
    event=SDL::Event2::poll()
    break if(!event)
    case event
    when SDL::Event2::Quit
      exit(0)
    when SDL::Event2::KeyDown
      case event.sym
      when SDL::Key::ESCAPE
        exit(0)
      end
    when SDL::Event2::MouseButtonDown
      xsq=event.x/SQS
      ysq=event.y/SQS
      ah.tickle(xsq,ysq,1.0)
    end
  end

  a=ah.values()

  y.times do |yi|
    x.times do |xi|
      v=(([a[yi][xi],MAX].min)/MAX*255).to_i
      col=screen.mapRGB(v,v,0.0)
      screen.fillRect(*sqcoord[yi][xi],SQS,SQS,col)
    end
  end

  cdots=cont.calc(a.flatten,[THRESH])[0]

  cdots.each do |x,y|
    screen.fillRect((x+0.5)*SQS-SQL2,(y+0.5)*SQS-SQL2,SQL2*2,SQL2*2,SQCOL2)
  end

  a=cont.find_centers(cdots,MIN_DIST)

  a.each do |x,y,ne|
    screen.fillRect((x+0.5)*SQS-SQL1,(y+0.5)*SQS-SQL1,SQL1*2,SQL1*2,SQCOL1) #if(ne>2)
  end

  screen.updateRect(0,0,0,0)
  screen.flip()
  sleep(SLEEP_TIME)
end
