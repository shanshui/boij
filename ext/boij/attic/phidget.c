/* phidget.c */

/*
 * 02/11/2010 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * Talk with phidget
 *
 * $Id: phidget.c 2823 2011-03-14 18:50:06Z karl $
 */

#include "boij.h"
#include <phidget21.h>

#define WAIT_FOR_ATTACH 6000

typedef struct
{
  CPhidgetInterfaceKitHandle ih[2];
  CPhidgetTextLCDHandle lh;
} phidget_stc;

VALUE cls_phidget;

static void free_phidget(void *p);

VALUE phidget_new(VALUE self)
{
  phidget_stc *s=ALLOC(phidget_stc);

  int i;

  for(i=0;i<2;i++)
  {
    CPhidgetInterfaceKit_create(&s->ih[i]);
    CPhidget_open((CPhidgetHandle)s->ih[i],-1);
    if(CPhidget_waitForAttachment((CPhidgetHandle)s->ih[i],WAIT_FOR_ATTACH))
      rb_raise(rb_eRuntimeError,"Waited too long for phidget attachment");
  
    int sernum,version;
    const char *deviceptr;
    
    CPhidget_getDeviceType((CPhidgetHandle)s->ih[i],&deviceptr);
    CPhidget_getSerialNumber((CPhidgetHandle)s->ih[i],&sernum);
    CPhidget_getDeviceVersion((CPhidgetHandle)s->ih[i],&version);
    
    fprintf(stderr,"%d: %s\n",i+1,deviceptr);
    fprintf(stderr,"Version: %8d SerialNumber: %10d\n",version,sernum);
  }
  return Data_Wrap_Struct(cls_phidget,NULL,free_phidget,s);
}

VALUE phidget_cvalue(VALUE self,VALUE v_card)
{
  phidget_stc *s;
  Data_Get_Struct(self,phidget_stc,s);

  int card=FIX2INT(v_card);
  int i,rv;
  VALUE a=rb_ary_new();

  for(i=0;i<16;i++)
  {
    CPhidgetInterfaceKit_getInputState(s->ih[card],i,&rv);
    rb_ary_push(a,INT2FIX(rv));  
  }
  
  return a;
}

static void free_phidget(void *ptr)
{
  phidget_stc *s=(phidget_stc *)ptr;
  int i;
  
  for(i=0;i<2;i++)
  {
    CPhidget_close((CPhidgetHandle)s->ih[i]);
    CPhidget_delete((CPhidgetHandle)s->ih[i]);
  }
  
  free(s);
}
