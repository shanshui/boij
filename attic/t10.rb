require 'boij.rb'

MIN_DIST=2.0

x=11
y=7
thr=[0.2,0.5,0.7]

c=Contour::new(x,y)

pp c

a=[]
(x*y).times do
  a.push(rand())
end

rv=c.calc(a,thr)

thr.each_with_index do |th,i|
  loggo("Thr #{th} has these centers:\n#{Contour::find_centers(rv[i],MIN_DIST)}")
end


