# engine2gl.rb

=begin

14/07/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Talking to the sd84 board

$Id: sd84.rb 2666 2010-12-29 16:37:27Z karl $

=end

class IO
  MYWAIT=0.05
  def my_read_nonblock(howmany,loops,firstchar=nil)
    rr=[]
    lh=howmany
    loops.times do 
      begin
        rv=read_nonblock(lh)
        a=rv.unpack('C*')

        if(firstchar && rr.length()<=0)
          while(a.length()>0 && a[0]!=firstchar)
            loggo("Dropping #{sprintf("%2.2x",a[0])}!")
            a.shift()
          end
        end
        lh-=a.length()
        rr+=a
        break if(rr.length>=howmany)
      rescue Errno::EAGAIN => err
        loggo("AGAIN! (#{err})")
      end
      sleep(MYWAIT)
    end
    rr.length()==howmany ? rr : nil
  end
end

class Sd84
  SPEED=115200

  def initialize(dev)
    @dev=dev
    @unit=File::open(@dev,File::RDWR|File::NONBLOCK)
    @unit.sync=true
    pp system("stty -F #{@dev} ispeed #{SPEED} ospeed #{SPEED} -parenb cstopb raw")
  end

  def cmd(cmd,anslen)    
    @unit.syswrite(([0xaa,0xa0,0x55]+cmd).pack('C*'))
    stuff=@unit.read(anslen)
    stuff.unpack('C*')
  end
end
  
