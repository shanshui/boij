#!/usr/bin/env ruby
# sick_runner.rb

=begin

14/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The logic that runs the process with a Sick sensor

$Id: sick_runner.rb 3472 2012-03-21 15:04:22Z karl $

=end

require 'boij'
require 'engine_sick'
require 'playground_sick'
require 'sick/sick_config'

Fluid_logfile::new('runner')

scfg = Sick_config::load

run_bgsa = false

unless scfg.replay
  if (File.exist?(BASE+'.background-saver'))
    mtime = File.stat(BASE+'.background-saver').mtime
    now = Time::now
    diff = now.to_i - mtime.to_i
    if diff / 60 / 60 > 23 # run it every day at first boot
      run_bgsa = true
    end
  else
    run_bgsa = true
  end
end

if run_bgsa
  FileUtils.touch(BASE+'.background-saver')
  loggo("Background detect")
  require 'sick/background_saver'
  begin
    Backgsa::new(scfg, scfg.get_sick_for_init()).runme()
  end
  exit()
elsif(File.exist?(BASE+'.lookaround') || !File.exist?(SICKCONF))
  FileUtils.rm_f(BASE+'.lookaround')
  loggo("Lookaround request")
  require 'sick/lookaround'
  Lookaround::new().runme()
  exit()
elsif(File.exist?(BASE+'.faker'))
  FileUtils.rm_f(BASE+'.faker')
  loggo("Faker request")
  require 'playground_faker_movireceptor'
  daemons=[[Engine,nil],2,[Faker,nil]]
else
  daemons=[[Engine,nil],2,[Playground_sick,nil]]
end

$eps=[]
daemons.each do |d|
  if(d.is_a?(Numeric))
    sleep(d)
  else
    $eps.push(External_proc::new(d[0],d[1]))
  end
end

Signal::trap('INT') do
  exit(0)
end
Signal::trap('HUP') do
  exit(0)
end

def interr
  $eps.reverse.each do |ep|
    loggo("Expiring pid #{ep.pid} (#{ep.cls.name})")
    begin
      ep.expire()
    rescue
    end
  end
  loggo('Terminating.')
  exit(0)
end

at_exit do
  interr()
end

loop do
  sleep(3)
  $eps.each do |ep|
    v=ep.is_dead?()
    if(v)
      loggo("#{ep.cls.to_s}/#{ep.pid} died! Must die to be reborn.")
      exit(0)
    end
  end
end
