#!/usr/bin/env ruby
# sensorlines_visio.rb

=begin

30/12/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

A visual prop for studying sensorlines effects

$Id: sensorlines_visio.rb 3891 2013-02-07 13:30:25Z karl $

=end

require_relative 'playground_sensorlines_3'
require 'sdl'

class Sensorgrid_visio
  PERSQUARE=40
  PERSQUARE_HALF=PERSQUARE/2.0
  SLEEP_TIME=0.06
  GAP=30
  Y_COORD=[0,PERSQUARE+GAP]
  Y_COORD_LINE=[0,PERSQUARE]
  HEIGHT=PERSQUARE*2+GAP

  MAX_KICK=0.8

  TOP=1.0
  TOP_HALF=TOP/2.0
  COLVAL=255/TOP_HALF

  MAX_LINEINTENSITY=30.0
  LINEW=3

  def initialize
    @pg=Playground_sensorlines::getremote()
    @width=Playground_sensorlines::NSENSORS*PERSQUARE
    SDL::init(SDL::INIT_VIDEO)
    @screen=SDL::Screen::open(@width,2*PERSQUARE+GAP,32,SDL::HWSURFACE|SDL::DOUBLEBUF)
  end

  def runme
    loop do
      loop do
        event=SDL::Event2::poll()
        break if(!event)
        case event
        when SDL::Event2::Quit
          exit(0)
        when SDL::Event2::KeyDown
          case event.sym
          when SDL::Key::ESCAPE
            exit(0)
          end
        when SDL::Event2::MouseButtonDown
          next if(event.y>PERSQUARE && event.y<=PERSQUARE+GAP)
          eve(event.x/PERSQUARE,event.y>PERSQUARE)
        end
      end

      @screen.fillRect(0,0,@width,HEIGHT,[0,0,0])

      en=@pg.energies()
      en.each_with_index do |enn,x|
        paintsquare(x,0,enn)
      end if(en)
      la=@pg.lineaves()
      la.each_with_index do |v,i|
        paintline(i,*v) if(v)
      end if(la)
      @screen.updateRect(0,0,0,0)
      @screen.flip()
      sleep(SLEEP_TIME)
    end
  end

  def eve(x,first_or_sec)
#    @pg.kick(x,first_or_sec ? 1 : 0,rand()*MAX_KICK)
    @pg.kick(x)
  end

  def paintsquare(x,y,v)
    v=TOP if(v>TOP)
    @screen.fillRect(x*PERSQUARE,Y_COORD[y],PERSQUARE,PERSQUARE,
                     [v<TOP_HALF ? 0 : (v-TOP_HALF)*COLVAL,0,v>TOP_HALF ? 0 : (TOP_HALF-v)*COLVAL])
  end

  def paintline(which,pct,v)
    v=[MAX_LINEINTENSITY,v].min/MAX_LINEINTENSITY*255
    pct*=(@width-PERSQUARE)
    @screen.fillRect(PERSQUARE_HALF+pct-LINEW,Y_COORD_LINE[which],LINEW*2,PERSQUARE+GAP,[v,v,0])
  end
end

if($0==__FILE__)
  Sensorgrid_visio::new().runme()
end

