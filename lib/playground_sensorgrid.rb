#!/usr/bin/env ruby
# playground_sensorgrid.rb

=begin

30/12/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This playground version is constituted by a grid of on-off sensors

$Id: playground_sensorgrid.rb 2690 2011-01-14 07:38:03Z karl $

=end

require 'boij'

class Ps_node
  attr_reader(:x,:y,:prog,:conn_nodes)
  attr_accessor(:energy,:temp_energy)

  def initialize(x,y,prog)
    @x,@y,@prog=x,y,prog
    @conn_nodes=[]
    @energy=0.0
  end

  def add_node(node,attenuation)
    @conn_nodes.push([node,attenuation])
  end

  def kick(val)
    @energy+=val
  end

  def Ps_node::make_grid(nx,ny,att_ortho,att_diag)
    grid=[]
    prog=0
    ny.times do |y|
      row=[]
      nx.times do |x|
        row.push(Ps_node::new(x,y,prog))
        prog+=1
      end
      grid.push(row)
    end
    ny.times do |y|
      nx.times do |x|
        n=grid[y][x]
        n.add_node(grid[y][x-1],att_ortho) if(x>0)
        n.add_node(grid[y][x+1],att_ortho) if(x<nx-1)
        n.add_node(grid[y-1][x],att_ortho) if(y>0)
        n.add_node(grid[y+1][x],att_ortho) if(y<ny-1)
        n.add_node(grid[y-1][x-1],att_diag) if(x>0 && y>0)
        n.add_node(grid[y+1][x-1],att_diag) if(x>0 && y<ny-1)
        n.add_node(grid[y-1][x+1],att_diag) if(x<nx-1 && y>0)
        n.add_node(grid[y+1][x+1],att_diag) if(x<nx-1 && y<ny-1)
      end
    end
    grid
  end
end

class Playground_sensorgrid
  NX=8
  NY=4
  ORTHO_ATTEN=0.2
  DIAG_ATTEN=0.18
  ABATEMENT=0.8

  CYCLE_ATTEN=0.2

  LOOP_SLEEP=0.06
  
  def initialize
    @grid=Ps_node::make_grid(NX,NY,ORTHO_ATTEN,DIAG_ATTEN)
    @nodes=@grid.flatten()
    @mtx=Mutex::new()
    
    DRb.start_service(SG_URI,self)
  end

  def runme
    loop do
      spread_cycle()
      sleep(LOOP_SLEEP)
    end
  end

  def spread_cycle
    @mtx.synchronize do
      @nodes.each do |n|
        n.temp_energy=n.energy
      end
    end
    @nodes.each do |n|
      n.conn_nodes.each do |cn,att|
        cn.temp_energy+=n.energy*att
      end
    end
    tot_energy=0.0
    @nodes.each do |n|
      tot_energy+=n.temp_energy
    end
    if(tot_energy>0.0)
      fact=(1.0+((tot_energy-1.0)*ABATEMENT))/tot_energy
      @mtx.synchronize do
        @nodes.each do |n|
          n.energy=n.temp_energy*rand()*fact
        end
      end
    end
  end

  def kick(x,y,val)
    if(x>=0 && y>=0 && x<NX && y<NY)
      @mtx.synchronize do
        @grid[y][x].kick(val)
      end
    end
  end

  def cur_values
    nm=nil
    @mtx.synchronize do
      nm=@nodes.map do |n|
        [n.x,n.y,n.energy]
      end
    end
    nm
  end

  def cols_at_a_time(ncols,thresh)
    colvals=[]
    @mtx.synchronize do
      NX.times do |x|
        v=0.0
        NY.times do |y|
          v+=@grid[y][x].energy
        end
        colvals.push(v/NY)
      end
    end
            
    retv=[]
    (NX-ncols+1).times do |x|
      v=0.0
      ncols.times do |cx|        
        v+=colvals[x+cx]
      end
      v/=ncols
      retv.push([x,v,colvals[x,ncols]]) if(v>=thresh)
    end
    retv.sort do |a,b|
      b[1]<=>a[1]
    end
  end

  def Playground_sensorgrid::getremote
    DRb.start_service()
    DRbObject::new(nil,SG_URI)
  end
end

if($0==__FILE__)
#  Fluid_logfile::new('sensorgrid')  
  Playground_sensorgrid::new().runme()
end
