/* regrouper.c */

/*
 * 18/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * zwollean regrouping
 * yes now groups are called blobs...
 *
 * $Id: regrouper.c 2626 2010-11-10 14:48:16Z karl $
 */

#include <ruby.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include "boij.h"

#define OVERSAMPLE_FACTOR 1.5
#define MAX_ACCEPTABLE_DISTANCE 80.0
#define MSEC_WAIT_BEFORE_LEAVING 800

VALUE cls_regroup;

typedef struct
{
  unsigned char fact;
  struct grid_pos *pos;
} ele_accum_stc;

typedef struct grid_pos
{
  __u16 x,y;
  unsigned int loc;
  ele_accum_stc **eles;
  int n_eles;
  float weight,eval;
  blob_stc *blob;
} grid_pos_stc;
  
typedef struct
{
  shared_stc *ss;
  __u8 maxblobs,*frame;
  float *myframe,myfe_max;
  grid_pos_stc *gridnodes;
  blob_stc *lblobs;
} regr_stc;

static void free_regr(void *p);

static void get_grid(regr_stc *r);
static void addgrid(grid_pos_stc *ep,regr_stc *r,int xc,int yc,int xo,int yo,int w);
static void freegrid(regr_stc *r);
static void gather(regr_stc *r);
static blob_stc *available_blob(regr_stc *r);
static void assign_blob(regr_stc *v,grid_pos_stc *gp,blob_stc *grp);
static int time_diff(struct timeb *a,struct timeb *b);

VALUE new_regrouper(VALUE self,VALUE sha_p)
{
  regr_stc *r=ALLOC(regr_stc);

  Data_Get_Struct(sha_p,void,r->ss);
  r->frame=(void *)r->ss+sizeof(shared_stc);

  r->maxblobs=(int)(MAX_MONITORED*OVERSAMPLE_FACTOR);

  r->lblobs=malloc(sizeof(blob_stc)*r->maxblobs); 
  
  r->myframe=malloc(sizeof(float)*r->ss->framesize);

  get_grid(r);
  
  return Data_Wrap_Struct(cls_regroup,NULL,free_regr,r);
}

static void free_regr(void *p)
{
  regr_stc *r=(regr_stc *)p;

  free(r->myframe);
  free(r->lblobs);
  freegrid(r);

  free(p);
}

VALUE regr_regroup(VALUE self)
{
  regr_stc *r;
  Data_Get_Struct(self,regr_stc,r);
  int i,j;
  float fact=M_PI_2/(1.0-r->ss->threshs[0]),fv,ang;

  for(i=0;i<r->ss->framesize;i++)
  {
    fv=((float)r->frame[i]/255.0);
    if(fv<r->ss->threshs[0])
      r->myframe[i]=0.0;
    else
    {
      ang=(fv-r->ss->threshs[0])*fact-M_PI_2;
      r->myframe[i]=sin(ang)+1.0;
//      fprintf(stderr,"<%.2f %.2f %.2f>",fv,ang,r->myframe[i]);
    } 
  }
  
  gather(r);

/*
 * Now associate to old ones if possible
 */

  for(i=0;i<r->maxblobs;i++)
    r->lblobs[i].associated=NULL;
  for(i=0;i<MAX_MONITORED;i++)
    r->ss->clients[i].newblob=NULL;

  blob_stc *bf;
  client_stc *cf;
  float distance,min_distance;
  __s16 dx,dy;
  
  while(1)
  {
    bf=NULL;
    cf=NULL;
    min_distance=MAX_ACCEPTABLE_DISTANCE;
    
    for(i=0;i<r->maxblobs;i++)
      if(r->lblobs[i].occupied && r->lblobs[i].associated==NULL)
	for(j=0;j<MAX_MONITORED;j++)
	  if(r->ss->clients[j].present && r->ss->clients[j].newblob==NULL)
	  {
	    dx=r->lblobs[i].cx-r->ss->clients[j].blob.cx;
	    dy=r->lblobs[i].cy-r->ss->clients[j].blob.cy;
	    
	    distance=sqrtf(dx*dx+dy*dy);
	    if(distance<min_distance)
	    {
	      bf=&r->lblobs[i];
	      cf=&r->ss->clients[j];
	      min_distance=distance;
	    }	    
	  }

    if(bf==NULL)
      break;

    /*fprintf(stderr,"Getto at %f\n",min_distance);*/

    bf->associated=cf;
    cf->newblob=bf;    
  }

/*
 * The associations that were found
 */

  struct timeb tb;
  ftime(&tb);
  
  for(i=0;i<MAX_MONITORED;i++)
  {
    if(r->ss->clients[i].newblob!=NULL)
      r->ss->clients[i].blob=*r->ss->clients[i].newblob;
    else if(r->ss->clients[i].present)
    {
      if(r->ss->clients[i].left.time==0)
	r->ss->clients[i].left=tb;
      else if(time_diff(&tb,&r->ss->clients[i].left)>=MSEC_WAIT_BEFORE_LEAVING)
	r->ss->clients[i].present=0;
    }
  }

/*
 * Some new ones?
 */

  for(i=0;i<r->maxblobs;i++)
    if(r->lblobs[i].occupied && r->lblobs[i].associated==NULL)
    {
      for(j=0;j<MAX_MONITORED;j++)	
	if(!r->ss->clients[j].present)
	{
	  r->ss->clients[j].present=1;
	  r->ss->clients[j].new_entry=1;
	  ftime(&r->ss->clients[j].arrived);
	  r->ss->clients[j].left.time=0;
	  r->ss->clients[j].blob=r->lblobs[i];
	  break;
	}
    }

  r->ss->produced=1;

  return self;
}

/*
 * Stuff for finding existing pixel blobs.
 */

static void get_grid(regr_stc *r)
{
  int x,y;
  
  r->gridnodes=malloc(sizeof(grid_pos_stc)*r->ss->framesize);
  grid_pos_stc *app=r->gridnodes;
  
  for(y=0;y<r->ss->y;y++)
  {
    for(x=0;x<r->ss->x;x++,app++)
    {
      bzero(app,sizeof(grid_pos_stc));
      app->loc=app-r->gridnodes;
      app->x=x;
      app->y=y;
      
      addgrid(app,r,x,y,0,0,8);
      addgrid(app,r,x,y,-1,0,4);
      addgrid(app,r,x,y,1,0,4);
      addgrid(app,r,x,y,0,-1,4);
      addgrid(app,r,x,y,0,1,4);
      addgrid(app,r,x,y,-1,-1,3);
      addgrid(app,r,x,y,-1,1,3);
      addgrid(app,r,x,y,1,-1,3);
      addgrid(app,r,x,y,1,1,3);
      addgrid(app,r,x,y,-2,0,2);
      addgrid(app,r,x,y,2,0,2);
      addgrid(app,r,x,y,0,-2,2);
      addgrid(app,r,x,y,0,2,2);
      addgrid(app,r,x,y,-1,-2,1);
      addgrid(app,r,x,y,-2,-1,1);
      addgrid(app,r,x,y,1,-2,1);
      addgrid(app,r,x,y,2,-1,1);
      addgrid(app,r,x,y,1,2,1);
      addgrid(app,r,x,y,2,1,1);
      addgrid(app,r,x,y,-1,2,1);
      addgrid(app,r,x,y,-2,1,1);
    }
  }
}

static void addgrid(grid_pos_stc *ep,regr_stc *r,int xc,int yc,int xo,int yo,int w)
{
  int nx=xc+xo;
  int ny=yc+yo;
  
  if(nx>=r->ss->x || nx<0 || ny>=r->ss->y || ny<0)
    return;
  
  ep->eles=realloc(ep->eles,sizeof(ele_accum_stc *)*(ep->n_eles+1));
  ep->eles[ep->n_eles]=malloc(sizeof(ele_accum_stc));
  ep->eles[ep->n_eles]->pos=r->gridnodes+(ny*r->ss->x+nx);
  ep->eles[ep->n_eles]->fact=w;
  ep->n_eles++;

  ep->weight+=(float)w;
}

static void freegrid(regr_stc *v)
{
  int i,j;
  
  for(i=0;i<v->ss->framesize;i++)
  {
    for(j=0;j<v->gridnodes[i].n_eles;j++)
      free(v->gridnodes[i].eles[j]);
    free(v->gridnodes[i].eles);
  }
  free(v->gridnodes);
}

static void gather(regr_stc *r)
{
  int i,j;
  
  r->myfe_max=0.0;
  
  for(i=0;i<r->ss->framesize;i++)
  {
    r->gridnodes[i].blob=NULL;
    r->gridnodes[i].eval=0.0;
    
    for(j=0;j<r->gridnodes[i].n_eles;j++)
      r->gridnodes[i].eval+=r->gridnodes[i].eles[j]->fact*
	r->myframe[r->gridnodes[i].eles[j]->pos->loc];
    
    r->gridnodes[i].eval/=r->gridnodes[i].weight;
    if(r->gridnodes[i].eval<r->ss->threshs[1])
      r->gridnodes[i].eval=0;
    else
    {
      r->gridnodes[i].eval-=r->ss->threshs[1];
      if(r->myfe_max<r->gridnodes[i].eval)
	r->myfe_max=r->gridnodes[i].eval;
    }
  }

/*
 * Now the blobs
 */

  for(i=0;i<r->maxblobs;i++)
    r->lblobs[i].occupied=0;

  grid_pos_stc *cur_gp;
  blob_stc *found_blob;
  int maxwgt=0;
  
  for(i=0;i<r->ss->framesize;i++)
  {
    cur_gp=&(r->gridnodes[i]);
    
    if(cur_gp->blob!=NULL || cur_gp->eval<(float)r->ss->threshs[2])
      continue;

/*
 * First search for the heaviest blob that we can connect to
 */
    
    for(found_blob=NULL,j=0;j<cur_gp->n_eles;j++)
    {
      if(cur_gp->eles[j]->pos->blob!=NULL && cur_gp->eles[j]->pos->blob->weight>maxwgt)
      {
	found_blob=cur_gp->eles[j]->pos->blob;
	maxwgt=found_blob->weight;
      }
    }
    if(found_blob==NULL)
      found_blob=available_blob(r);
    if(found_blob==NULL)
      continue;
    assign_blob(r,cur_gp,found_blob);
  }

  for(i=0;i<r->maxblobs;i++)
    r->lblobs[i].fweight=0.0;
  
  for(i=0;i<r->ss->framesize;i++)
    if(r->gridnodes[i].blob)
      r->gridnodes[i].blob->fweight+=r->myframe[i];
  
  int nblobs=0;
  float cmpv=r->ss->threshs[4]*r->ss->framesize*0.05;
  
  for(i=0;i<r->maxblobs;i++)
    if(r->lblobs[i].occupied>0)
    {
/*      fprintf(stderr,"{%d: %.2f,%.2f,%.2f}",i,r->lblobs[i].fweight,r->ss->threshs[4],cmpv);*/
      
      if(r->lblobs[i].weight<cmpv)
	r->lblobs[i].occupied=0;
      else
      {
	r->lblobs[i].cx=r->lblobs[i].minx+((r->lblobs[i].maxx-r->lblobs[i].minx)>>1);
	r->lblobs[i].cy=r->lblobs[i].miny+((r->lblobs[i].maxy-r->lblobs[i].miny)>>1);
	nblobs++;
/*	fprintf(stderr,"[%2.2d] %d,%d (%d,%.2f)\n",nblobs,r->lblobs[i].cx,r->lblobs[i].cy,r->lblobs[i].weight,r->lblobs[i].fweight);*/
      }
    }

/*  fputc('\n',stderr);*/
}

static blob_stc *available_blob(regr_stc *r)
{
  int i;

  for(i=0;i<r->maxblobs;i++)
    if(r->lblobs[i].occupied<=0)
    {
      r->lblobs[i].occupied=1;
      r->lblobs[i].maxx=r->lblobs[i].maxy=0;
      r->lblobs[i].minx=r->ss->x;
      r->lblobs[i].miny=r->ss->y;
      r->lblobs[i].weight=0;
      return &r->lblobs[i];
    }

  return NULL;
}

static void assign_blob(regr_stc *r,grid_pos_stc *gp,blob_stc *blob)
{
  if(gp->blob)
  {
    if(gp->blob->weight<=1)
      gp->blob->occupied=0;
    else
      gp->blob->weight-=1;
  }
  
  gp->blob=blob;
  if(blob->minx>gp->x)
    blob->minx=gp->x;
  else if(blob->maxx<gp->x)
    blob->maxx=gp->x;
  if(blob->miny>gp->y)
    blob->miny=gp->y;
  else if(blob->maxy<gp->y)
    blob->maxy=gp->y;
  blob->weight++;
  
/*
 * Now see if I can assign blob to some other remote locations
 */

  int i;
  
  for(i=0;i<gp->n_eles;i++)
    if(gp->eles[i]->pos->blob==NULL && gp->eles[i]->pos->eval>=(float)r->ss->threshs[3])
      assign_blob(r,gp->eles[i]->pos,blob);
}

static int time_diff(struct timeb *a,struct timeb *b)
{
  return (a->time-b->time)*1000+a->millitm-b->millitm;
}
