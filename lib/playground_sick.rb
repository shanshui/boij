# playground_sick.rb

=begin

14/03/2012 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This playground version uses a Sick LMS-100 sensor

$Id: playground_sick.rb 3468 2012-03-20 13:56:50Z karl $

=end

require 'boij'
require 'movireceptor'
require 'sick/sick_config'

class Playground_sick
  def initialize
    @mrecept=Movireceptor::link_to(0)
    @scfg=Sick_config::load
    @sick=@scfg.get_sick()
  end

  def runme
    loop do
      cycle()
      sleep(@scfg.scan_interval)
    end
  end

  def cycle
    res=@sick.tracker(false)
    @mrecept.newstuff(res) if(res and @mrecept.respond_to?(:newstuff))
  end
end
