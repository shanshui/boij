=begin

20/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

Sick camera data

$Id$

=end

require 'sick/sick_replay'
require 'yaml'

class Sick_config
  attr_accessor(:addr,:ang_range,:ang_resol,:scan_interval,:bg_tolerance,
                :range,:min_weight,:max_dist,:max_disapp,:max_groups,
                :replay,:replay_n,:backg)

  RANGE = [0.0, 1.0, 0.0, 1.0]
  BG_TOLERANCE = 50
  MIN_WEIGHT = 4
  MAX_DIST = 0.07
  MAX_DISAPP = 300000 # usec
  MAX_GROUPS = 8 #32

  IP = '192.168.0.1'
  PORT = 2111
  ANG_RANGE = 180
  ANG_RESOL = 25
  SCAN_INTERVAL = 0.06

  def initialize()
    set_defaults()
  end

  def set_defaults()
    @ip = IP if @ip.nil?
    @port = PORT if @port.nil?
    @ang_range = ANG_RANGE if @ang_range.nil?
    @ang_resol = ANG_RESOL if @ang_resol.nil?
    @scan_interval = SCAN_INTERVAL if @scan_interval.nil?
    @bg_tolerance = BG_TOLERANCE if @bg_tolerance.nil?
    @range = RANGE[0..-1] if @range.nil?
    @min_weight = MIN_WEIGHT if @min_weight.nil?
    @max_dist = MAX_DIST if @max_dist.nil?
    @max_disapp = MAX_DISAPP if @max_disapp.nil?
    @max_groups = MAX_GROUPS if @max_groups.nil?
    @replay = false if @replay.nil?
    @replay_n = 1 if @replay_n.nil?
  end

  def Sick_config::load
    begin
      cfg = YAML::load(File::read(SICKCONF))
    rescue
      cfg = Sick_config.new
    end
    cfg.set_defaults()
    cfg
  end

  def save
    File::open(SICKCONF,'wb') do |f|
      f.write(YAML::dump(self))
    end
  end

  def replay_file
    SICKCAP.gsub(/#/, sprintf('%03d', @replay_n))
  end

  def get_sick(no_replay = false)
    if (@replay && !no_replay && File.exist?(replay_file()))
      Sick_replay::new(replay_file())
    else
      Sick::new(@ip,@port,@ang_range,@ang_resol,@backg,@range,@bg_tolerance,
                @min_weight,@max_groups,@max_dist,@max_disapp)
    end
  end

  def get_sick_for_init
    Sick::new(@ip,@port,@ang_range,@ang_resol,nil,nil,nil,nil,nil,nil,nil)
  end
end
