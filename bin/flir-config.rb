#!/usr/bin/env ruby
# flirconfig.rb

=begin

15/12/2008 c.e prelz AS FLUIDO fluido@fluido.as
project .:laspal:.

The controller for FLIR camera data

$Id: flirconfig.rb 2626 2010-11-10 14:48:16Z karl $

=end

require 'boij.so'
require 'moving_people'
require 'gtk2'

class Regrouper
  def Regrouper::tlabels
    @@thresh_labels
  end
end

class Quadrilat_dot
  attr_reader(:layoutline,:xoffs,:yoffs,:xm,:ym)

  def layoutline=(lo)
    @layoutline=lo
#    pp [@layout.width,@layout.size,@layout.iter.line.extents[1].to_a]
    @xoffs=Flirconfig::BORD
    @yoffs=Flirconfig::BORD
  end

  def shift(xd,yd)
    @xm+=xd
    @x=@xm/@qlat.areasize[0]
    @ym+=yd
    @y=@ym/@qlat.areasize[1]
  end

  def scale_dot
    @xm=@x*@qlat.areasize[0]
    @ym=@y*@qlat.areasize[1]
  end

  def mapped_dist_from(x,y)
    xd=@xm-x
    yd=@ym-y
    Math::sqrt(xd*xd+yd*yd)
  end
end

class Quadrilat
  attr_accessor(:areasize,:areasize2,:colour,:tcolours,:woffset)
  attr_reader(:objects)

  def scale
    @dots.each do |d|
      d.scale_dot()
    end
  end

  def objects=(ob)
    @objects=ob.map do |o|
      [o[0]*@areasize2[0],
       o[2]*@areasize2[1],
       (o[1]-o[0])*@areasize2[0],
       (o[3]-o[2])*@areasize2[1]]
    end
#    @objects.each_with_index do |o,i|
#      STDERR.puts("#{@progr}/#{i+1}: #{ob[i]} gives #{o}")
#    end
  end
end

class Mappedcontrol < Gtk::VBox
  LINEWIDTH=2

  attr_reader(:da)

  def initialize(id,fc)
    @id,@fc=id,fc

    super(false,2)

    @da=Gtk::DrawingArea.new()
    @da.set_size_request(*Flirconfig::SFRAME)
    @da.signal_connect('expose_event') do |w,e|
      @wi,@he=w.window.size()
      gc=Gdk::GC.new(w.window)

      gc.set_rgb_fg_color(@fc.backg_col)
      w.window.draw_rectangle(gc,true,0,0,@wi,@he)
      if(@fc.pixbufs)
        w.window.draw_pixbuf(gc,@fc.pixbufs[@id+1],0,0,0,0,*Flirconfig::SFRAME,Gdk::RGB::DITHER_NORMAL,0,0)

        gc.set_rgb_fg_color(@fc.ocolour)
        gc.set_line_attributes(LINEWIDTH,Gdk::GC::LINE_SOLID,Gdk::GC::CAP_NOT_LAST,Gdk::GC::JOIN_ROUND)
        @fc.qlats[@id].objects.each do |ob|
          w.window.draw_rectangle(gc,false,*ob)
        end
      end
    end
    pack_start(@da)
    @curtvalues=@fc.mp.areas[@id].threshpara
    @fc.tlabels.each_with_index do |tl,i|
      fr=Gtk::Frame::new(tl)
      adj=Gtk::Adjustment::new(@curtvalues[i],0,100,1,10,0)
      adj.signal_connect('value-changed') do |a|
        @curtvalues[i]=a.value.to_i()
        @fc.mp.update_threshparas(@id,@curtvalues)
      end
      fr.add(Gtk::SpinButton::new(adj,1,0))
      pack_start(fr)
    end
  end
end

class Flircontrol < Gtk::Frame
  N_TRIES=5

  def initialize(name,cmd,fc)
    @name,@cmd,@fc=name,cmd,fc
    super(name)
    @box=Gtk::HBox::new(false,2)
    add(@box)
  end

  def cur_val
    ans=nil
    N_TRIES.times do |i|
      begin
        ans=@fc.mp.flircmd(@cmd,nil)
      rescue => err
        raise err if(i==N_TRIES-1)
        loggo("#{i+1} Getting (#{err}) (trying to get curval)")
        sleep(0.1)
      end
      break if(ans)
    end
    set_val_from_flir(ans[1]) if(ans)
  end

  def set_val
    succ=nil
    N_TRIES.times do |i|
      begin
        @fc.mp.flircmd(@cmd,get_var_for_flir())
        succ=true
      rescue => err
        raise err if(i==N_TRIES-1)
        loggo("#{i+1} Getting (#{err}) (trying to do setval)")
        sleep(0.1)
      end
      break if(succ)
    end
  end
end

class Flircontrol_int < Flircontrol
  def initialize(name,cmd,fc,min,max)
    super(name,cmd,fc)

    @min,@max=min,max

    @adj=Gtk::Adjustment::new(@min,@min,@max,1,10,0)
    cur_val()

    @adj.signal_connect('value-changed') do |a|
      save_val(a.value.to_i())
    end
    @box.pack_start(Gtk::SpinButton::new(@adj,1,0))
  end

  def save_val(val)
    set_val
#    pp [@name,val]
  end

  def set_val_from_flir(arr)
    @adj.value=arr[0]*256+arr[1]
  end

  def get_var_for_flir
    v=@adj.value().to_i()
    [v>>8,v&0xff]
  end
end

class Flircontrol_radiobox < Flircontrol
  COLS=4

  def initialize(name,cmd,fc,values)
    super(name,cmd,fc)

    rows=(values.length-1)/COLS+1

    @values=values

    @tbl=Gtk::Table::new(rows,COLS,true)
    @box.pack_start(@tbl)

    @btns=[]
    bb=nil
    @values.each_with_index do |v,i|
      row=i/COLS
      col=i%COLS
      if(bb)
        nb=Gtk::RadioButton::new(bb,v[1])
      else
        nb=Gtk::RadioButton::new(v[1])
      end
      @btns.push(nb)
      bb=nb
      @tbl.attach(nb,col,col+1,row,row+1)
    end
    cur_val()

    @btns.each do |b|
      b.signal_connect("clicked") do
        set_val() if(b.active?())
      end
    end
  end

  def set_val_from_flir(arr)
    @values.each_with_index() do |v,i|
      if(arr==v[0])
        pp v
        @btns[i].active=true
      end
    end
  end

  def get_var_for_flir
    @btns.each_with_index do |b,i|
      return @values[i][0] if(b.active?())
    end
    raise 'BAD RADIOBUTTON!!!!!'
  end
end

class Flirconfig
  attr_reader(:mp,:qlats,:backg_col,:ocolour,:pixbufs,:tlabels)

  BORD=20
  DFRAME=[500,375]
  PFRAME=[DFRAME[0]+BORD*2,DFRAME[1]+BORD*2]
  SFRAME=[PFRAME[0]>>1,PFRAME[1]>>1]
  LOOPTIMEOUT=100
  LINEWIDTH_SMALL=1
  LINEWIDTH_BIG=4
  THRESHPARA=[34,16,21,4,3]

  def initialize
    DRb.start_service()
    @mp=DRbObject::new(nil,MP_URI)

    @backg_col=Gdk::Color::new(0,0,80*256)
    @pixbufs=nil

    @qlats=[Quadrilat::new(0)]
    @qlats[0].update_dots(@mp.areas[0].qlat)
    @qlats[0].areasize=DFRAME
    @qlats[0].areasize2=SFRAME
    @qlats[0].scale()
    @qlats[0].tcolours=['yellow','blue']
    @qlats[0].woffset=[PFRAME[0],0]
    @selpoint=nil

    @tlabels=Regrouper::tlabels()

    rr=@mp.roi_rect()
    @roirect=[rr[0]*DFRAME[0],rr[1]*DFRAME[1],(rr[2]-rr[0])*DFRAME[0],(rr[3]-rr[1])*DFRAME[1]] if(rr)
    pp ['ZLZL',rr,@roirect]
  end

  def runme
    Gtk::init()

    @ocolour=Gdk::Color::new(255*256,255*256,0)
    @roicolour=Gdk::Color::new(200*256,200*256,200*256)

    @qlats[0].colour=Gdk::Color::new(130*256,0,40*256)

    fd=Pango::FontDescription.new("Sans 14")
    context=Gdk::Pango.context()
    context.language=Pango::Language.new("en_UK")
    context.base_dir=Pango::Context::DIRECTION_LTR
    font=context.load_font(fd)
    font_map=context.font_map

    @qlats.each do |ql|
      ql.dots.each do |d|
        lo=Pango::Layout.new(context)
        lo.set_markup("<span background=\"#{ql.tcolours[0]}\" foreground=\"#{ql.tcolours[1]}\">#{d.label}</span>")
        lo.font_description=fd
        lo.auto_dir=true
        lo.alignment=Pango::Layout::ALIGN_LEFT
        d.layoutline=lo.iter.line
      end
    end

    dwindow=Gtk::Window.new('Flirconfig')
    dwindow.signal_connect('destroy') do
      Gtk::main_quit()
    end

    dwindow.signal_connect('delete-event') do
      false
    end

    dmain_hbox=Gtk::HBox.new(false,1)
    dwindow.add(dmain_hbox)

    @mapc1=Mappedcontrol::new(0,self)
    dmain_hbox.pack_start(@mapc1)

    vbox=Gtk::VBox.new(false,1)
    dmain_hbox.pack_start(vbox)

    eb=Gtk::EventBox::new()
    eb.signal_connect('motion_notify_event') do |w,e|
      if(@selpoint)
        @selpoint[0][0].shift(e.x-@selpoint[1],e.y-@selpoint[2])
        @selpoint[1]=e.x
        @selpoint[2]=e.y
      end
    end

    eb.signal_connect('button_press_event') do |w,e|
      if(e.button==1)
        sp=(@qlats[0].dots).map do |dot|
          [dot,dot.mapped_dist_from(e.x,e.y)]
        end.sort do |a,b|
          a[1]<=>b[1]
        end[0]
        @selpoint=[sp,e.x,e.y]
      end
    end

    eb.signal_connect('button_release_event') do |w,e|
      if(e.button==1 && @selpoint)
        @mp.sync_qlat(@selpoint[0][0].qlat)
        @selpoint=nil
      end
    end

    vbox.pack_start(eb,false,false)

    @da=Gtk::DrawingArea.new()
    @da.set_size_request(*PFRAME)
    @da.signal_connect('expose_event') do |w,e|
      @wi,@he=w.window.size()
      gc=Gdk::GC.new(w.window)

      gc.set_rgb_fg_color(@backg_col)
      w.window.draw_rectangle(gc,true,0,0,@wi,@he)


      if(@pixbufs)
        w.window.draw_pixbuf(gc,@pixbufs[0],0,0,BORD,BORD,*DFRAME,Gdk::RGB::DITHER_NORMAL,0,0)

        gc.set_rgb_fg_color(@roicolour)
        gc.set_line_attributes(LINEWIDTH_SMALL,Gdk::GC::LINE_DOUBLE_DASH,Gdk::GC::CAP_NOT_LAST,Gdk::GC::JOIN_ROUND)
        w.window.draw_rectangle(gc,false,BORD+@roirect[0],BORD+@roirect[1],@roirect[2],@roirect[3]) if(@roirect)

        gc.set_line_attributes(LINEWIDTH_BIG,Gdk::GC::LINE_SOLID,Gdk::GC::CAP_NOT_LAST,Gdk::GC::JOIN_ROUND)

        @qlats.each do |ql|
          gc.set_rgb_fg_color(ql.colour)
          w.window.draw_line(gc,ql.ul.xm+BORD,ql.ul.ym+BORD,ql.ur.xm+BORD,ql.ur.ym+BORD)
          w.window.draw_line(gc,ql.ur.xm+BORD,ql.ur.ym+BORD,ql.lr.xm+BORD,ql.lr.ym+BORD)
          w.window.draw_line(gc,ql.lr.xm+BORD,ql.lr.ym+BORD,ql.ll.xm+BORD,ql.ll.ym+BORD)
          w.window.draw_line(gc,ql.ll.xm+BORD,ql.ll.ym+BORD,ql.ul.xm+BORD,ql.ul.ym+BORD)
        end
        @qlats.each do |ql|
          ql.dots.each do |d|
            w.window.draw_layout_line(gc,d.xm+d.xoffs,d.ym+d.yoffs,d.layoutline)
          end
        end
      end
    end
    eb.add(@da)

    vbox.pack_start(Flircontrol_radiobox::new('AGC type',0x13,self,
                                              [[[0,0],'Plateau hist'],
                                               [[0,1],'Once bright'],
                                               [[0,2],'Auto bright'],
                                               [[0,3],'Manual'],
                                               #                                                     [[0,4],'Linear'],
                                               [[0,5],'Log'],
                                              ]))
    vbox.pack_start(Flircontrol_int::new('Contrast',0x14,self,0,0xff))
    vbox.pack_start(Flircontrol_int::new('Brightness',0x15,self,0,0x3fff))
    vbox.pack_start(Flircontrol_int::new('AGC ITT filter',0x3e,self,0,0xff))
    vbox.pack_start(Flircontrol_int::new('Plateau',0x3f,self,0,1000))
    vbox.pack_start(Flircontrol_int::new('ITT Midpoint',0x55,self,0,0xff))
    vbox.pack_start(Flircontrol_int::new('Max AGC gain',0x6a,self,0,2048))

#    @mapc2=Mappedcontrol::new(1,self)
#    dmain_hbox.pack_start(@mapc2)

    dwindow.show_all()

    @latest_cnt=-1
    Gtk::timeout_add(LOOPTIMEOUT) do
      loop()
      true
    end

    Gtk::main()
  end

  def loop
    arr=@mp.latest_stuff(@latest_cnt)
    if(arr)
      @latest_cnt=arr[0]
      @pixbufs=[]
      px=Videocapt::triplicate(arr[1][0])
      pb=Gdk::Pixbuf::new(px,Gdk::Pixbuf::COLORSPACE_RGB,
                          false,8,*CAPT.clipped_frame,CAPT.clipped_frame[0]*3)
      @pixbufs[0]=pb.scale(*DFRAME,Gdk::Pixbuf::INTERP_HYPER)
      1.upto(1) do |i|
        px=Videocapt::triplicate(arr[1][i])
        pb=Gdk::Pixbuf::new(px,Gdk::Pixbuf::COLORSPACE_RGB,
                            false,8,*CAPT.clipped_frame,CAPT.clipped_frame[0]*3)
        @pixbufs[i]=pb.scale(*SFRAME,Gdk::Pixbuf::INTERP_HYPER)
      end

      @qlats.each_with_index do |ql,i|
        ql.objects=arr[2][i]
      end

      [@da,@mapc1.da].each do |da|
        da.queue_draw() if(da)
      end
    end
  end
end

Flirconfig::new().runme()
