#!/usr/bin/env ruby
# engine_sick.rb

=begin

14/03/2012 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

The engine that runs it all. Second version with resized images
Version using GL
Version getting presence from more modern thermocams
Or from other sources
Version using fast raalte column mgmt, specifically set for Sick sensors

$Id: engine_sick.rb 4105 2014-01-08 13:51:03Z karl $

=end

require 'boij'
require 'boij.so'
require 'hours_config'
require 'player_config'
require 'sick/sick_config'
require 'image_dbase'
require 'movireceptor'

require 'sdl'
require 'opengl'

class Engine
  attr_reader(:rc)

  DBG_FLG=(ENV['BOIJ_DBG']!=nil)
  FULLSCREEN_FLAG=!DBG_FLG

  AUDIO_FLAG=(ENV['BOIJ_NOAUDIO']==nil)
  SOUNDFILE_ATTN=0.4 #0.8 #0.5

  LOOPSLEEP=0.01

  attr_reader(:audiotoy,:sounds,:b_i,:curloudspeaker,:pictures,:imarr_mutex,:orphaned_images)

  def initialize
    FileUtils.cd('/tmp',:verbose => true)
    Process.setrlimit(:CORE, Process.getrlimit(:CORE)[1]) # to allow core dump

    ENV['__GL_SYNC_TO_VBLANK']='1'
    ENV['__GL_FSAA_MODE']='12'
    ENV['__GL_LOG_MAX_ANISO']='4'

    @hcfg = Hours_config::load
    @pcfg = Player_config::load
    @scfg = Sick_config::load

    if @hcfg.enabled
      if @hcfg.today
        now = `date '+%s' -d 'now'`.to_i
        shutdown = `date '+%s' -d '#{@hcfg.shutdown}'`.to_i
        wakeup = `date '+%s' -d '#{@hcfg.wakeup}'`.to_i

        if now < shutdown and shutdown < wakeup
        then
          loggo("Scheduling shutdown at #{@hcfg.shutdown} & wakeup at #{@hcfg.wakeup}")
          loggo(`sudo rtcwake -m no -t #{wakeup}`)
          loggo(`sudo shutdown -h #{@hcfg.shutdown}`)
        else
          loggo("Invalid configuration for shutdown at #{@hcfg.shutdown} & wakeup at #{@hcfg.wakeup}")
        end
      else
        loggo("No shutdown/wakeup hours configuration for today")
      end
    end

    @rc=Raalte_columns::new(*WINDOW_SIZE,CUT_TOP,CUT_BOTTOM,(HORIZON_POS*WINDOW_SIZE[1]).to_i)

    @idb=Image_dbase::new()
    @imarr_mutex=Mutex::new()
    reload_folder_tree()

    @mrecep=Movireceptor::fire_up(0,method(:new_positions))

    @sounds=[]
    if(AUDIO_FLAG)
      @audiotoy=Audiotoy::new()
      @sounds=[]
      Dir::entries(OPEN_SOUNDDIR).each do |f|
	next if(f[-4,4]!='.wav')
	@sounds.push(@audiotoy.load_file(OPEN_SOUNDDIR+f,SOUNDFILE_ATTN))
      end
    end

    @players=[]
    @scfg.max_groups.times do |i|
      @players.push(Player::new(self,i))
    end

    @old_positions=nil
    @updatepos_flag=false
    @latest_pixels=nil

    @done=false
    Signal::trap('INT') do
      @done=true
    end
    Signal::trap('TERM') do
      @done=true
    end
    @curloudspeaker=0
#    Signal::trap('USR1') do
#      @curloudspeaker+=1
#      @curloudspeaker%=7
#      @audiotoy.fix_loudspeaker(@curloudspeaker==0 ? nil : @curloudspeaker-1)
#    end
    Signal::trap('USR1') do
      loggo("Request to reload")
      reload_folder_tree()
      true
    end
    Signal::trap('USR2') do
      take_snap()
      true
    end
  end

  def new_positions(pos)
    @new_positions=pos
  end

  def reload_folder_tree
    rootfolders,folders,images=Wfolder::load(@idb)
    np=[]
    rootfolders.each do |k,rf|
      recurse_add_image_from_folder(np,rf)
    end
    @imarr_mutex.synchronize do
      @pictures=np
    end
    loggo("Found #{@pictures.length} pictures")

    @pictures.sort!() do |a,b|
      a.prog<=>b.prog
    end

    build_new_picture_array()
  end

  def build_new_picture_array
    @pictures_to_send=@pictures.sort() do |a,b|
      rand()<=>0.5
    end
  end

  def recurse_add_image_from_folder(arr,folder)
    return if(!folder.used)
    folder.local_images.each do |im|
      arr.push(Picture::new(im.id,true))
    end
    folder.sons.each do |k,son|
      recurse_add_image_from_folder(arr,son)
    end
  end

  def runme
    loggo("At initvideo")
    SDL::init(SDL::INIT_VIDEO)
    if(FULLSCREEN_FLAG)
#      @screen=SDL::Screen::open(*WINDOW_SIZE,32,SDL::OPENGL|SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME|SDL::FULLSCREEN)
      @screen=SDL::Screen::open(*DUMP_WINDOW_SIZE,32,SDL::OPENGL|SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME)
      SDL::Mouse.warp(1,1)
      SDL::Mouse.hide
    else
      @screen=SDL::Screen::open(*DUMP_WINDOW_SIZE,32,SDL::OPENGL|SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME)
    end
    loggo("At generics")

    #
    # Generics
    #

    GL::DepthFunc(GL::LESS)
    GL::ShadeModel(GL::SMOOTH)
    GL::PixelStorei(GL::UNPACK_ALIGNMENT,1)
    GL::ClearColor(0.0,0.0,0.0,0.0)
    GL::ClearDepth(1.0)

    GL::MatrixMode(GL::PROJECTION)
    GL::LoadIdentity()

    GL::MatrixMode(GL::MODELVIEW)
    GL::LoadIdentity()

    nxt_refresh=Time::now()
    loggo("Befo mainloop")
    until(@done)
      event=SDL::Event2::poll()
      if(event)
	case event
        when SDL::Event2::Quit
          @done=true
        when SDL::Event2::KeyDown
          #shft = event.mod & SDL::Key::MOD_SHIFT != 0
          ctrl = event.mod & SDL::Key::MOD_CTRL != 0
	  case event.sym
          when SDL::Key::ESCAPE
	    @done=true
          when SDL::Key::RETURN
            FileUtils.touch(BASE+'.lookaround')
            @done=true
          when SDL::Key::R
            if ctrl
              if @rec
                stop_rec()
              else
                start_rec()
              end
            end
          when SDL::Key::S
            if ctrl
	      take_snap()
            end
	  end
        when SDL::Event2::Active
          @refresh_flg=true
#        else
#          loggo("Event #{event}")
	end
      end

      if(@new_positions)
        update_positions(@new_positions)
        @new_positions=nil
      end

      @players.each do |p|
        if(p.arrival)
          p.start_new_image_if_needed_sick()
        end
      end

      @latest_pixels=@rc.curframe()
      refresh_screen()

      sleep(LOOPSLEEP)
    end

    stop_rec()
    @screen.destroy()
    Movireceptor::stop()
    @audiotoy=nil
  end

  def update_positions(positions)
    #    loggo("New positions #{positions}")
    @players.each_with_index do |p,i|
      if(!p.arrival && positions[i])
        loggo("Player #{p.prog} arrived.")
        p.image=nil
        p.arrival=Time::now()
        p.position=positions[i]
      elsif(p.arrival && !positions[i])
        loggo("Player #{p.prog} left.")
        p.give_owned_pictures_and_leave()
      elsif(p.arrival && positions[i])
        p.position=positions[i]
      end
    end
  end

  def refresh_screen
    return if(!@latest_pixels)

    GL::DrawPixels(*DUMP_SIZE,GL::RGB,GL::UNSIGNED_BYTE,@latest_pixels)
    SDL::GL::swap_buffers()
  end

  def start_rec
    return if @pcfg.record_cmd.nil?
    rec_file = RECDIR+'rec-'+Time::now().stamp()+'.mkv'
    loggo("Starting screen capture to #{rec_file}")
    @rec = Process.spawn("#{@pcfg.record_cmd} #{rec_file}")
    Process.detach(@rec)
  end

  def stop_rec
    if @rec
      loggo("Stopping screen capture")
      Process.kill(:SIGTERM, @rec)
      @rec = nil
    end
  end

  def take_snap
    return if(!@latest_pixels)

    snap_file=SNAPDIR+'snap-'+Time::now().stamp()+'.ppm'
    loggo("Saving screen snapshot to #{snap_file}")
    File::open(snap_file,'wb') do |f|
      f.printf("P6\n%d %d\n255\n",*WINDOW_SIZE)
      f.print(Imager::pixel_flip(@latest_pixels))
    end
  end

  def firstpowerof2(v)
    i=2
    loop do
      return i if(i>v)
      i*=2
    end
  end

  def next_picture
    build_new_picture_array() if(@pictures_to_send.length<=0)
    @pictures_to_send.shift()
  end
end

if($0==__FILE__)
#  Fluid_logfile::new('engine')
  Engine::new().runme()
end
