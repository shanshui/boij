/* boij_columns.c */

/*
 * 7/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * In boijmans, handling column appartition and image composition
 *
 * $Id: boij_columns.c 1494 2008-02-12 15:02:20Z karl $
 */

#include <ruby.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>

#include "boij.h"

typedef struct
{
  int height,width,offset_from_bottom;
  __u8 **columns;
} pict_stc;

typedef struct
{
  int prog,n_smooth_components;
  __u8 **map_pointers,*pixels;
  __u16 smooth_components[MAX_SMOOTH_COMPONENTS];
} col_stc;

typedef struct
{
  col_stc *cols;
  pict_stc *picts;
  int n_picts,sc_w,sc_h;
  __u8 *screen;
} boij_columns_stc;

static void free_boij_columns(void *p);
static void apply_smooth(boij_columns_stc *bc,col_stc *c,int i,int pic_prog,int col_to_use,float smfactor);

VALUE cls_columns;

static float smooths[N_SMOOTHS];

void init_boij_columns()
{
  int i;

  if(N_SMOOTHS>=2)
    for(i=0;i<N_SMOOTHS;i++)
      smooths[i]=(cosf(M_PI/(N_SMOOTHS-1)*i-M_PI)+1.0)/2.0;
}

VALUE new_boij_columns(VALUE self,VALUE screen,VALUE pictures)
{
  boij_columns_stc *bs=ALLOC(boij_columns_stc);
  int i,j;
  VALUE v1,v2;

  bs->sc_w=FIX2INT(rb_ary_entry(screen,0));
  bs->sc_h=FIX2INT(rb_ary_entry(screen,1));
  bs->screen=malloc(bs->sc_w*bs->sc_h*3);

  bs->cols=malloc(sizeof(col_stc)*bs->sc_w);
  for(i=0;i<bs->sc_w;i++)
  {
    bs->cols[i].map_pointers=malloc(sizeof(__u8 *)*bs->sc_h);
    for(j=0;j<bs->sc_h;j++)
      bs->cols[i].map_pointers[j]=bs->screen+(j*bs->sc_w+i)*3;
    bs->cols[i].pixels=malloc(bs->sc_h*3);
    bzero(bs->cols[i].pixels,bs->sc_h*3);
  }

  bs->n_picts=RARRAY_LEN(pictures);
  bs->picts=malloc(sizeof(pict_stc)*bs->n_picts);

  for(i=0;i<bs->n_picts;i++)
  {
    v1=rb_ary_entry(pictures,i);
    bs->picts[i].width=FIX2INT(rb_ary_entry(v1,0));
    bs->picts[i].height=FIX2INT(rb_ary_entry(v1,1));
    bs->picts[i].offset_from_bottom=FIX2INT(rb_ary_entry(v1,2));
    bs->picts[i].columns=malloc(sizeof(__u8 *)*bs->picts[i].width);

    v2=rb_ary_entry(v1,3);

    for(j=0;j<bs->picts[i].width;j++)
    {
      bs->picts[i].columns[j]=malloc(bs->picts[i].height*3);
      memcpy(bs->picts[i].columns[j],RSTRING_PTR(rb_ary_entry(v2,j)),bs->picts[i].height*3);
    }
  }
  
  return Data_Wrap_Struct(cls_columns,NULL,free_boij_columns,bs);
}

static void free_boij_columns(void *p)
{
  boij_columns_stc *bs=(boij_columns_stc *)p;
  int i,j;
  
  for(i=0;i<bs->n_picts;i++)
  {
    for(j=0;j<bs->picts[i].width;j++)
      free(bs->picts[i].columns[j]);
    free(bs->picts[i].columns);
  }
  free(bs->picts);

  for(i=0;i<bs->sc_w;i++)
  {
    free(bs->cols[i].map_pointers);
    free(bs->cols[i].pixels);
  }
  
  free(bs->cols);
  
  free(bs->screen);
}
  
VALUE compuscreen(VALUE self,VALUE images)
{
  boij_columns_stc *bs;
  Data_Get_Struct(self,boij_columns_stc,bs);
  int n_images=RARRAY_LEN(images),i,j,start_pos,cur_width,pic_prog,col_to_use,left_or_right,
    begin,size_to_use;
  VALUE v;
  pict_stc *ps;

  for(i=0;i<bs->sc_w;i++)
  {
    bs->cols[i].prog=-1;
    bs->cols[i].n_smooth_components=0;
  }

  for(i=0;i<n_images;i++)
  {
    v=rb_ary_entry(images,i);
    start_pos=FIX2INT(rb_ary_entry(v,1));
    cur_width=FIX2INT(rb_ary_entry(v,2));
    pic_prog=FIX2INT(rb_ary_entry(v,3));
    left_or_right=FIX2INT(rb_ary_entry(v,4));
    begin=FIX2INT(rb_ary_entry(v,5));
    size_to_use=FIX2INT(rb_ary_entry(v,6));
    
    ps=&bs->picts[pic_prog];

    for(j=0;j<cur_width;j++)
    {
//      if((cur_width-j)>(bs->sc_w>>1))
//	continue;
      if(start_pos-j<0 && start_pos+j>=bs->sc_w)
	break;
      
      col_to_use=begin+(((j/size_to_use+left_or_right)%2) ? size_to_use-(j%size_to_use)-1 : j%size_to_use);
			
      if(start_pos-j>=0)
      {
	col_stc *c=&bs->cols[start_pos-j];
	
	if(cur_width-j<=N_SMOOTHS)
	  apply_smooth(bs,c,i,pic_prog,col_to_use,smooths[cur_width-j-1]);
	else
	{	  
	  ps=&bs->picts[pic_prog];
	  bzero(c->pixels,bs->sc_h*3);
	  memcpy(c->pixels+ps->offset_from_bottom*3,ps->columns[col_to_use],ps->height*3);
	  c->n_smooth_components=0;
	  c->prog=i+1;
	}
      }
      if(start_pos+j<bs->sc_w)
      {
	col_stc *c=&bs->cols[start_pos+j];
	
	if(cur_width-j<=N_SMOOTHS)
	  apply_smooth(bs,c,i,pic_prog,col_to_use,smooths[cur_width-j-1]);
	else
	{
	  ps=&bs->picts[pic_prog];
	  bzero(c->pixels,bs->sc_h*3);
	  memcpy(c->pixels+ps->offset_from_bottom*3,ps->columns[col_to_use],ps->height*3);
	  c->n_smooth_components=0;
	  c->prog=i+1;
	}
      }
    }
  }

  __u8 *flags=malloc(n_images),*ptr;
  bzero(flags,n_images);

  for(i=0;i<bs->sc_w;i++)
  {
    col_stc *c=&bs->cols[i];
    if(c->prog<0)
      continue;
    
    for(ptr=c->pixels,j=0;j<bs->sc_h;j++,ptr+=3)
      memcpy(c->map_pointers[j],ptr,3);

    if(c->n_smooth_components>0) // a smoothed col
    {
      for(j=0;j<c->n_smooth_components;j++)
	flags[c->smooth_components[j]]=1;
    }
    else if(c->prog>0)
      flags[c->prog-1]=1;
  }

  VALUE to_ret=rb_ary_new();

  rb_ary_push(to_ret,rb_str_new((char *)bs->screen,bs->sc_w*bs->sc_h*3));

  VALUE disapp_arr=rb_ary_new();

  for(i=0;i<n_images;i++)
    if(!flags[i])
      rb_ary_push(disapp_arr,rb_ary_entry(rb_ary_entry(images,i),0));

  rb_ary_push(to_ret,disapp_arr);

  free(flags);
	
  return to_ret;
}

static void apply_smooth(boij_columns_stc *bs,col_stc *c,int img,int pic_prog,int col_to_use,float smfactor)
{
//  if(c->n_smooth_components>=MAX_SMOOTH_COMPONENTS)
//    return;
  
  if(c->n_smooth_components==0)
  {
    if(c->prog>0)
    {
      c->smooth_components[c->n_smooth_components]=c->prog-1;
      c->n_smooth_components=1;
    }
    else
      c->n_smooth_components=0;
    c->prog=0;
  }

  if(c->n_smooth_components>=MAX_SMOOTH_COMPONENTS)
  {
    memmove(c->smooth_components,c->smooth_components+1,sizeof(__u16)*(MAX_SMOOTH_COMPONENTS-1));
    c->smooth_components[c->n_smooth_components-1]=img;
  }
  else
    c->smooth_components[c->n_smooth_components++]=img;

  pict_stc *ps=&bs->picts[pic_prog];
  __u8 *ptr,*ptr2;
  int i;
  float oneminus_smfactor=1.0-smfactor;
  
  for(ptr=c->pixels,i=0;i<bs->sc_h*3;i++,ptr++)
    (*ptr)*=oneminus_smfactor;
  
  for(ptr=c->pixels+ps->offset_from_bottom*3,ptr2=ps->columns[col_to_use],i=0;
      i<ps->height*3;
      i++,ptr++,ptr2++)
    (*ptr)+=(*ptr2)*smfactor;
}

