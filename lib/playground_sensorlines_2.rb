#!/usr/bin/env ruby
# playground_sensorlines_2.rb

=begin

18/01/2011 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This playground version is constituted by two lines of N sensors
Second version, based on Geert's idea of one image per sensor position
following a fixed curve

Second line kicks into second curve pattern

$Id: playground_sensorlines_2.rb 2714 2011-02-01 07:11:21Z karl $

=end

require 'boij'
require 'movireceptor'

class Sensor
  ATT_FACTOR=0.03

  MIN_LIFETIME=2.0
  MAX_LIFETIME=10.0
  DIFF_LIFETIME=MAX_LIFETIME-MIN_LIFETIME

  MIN_CASTVALUES=4
  MAX_CASTVALUES=30
  DIFF_CASTVALUES=MAX_CASTVALUES-MIN_CASTVALUES
  MIN_CASTRANGE=0.0
  MAX_CASTRANGE=1.0
  DIFF_CASTRANGE=MAX_CASTRANGE-MIN_CASTRANGE
  MIN_CASTRANGE_LAST=0.37
  MAX_CASTRANGE_LAST=0.39
  DIFF_CASTRANGE_LAST=MAX_CASTRANGE_LAST-MIN_CASTRANGE_LAST

  attr_reader(:line,:pos,:birth,:energy,:hor_pos,:cast)

  def initialize(line,pos,hor_min,hor_max)
    @line,@pos,@hor_min,@hor_max=line,pos,hor_min,hor_max
    @hor_diff=@hor_max-@hor_min
    @energy=0.5
    @birth=nil
  end

  def kick
    if(!@birth)
      @birth=Time::now()
      @hor_pos=@hor_min+rand()*@hor_diff
      @lifetime=MIN_LIFETIME+rand()*DIFF_LIFETIME
      new_cast()
    end
  end

  def curpos(time)
    return if(!@birth)

    lifetime=time-@birth
    if(lifetime>=@lifetime)
      @birth=nil
      return
    end
    @energy=@cast.calc(lifetime/@lifetime.to_f)[0]
  end

  def new_cast
    cv=[]
    (MIN_CASTVALUES+rand(DIFF_CASTVALUES)-2).times do
      cv.push(MIN_CASTRANGE+rand()*DIFF_CASTRANGE)
    end
    l=MIN_CASTRANGE_LAST+rand()*DIFF_CASTRANGE_LAST
    cv.push(l)
    cv.push(l)
    #    loggo("Cast: #{cv}")
    @cast=Casteljau::new([cv])
  end
end

class Sensorline
  MIN_ENERGY=0.03
  EXPFACTOR=7.0
  MAX_MOTION_PER_SECOND=0.15

  attr_reader(:line_no,:sensors)

  def initialize(line_no,nsensors)
    @line_no=line_no
    @sensors=[]
    vf=1.0/nsensors
    nsensors.times do |i|
      @sensors.push(Sensor::new(self,i,vf*i,vf*(i+1)))
    end
  end


  def energies
    @sensors.map do |s|
      s.energy ? s.energy : 0.0
    end
  end
end

class Playground_sensorlines
  NSENSORS=32 #16
  SENSORPOS=[]
  NSENSORS.times do |i|
    SENSORPOS.push(i.to_f/(NSENSORS-1)*2.0-1.0)
  end

  LOOP_SLEEP=0.06
  REFR_POS=1 # every N cycles

  def initialize
    @mrecept=Movireceptor::link_to(0)

    @vertline=Sensorline::new(0,NSENSORS)
    @horline=Sensorline::new(1,NSENSORS)
    @lines=[@vertline,@horline]
    @mtx=Mutex::new()

    DRb.start_service(SL_URI,self)
  end

  def runme
    @ncycle=0
    loop do
      cycle()
      sleep(LOOP_SLEEP)
    end
  end

  def cycle
    t=Time::now
    @lines.each do |l|
      l.sensors.each do |s|
        s.curpos(t)
      end
    end
    @ncycle+=1
    if(@ncycle%REFR_POS==0)
      a=[]
      NSENSORS.times do |i|
=begin
        s1=@vertline.sensors[i]
        s2=@horline.sensors[i]
        if(!s1.birth && !s2.birth)
          a.push(nil)
        else
          if(s1.birth && s2.birth)
            horpos=s1.hor_pos+rand()*(s2.hor_pos-s1.hor_pos)
          elsif(s1.birth)
            horpos=s1.hor_pos
          else
            horpos=s2.hor_pos
          end
          verpos=1.0-Math::log(1.0+((s1.energy+s2.energy)/2.0*(Math::E-1.0)))
          loggo(sprintf("%.2f+%.2f -> %.3f",s1.energy,s2.energy,verpos))
          a.push([horpos,verpos])
=end
        s1=@vertline.sensors[i]
        if(!s1.birth)
          a.push(nil)
        else
#          verpos=(Math::exp(s1.energy)-1.0)/(Math::E-1.0)
#          loggo(sprintf("%.2f -> %.3f",s1.energy,verpos))
          a.push([s1.hor_pos,s1.energy])
        end
      end
      @mrecept.newstuff(a)
    end
  end

  def energies
    @lines.map do |l|
      l.energies
    end
  end

  def lineaves
    nil
  end

  def kick(x,y,val)
    if(x>=0 && y>=0 && x<NSENSORS && y<1) # WAS 2
      @mtx.synchronize do
        @lines[y].sensors[x].kick
      end
    end
  end

  def Playground_sensorlines::getremote
    DRb.start_service()
    DRbObject::new(nil,SL_URI)
  end
end

if($0==__FILE__)
#  Fluid_logfile::new('sensorlines') 
  Playground_sensorlines::new().runme()
end
