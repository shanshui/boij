# boij.rb

=begin

4/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

General includes

$Id: boij.rb 4400 2015-01-24 17:55:17Z karl $

=end

#$KCODE='u'
#require 'jcode'
require 'fileutils'
require 'socket'
require 'pp'
require 'boij.so'
require 'player_config'

HOME=ENV['HOME']
PROJECT_LABEL='boij'

if (ENV['BOIJ_BASE']==nil)
  BASE="#{HOME}/.local/share/fluido/#{PROJECT_LABEL}/"
else
  BASE=ENV['BOIJ_BASE']+'/'
end

CONFBASE=BASE+'config/'
LOGBASE=BASE+'logs/'
PIDBASE=BASE+'pids/'
PICTBASE=BASE+'pictures/'
GRIDFILE=BASE+'grid.data'
MATBASE=BASE+'material/'
MATBASE_SMALL=MATBASE+'small/'
MATBASE_MEDIUM=MATBASE+'medium/'
PARKINGBASE=MATBASE+'parking/'
RECDIR=BASE+'rec/'
SNAPDIR=BASE+'snap/'
OPEN_SOUNDDIR=BASE+'sounds/'

HOURSCONF=CONFBASE+'hours.yml'
PLAYERCONF=CONFBASE+'player.yml'
SICKCONF=CONFBASE+'sick.yml'
SICKCAP=CONFBASE+'sick-#.cap'

DIRS_TO_CREATE=[CONFBASE,LOGBASE,PIDBASE,MATBASE_SMALL,MATBASE_MEDIUM,PARKINGBASE,RECDIR,SNAPDIR,OPEN_SOUNDDIR,PICTBASE]

# detect screen size
CURR_SIZE=`xrandr`.scan(/connected primary (\d+)x(\d+)/).flatten
CURR_SIZE=`xrandr`.scan(/connected (\d+)x(\d+)/).flatten unless 0 < CURR_SIZE.length
CURR_SIZE=`xrandr`.scan(/current (\d+) x (\d+)/).flatten unless 0 < CURR_SIZE.length
DUMP_SIZE=[CURR_SIZE[0].to_i, CURR_SIZE[1].to_i]

PCFG = Player_config::load
CUT = (DUMP_SIZE[1] - PCFG.image_height)
CUT = 0 if CUT < 0
CUT_TOP = CUT_BOTTOM = CUT / 2

# size of picture
SCREEN_SIZE=[DUMP_SIZE[0],DUMP_SIZE[1]-CUT]
#
# The two below are REVERSED!!! but I do not want to change them in the algo
#
N_SCREENS=1
DUMP_WINDOW_SIZE=[DUMP_SIZE[0]*N_SCREENS,DUMP_SIZE[1]]
WINDOW_SIZE=[SCREEN_SIZE[0]*N_SCREENS,SCREEN_SIZE[1]]
MAX_EXPAND=(WINDOW_SIZE[0]+Boij_columns::N_SMOOTHS)*2
HORIZON_POS=0.5
HORIZON_PIX=(SCREEN_SIZE[1]*HORIZON_POS).to_i()

SOCK_NAME=BASE+'intercom.sock'

IMGSIZE=[720,240]
CAPTSIZE=[720,576]
SKIPX=[0,0]
SKIPY=[0,0]
CAPTURE_PARA=[
              [CAPTSIZE[0],IMGSIZE[0],SKIPX[0],CAPTSIZE[0]-SKIPX[0]-SKIPX[1]],
              [CAPTSIZE[1],IMGSIZE[1],SKIPY[0],CAPTSIZE[1]-SKIPY[0]-SKIPY[1]]
             ]
#IMG_PARA=[80,128,128,100,256]
IMG_PARA=[140,100,128,100,256]

#THRESHPARA=[40,3,2,1,2]
#THRESHPARA=[40,43,20,19,5]
#THRESHPARA=[20,10,43,5,5]

#THRESHPARA=[20,18,40,5,5]
THRESHPARA=[38,36,40,5,5]

GAIN_VALUE=0x6200

#
# -2550 DARK
# -2750 LIGHT
#

#BIAS_VALUE=-2500 #-2650 #-2820
BIAS_VALUE=-2100 #23/04

REMOTE_INPUT=3

#SCANNER_DEVICE='mustek:/dev/sg6'

IMAGEADD_WEB_PORT=11222
MANAGER_WEB_PORT=11223
DRB_PORT_IMG=11224
DRB_PORT_ACTIVITY=11225

MP_URI='druby://127.0.0.1:55150'
SG_URI='druby://127.0.0.1:55151'
SL_URI='druby://127.0.0.1:55152'

require 'asfluidities'
require_relative 'picture'
require_relative 'player'

CAPT_FRAME=[320,240]
CLIP=[32,30,14,16]

class Capt_stuff
  attr_reader(:vcapt_arr,:clipped_frame,:serunit,:serspeed)

  def initialize(unit,frame,clip,img_para,capt_type,input,std,serunit,serspeed)
    @vcapt_arr=[unit,*frame,clip,img_para,capt_type,input,std]
    @clipped_frame=[frame[0]-clip[0]-clip[1],frame[1]-clip[2]-clip[3]]
    @serunit,@serspeed=serunit,serspeed
  end
end

#CAPT=Capt_stuff::new(0,CAPT_FRAME,CLIP,IMG_PARA,'8 bpp gray','Composite1','PAL-BG','/dev/ttyUSB0',57600)
