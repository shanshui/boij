#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__attribute_used__
__attribute__((section("__versions"))) = {
	{ 0x8e909bc6, "struct_module" },
	{ 0xb0c82c27, "usb_serial_disconnect" },
	{ 0x76dea909, "usb_serial_probe" },
	{ 0x5422e147, "usb_register_driver" },
	{ 0xb05a4265, "usb_serial_register" },
	{ 0xdd132261, "printk" },
	{ 0xcf9f6091, "usb_serial_deregister" },
	{ 0x373dc1c0, "usb_deregister" },
};

static const char __module_depends[]
__attribute_used__
__attribute__((section(".modinfo"))) =
"depends=usbserial";

MODULE_ALIAS("usb:v1553p3500d*dc*dsc*dp*ic*isc*ip*");

MODULE_INFO(srcversion, "73F685EA910CAB45F352771");
