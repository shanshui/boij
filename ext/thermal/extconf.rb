require "mkmf"

# $Id: extconf.rb 1340 2007-08-18 11:28:36Z karl $
$CFLAGS = "-g -O3 -funsigned-char -ffast-math -Wall -Wcast-align -fPIC"
if(have_header('sys/ioctl.h') &&
   have_header('pthread.h') &&
   have_library('pthread','pthread_join')) 
  create_makefile ('thermal')
else
  STDERR.printf('Some files missing!!!')
end
