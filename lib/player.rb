# player.rb

=begin

7/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

One player

$Id: player.rb 3887 2013-02-04 11:55:43Z karl $

=end

require 'player_config'

class Player
  attr_accessor(:arrival,:position,:last_image_started,:traveled_distance,:image)
  attr_reader(:imsize,:imbegin,:prog,:owned_pictures,:engine,:left_or_right)

  def initialize(engine,prog)
    @engine,@prog=engine,prog
    @run_dist=0.0
    @audiocnt=0
    @cfg=Player_config::load
    clear()
  end

  def clear
    @arrival=nil
    @position=nil
    @last_image_started=nil
    @owned_pictures=[]
    @traveled_distance=0 #GENERATE_NEW_IMAGE
    @latest_y=nil
    @image=nil
    @left_or_right=rand(2)>0
#    @engine.audiotoy.del_chunk(@achunk) if(@achunk)
  end

=begin
  def start_new_image_if_needed_2(time)
    return if(!@position)

    #    curinterval=IMGSTART_MIN+@position[1]*IMGSTART_DIFF
    #    return if(@last_image_started && @last_image_started+curinterval>time)

    #    @last_image_started=time

    if(!@image)
      genimageflag=true
      @last_generated_pos=[]+@position
      genflag=false
    else
      xd=(@position[0]-@last_chosen_pos)*PLAYGROUND_SIZE[0]
      genimageflag=xd.abs()>=CHOOSE_NEW_IMAGE
      xd=(@position[0]-@last_generated_pos[0])*PLAYGROUND_SIZE[0]
      yd=(@position[1]-@last_generated_pos[1])*PLAYGROUND_SIZE[1]*SPACE_COMPRESSION_FACT
      genflag=Math::sqrt(xd*xd+yd*yd)>=GENERATE_NEW_IMAGE
    end

    if(genimageflag)
      @engine.imarr_mutex.synchronize do
        @image=@engine.next_picture()
        @imsize=@image.min_size+rand(@image.diff_size)
        @imbegin=rand(@image.size[0]-@imsize)
      end

      if(Engine::AUDIO_FLAG)
        @achunk=@engine.audiotoy.add_chunk(@engine.sounds[rand(@engine.sounds.length())],false,false,1.0)

        xpos,ypos=*@position
        volumes=[(1.0-xpos)*ypos,xpos*ypos]
        @engine.audiotoy.set_volume(@achunk,volumes)

        rate=MIN_SOUNDRATE+DIFF_SOUNDRATE*ypos
        #      rate=-rate if(!@forw_backw)
        #    STDERR.puts("Chunk #{@achunk} gets #{rate} (ypos=#{ypos})")

        @engine.audiotoy.set_rate(@achunk,rate)
        @engine.audiotoy.fire_chunk(@achunk,0)
      else
        @achunk=nil
      end

      loggo("Player ##{@prog} gets image ##{@image.prog}")
      @last_chosen_pos=@position[0]
      genflag=true
    end

    if(genflag)
      #      loggo("Pos is now #{@position}")
      yp=@position[1] # 1.0-@position[1]
      pct=(MIN_PCT_TO_USE==MAX_PCT_TO_USE) ? MIN_PCT_TO_USE : (MIN_PCT_TO_USE+(1.0-yp)*DIFF_PCT_TO_USE)

      #      ypn=yp*2.0
      #      ypn=2.0-ypn if(ypn>=1.0)
      #      zoom=MIN_ZOOM+ypn*DIFF_ZOOM

      zoom=[MIN_ZOOM,MIN_ZOOM+yp*DIFF_ZOOM].max
      #      loggo(sprintf("yp %.2f zoom %.2f",yp,zoom))
      pixels=@engine.b_i.resize(@image.pixels,@image.size[1]-@image.bottom_offset,@imbegin,@imbegin+@imsize-1,zoom)
      @owned_pictures.push(Displayed_picture::new(@engine,self,@position[0],@image,pixels,@left_to_right))
      #      loggo("#{@prog+1}: Pos is now #{@position}. I have #{@owned_pictures.length} owned p's")
      @last_generated_pos=[]+@position
    end
  end
=end

  def start_new_image_if_needed_raalte
    return if(!@position)

    if(!@image)
      genimageflag=true
      @last_generated_pos=[]+@position
      genflag=false
    else
      xd=(@position[0]-@last_chosen_pos)*@cfg.playground_size[0]
      genimageflag=xd.abs()>=@cfg.choose_new_image
      xd=(@position[0]-@last_generated_pos[0])*@cfg.playground_size[0]
      yd=(@position[1]-@last_generated_pos[1])*@cfg.playground_size[1]*@cfg.space_compression_fact
      genflag=Math::sqrt(xd*xd+yd*yd)>=@cfg.generate_new_image
    end

    if(genimageflag)
      @engine.imarr_mutex.synchronize do
        @image=@engine.next_picture()
        @imsize=@image.min_size+rand(@image.diff_size)
        @imbegin=rand(@image.size[0]-@imsize)
        @flip=rand(2)==0
      end

      if(Engine::AUDIO_FLAG)
        @achunk=@engine.audiotoy.add_chunk(@engine.sounds[rand(@engine.sounds.length())],false,false,1.0)

        xpos,ypos=*@position
        volumes=[(1.0-xpos)*ypos,xpos*ypos]
        @engine.audiotoy.set_volume(@achunk,volumes)

        rate=@cfg.min_sound_rate+@cfg.diff_sound_rate*ypos

        @engine.audiotoy.set_rate(@achunk,rate)
        @engine.audiotoy.fire_chunk(@achunk,0)
      else
        @achunk=nil
      end

#      loggo("Player ##{@prog} gets image ##{@image.prog}")
      @last_chosen_pos=@position[0]
      genflag=true
    end

    if(genflag)
#      loggo("Pos is now #{@position} (#{[@imbegin,@imbegin+@imsize]})")

      yp=@position[1]
      zoom=[@cfg.min_zoom,@cfg.min_zoom+yp*@cfg.diff_zoom].max
      @engine.rc.add_ele(@image.fn,zoom,@position[0],@imbegin,@imbegin+@imsize,@flip)
    end
  end

  def start_new_image_if_needed_sick
    if(!@position)
      @image=nil
      @last_pos=nil
      @run_dist=0.0
      return
    end

    if(@image)
      if(@last_pos)
        xd=@position[0]-@last_pos[0]
        yd=@position[1]-@last_pos[1]
        @run_dist+=Math::sqrt(xd*xd+yd*yd)
      end
      @last_pos=[]+@position
      xd=@position[0]-@last_generated_pos[0]
      yd=@position[1]-@last_generated_pos[1]
      dist=Math::sqrt(xd*xd+yd*yd)

      return if(dist<@cfg.min_dist)
      if(@run_dist>=@cfg.new_image_travel)
        @run_dist=0.0
        @image=nil
      end
    end
    unless(@image)
      @engine.imarr_mutex.synchronize do
        @image=@engine.next_picture()
        @imsize=@image.min_size+rand(@image.diff_size)
        @imbegin=rand(@image.size[0]-@imsize)
        @flip=rand(2)==0
      end
    end
    @last_generated_pos=[]+@position

    xpos,ypos=@last_generated_pos

    if(Engine::AUDIO_FLAG)
      @audiocnt+=1
      if((@audiocnt%2)==1)
        @achunk=@engine.audiotoy.add_chunk(@engine.sounds[rand(@engine.sounds.length())],false,false,1.0)

        v=@cfg.min_volume+ypos*@cfg.diff_volume
        volumes=[(1.0-xpos)*v,xpos*v]
        @engine.audiotoy.set_volume(@achunk,volumes)

        rate=@cfg.min_sound_rate+@cfg.diff_sound_rate*ypos
        #      loggo("Volumes now #{volumes} rate #{rate} (#{@last_generated_pos})")

        @engine.audiotoy.set_rate(@achunk,rate)
        @engine.audiotoy.fire_chunk(@achunk,0)
      end
    else
      @achunk=nil
    end

    #loggo("P#{@prog} at #{@last_generated_pos}")
    #loggo(sprintf("ypos: %f", ypos))
    #loggo(sprintf("exp: %f",(@cfg.min_zoom+(1.0-ypos)*@cfg.diff_zoom)))

    case @cfg.zoom_alg
    when 'lin'
      z = (1.0 - ypos) * @cfg.diff_zoom
    when 'sin'
      n = @cfg.zoom_alg_n
      z = (Math::sin(Math::PI*2*n*ypos)+1.0) * @cfg.diff_zoom/2.0
    when 'cos'
      n = @cfg.zoom_alg_n
      z = (Math::cos(Math::PI*2*n*ypos)+1.0) * @cfg.diff_zoom/2.0
    end
    zoom = Math::exp(@cfg.min_zoom + z)

    #STDERR.printf("<%.2f>",zoom)
    @engine.rc.add_ele(@image.fn,zoom,xpos,@imbegin,@imbegin+@imsize,@flip)
  end

  def give_owned_pictures_and_leave
    retv=[]+@owned_pictures
    retv.each do |p|
      p.owner=nil
    end
    clear()
    retv
  end
end
