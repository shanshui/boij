#!/usr/bin/env ruby
# thermalautofitter.rb

=begin

05/05/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Automatic thermal range fitter

$Id: thermalautofitter.rb 1629 2008-07-13 09:17:22Z karl $

=end

require 'boij'
require 'thermal'

BORDERSKIP=80
LOC_CAPTURE_PARA=
  [
   [CAPTSIZE[0],CAPTSIZE[0],BORDERSKIP,CAPTSIZE[0]-BORDERSKIP*2],
   [CAPTSIZE[1],CAPTSIZE[1],BORDERSKIP,CAPTSIZE[1]-BORDERSKIP*2]
  ]
BIAS_RANGE=[-3500,600]
SETTLE_TIME=0.9
MORE_SLEEP=0.2
RETRY_TIMES=10
GREYDIFF_THRESH=5.0
GREYVALUE_SEARCH=8.0
MAIN_SETTLE_TIME=2.0

MAIN_PROCESS_NAME='main'
REMOTER_PROCESS_NAME='remoter'
RESTART_PROCESS_NAME='boij_Engine'
KILLWAIT=3 # seconds to wait before checking for successful kill

def therma_command(th,cmd,para)
  th.send_command(cmd,para)

  rd=nil
  loop do
    rd=th.read()
    break if(rd)
  end
#  rd.unpack('v*').each_with_index do |v,i|
#    STDERR.printf("%d: <%.4x> -> %d\n",i,v,v)
#  end

  rd
end

def update_values(th,gain,bias,savef=false)
  therma_command(th,156,[0,0,gain,bias,0,0,0,0,0,0,0,0]) if(savef)
  therma_command(th,156,[1,0,gain,bias,0,0,0,0,0,0,0,0])
end

def current_greyness(sva)
  ret=sva.latest_framedata()
  return false if(!ret)

  pix=sva.get_frame().unpack('C*')

  sum=0.0
  pix.each do |v|
    sum+=v
  end
  sum/=pix.length()

  sum
end

def greyness_with_given_bias(sva,th,bias)
  update_values(th,GAIN_VALUE,bias,false)
  sleep(SETTLE_TIME)
  RETRY_TIMES.times do
    ret=current_greyness(sva)
    return ret if(ret)
    sleep(MORE_SLEEP)
  end
  raise("Too much time waiting for current greyness!")
end

def split_range(sva,th,range,rangevalues)
  newbias=range[0]+(range[1]-range[0])/2
  newgrey=greyness_with_given_bias(sva,th,newbias)
  #  return [[range[0],newbias],[rangevalues[0],newgrey]] if((rangevalues[0]-newgrey).abs()>(newgrey-rangevalues[1]).abs())
  return [[range[0],newbias],[rangevalues[0],newgrey]] if(newgrey<GREYVALUE_SEARCH)
  [[newbias,range[1]],[newgrey,rangevalues[1]]]
end

###########

Fluid_logfile::new("Thermalautofitter")

#
# Is main running? If so, we must stop it!
#

remoter_pid,remoter_flg=Fluid_logfile::alive?(REMOTER_PROCESS_NAME)
raise "Remoter is not running!!" if(!remoter_flg)
pid,runfl=Fluid_logfile::alive?(MAIN_PROCESS_NAME)
if(runfl)
  loggo("Main process is running! (pid #{pid})")

  Process::kill('USR2',remoter_pid)
  sleep(KILLWAIT)
  loop do
    a,b=Fluid_logfile::alive?(MAIN_PROCESS_NAME)
    break if(!b)
    sleep(0.5)
  end
  loggo("Main process interrupted")
else
  loggo("Main process is not running")
end

videodata=[0,'Composite2','8 bpp gray',LOC_CAPTURE_PARA[0],LOC_CAPTURE_PARA[1],IMG_PARA]
sva=nil
10.times do |i|
  begin
    sva=Boij_sharedvideoarea::new(videodata)
    break
  rescue => err
    loggo("Attempt #{i+1}: Could not open video port (#{err.to_s()})")
    sva=nil
    sleep(1.5)
  end
end

if(sva)
  sva.set_threshold(THRESHPARA)

  grid=[]
  (Boij_sharedvideoarea::GRID_Y+1).times do |i|
    g=[]
    (Boij_sharedvideoarea::GRID_X+1).times do |i|
      g.push([0,0])
    end
    grid.push(g)
  end
  sva.set_grid(grid,false,false)
  sva.reload_grid_if_needed()

  th=nil
  10.times do |i|
    begin
      th=Thermal.new('/dev/ttyUSB0')
      break
    rescue => err
      loggo("Attempt #{i+1}: Could not open USB port (#{err.to_s()})")
      th=nil
      sleep(1.5)
    end
  end

  if(th)
    therma_command(th,1,[])

    sleep(MAIN_SETTLE_TIME)

    lowv=greyness_with_given_bias(sva,th,BIAS_RANGE[0])
    highv=greyness_with_given_bias(sva,th,BIAS_RANGE[1])
    range=BIAS_RANGE
    rangevalues=[lowv,highv]
    newrange=nil
    newrangevalues=nil

    loop do
      newrange,newrangevalues=split_range(sva,th,range,rangevalues)
      loggo("Range #{newrange[0]}-#{newrange[1]} has diff #{newrangevalues[1]-newrangevalues[0]} (#{newrangevalues[0]},#{newrangevalues[1]}).")
      break if((newrangevalues[1]-newrangevalues[0]).abs()<=GREYDIFF_THRESH)
      range=newrange
      rangevalues=newrangevalues
    end

    sva=nil

    gv=newrange[0]+(newrange[1]-newrange[0])/2
    loggo("Good value for these temporal coordinates is #{gv}.")
    update_values(th,GAIN_VALUE,gv,true)
  end
end

GC::start()

if(runfl)
  loggo("Restarting main process.")
  Process::kill('USR1',remoter_pid)
  sleep(KILLWAIT)
  a=nil
  loop do
    a,b=Fluid_logfile::alive?(RESTART_PROCESS_NAME)
    break if(b)
    sleep(0.5)
  end
  loggo("Main process restarted (pid #{a}).")
end
