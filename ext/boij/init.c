/* init.c */

/*
 * 17/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * The file containing definitions of the C classes
 *
 * $Id: init.c 4105 2014-01-08 13:51:03Z karl $
 */

#include "boij.h"

#define MAX_TANV 0.99

VALUE cls_obj;

extern VALUE cls_columns,cls_audiotoy,cls_regr,cls_inputreader,cls_imager,
  cls_contour,cls_videocapt,cls_casteljau,cls_raalte_columns,cls_sick/*,cls_scannatoio*/;

#ifdef USE_PHIDGET
extern VALUE cls_phidget;
#endif

extern void init_boij_columns(void);
extern VALUE new_boij_columns(VALUE self,VALUE screen,VALUE pictures);
extern VALUE compuscreen(VALUE self,VALUE data);

/*
extern VALUE new_boij_sharedvideoarea(VALUE self,VALUE videodata);
extern VALUE sva_tripl(VALUE self,VALUE frm);
extern VALUE sva_pixflip(VALUE self,VALUE frm);
extern VALUE sva_squaret(VALUE self,VALUE sq,VALUE dot);
extern VALUE sva_extendline(VALUE self,VALUE line,VALUE linelen_to,VALUE bottom_offset,VALUE blackfill);
extern VALUE sva_tanpropval(VALUE self,VALUE voval);
extern VALUE sva_latest_framedata(VALUE self);
extern VALUE sva_shp(VALUE self);
extern VALUE sva_getthresh(VALUE self);
extern VALUE sva_setthresh(VALUE self,VALUE thresh);
extern VALUE sva_getclients(VALUE self,VALUE bypass_flags);
extern VALUE sva_getframesize(VALUE self);
extern VALUE sva_getframe(VALUE self);
extern VALUE sva_getgrid(VALUE self);
extern VALUE sva_setgrid(VALUE self,VALUE grid,VALUE togx,VALUE togy);
extern VALUE sva_relogrid(VALUE self);
extern VALUE sva_bkglevel(VALUE self);
extern VALUE sva_fixgreyness(VALUE self);
extern VALUE sva_fliprotate(VALUE self,VALUE lines);
*/

static float sva_max_tanres;

extern VALUE new_audiotoy(VALUE self);
extern VALUE audiotoy_loadfile(VALUE self,VALUE name,VALUE attn);
extern VALUE audiotoy_unloadfile(VALUE self,VALUE v);
extern VALUE audiotoy_addchunk(VALUE self,VALUE v,VALUE loop,VALUE active,VALUE factor);
extern VALUE audiotoy_delchunk(VALUE self,VALUE v);
extern VALUE audiotoy_firechunk(VALUE self,VALUE v,VALUE pos);
extern VALUE audiotoy_setrate(VALUE self,VALUE v,VALUE rate);
extern VALUE audiotoy_setvolume(VALUE self,VALUE v,VALUE vol);
extern VALUE audiotoy_setvolume51(VALUE self,VALUE v,VALUE vx,VALUE vy);
extern VALUE audiotoy_nchunks(VALUE self);
extern VALUE audiotoy_chunkfromname(VALUE self,VALUE v);
extern VALUE audiotoy_chunkname(VALUE self,VALUE v);
extern VALUE audiotoy_fixloud(VALUE self,VALUE v);

extern VALUE regr_new(VALUE self,VALUE x,VALUE y,VALUE framep,VALUE groupsp,VALUE threshp,VALUE flagp);
extern VALUE regr_set_threshs(VALUE self,VALUE v_t);
extern VALUE regr_get_threshs(VALUE self);
extern VALUE regr_regroup(VALUE self,VALUE frame);

extern VALUE new_inputreader(VALUE self,VALUE vunit,VALUE reqevents);
extern VALUE ireader_getevent(VALUE self);

extern VALUE new_imager(VALUE self,VALUE x,VALUE y,VALUE horizon);
extern VALUE imager_resize(VALUE self,VALUE pixels,VALUE v_horizon_pos,VALUE v_xfrom,VALUE v_xto,VALUE v_zoom);
extern VALUE imager_screenmaker(VALUE self,VALUE curclients);
extern VALUE imager_resetcnt(VALUE self,VALUE v);
extern VALUE imager_pixflip(VALUE self,VALUE frm);

extern VALUE new_contour(VALUE self,VALUE x,VALUE y);
extern VALUE contour_calc(VALUE self,VALUE varr,VALUE levels);
extern VALUE contour_find_centers(VALUE self,VALUE varr,VALUE vmindist);

extern VALUE new_videocapt(VALUE self,VALUE videodata,VALUE v_nmaps);
extern VALUE vc_get_latest_frame(VALUE self,VALUE add_mapped);
extern VALUE vc_set_mapping_rect(VALUE self,VALUE v_which,VALUE ul,VALUE ur,VALUE lr,VALUE ll);
extern VALUE vc_triplicate(VALUE self,VALUE arr);
extern VALUE vc_tanpropval(VALUE self,VALUE voval);
extern VALUE vc_fliprotate(VALUE self,VALUE lines);

extern VALUE new_casteljau(VALUE self,VALUE arr);
extern VALUE casteljau_calc(VALUE self,VALUE pos);

#ifdef USE_PHIDGET
extern VALUE phidget_new(VALUE self);
extern VALUE phidget_cvalue(VALUE self,VALUE v_card);
#endif

extern VALUE new_raalte_columns(VALUE self,VALUE v_w,VALUE v_h,VALUE v_marg_up,VALUE v_marg_down,VALUE v_screen_horiz);
extern VALUE raalte_add_ele(VALUE self,VALUE v_path,VALUE v_scale,
			    VALUE v_colstart,VALUE v_colfrom,VALUE v_colto,
			    VALUE v_backw);
extern VALUE raalte_curframe(VALUE self);

extern VALUE new_sick(VALUE self,VALUE v_addr,VALUE v_port,VALUE v_scanrange,VALUE v_angres,VALUE v_background,VALUE v_ranges,VALUE v_tolerance,VALUE v_minwgt,
		      VALUE v_maxobj,VALUE v_maxdist,VALUE v_maxdisappeared);
extern VALUE sick_msg(VALUE self);
extern VALUE sick_tracker(VALUE self,VALUE f_gensum);
extern VALUE sick_close(VALUE self);
extern VALUE sick_mapped_background(VALUE self);
extern VALUE sick_update_vals(VALUE self,VALUE v_ranges,VALUE v_minwgt,VALUE v_maxdist,VALUE v_maxdisappeared);

//extern VALUE new_scannatoio(VALUE self,VALUE device);
//extern VALUE scanna_list_devices(VALUE self);
//extern VALUE scanna_options(VALUE self);
//extern VALUE scanna_set_option(VALUE self,VALUE v_n,VALUE v_val);

VALUE reverse_threebythree(VALUE self,VALUE v_str)
{
  int len=(RSTRING_LEN(v_str))/3,lenhalf=len>>1,i;
  __u8 c[3],*ptrb=(__u8 *)RSTRING_PTR(v_str),*ptr1,*ptr2;

  for(i=0;i<lenhalf;i++)
  {
    ptr1=ptrb+(i*3);
    ptr2=ptrb+((len-i-1)*3);
    memcpy(c,ptr1,3);
    memcpy(ptr1,ptr2,3);
    memcpy(ptr2,c,3);
  }

  return v_str;
}

VALUE to_roworder(VALUE self,VALUE v_str,VALUE v_x,VALUE v_y)
{
  int x=FIX2INT(v_x);
  int y=FIX2INT(v_y);
  int totl=x*y*3;

  if(RSTRING_LEN(v_str)!=totl)
    rb_raise(rb_eRuntimeError,"%s: bad xy (got %d,%d=%ld, should be %d)",__func__,x,y,RSTRING_LEN(v_str),totl);

  __u8 *b=malloc(totl),*bp=b,*ob=(__u8 *)RSTRING_PTR(v_str);
  int i,j;

  for(i=x-1;i>=0;i--)
    for(j=y-1;j>=0;j--,bp+=3)
      memcpy(bp,ob+((j*x+i)*3),3);

  VALUE to_ret=rb_str_new((char *)b,totl);

  free(b);

  return to_ret;
}

void Init_boij(void)
{
  cls_obj=rb_path2class("Object");

  cls_columns=rb_define_class("Boij_columns",rb_cObject);
  rb_define_singleton_method(cls_columns,"new",new_boij_columns,2);
  rb_define_method(cls_columns,"compute_screen",compuscreen,1);
  rb_define_const(cls_columns,"N_SMOOTHS",INT2NUM(N_SMOOTHS));

  init_boij_columns();
/*
  cls_sharedvideoarea=rb_define_class("Boij_sharedvideoarea",rb_cObject);
  rb_define_singleton_method(cls_sharedvideoarea,"new",new_boij_sharedvideoarea,1);
  rb_define_singleton_method(cls_sharedvideoarea,"triplicate",sva_tripl,1);
  rb_define_singleton_method(cls_sharedvideoarea,"pixel_flip",sva_pixflip,1);
  rb_define_singleton_method(cls_sharedvideoarea,"squaret",sva_squaret,2);
  rb_define_singleton_method(cls_sharedvideoarea,"extend_line",sva_extendline,4);
  rb_define_singleton_method(cls_sharedvideoarea,"tan_propval",sva_tanpropval,1);
  rb_define_singleton_method(cls_sharedvideoarea,"fliprotate",sva_fliprotate,1);
  rb_define_method(cls_sharedvideoarea,"latest_framedata",sva_latest_framedata,0);
  rb_define_method(cls_sharedvideoarea,"sharedarea_pointer",sva_shp,0);
  rb_define_method(cls_sharedvideoarea ,"get_threshold",sva_getthresh,0);
  rb_define_method(cls_sharedvideoarea ,"set_threshold",sva_setthresh,1);
  rb_define_method(cls_sharedvideoarea ,"get_clients",sva_getclients,1);
  rb_define_method(cls_sharedvideoarea ,"get_framesize",sva_getframesize,0);
  rb_define_method(cls_sharedvideoarea ,"get_frame",sva_getframe,0);
  rb_define_method(cls_sharedvideoarea ,"get_grid",sva_getgrid,0);
  rb_define_method(cls_sharedvideoarea ,"set_grid",sva_setgrid,3);
  rb_define_method(cls_sharedvideoarea ,"reload_grid_if_needed",sva_relogrid,0);
  rb_define_method(cls_sharedvideoarea ,"background_level",sva_bkglevel,0);
  rb_define_method(cls_sharedvideoarea ,"fix_greyness",sva_fixgreyness,0);
  rb_define_const(cls_sharedvideoarea,"MAX_MONITORED",INT2NUM(MAX_MONITORED));
  rb_define_const(cls_sharedvideoarea,"N_THRESHS",INT2NUM(N_THRESHS));
  rb_define_const(cls_sharedvideoarea,"GRID_X",INT2NUM(GRID_X));
  rb_define_const(cls_sharedvideoarea,"GRID_Y",INT2NUM(GRID_Y));
  rb_define_const(cls_sharedvideoarea,"MAX_GRID_OFFSET",INT2NUM(MAX_GRID_OFFSET));
  rb_define_const(cls_sharedvideoarea,"BLACKFILL_NONE",INT2NUM(BLACKFILL_NONE));
  rb_define_const(cls_sharedvideoarea,"BLACKFILL_EXTEND",INT2NUM(BLACKFILL_EXTEND));
  rb_define_const(cls_sharedvideoarea,"BLACKFILL_MIRROR",INT2NUM(BLACKFILL_MIRROR));
*/

  sva_max_tanres=1.0/tan(MAX_TANV);

  cls_audiotoy=rb_define_class("Audiotoy",rb_cObject);
  rb_define_singleton_method(cls_audiotoy,"new",new_audiotoy,0);
  rb_define_method(cls_audiotoy,"load_file",audiotoy_loadfile,2);
  rb_define_method(cls_audiotoy,"unload_file",audiotoy_unloadfile,1);
  rb_define_method(cls_audiotoy,"add_chunk",audiotoy_addchunk,4);
  rb_define_method(cls_audiotoy,"del_chunk",audiotoy_delchunk,1);
  rb_define_method(cls_audiotoy,"fire_chunk",audiotoy_firechunk,2);
  rb_define_method(cls_audiotoy,"set_rate",audiotoy_setrate,2);
  rb_define_method(cls_audiotoy,"set_volume",audiotoy_setvolume,2);
  rb_define_method(cls_audiotoy,"set_volume_51",audiotoy_setvolume51,3);
  rb_define_method(cls_audiotoy,"n_chunks?",audiotoy_nchunks,0);
  rb_define_method(cls_audiotoy,"chunk_from_name",audiotoy_chunkfromname,1);
  rb_define_method(cls_audiotoy,"chunkname?",audiotoy_chunkname,1);
  rb_define_method(cls_audiotoy,"fix_loudspeaker",audiotoy_fixloud,1);

  cls_regr=rb_define_class("Regrouper",rb_cObject);
  rb_define_singleton_method(cls_regr,"new",regr_new,3);
  rb_define_method(cls_regr,"set_threshs",regr_set_threshs,1);
  rb_define_method(cls_regr,"get_threshs",regr_get_threshs,0);
  rb_define_method(cls_regr,"regroup",regr_regroup,1);

  VALUE regr_vl=rb_ary_new();
  int i;

  for(i=0;i<N_THRESHS;i++)
    rb_ary_push(regr_vl,rb_str_new2(thresh_labels[i]));

  rb_define_class_variable(cls_regr,"@@thresh_labels",regr_vl);
  rb_define_const(cls_regr,"GCOND_ABSENT",INT2FIX(GCOND_ABSENT));
  rb_define_const(cls_regr,"GCOND_PRESENT",INT2FIX(GCOND_PRESENT));
  rb_define_const(cls_regr,"GCOND_JUSTLEFT",INT2FIX(GCOND_JUSTLEFT));

  cls_inputreader=rb_define_class("Inputreader",rb_cObject);
  rb_define_singleton_method(cls_inputreader,"new",new_inputreader,2);
  rb_define_method(cls_inputreader,"event",ireader_getevent,0);

  cls_imager=rb_define_class("Imager",rb_cObject);
  rb_define_singleton_method(cls_imager,"new",new_imager,3);
  rb_define_method(cls_imager,"resize",imager_resize,5);
  rb_define_method(cls_imager,"screenmaker",imager_screenmaker,1);
  rb_define_method(cls_imager,"reset_cnt",imager_resetcnt,1);
  rb_define_singleton_method(cls_imager,"pixel_flip",imager_pixflip,1);

  cls_contour=rb_define_class("Contour",rb_cObject);
  rb_define_singleton_method(cls_contour,"new",new_contour,2);
  rb_define_method(cls_contour,"calc",contour_calc,2);
  rb_define_method(cls_contour,"find_centers",contour_find_centers,2);

  cls_videocapt=rb_define_class("Videocapt",rb_cObject);
  rb_define_singleton_method(cls_videocapt,"new",new_videocapt,2);
  rb_define_method(cls_videocapt,"latest_frame",vc_get_latest_frame,1);
  rb_define_method(cls_videocapt,"set_mapping_rect",vc_set_mapping_rect,5);
  rb_define_singleton_method(cls_videocapt,"triplicate",vc_triplicate,1);
  rb_define_singleton_method(cls_videocapt,"tan_propval",vc_tanpropval,1);
  rb_define_singleton_method(cls_videocapt,"fliprotate",vc_fliprotate,1);

  cls_casteljau=rb_define_class("Casteljau",rb_cObject);
  rb_define_singleton_method(cls_casteljau,"new",new_casteljau,1);
  rb_define_method(cls_casteljau,"calc",casteljau_calc,1);

#ifdef USE_PHIDGET
  cls_phidget=rb_define_class("Phidget",rb_cObject);
  rb_define_singleton_method(cls_phidget,"new",phidget_new,0);
  rb_define_method(cls_phidget,"cvalue",phidget_cvalue,1);
#endif

  cls_raalte_columns=rb_define_class("Raalte_columns",rb_cObject);
  rb_define_singleton_method(cls_raalte_columns,"new",new_raalte_columns,5);
  rb_define_method(cls_raalte_columns,"add_ele",raalte_add_ele,6);
  rb_define_method(cls_raalte_columns,"curframe",raalte_curframe,0);

  cls_sick=rb_define_class("Sick",rb_cObject);
  rb_define_singleton_method(cls_sick,"new",new_sick,11);
  rb_define_method(cls_sick,"msg",sick_msg,0);
  rb_define_method(cls_sick,"tracker",sick_tracker,1);
  rb_define_method(cls_sick,"close",sick_close,0);
  rb_define_method(cls_sick,"get_mapped_background",sick_mapped_background,0);
  rb_define_method(cls_sick,"update_vals",sick_update_vals,4);
  rb_define_const(cls_sick,"SCANRANGE_100",INT2FIX(100));
  rb_define_const(cls_sick,"SCANRANGE_180",INT2FIX(180));
  rb_define_const(cls_sick,"ANGRES_0_50",INT2FIX(50));
  rb_define_const(cls_sick,"ANGRES_0_25",INT2FIX(25));

  /* cls_scannatoio=rb_define_class("Scannatoio",rb_cObject); */
  /* rb_define_singleton_method(cls_scannatoio,"new",new_scannatoio,1); */
  /* rb_define_singleton_method(cls_scannatoio,"list_devices",scanna_list_devices,0); */
  /* rb_define_method(cls_scannatoio,"options",scanna_options,0); */
  /* rb_define_method(cls_scannatoio,"set_option",scanna_set_option,2); */
  /* rb_define_const(cls_scannatoio,"TYPE_BOOL",INT2NUM(SANE_TYPE_BOOL)); */
  /* rb_define_const(cls_scannatoio,"TYPE_INT",INT2NUM(SANE_TYPE_INT)); */
  /* rb_define_const(cls_scannatoio,"TYPE_FIXED",INT2NUM(SANE_TYPE_FIXED)); */
  /* rb_define_const(cls_scannatoio,"TYPE_STRING",INT2NUM(SANE_TYPE_STRING)); */
  /* rb_define_const(cls_scannatoio,"TYPE_BUTTON",INT2NUM(SANE_TYPE_BUTTON)); */
  /* rb_define_const(cls_scannatoio,"TYPE_GROUP",INT2NUM(SANE_TYPE_GROUP)); */

  rb_define_global_function("reverse_threebythree",reverse_threebythree,1);
  rb_define_global_function("to_roworder",to_roworder,3);
}

void loggo(const char *fmt,...)
{
  char bfr[256];
  va_list ap;

  va_start(ap,fmt);
  vsnprintf(bfr,256,fmt,ap);
  va_end(ap);

  rb_funcall(cls_obj,rb_intern("loggo"),1,rb_str_new_cstr(bfr));
}

extern VALUE vc_tanpropval(VALUE self,VALUE voval)
{
  float oval=NUM2DBL(voval);

  return rb_float_new(1.0-tan((1.0-oval)*MAX_TANV*sva_max_tanres));
}
