require 'sdl'
require 'boij'
require 'playground_sick'

WX=1600
WY=800
SQ=26
SQHALF=SQ>>1

cur_range=[]+Playground_sick::RANGE
dname,srange,angres,acc=Marshal::restore(File::read(BASE+'sick.conf'))
s=Sick::new(dname,srange,angres,acc,cur_range,Playground_sick::TOL,Playground_sick::MINWGT,MAXGRPS,Playground_sick::MAXDIST,Playground_sick::MAXDISAPP)
bgp=s.get_mapped_background()

SDL.init(SDL::INIT_VIDEO)
screen=SDL::Screen.open(WX,WY,16,SDL::SWSURFACE)
SDL::WM::set_caption('SICK!','SICK!')

cols=[]
MAXGRPS.times do
  cols.push([100+rand(150),100+rand(150),100+rand(150)])
end

loop do
  while event = SDL::Event.poll
    case event
    when SDL::Event::KeyDown
      case(event.sym)
      when 27
        break
      when SDL::Key::LEFT
        update_range(event.mod&1 ? -STEP : STEP,0,0,0)
      when SDL::Key::RIGHT
        update_range(0,event.mod&1 ? STEP : -STEP,0,0)
      when SDL::Key::UP
        update_range(0,0,event.mod&1 ? -STEP : STEP,0)
      when SDL::Key::DOWN
        update_range(0,0,0,event.mod&1 ? STEP : -STEP)
      end
    when SDL::Event::Quit
      break
    end
  end

  res=s.tracker(true)
  if(res)
    screen.fill_rect(0,0,WX,WY,[0,0,0])

    bgp.each do |a|
      if(a)
        x,y=a
        c=[x*WX-2,y*WY-2]
        screen.fill_rect(*c,4,4,[0,0,100])
      end
    end

    bb=res[false]
    res.delete(false)
    bb.each do |x,y|
      c=[x*WX-2,y*WY-2]
      screen.fill_rect(*c,4,4,[255,255,0])
    end

    res.each do |k,v|
      c=[v[0]*WX-SQHALF,v[1]*WY-SQHALF]
      screen.fill_rect(*c,SQ,SQ,cols[k])
    end
    screen.flip
  else
    sleep(0.02)
  end
end
