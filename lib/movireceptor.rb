# movireceptor.rb

=begin

14/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Simple mechanism for receiving motions (was developed for Raalte)

$Id: movireceptor.rb 3448 2012-03-14 11:13:45Z karl $

=end

require 'drb/drb'

class Movireceptor
  PORTSTART=5200
  URI='druby://127.0.0.1:%d'

  def initialize(func)
    @func=func
    @arr=[]
  end

  def newstuff(stuff)
    @arr=stuff
    @func.call(@arr)
  end

  def Movireceptor::fire_up(port,func)
    mr=Movireceptor::new(func)
    uri=sprintf(URI,PORTSTART+port)
    DRb.start_service(uri,mr)
    mr
  end

  def Movireceptor::link_to(port)
    DRb.start_service()
    uri=sprintf(URI,PORTSTART+port)
    DRbObject::new(nil,uri)
  end

  def Movireceptor::stop
    DRb.stop_service()
  end
end
