require "mkmf"

# $Id: extconf.rb 3469 2012-03-21 11:14:31Z karl $

# $LOCAL_LIBS="-lasound -lpthread #{(`sane-config --libs`).chomp}"

OPTF=6
$CFLAGS+=" -g -std=gnu99 -O#{OPTF} -funsigned-char -fPIC -ffast-math -Werror -Wall -Wcast-align -Wno-declaration-after-statement -Wno-unused-function -Wno-unused-but-set-variable -Wno-sign-compare"
$LOCAL_LIBS="-lasound -lpthread -lLMS1xx"
#$LOCAL_LIBS+="-lphidget21"

create_makefile ('boij')
