#!/usr/bin/env ruby

=begin

08/11/2010 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Easily pass along the images and tune the horizon

$Id: tune_horizons.rb 3877 2013-01-26 10:48:18Z karl $

=end

require 'boij.so'
require 'gtk2'
require 'image_dbase'

BW=3
HOR_STEP=5

class Picture
  attr_accessor(:modflag,:flipro)

  def get_flipro
    unless(@flipro)
      @flipro=Videocapt::fliprotate(@pixels).reverse
    end
    @flipro
  end
end

class Tuneho
  def initialize
    @idb=Image_dbase::new()
    rootfolders,folders,images=Wfolder::load(@idb)
    @imgs=[]
    rootfolders.each do |k,rf|
      recurse_add_image_from_folder(@imgs,rf)
    end
    @imgs.sort! do |a,b|
      a.prog<=>b.prog
    end

    @pos=0
    pp @imgs[0].class
    pp @imgs.length
  end

# Dir::foreach(PICTBASE) do |entry|
#   next if(entry[0,1]=='.')
#   a=entry.split('.')
#   next if(a.length!=2 || a[1]!='bo2')
#   n=a[0].to_i
#   p=Picture::new(n,true)
#   p.modflag=false
#   loggo("UUUH #{entry} #{p.pixels.length}")
#   $imgs.push([n,p,Videocapt::fliprotate(p.pixels)])
# end

# $imgs.sort! do |a,b|
#   a[0]<=>b[0]
# end


  def get_curpxb
    i=@imgs[@pos]
    @da.set_size_request(*i.size)
    [Gdk::Pixbuf::new(@imgs[@pos].get_flipro(),Gdk::Pixbuf::COLORSPACE_RGB,false,8,*i.size,
                      i.size[0]*3),*i.size]
  end

  def runme
    Gtk::init()
    window=Gtk::Window.new("Tune the horizons")
    main_vbox=Gtk::VBox.new(false,5)
    window.add(main_vbox)

    @label=Gtk::Label::new("Label")
    main_vbox.pack_start(@label,false,false)
    @label.set_text("#{@pos+1} of #{@imgs.length}")

    @col=Gdk::Color::new(200*256,200*256,0)
    @col2=Gdk::Color::new(150*256,50*256,0)

    @da=Gtk::DrawingArea.new
    a=get_curpxb()
    @da.set_size_request(a[1],a[2])
    @da.set_property('can-focus',TRUE)
    @da.add_events(Gdk::Event::KEY_PRESS_MASK)
    @da.signal_connect("key_press_event") do |widget, event|
      #  f=event.state.to_i()
      #  shift=(event.state.to_i()&1)>0
      #  ctrl=(event.state.to_i()&4)>0

      case event.keyval
      when Gdk::Keyval::GDK_Down
        @imgs[@pos].update_bottom_offset(-HOR_STEP)
        @imgs[@pos].modflag=true
        @da.queue_draw()
      when Gdk::Keyval::GDK_Up
        @imgs[@pos].update_bottom_offset(HOR_STEP)
        @imgs[@pos].modflag=true
        @da.queue_draw()
      when Gdk::Keyval::GDK_Left
        @pos-=1
        @pos=0 if(@pos<0)
        @da.queue_draw()
        @label.set_text("#{@pos+1} of #{@imgs.length}")
      when Gdk::Keyval::GDK_Right
        @pos+=1
        @pos=@imgs.length-1 if(@pos==@imgs.length)
        @da.queue_draw()
        @label.set_text("#{@pos+1} of #{@imgs.length}")
      when Gdk::Keyval::GDK_s
        if(@imgs[@pos].modflag)
          @imgs[@pos].dump()
          @imgs[@pos].modflag=false
        end
        @da.queue_draw()
      when Gdk::Keyval::GDK_Escape
        Gtk::main_quit
      end
    end
    @da.signal_connect('expose_event') do |w,e|
      gc=Gdk::GC::new(w.window)
      pixbuf,ix,iy=get_curpxb()
      w.window.draw_pixbuf(gc,pixbuf,0,0,0,0,ix,iy,Gdk::RGB::DITHER_NORMAL,0,0)
      gc.set_rgb_fg_color(@imgs[@pos].modflag ? @col2 : @col)
      w.window.draw_rectangle(gc,true,0,iy-@imgs[@pos].bottom_offset-BW,ix,BW*2)
    end

    eb=Gtk::EventBox::new
    main_vbox.pack_start(eb,true,true)

    #paste_curpix
    eb.add(@da)
    #eb.signal_connect('button_press_event') do |w,e|
    #  loggo("Ho #{e.x} #{e.y} #{e.button} #{e.state}")
    #end

    window.show_all()
    Gtk::main()

    #ni.set(img)
  end
  def recurse_add_image_from_folder(arr,folder)
    return if(!folder.used)
    folder.local_images.each do |im|
      begin
        arr.push(Picture::new(im.id,true))
      rescue
      end
    end
    folder.sons.each do |k,son|
      recurse_add_image_from_folder(arr,son)
    end
  end
end

Tuneho::new().runme
