#!/usr/bin/env ruby
# sick_runner.rb

=begin

14/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The logic that runs the process with a Sick sensor

$Id: faker_runner.rb 3470 2012-03-21 14:37:17Z karl $

=end

require 'boij'
require 'engine_sick'
require 'playground_faker_movireceptor'

Fluid_logfile::new('runner')

def interr
  $eps.reverse.each do |ep|
    loggo("Expiring pid #{ep.pid} (#{ep.cls.name})")
    begin
      ep.expire()
      sleep(2)
    rescue
    end
  end
  sleep(3)
  loggo('Terminating.')
  exit(0)
end

daemons=[[Engine,nil],2,[Faker,nil]]

$eps=[]
daemons.each do |d|
  if(d.is_a?(Numeric))
    sleep(d)
  else
    $eps.push(External_proc::new(d[0],d[1]))
  end
end

Signal::trap('INT') do
  exit(0)
end
Signal::trap('HUP') do
  exit(0)
end

at_exit do
  interr()
end

loop do
  sleep(20)
  $eps.each do |ep|
    v=ep.is_dead?()
    if(v)
      loggo("#{ep.cls.to_s}/#{ep.pid} died! Must die to be reborn.")
      exit(0)
    end
  end
end
