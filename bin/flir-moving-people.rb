#!/usr/bin/env ruby
# moving_people_master.rb

=begin

16/12/2008 c.e prelz AS FLUIDO fluido@fluido.as
project .:laspal:.

Daemon to share the flir people motion tracker, and to run the update thread

$Id: moving_people_master.rb 2629 2010-11-11 12:30:36Z karl $

=end

require 'boij.so'
require 'moving_people'

SLEEPTIME=0.1
N_AREAS=2

class Moving_people_master
  def looper(obj)
    while(!obj.die)
      obj.looper_loop()
      sleep(SLEEPTIME)
    end
  end

  def initialize(n_areas,flirtalk=true)
    @mp=Moving_people::new(n_areas,flirtalk)
    loggo("Started (#{@mp})")
    Signal::trap('USR1') do
      @mp.save_values()
      true
    end

    DRb.start_service(MP_URI,@mp)
  end

  def runme
    @th=Thread::new(@mp) do |mpm|
      looper(mpm)
    end

    @th.join()
    DRb.thread.join()
  end
end

if($0==__FILE__)
  Fluid_logfile::new('mpm')
  Moving_people_master::new(1,true).runme
end
