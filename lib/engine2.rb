#!/usr/bin/env ruby
# engine2.rb

=begin

11/2/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

The engine that runs it all. Second version with resized images

$Id: engine2.rb 2410 2010-07-09 11:41:42Z karl $

=end

require 'boij'
require 'boij_xserver'
require 'image_dbase'

require 'sdl'

#FULLSCREEN_FLAG=false
FULLSCREEN_FLAG=true
EXIT_WITH_ESC=!FULLSCREEN_FLAG
AUDIO_FLAG=true

SOUNDFILE_ATTN=0.8 #0.5

GRIDF=false

class Engine
  BLACKFILL=Boij_sharedvideoarea::BLACKFILL_EXTEND
  #BLACKFILL=Boij_sharedvideoarea::BLACKFILL_MIRROR
  GRIDV_STEP=6
  GRIDH_STEP=GRIDV_STEP*4
  GRIDB=2
  GRC=[255,255,40]

  attr_reader(:audiotoy,:sounds,:b_i,:curloudspeaker,:pictures,:imarr_mutex)

  def initialize
    Fluid_logfile::new('engine')

    @idb=Image_dbase::new()
    @imarr_mutex=Mutex::new()
    reload_folder_tree()

    DRb::start_service(drb_uri(DRB_PORT_IMG),self)

    if(GRIDF)
      @gridr=[]
      sx=WINDOW_SIZE[0].to_f()/GRIDH_STEP
      (GRIDH_STEP+1).times do |x|
	@gridr.push([sx*x-GRIDB,0,GRIDB*2+1,WINDOW_SIZE[1]])
      end
      sy=WINDOW_SIZE[1].to_f()/GRIDV_STEP
      (GRIDV_STEP+1).times do |y|
	@gridr.push([0,sy*y-GRIDB,WINDOW_SIZE[0],GRIDB*2+1])
      end
    else
      @gridr=nil
    end

    if(FULLSCREEN_FLAG)
      @xsv=Boij_xserver::new(*SCREEN_SIZE)
      sleep(3)
      ENV['DISPLAY']=':0'
    else
      @xsv=nil
    end

    @b_i=Imager::new(*WINDOW_SIZE,HORIZON_POS)

    @sounds=[]
    if(AUDIO_FLAG)
      @audiotoy=Audiotoy::new()
      @sounds=[]
      Dir::entries(OPEN_SOUNDDIR).each do |f|
	next if(f[-4,4]!='.wav')
	@sounds.push(@audiotoy.load_file(OPEN_SOUNDDIR+f,SOUNDFILE_ATTN))
      end
    end

    @players=[]
    Boij_sharedvideoarea::MAX_MONITORED.times do |i|
      @players.push(Player::new(self,i))
    end

    @old_positions=nil
    @updatepos_flag=false
    @orphaned_images=[]
    @latest_pixels=nil

    @done=false
    Signal::trap('INT') do
      @done=true
    end
    Signal::trap('TERM') do
      @done=true
    end
    @curloudspeaker=0
#    Signal::trap('USR1') do
#      @curloudspeaker+=1
#      @curloudspeaker%=7
#      @audiotoy.fix_loudspeaker(@curloudspeaker==0 ? nil : @curloudspeaker-1)
#    end
    Signal::trap('USR1') do
      loggo("Request to reload")
      reload_folder_tree()
      true
    end
    Signal::trap('USR2') do
      take_snap()
      true
    end
  end

  def new_positions(pos)
    @new_positions=pos
  end

  def reload_folder_tree
    rootfolders,folders,images=Wfolder::load(@idb)
    np=[]
    rootfolders.each do |k,rf|
      recurse_add_image_from_folder(np,rf)
    end
    @imarr_mutex.synchronize do
      @pictures=np
    end
    loggo("Found #{@pictures.length} pictures")

    @pictures.sort!() do |a,b|
      a.prog<=>b.prog
    end
  end

  def recurse_add_image_from_folder(arr,folder)
    return if(!folder.used)
    folder.local_images.each do |im|
      arr.push(Picture::new(im.id,true))
    end
    folder.sons.each do |k,son|
      recurse_add_image_from_folder(arr,son)
    end
  end

  def runme
    SDL::init(SDL::INIT_VIDEO)
    if(FULLSCREEN_FLAG)
      @screen=SDL::Screen::open(*WINDOW_SIZE,24,SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME|SDL::FULLSCREEN)
      SDL::Mouse.warp(1,1)
      SDL::Mouse.hide
    else
      @screen=SDL::Screen::open(*WINDOW_SIZE,24,SDL::HWSURFACE|SDL::DOUBLEBUF)
    end

    nxt_refresh=Time::now()
    until(@done)
      event=SDL::Event2::poll()
      if(event)
	case event
        when SDL::Event2::Quit
          @done=true
        when SDL::Event2::KeyDown
	  case event.sym
          when SDL::Key::ESCAPE
	    @done=true if EXIT_WITH_ESC
#	  when 115
#	    take_snap()
	  end
	end
      end

      if(@new_positions)
        update_positions(@new_positions)
        @new_positions=nil
      end

      now=Time::now()
      imgs=[]+@orphaned_images
      @players.each do |p|
        if(p.arrival)
          p.start_new_image_if_needed_2(now)
          imgs+=p.owned_pictures
        end
      end

      imgs.sort!() do |a,b|
        a.start_time<=>b.start_time
      end

      to_send=[]
      imgs.each_with_index do |img,i|
        img.update_width(now)
        to_send.push([i,img.picture,img.start_position,img.width.to_i(),img.left_or_right]) if(img.width.to_i()>0)
      end

      if(to_send.length()>0)
        @latest_pixels,discardables=@b_i.screenmaker(to_send)

        discardables.each() do |i|
#          loggo("Discardo #{i}")
          img=imgs[i]

	  @audiotoy.del_chunk(img.achunk)

          if(!img.owner)
            @orphaned_images.delete(img)
          else
            img.owner.owned_pictures.delete(img)
          end
        end
      end

      refresh_screen()
      sleep(0.001)
    end

    @screen.destroy()
    @xsv.reap() if(@xsv)
  end

  def update_positions(positions)
    Boij_sharedvideoarea::MAX_MONITORED.times do |i|
      if(!positions[i])
        if(@players[i].arrival)
          loggo("Player #{i+1} left.")
          @orphaned_images+=@players[i].give_owned_pictures_and_leave()
        end
      else
        if(!@players[i].arrival)
          loggo("Player #{i+1} arrived.")
	  @players[i].image=nil
          @players[i].arrival=Time::now()
        end
        @players[i].position=positions[i]
      end
    end
  end

  def refresh_screen
    return if(!@latest_pixels)
    #    @screen.lock
    #    @screen.pixels=@latest_pixels
    #    @screen.unlock
    s=SDL::Surface::new_from(@latest_pixels,*SCREEN_SIZE,24,SCREEN_SIZE[0]*3,0xff0000,0xff00,0xff,0)
    @screen.put(s,0,0)
    s.destroy()
    if(@gridr)
      @gridr.each do |rect|
	@screen.fill(GRC,rect)
      end
    end
    @screen.flip()
  end

  def random_picture
    @imarr_mutex.synchronize do
      return @pictures[rand(@pictures.length())]
    end
  end

  def take_snap
    return if(!@latest_pixels)

    cmd='cjpeg > '+SNAPDIR+'s.'+Time::now().stamp()+'.jpg'
    IO::popen(cmd,'w') do |f|
      f.printf("P6\n%d %d\n255\n",*WINDOW_SIZE)
      f.print(Boij_sharedvideoarea::pixel_flip(@latest_pixels))
    end
  end
end

if($0==__FILE__)
#  lf=Fluid_logfile::new('engine')

  eo=Engine::new()
  begin
    eo.runme()
  rescue
    pp ["Engine crash",$!,$!.backtrace()]
  end
end
