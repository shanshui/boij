#!/usr/bin/env ruby
# thermalcontrast.rb

=begin

6/2/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Changes a couple of thermal camera settings
while watching output

$Id: thermalcontrast.rb 1499 2008-02-18 16:11:17Z karl $

=end

require 'boij'
require 'thermal'
require 'gtk2'

class Thermalcontrast
  LOC_CAPTURE_PARA=[
    [CAPTSIZE[0],CAPTSIZE[0],0,CAPTSIZE[0]],
    [CAPTSIZE[1],CAPTSIZE[1],0,CAPTSIZE[1]]
  ]

  def initialize
    videodata=[0,'Composite2','8 bpp gray',LOC_CAPTURE_PARA[0],LOC_CAPTURE_PARA[1],IMG_PARA]
    @sva=Boij_sharedvideoarea::new(videodata)
    @sva.set_threshold(THRESHPARA)
    
    grid=[]
    (Boij_sharedvideoarea::GRID_Y+1).times do |i|
      g=[]
      (Boij_sharedvideoarea::GRID_X+1).times do |i|
	g.push([0,0])
      end
      grid.push(g)
    end
    @sva.set_grid(grid,false,false)
    @sva.reload_grid_if_needed()

    @th=Thermal.new('/dev/ttyUSB0')
    
    therma_command(1,[])
  end
  
  def therma_command(cmd,para)
    @th.send_command(cmd,para)
    
    rd=nil
    loop do
      rd=@th.read()
      break if(rd)
    end
    rd.unpack('v*').each_with_index do |v,i|
      STDERR.printf("%d: <%.4x> -> %d\n",i,v,v)
    end

    rd
  end

  def runme
    Gtk::init()
    window=Gtk::Window.new("Boijmans thermatuner")
    main_hbox=Gtk::HBox.new(false,5)
    window.add(main_hbox)

    #
    # The drawing area
    #

    @da=Gtk::DrawingArea.new
    @da.set_size_request(*CAPTSIZE)

    main_hbox.pack_start(Gtk::VBox.new(false,5).pack_start(@da,false,false))
    
    @da.signal_connect('expose_event') do |w,e|
      repaint(w)
    end

    @gain_adj=Gtk::Adjustment.new(GAIN_VALUE,0,0xffff,1,10,0)
    @bias_adj=Gtk::Adjustment.new(BIAS_VALUE,-0x7fff,0x7fff,1,10,0)
    
    gainc=Gtk::SpinButton.new(@gain_adj,0,0)
    
    biasc=Gtk::SpinButton.new(@bias_adj,0,0)
    
    feedbtn=Gtk::Button.new("Feed")
    feedbtn.signal_connect("clicked") do
      update_values(@gain_adj.value(),@bias_adj.value())
    end

    savebtn=Gtk::Button.new("Save")
    savebtn.signal_connect("clicked") do
      update_values(@gain_adj.value(),@bias_adj.value(),true)
    end

    closebtn=Gtk::Button.new("Close")
    closebtn.signal_connect("clicked") do
      Gtk::main_quit()
    end
    
    main_hbox.pack_start(Gtk::VBox.new(false,5).pack_start(Gtk::Frame.new('Gain').add(gainc),false,false).
			 pack_start(Gtk::Frame.new('Bias').add(biasc),false,false).
			 pack_start(feedbtn,false,false).pack_start(savebtn,false,false).pack_start(closebtn,false,false))
    
    window.signal_connect("destroy") do
      Gtk::main_quit()
    end
    
    window.show_all()
    
    Gtk::timeout_add(40) do
      timeout_func()
      true
    end

    Gtk::main()
  end
  
  def update_values(gain,bias,savef=false)
    therma_command(156,[0,0,gain,bias,0,0,0,0,0,0,0,0]) if(savef)
    therma_command(156,[1,0,gain,bias,0,0,0,0,0,0,0,0])
  end
  
  def repaint(w)
    return if(!@rpix || w.allocation.width<CAPTSIZE[0] || w.allocation.height<CAPTSIZE[1])
    
    gc=Gdk::GC::new(w.window)
    
    pixbuf=Gdk::Pixbuf::new(@rpix,Gdk::Pixbuf::COLORSPACE_RGB,
			    false,8,CAPTSIZE[0],CAPTSIZE[1],CAPTSIZE[0]*3)
    w.window.draw_pixbuf(gc,pixbuf,0,0,0,0,CAPTSIZE[0],CAPTSIZE[1],Gdk::RGB::DITHER_NORMAL,0,0)    
  end

  def timeout_func
    ret=@sva.latest_framedata()
    if(ret)
      pix=@sva.get_frame()

      if(pix)
	@rpix=Boij_sharedvideoarea::triplicate(pix)
	@da.queue_draw()
      end
    end
  end
end

tc=Thermalcontrast::new()

begin
  tc.runme()
rescue
  pp ["TC crash",$!,$!.backtrace()]
end
