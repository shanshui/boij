# asfluidities.rb

=begin

29/12/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>

Classes and stuff used in many projects...

$Id: asfluidities.rb 3798 2012-11-17 17:29:33Z karl $

=end

require 'net/smtp'
require 'base64'
require 'drb/drb'
require 'pp'
require 'fileutils'
require 'timeout'

MIN_FREE_SPACE=0.05

def loggo(msg)
  STDERR.puts('['+Time::now().stamp+'] '+msg)
  STDERR.flush()
end

def drb_uri(port)
  "druby://127.0.0.1:#{port}"
end

def partition_occupation(fn)
  b=' '*128
  syscall(137,fn,b)
  a=b.unpack('QQQQQ')
  [a[2],a[3],a[4],(a[2]-a[4]).to_f/a[2]]
end

def most_empty_partition(parts)
  ar=parts.map do |p|
    [p,partition_occupation(p)[3]]
  end.sort do |a,b|
    a[1]<=>b[1]
  end

  raise "No partition under threshold!" if((1.0-ar[0][1])<MIN_FREE_SPACE)

  ar[0][0]
end

def is_alive(pid)
  begin
    Process::getpgid(pid)
  rescue Errno::ESRCH
    return false
  end
  true
end

def remove_stale(pid,sig='KILL')
  alive=is_alive(pid)
  return false unless(alive)

  loggo("Trying to kill stale pid #{pid}")
  begin
    Process::kill(sig,pid)
    Process::waitpid(pid)
  rescue =>err
    loggo("Stale kill unsuccessful! (#{err.to_s()})\n")
  end
  true
end

class Time
  def stamp
    #    sprintf("%.2d%.2d%.2d.%.2d%.2d%.2d",year%100,month,mday,hour,min,sec)
    strftime('%y%m%d.%H%M%S')
  end
  def longstamp
    sprintf("%4.4d.%2.2d.%2.2d %2.2d:%2.2d:%2.2d",year,month,mday,hour,min,sec)
  end
  def spacestamp
    sprintf("%.2d%.2d%.2d %.2d%.2d%.2d",year%100,month,mday,hour,min,sec)
  end

  def mydesc
    strftime('%d %b %y, %T')
  end

  def timestamp
    strftime('%R')
  end
  def datestamp
    strftime('%y%m%d')
#    strftime('%a %d %b %Y')
  end

  def ddstamp
    strftime('%Y%m%d%u%H%M%S')
  end

  def distance(other)
    d=((other-self)/60).to_i
    return "#{d} minutes ago" if(d<60)
    d/=60
    return "#{d} hours ago" if(d<24)
    mydesc()
  end

  def distance2(other)
    minutes=((other-self)/60).to_i
    "#{minutes/1440}d#{(minutes%1440)/60}h#{minutes%60}m"
  end
end

class Date
  def mytag
    sprintf("%4.4d-%2.2d-%2.2d",year,month,day)
  end
end

class Array
  def x=(v)
    self[0]=v
  end
  def y=(v)
    self[1]=v
  end
  def z=(v)
    self[2]=v
  end

  def x
    self[0]
  end
  def y
    self[1]
  end
  def z
    self[2]
  end

  def randomsort
    na=[]
    oa=[]+self
    while(oa.length>0)
      na.push(oa.slice!(rand(oa.length)))
    end
    na
  end

  def seqprint
    return "NONE" if(self.length<1)
    s=''
    a=self.sort
    beg=false
    a.length.times do |i|
      unless(beg)
        beg=i
        next
      end
      if((a[i]-a[i-1])>1)
        s+=', ' if(s.length>0)
        if(beg==i-1)
          s+=a[beg].to_s
        else
          s+="#{a[beg]}-#{a[i-1]}"
        end
        beg=i
      end
    end
    s+=', ' if(s.length>0)
    if(beg==a.length-1)
      s+=a[beg].to_s
    else
      s+="#{a[beg]}-#{a[length-1]}"
    end

    s
  end
end

class Range
  attr_accessor(:diff,:fact)

  def clamp(v)
    v=[self.begin,v].max
    [self.end,v].min
  end

  def normalize(v)
    begin
      return (clamp(v)-self.begin)*self.fact
    rescue
    end
    false
  end

  def randv
    self.begin+rand()*self.diff
  end

  def at_v(v)
    self.begin+self.diff*v
  end

  def Range::my_init(b,e)
    r=Range::new(b,e)
    r.diff=e-b
    r.fact=1.0/r.diff

    r
  end
end

class Fluid_logfile
  def initialize(name)
    path=LOGBASE+name+'.'+Process::pid().to_s()+'.'+Time::now.stamp()+'.log'

    STDERR.reopen(path,'w')
    STDERR.sync=true
    STDOUT.reopen(STDERR)
    STDOUT.sync=true

    #
    # Save the latest PID for this type
    #

    File::open(PIDBASE+name+'.pid','w') do |f|
      f.write([Process::pid()].pack('L'))
    end
  end

  def Fluid_logfile::getpid(name)
    File::open(PIDBASE+name+'.pid','r') do |f|
      ((f.read(Process::pid())).unpack('L'))[0]
    end
  end

  def Fluid_logfile::killpid(name,signal='INT')
    pid=Fluid_logfile::getpid(name)
    loggo("Trying to kill pid #{pid} (#{name})")
    begin
      Process::kill(signal,pid)
      Process::waitpid(pid)
    rescue =>er
#      loggo("Unsuccessful! (#{er.to_s()})\n")
    end
  end

  def Fluid_logfile::isalive(name)
    begin
      pid=Fluid_logfile::getpid(name)
      loggo("#{name} -> #{pid}")
      Process::getpgid(pid)
      return true
    rescue
      false
    end
  end
end

class External_proc
  attr_reader(:cls,:pid)

  def initialize(cls,paras=[])
    @cls=cls
    @pid=fork() do
      Fluid_logfile::new(PROJECT_LABEL+'_'+@cls.name)
      cv=@cls::new(*paras)
      begin
        cv.runme()
      rescue
        loggo("#{cls.name} crash: #{$!}\n#{$!.backtrace()}")
      end
      exit(0)
    end
  end

  def expire
    loggo("Expire #{@pid}")
    Process::kill('INT',@pid)
    loggo("Wait #{@pid}")
    begin
      Timeout.timeout(3) do
        Process::waitpid(@pid)
      end
    rescue
      loggo("Kill #{@pid}")
      Process::kill('KILL',@pid)
    end
    loggo("Done (#{@pid})")
  end

  def is_dead?
    pidr=Process::wait(@pid,Process::WNOHANG)
    return pidr ? $? : false
  end
end

class Expo
  def initialize(v1,v2)
    @v1,@v2=v1,v2
    @vd=@v2-@v1
    @ve1=Math::exp(@v1)
    @ve2=Math::exp(@v2)
    @ved=@ve2-@ve1
  end

  def calc(v)
    (Math::exp(v*@vd+@v1)-@ve1)/@ved
  end
end

class Emailer
  LOCAL_ADDRESS='fluido.as'
  MAILSERVER='10.101.0.1'
  SENDER="donotreply@#{LOCAL_ADDRESS}"
  BOUNDA='zottaZOTTAzotta123'
  WDAY=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
  MONTH=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

  def Emailer::time_to_emailformat(tm)
    # [ruby-list:7928]
    gmt=Time.at(tm.to_i)
    gmt.gmtime
    offset=tm.to_i-Time.local(*gmt.to_a[0,6].reverse).to_i

    # DO NOT USE strftime: setlocale() breaks it
    sprintf('%s, %s %s %d %02d:%02d:%02d %+.2d%.2d',
            WDAY[tm.wday], tm.mday, MONTH[tm.month-1],
            tm.year, tm.hour, tm.min, tm.sec,
            *(offset / 60).divmod(60))
  end

  def Emailer::mailtext(rec,subj,txt,parts)
    receivers=rec.split(',')
    receivers.each do |r|
      msg="Date: "+time_to_emailformat(Time::now)+"\n"+
        "From: <#{SENDER}>\n"+
        "To: <#{r}>\n"+
        "Subject: #{subj}\n"+
        "MIME-Version: 1.0\n"+
        "Content-Type: multipart/mixed; boundary=\"#{BOUNDA}\"\n"+
        "Content-Disposition: inline\n"+
        "User-Agent: EVOTEXTOMAT\n\n\n"+
        "--#{BOUNDA}\n"+
        "Content-Type: text/plain; charset=us-ascii\n"+
        "Content-Disposition: inline\n\n"+
        txt

      parts.each do |p|
        m=nil
        File::open(p[2],'r') do |f|
          m=f.read()
        end
        msg+="\n"+
          "--#{BOUNDA}\n"+
          "Content-Type: #{p[0]}\n"+
          "Content-Transfer-Encoding: base64\n"+
          "Content-Disposition: attachment; filename=#{p[1]}\n"+
          "\n"+Base64::encode64(m)
      end if(parts)

      smtp=Net::SMTP.new(MAILSERVER,25)
      smtp.start do
        smtp.send_mail(msg,SENDER,r)
      end
    end
  end
end

class PObox
  def initialize(func)
    @func=func
  end

  def newstuff(stuff)
    @func.call(stuff)
  end

  def PObox::fire_up(port,func)
    mr=PObox::new(func)
    uri=sprintf(POBOX_URI,port)
    DRb::start_service(uri,mr)
    mr
  end

  def PObox::link_to(port)
    DRb::start_service()
    uri=sprintf(POBOX_URI,port)
    begin
      return DRbObject::new(nil,uri)
    rescue => err
      loggo("Could not link to port #{port} (#{err})")
    end
    nil
  end
end

class Machine_type
  attr_reader(:hostname,:address,:daemons)

  def initialize(hostname,address,daemons)
    @hostname,@address,@daemons=hostname,address,daemons
  end
end

#
# To read/write conf files
# Object must have a conf_name, and conf_save and conf_restore functions
#

class Confluido
  MAX_TO_HOLD=20
  ENDLABEL='ENDENDENDEND'

  def initialize(label,objects)
    @label=label

    @objects={}
    objects.each do |ob|
      @objects[ob.conf_name]=ob
    end
  end

  def save
    File::unlink(CONFBASE+@label+".v#{MAX_TO_HOLD}") if(File::exists?(CONFBASE+@label+".v#{MAX_TO_HOLD}"))
    MAX_TO_HOLD.downto(1) do |i|
      FileUtils::mv(CONFBASE+@label+".v#{i-1}",CONFBASE+@label+".v#{i}",:force => true)
    end
    FileUtils::mv(CONFBASE+@label,CONFBASE+@label+".v1",:force => true)

    File::open(CONFBASE+@label,'w') do |f|
      f.write("# #{CONFBASE+@label}\n#\n# Generated by Confluido on #{Time::now.mydesc()}\n#\n")
      @objects.each() do |key,value|
        material=value.conf_save()
        material.each() do |k,v|
          f.write("#{key}_#{k}##{v}\n")
        end
        f.write("#{key}_#{ENDLABEL}\n")
      end
    end
  end

  def restore
    if(!File::exists?(CONFBASE+@label))
      loggo("Restore file for #{@label} not found! Default values kept.")
      return false
    end

    arr={}
    ob=nil
    oldlabel=nil

    File::readlines(CONFBASE+@label).each do |line|
      next if(line[0,1]=='#')
      line.chomp!()
      undersc_pos=line.index('_')
      next if(!undersc_pos)
      label=line[0,undersc_pos]
      cont=line[undersc_pos+1..-1]

      if(label!=oldlabel)
        oldlabel=label
        ob=@objects[label]
        loggo("Warning: found material for unknown object with label #{label} in conf file for #{@label}") if(!ob)
      end

      if(cont==ENDLABEL)
        ob.conf_restore(arr) if(ob)
        arr={}
        ob=nil
      else
        hash_pos=cont.index('#')

        arr[cont[0,hash_pos]]=cont[hash_pos+1..-1]
      end
    end
  end
end

DRB_LOAD_LIMIT=90214400
DRb::DRbServer::default_load_limit(DRB_LOAD_LIMIT)

class Drb_linked_process
  attr_accessor(:breaker,:local,:cond)
  attr_reader(:sockpath)

  COND_IDLE=0
  COND_WORKING=1
  COND_DONE=2
  COND_RESERVED=3
  LOOP_SLEEP=0.2

  def initialize(sockpath,logfile=nil)
    @sockpath,@logfile=sockpath,logfile
    @breaker=false

    begin
      File::unlink(@sockpath)
    rescue
    end

    @pid=fork do
      Fluid_logfile::new(@logfile) if(@logfile)
      @cond=COND_IDLE

      DRb::start_service('drbunix:'+@sockpath,self,{:load_limit => DRB_LOAD_LIMIT})
      method('runner').to_proc().call()
      exit(0)
    end

    DRb::start_service()
    @local=DRbObject::new(nil,'drbunix:'+@sockpath)

    at_exit do
      begin
        stop!()
        File::unlink(@sockpath)
      rescue
      end
      loggo("Closing...")
    end

    loop do
      alive=false
      begin
        alive=@local.respond_to?(:give_work)
      rescue
      end
      break if(alive)
      loggo("#{@sockpath}: waiting for connection...")
      sleep(0.75)
    end
  end

  def runner
    until(@breaker)
      while(@cond!=COND_WORKING)
        sleep(LOOP_SLEEP)
      end
      local_runner()
    end
    loggo("Have broken out")
  end

  def give_work
    raise "NOT IDLE!! (cond #{@cond})" if(@cond!=COND_IDLE && @cond!=COND_RESERVED)
    @cond=COND_WORKING
  end

  def reset
    @cond=COND_IDLE
  end

  def work_completed
    @cond=COND_DONE
  end

  def reserve
    @cond=COND_RESERVED
  end

  def idle?
    @cond==COND_IDLE
  end

  def loaded?
    @cond!=COND_IDLE
  end

  def data_ready?
    @cond==COND_DONE
  end

  def stop!
    begin
      Process::kill('HUP',@pid)
      Process::waitpid(@pid)
    rescue
    end
  end

  def other_local
    DRb::start_service()
    DRbObject::new(nil,'drbunix:'+@sockpath)
  end
end

class Fluid_tag
  def initialize(basepath)
    @basepath=basepath
    @runner=@basepath+'runner'
    @sealer=@basepath+'seal'
  end

  def is_running?(remove_stale=true)
    return false unless(File::file?(@runner))
    pid=File::open(@runner,'r') do |f|
      f.read.unpack('S')[0]
    end

    return pid unless(remove_stale)

    begin
      Process::getpgid(pid)
      return pid
    rescue Errno::ESRCH
    end
    loggo("stale running tag for #{@basepath}!")
    File::unlink(@runner)
    false
  end

  def is_sealed?
    #    loggo("Searching for #{@sealer}:#{File::file?(@sealer)}")
    return false unless(File::file?(@sealer))
    File::open(@sealer,'r') do |f|
      Time::at(f.read().unpack('L')[0])
    end
  end

  def run(pid)
    File::open(@runner,'w') do |f|
      f.write([pid].pack('S'))
    end
  end

  def seal
    File::open(@sealer,'w') do |f|
      f.write([Time::now.to_i].pack('L'))
    end
  end

  def unrun
    File::unlink(@runner) if(File::file?(@runner))
  end

  def unseal
    File::unlink(@sealer) if(File::file?(@sealer))
  end
end

begin
  DIRS_TO_CREATE.each do |d|
    FileUtils::mkdir_p(d) if(!File::directory?(d))
  end
rescue
end
