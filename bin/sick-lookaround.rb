#!/usr/bin/env ruby
# lookaround.rb

=begin

20/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

Monitors the playground, as detected by the Sick camera

$Id$

=end

require 'sick/lookaround'

Lookaround::new().runme()
