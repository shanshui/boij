/* boij.h */

/*
 * 17/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * generic defines
 *
 * $Id: boij.h 3451 2012-03-16 08:15:02Z karl $
 */

#ifndef BOIJ_H
#define BOIJ_H

/*
 * This one to stop compiler from complaining that
 * we include kernel headers directly
 */

#define __EXPORTED_HEADERS__

#include <ruby.h>
#include <math.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <linux/types.h>
//#include <sane/sane.h>

#define MAX_SMOOTH_COMPONENTS 4
//#define N_SMOOTHS 16
#define N_SMOOTHS 0

#define MAX_MONITORED 10 //16
#define N_THRESHS 5

#define GRID_X 9
#define GRID_Y 3
#define MAX_GRID_OFFSET 200

#define IPC_KEY 0xbaba6ba

enum blackfill
{
  BLACKFILL_NONE,
  BLACKFILL_EXTEND,
  BLACKFILL_MIRROR
};

typedef struct
{
  __u8 produced,request_grid_recalc,togx,togy;
  __u16 x,y;
  __s16 grid_offsets[GRID_Y+1][GRID_X+1][2];
  __u32 framesize;
  float threshs[N_THRESHS],horizpos;
//  client_stc clients[MAX_MONITORED];
} shared_stc;

enum group_cond
{
  GCOND_ABSENT,
  GCOND_PRESENT,
  GCOND_JUSTLEFT
};

extern const char *thresh_labels[N_THRESHS];
extern void loggo(const char *fmt,...);

#endif
