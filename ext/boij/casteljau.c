/* casteljau.c */

/*
 * 13/04/2005 c.e. prelz AS FLUIDO <fluido@fluido.as>
 *
 * Project CF=LOUNGE
 *
 * My first ruby-callable C code!
 * The casteljau routines for spline generation
 *
 * $Id: casteljau.c 1884 2009-02-16 10:49:26Z karl $
 */

#include "boij.h"

typedef struct
{
  double *steps;
  int n_steps;
} casteljau_each_stc;

typedef struct
{
  casteljau_each_stc *each;
  int ord,n;
} casteljau_stc;  

VALUE cls_casteljau;

static void free_cas(void *p);

VALUE new_casteljau(VALUE self,VALUE arr)
{
  int len=RARRAY_LEN(arr),i,j;
  
  if(len<=0)
    return Qnil;
  
  casteljau_stc *cas=ALLOC(casteljau_stc);
  
  bzero(cas,sizeof(casteljau_stc));

  cas->ord=len;
  cas->each=malloc(sizeof(casteljau_each_stc)*cas->ord);

  for(i=0;i<cas->ord;i++)
  {
    VALUE v=rb_ary_entry(arr,i);

    cas->each[i].n_steps=RARRAY_LEN(v);
    cas->each[i].steps=malloc(sizeof(double)*cas->each[i].n_steps*2);
    for(j=0;j<cas->each[i].n_steps;j++)
      cas->each[i].steps[j]=NUM2DBL(rb_ary_entry(v,j));
  }
  
  return Data_Wrap_Struct(cls_casteljau,NULL,free_cas,cas);
}

static void free_cas(void *p)
{
  casteljau_stc *c=(casteljau_stc *)p;
  int i;

  for(i=0;i<c->ord;i++)
    free(c->each[i].steps);
  free(c->each);

  free(c);
}

VALUE casteljau_calc(VALUE self,VALUE vpos)
{
  casteljau_stc *cas;
  double pos=NUM2DBL(vpos);
  VALUE ret_arr=rb_ary_new();
  int i,j,k;

  Data_Get_Struct(self,casteljau_stc,cas);

  for(i=0;i<cas->ord;i++)
  {
    casteljau_each_stc *e=&cas->each[i];
    double *dp=e->steps+e->n_steps;
    
    memcpy(dp,e->steps,sizeof(double)*e->n_steps);

    for(j=1;j<e->n_steps;j++)
      for(k=0;k<e->n_steps-j;k++)
	dp[k]=(1.0-pos)*dp[k]+pos*dp[k+1];
    
    rb_ary_push(ret_arr,rb_float_new(dp[0]));
  }
  
  return ret_arr;
}

