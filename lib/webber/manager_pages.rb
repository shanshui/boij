# manager_pages.rb

=begin

05/07/2010 c.e.prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The page methods for the image database manager webserver

$Id: manager_pages.rb 3448 2012-03-14 11:13:45Z karl $

=end

I_NICK='P1'
I_PWD='P2'
I_LOGOUT='P3'
I_NEWFOLDER='P4'
I_DELETEFOLDER='P5'
I_CONFIRM='P6'
I_FORGET='P7'
I_ACTIVATE='P8'
I_DEACTIVATE='P9'
I_DELETEIMAGE='P10'
I_PATH='P11'
I_MD5='P12'
I_HORPOS='P13'
I_FOLDER='P14'
I_SET_AS_DEFAULT='P15'
I_CLEAN_LIST='P16'
I_DEFFOLDER='P17'
I_ROTRIGHT='P18'
I_ROTLEFT='P19'
I_RESTARTPROC='P20'

def process_req(req)
  cookies={}
  req.cookies.each do |ck|
    cookies[ck.name]=ck.value
  end

  paras=req.query.update(WEBrick::HTTPUtils::parse_query(req.query_string))
  
  [cookies,paras]
end

def process_sess_req(req,res)
  cookies,paras=process_req(req)
  
  return errpage(res,"Cookies must be activated!") if(!cookies['sid'])

  sess=@sessions[cookies['sid']]
  return errpage(res,"Sorry! Session has expired!") if(!sess)
  
  sess.touch()

  [cookies,paras,sess]
end

def header(res,title,sess,options=nil)
  res.header['content-type']='text/html; charset=UTF-8'
  res.body+=<<EOF
<html><head><title>#{title}</title>
<style type=\"text/css\">
#paradiv
{
text-align: center;
border-style: dashed;
border-width: medium;
border-color: red;
margin-bottom: 25pt;
}
#titlediv
{
font-size: 25pt;
text-align: center;
font-weight: bold;
color: blue;
background-color: orange;
margin-bottom: 25pt;
}
#paramessage
{
text-align: center;
font-weight: bold;
font-size: 20pt;
border: thick solid red;
}
#sidebar
{
position:fixed;
width:15%;
height:100%;
top:0;
right:auto;
bottom:0;
left: 0;
border: 2px solid #0091FF;
background-color: white;
padding:15px;
}
#sidemessage
{
text-align: center;
font-weight: bold;
font-size: 15pt;
border: 2px solid #0091FF;
margin: 4px;
}
#mainbar
{
position:absolute;
width:auto;
height:auto;
top:0;
right:0;
bottom:auto;
left:20%;
padding:15px;
}
</style>
<script type="text/javascript" src="material/jsDraw2D.js"></script>
</head><body id="zbody">
<script type="text/javascript">
<!--
function waitclean(e)
{
//  alert("e: "+e);
  document.write("Wait!")
//  document.getElementById("zform").submit();
//  document.getElementById("zbody").innerHTML="<h1>WAIT!</h1>";
}
//-->
</script>
<noscript>
 <b>You don't have JavaScript enabled or your browser does not support JavaScript</b>
</noscript>
EOF
  res.body+=sidebar(options) if(options)
  res.body+="<div id=\"mainbar\">"
  if(sess && sess.message)
    res.body+="<div id=\"paramessage\">#{sess.message}</div>"
    sess.message=nil
  end
  res.body+="<center><h1>#{title}</h1></center>"
end

def footer(res,status)
  res.status=status
  res.body+="</div></body></html>"
end

def sidebar(options)
  s="<div id=\"sidebar\">"
  if(options.is_a?(Array))
    options.each do |op|
      if(!op)
        s+="<hr/>"
      elsif(op.is_a?(Array))
        label,url=op
        s+="<div id=\"sidemessage\"><a href=\"#{url}\">#{label}</a></div>"
      elsif(op.is_a?(String))
        s+="<div id=\"sidemessage\">#{op}</div>"
      else
        raise "What? Sidebar received a #{op.class}!"
      end      
    end
  elsif(options.is_a?(String))
    s+=options
  end
  s+='</div>'
  s
end

def errpage(res,msg,sess=nil)
  header(res,'Error!',sess)
  res.body+="<h1>#{msg}</h1>"
  if(sess)
    res.body+="<p><a href=\"/session\">BACK</a></p>"
  else
    res.body+="<p><a href=\"/\">HOME</a></p>"
  end
  footer(res,WEBrick::HTTPStatus::RC_NOT_ACCEPTABLE)
  false
end

def rootpage(req,res)
  cookies,paras=process_req(req)

  if(paras[I_NICK] && paras[I_PWD])
    user=@db.verify_user(paras[I_NICK],paras[I_PWD])
    return errpage(res,"Bad nick or passwd (nick #{paras[I_NICK]})") if(!user)

    @sessions.each do |k,v|
      return errpage(res,"A session for your user (#{user.id}) is already open!") if(v.user.id==user.id)
    end
    new_sess=Wsession::new(@db,user)
    @sessions[new_sess.md5]=new_sess
    res.cookies.push(WEBrick::Cookie::new('sid',new_sess.md5))
    
    res.cookies.push(WEBrick::Cookie::new('folder',@default_folder.md5)) if(!cookies['folder'] || !@folders[cookies['folder']])
    
    res.set_redirect(WEBrick::HTTPStatus::Found,'/session')
    return
  end
  
  header(res,'Login',nil)
  res.body+="<form action=\"/\" method=post><p>User name: <input type=\"text\" name=\"#{I_NICK}\" /></p>\
<p>Password: <input type=\"password\" name=\"#{I_PWD}\" /></p><p><button type=submit>Submit</button></p>"  
  footer(res,WEBrick::HTTPStatus::RC_OK)
end

def recurse_add_table_cont(folder,indentlevel,str,defaultfolder)
  str+=<<EOF
<tr><form action="/folder?#{I_FOLDER}=#{folder.md5}" method=post>
<td>#{("&nbsp;"*indentlevel*6)+folder.name}&nbsp;#{folder.local_images.length} Images.&nbsp;&nbsp;#{folder.used ? 'ACTIVE' : '<em>INACTIVE</em>'}</td>
<td><button type="submit">Manage</button></td>
<td>#{defaultfolder==folder ? 'Default folder for saving' : '<button type="submit" name="'+I_SET_AS_DEFAULT+'">Set as default folder for saving</button>'}
</td></form></tr>
EOF
  folder.sons.values.each do |son|
    str=recurse_add_table_cont(son,indentlevel+1,str,defaultfolder)
  end
  #  loggo("After #{folder.name}: #{str}")
  str
end
  
def sesspage(req,res)
  cookies,paras,sess=process_sess_req(req,res)
  return if(!cookies)

  if(paras[I_RESTARTPROC])
    system('/home/karl/svn/geert/boij/raalte_stopper.rb')
    sess.message='Restart is now underway'
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
    return
  end
    
  if(paras[I_LOGOUT])
    @sessions.delete(sess.md5)
    res.set_redirect(WEBrick::HTTPStatus::Found,'/')
    return
  end

  if(paras[I_NEWFOLDER])
    nf=Wfolder::new(@db,paras[I_NEWFOLDER],true,nil)
    cn=nf.complete_name()
    fid=@db.get_folder(cn,true)
    loggo("New root folder created #{cn} (gets #{fid})")
    nf.set_id(fid)
    @rootfolders[nf.md5]=nf
    ask_engine_to_reload()
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
    return
  end
  
  header(res,"Session page (user #{sess.user.nick})",sess,[['Restart display process',"/session?#{I_RESTARTPROC}"],
                                                           ['Log out',"/session?#{I_LOGOUT}"]])
  res.body+=<<EOF
<div id=\"paradiv\"><div id=\"titlediv\">Uploading of new images</div>
<form id=\"zform\" enctype=\"multipart/form-data\" action=\"/digest_images\" method=\"post\">\
<p><input id=\"zinput\" name=\"#{I_PATH}\"type=\"file\" multiple=\"1\" /></form></div>

<script type="text/javascript">
<!--
var form=document.getElementById("zform");
var inp=document.getElementById("zinput");
inp.onchange=zfunc;
function zfunc(e)
{
  form.submit();
  document.getElementById("zbody").innerHTML="<h1>WAIT!</h1>";
}
//-->
</script>
<noscript>
 <b>You don't have JavaScript enabled or your browser does not support JavaScript</b>
</noscript>
EOF

  pi=parked_images()
  res.body+="<div id=\"paradiv\"><div id=\"titlediv\">Management of parked images</div>\
<form action=\"/process_parked\">There are #{pi.length} parked images. \
Press <input type=\"submit\" value=\"HERE\"> to manage them. \
Press <input type=\"submit\" value=\"HERE\" name=\"#{I_CLEAN_LIST}\"> to clean the list. </form></div>"  if(pi)    
    
  res.body+="<div id=\"paradiv\"><div id=\"titlediv\">Folder management</div>\
<center><table border=\"1\"><tr><th>Title</th><th>&nbsp;</th><th>&nbsp;</th></tr>"
  str=''
  @rootfolders.values.sort do |a,b|
    a.name<=>b.name
  end.each do |f|
    str=recurse_add_table_cont(f,0,str,@folders[cookies['folder']])
  end
  res.body+=str+"</table></center>\
<p><form action=\"/session\" method=\"post\">Add new root folder: \
<input type=\"text\" name=\"#{I_NEWFOLDER}\" /> (do not use /)</p></div>"
  footer(res,WEBrick::HTTPStatus::RC_OK)
end

def folderpage(req,res)
  cookies,paras,sess=process_sess_req(req,res)
  return if(!cookies)

  if(!paras[I_FOLDER] || !@folders[paras[I_FOLDER]])
    sess.message='No valid folder tag!'
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
  end
  
  folder=@folders[paras[I_FOLDER]]

  if(paras[I_SET_AS_DEFAULT])
    res.cookies.push(WEBrick::Cookie::new('folder',folder.md5))
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
    return
  end

  if(paras[I_DELETEFOLDER])
    if(paras[I_FORGET])
      res.set_redirect(WEBrick::HTTPStatus::Found,"/folder?#{I_FOLDER}=#{folder.md5}")
      return
    end
    if(paras[I_CONFIRM])
      sess.message="Folder #{folder.complete_name} has been deleted."
      parent=folder.parent
      folder.delete_me!()
      reload_folder_tree()
      ask_engine_to_reload()
      res.set_redirect(WEBrick::HTTPStatus::Found,parent ? "/folder?#{I_FOLDER}=#{parent.md5}" : '/session')
      return
    end
    header(res,"About to delete folder #{folder.complete_name}",sess)
    res.body+="<center><h3>This folder contains #{folder.sons.length} subfolders, and it and its subfolders contain \
#{folder.n_images} images.</h3><h2>Ok to delete?</h2><form action=\"/folder?#{I_FOLDER}=#{folder.md5}&#{I_DELETEFOLDER}=\"1\"\" method=\"post\">\
<p><input type=\"submit\" name=\"#{I_CONFIRM}\" value=\"DELETE\"/></p>\
<p><input type=\"submit\" name=\"#{I_FORGET}\" value=\"Forget it\"/></p></form></center>"
    footer(res,WEBrick::HTTPStatus::RC_OK)
    return
  end

  if(paras[I_NEWFOLDER])
    nf=Wfolder::new(@db,paras[I_NEWFOLDER],folder.used,folder)
    cn=nf.complete_name()
    fid=@db.get_folder(cn,true)
    loggo("New folder created #{cn} (gets #{fid})")
    nf.set_id(fid)
    ask_engine_to_reload()
    res.set_redirect(WEBrick::HTTPStatus::Found,"/folder?#{I_FOLDER}=#{folder.md5}")
    return
  end
  
  if(paras[I_ACTIVATE] || paras[I_DEACTIVATE])
    folder.set_active!(paras[I_ACTIVATE] ? true : false)
    ask_engine_to_reload()
    res.set_redirect(WEBrick::HTTPStatus::Found,"/folder?#{I_FOLDER}=#{folder.md5}")
    return    
  end

  options=[          
           [(folder.used ? 'Deactivate' : 'Activate')+' folder',"/folder?#{I_FOLDER}=#{folder.md5}&"+
            (folder.used ? I_DEACTIVATE : I_ACTIVATE)],
           ['Erase folder',"/folder?#{I_FOLDER}=#{folder.md5}&#{I_DELETEFOLDER}"],
           "<form action=\"/folder?#{I_FOLDER}=#{folder.md5}\" method=\"post\">Add new subfolder: \
<input type=\"text\" name=\"#{I_NEWFOLDER}\" /> (do not use /)</form>",
           nil]
  
  if(folder.sons.length>0)
    options.push('Go to subfolders:')
    folder.sons.values.sort do |a,b|
      a.name<=>b.name
    end.each do |sf|
      options.push(["#{sf.name} #{sf.local_images.length()} Images, |#{sf.used ? 'ACTIVE' : 'INACTIVE'}|",
                    "/folder?#{I_FOLDER}=#{sf.md5}"])
    end
    options.push(nil)
  end

  options+=[["Go to parent folder (#{folder.parent.name})","/folder?#{I_FOLDER}=#{folder.parent.md5}"],nil]  if(folder.parent)
  options.push(['Back to session page','/session'])
  
  
  header(res,"Folder #{folder.complete_name}",sess,options)

  res.body+="<center>"
  
  res.body+="<form action=\"/folder?#{I_FOLDER}=#{folder.md5}\" method=\"post\">\
<p>Currently #{folder.used ? 'active' : 'INACTIVE'}</p></form>"

  if(folder.local_images.length>0)
    res.body+="<h3>Local images</h3><p><form action=\"/image\" method=\"post\" >"
    folder.local_images.each do |img|
      res.body+="<button name=\"#{I_MD5}\" value=\"#{img.md5}\" type=\"submit\">\
<img src=\"/material/small/#{img.md5}.png\"/></button>"
    end
    res.body+='</form></p><hr/>'
  end

  footer(res,WEBrick::HTTPStatus::RC_OK)
end

def imagepage(req,res)
  cookies,paras,sess=process_sess_req(req,res)
  return if(!cookies)

  if(!paras[I_MD5] || !@images[paras[I_MD5]])
    sess.message="No valid image tag!"
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
  end

  image=@images[paras[I_MD5]]

  if(paras[I_HORPOS])
    @loaded_images[image.md5]=[Woldimage::new(image),false]
    res.set_redirect(WEBrick::HTTPStatus::Found,"/sethor_image?#{I_MD5}=#{image.md5}")
    return
  end
    
  if(paras[I_DELETEIMAGE])
    if(paras[I_FORGET])
      res.set_redirect(WEBrick::HTTPStatus::Found,"/image?#{I_MD5}=#{image.md5}")
      return
    end
    if(paras[I_CONFIRM])
      folder_md5=image.folder.md5
      image.delete_me!()
      reload_folder_tree()
      ask_engine_to_reload()
      res.set_redirect(WEBrick::HTTPStatus::Found,"/folder?#{I_FOLDER}=#{folder_md5}")
      return
    end
    header(res,"About to delete image ##{image.id} (from #{image.folder.complete_name})",sess)
    res.body+="<center><p><img src=\"/material/medium/#{image.md5}.png\"/></p><h2>Ok to delete?</h2>\
<form action=\"/image?#{I_MD5}=#{image.md5}&#{I_DELETEIMAGE}=\"1\"\" method=\"post\">\
<p><input type=\"submit\" name=\"#{I_CONFIRM}\" value=\"DELETE\"/></p>\
<p><input type=\"submit\" name=\"#{I_FORGET}\" value=\"Forget it\"/></p></form></center>"
    footer(res,WEBrick::HTTPStatus::RC_OK)
    return
  end

  if(paras[I_DEFFOLDER])
    old_f=image.folder.md5
    image.move_to_folder(@folders[cookies['folder']])
    sess.message='Moved'
    res.set_redirect(WEBrick::HTTPStatus::Found,"/folder?#{I_FOLDER}=#{old_f}")
    return
  end

  header(res,"Image ##{image.id} (from folder #{image.folder.complete_name})",sess,
         [['Folder page',"/folder?#{I_FOLDER}=#{image.folder.md5}"],
          ['Session page','/session'],nil,['Log out',"/session?#{I_LOGOUT}"]])

  res.body+="<center><p><img src=\"/material/medium/#{image.md5}.png\"/></p>\
<p><form action=\"/image?#{I_MD5}=#{image.md5}\" method=\"post\">\
<button type=\"submit\" name=\"#{I_HORPOS}\">Modify horizon</button>"
  res.body+="<button type=\"submit\" name=\"#{I_DEFFOLDER}\">Move to default folder</button>" if(image.folder!=
                                                                                                 @folders[cookies['folder']])
res.body+="<button type=\"submit\" name=\"#{I_DELETEIMAGE}\">REMOVE IT</button>"
  footer(res,WEBrick::HTTPStatus::RC_OK)
end

def digestpage(req,res)
  cookies,paras,sess=process_sess_req(req,res)
  return if(!cookies)
  
  #
  # Is a batch of images being uploaded?
  #
  
  if(paras[I_PATH])
    paths=[]
    paras[I_PATH].each_data() do |ed|
      paths.push([ed.filename,ed.to_s])
    end
    sess.add_images_to_upload(paths)
    res.set_redirect(WEBrick::HTTPStatus::Found,"/digest_images")    
  end

  path,blob=sess.next_image()

  if(!path)
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")    
    return
  end
  
  res['refresh']='0'

  md5=Digest::MD5::hexdigest(blob)
  exv=@db.image_with_md5(md5)
  if(exv)  
    header(res,'Already exists!',sess)
    res.body+="<h1>Image #{path} is already in our database!</h1>"
    footer(res,WEBrick::HTTPStatus::RC_OK)
    return
  end

  img=Magick::Image::from_blob(blob)[0]
  if(!img)
    header(res,'Bad image!',sess)
    res.body+="<h1>Image #{path} could not be imported!</h1>"
    footer(res,WEBrick::HTTPStatus::RC_OK)
    return
  end

  x=img.columns()
  y=img.rows()

  img.write(PARKINGBASE+md5+'.png')

  ratio=x.to_f/y
  
  rx=(Math::sqrt(Manager_webber::STAMP_AREA*ratio)).to_i
  ry=(rx/ratio).to_i

  img.resize!(rx,ry)
  img.write(PARKINGBASE+md5+'.stamp.png')
  img.destroy!()

  res.body+="<center><h1>Image #{path} has been parked! (#{x}X#{y}) Still #{sess.uploaded_images.length} to process.</h1></center>"
  footer(res,WEBrick::HTTPStatus::RC_OK)
end
  
def processparkedpage(req,res)
  cookies,paras,sess=process_sess_req(req,res)
  return if(!cookies)

  if(paras[I_MD5])
    md5=paras[I_MD5]

    if(md5[0,4]=='<IMG')
      md5=md5[28,32]
    end
    
    @loaded_images[md5][0].destroy!() if(@loaded_images[md5])

    img=Wnewimage::new(Magick::Image::read(PARKINGBASE+md5+'.png')[0],md5,
                       cookies['folder'] ? @folders[cookies['folder']] : @default_folder)
    @loaded_images[md5]=[img,true]
    res.set_redirect(WEBrick::HTTPStatus::Found,"/sethor_image?#{I_MD5}=#{md5}")
    return
  end
  
  pi=parked_images()

  if(!pi)
    sess.message="NO PARKED IMAGES!"
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
    return
  end

  if(paras[I_CLEAN_LIST])
    header(res,"About to clean parked image folder (#{pi.length} images)",sess)
    res.body+="<center><h2>Ok to proceed?</h2>\
<p><form action=\"/process_parked\" method=\"post\"><input type=\"submit\" name=\"#{I_CONFIRM}\" value=\"DELETE\"/></form></p>\
<p><form action=\"/session\" method=\"post\"><input type=\"submit\" value=\"Forget it\"/></form></p></center>"
    footer(res,WEBrick::HTTPStatus::RC_OK)
    return
  end

  if(paras[I_CONFIRM])
    FileUtils::rm(Dir::glob(PARKINGBASE+'*.png'))
    sess.message='Parking has been cleaned.'
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
  end    
  
  header(res,"Processing parked images (saved to #{@folders[cookies['folder']].complete_name()})",
         sess,[['Back to session page','/session']])
  res.body+='<p><form action="/process_parked" method="post" >'
  pi.each do |md5|
    res.body+="<button name=\"#{I_MD5}\" value=\"#{md5}\" type=\"submit\">\
<img src=\"/material/parking/#{md5}.stamp.png\" /></button>"
  end
  res.body+="</form></p>"
  footer(res,WEBrick::HTTPStatus::RC_OK)
end

def sethorpage(req,res)
  cookies,paras,sess=process_sess_req(req,res)
  return if(!cookies)

  md5=paras[I_MD5]
  
  if(!md5)
    sess.message="No image specified!"
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
    return
  end

  img=@loaded_images[md5]
  if(!img)
    sess.message="Image was not previously loaded!"
    res.set_redirect(WEBrick::HTTPStatus::Found,"/session")
    return
  end

  img,park_flg=img  

  if(paras[I_CONFIRM])
    if(img.is_a?(Wnewimage))
      imid=@db.insert_image(img.md5,img.folder.id,img.horpos)
      img.save(imid)
    else
      img.dbimage.update_horizon(img.horpos)
      img.save()
    end

    ask_engine_to_reload()

    img.destroy!()

    @loaded_images.delete(md5)

    reload_folder_tree()

    #
    # if it was from parking, delete file from it
    #

    if(park_flg)
      File::unlink(PARKINGBASE+md5+'.png')
      File::unlink(PARKINGBASE+md5+'.stamp.png')
      sess.message="The image has been successfully loaded!"
      res.set_redirect(WEBrick::HTTPStatus::Found,"/process_parked")
    else
      sess.message="The image has been successfully updated!"
      res.set_redirect(WEBrick::HTTPStatus::Found,"/folder?#{I_FOLDER}=#{img.dbimage.folder.md5}")
    end

    return
  end
  
  if(paras[I_HORPOS])
    v=paras[I_HORPOS].to_i
    if(v!=img.horpos)
      loggo("New hor #{v}")
      if(v<0 || v>Wnewimage::DIGFACT)        
        sess.message="#{v}: BAD HORIZON VALUE (must be between 0 and #{Wnewimage::DIGFACT})"
        res.set_redirect(WEBrick::HTTPStatus::Found,"/sethor_image?#{I_MD5}=#{md5}")
        return
      end
      img.horpos=v
      img.save_ref_image()
    end
  end

  if(paras[I_ROTRIGHT])
    img.rot(90)
  end    
  if(paras[I_ROTLEFT])
    img.rot(-90)
  end    
  
  header(res,'Tune horizon',sess,[['Rotate right',"/sethor_image?#{I_MD5}=#{md5}&#{I_ROTRIGHT}"],
                                  ['Rotate left',"/sethor_image?#{I_MD5}=#{md5}&#{I_ROTLEFT}"],
                                  ['Validate',"/sethor_image?#{I_MD5}=#{md5}&#{I_CONFIRM}"],
                                  ['Forget',park_flg ? '/process_parked' : "/folder?#{I_FOLDER}=#{img.dbimage.folder.md5}"]])
  
  res.body+=<<EOF
<center><p>The size of the image is <strong>#{img.image.columns}</strong>X<strong>#{img.image.rows}</strong></p>
<p>Horizon is currently at <strong>#{img.horpos}</strong></p>
<div id="canvas" style="overflow:hidden;position:relative;height:#{img.ry}px;width:#{img.rx}px;"></div>
<p><form id="shor" action="/sethor_image?#{I_MD5}=#{md5}" method="post">Set horizon to
<input type="text" name="#{I_HORPOS}" value="#{img.horpos}"/>
(value from 0 to #{Wnewimage::DIGFACT})</form></p>
</center>
<script type="text/javascript">
<!--
var canvas_div=document.getElementById("canvas");
var gr=new jsGraphics(canvas_div);
var col=new jsColor("red");
var pen=new jsPen(col,5);
var zeropoint=new jsPoint(0,0);
var mouse_y=0

var form=document.getElementById("shor");

canvas_div.onmousemove=mouo;
canvas_div.onclick=clio;

function mouo(e)
{
  mouse_y=e.pageY;
  if(mouse_y<0 || isNaN(mouse_y)){mouse_y=0}
  mouse_y=mouse_y-canvas_div.offsetTop;

  gr.clear()

  gr.drawImage("material/#{img.md5}.png?#{rand(10000)}",zeropoint,#{img.rx},#{img.ry})
  gr.drawLine(pen,new jsPoint(0,mouse_y),new jsPoint(#{img.rx},mouse_y)); 

  return true;
}

function clio(e)
{
  form.#{I_HORPOS}.value=(#{img.ry}-mouse_y)/#{img.ry}.0*1000
  form.submit();
//  alert("Val: "+mouse_y);
//  return true;
}

mouo(document);
//-->
</script>
<noscript>
 <b>You don't have JavaScript enabled or your browser does not support JavaScript</b>
</noscript>
EOF
  footer(res,WEBrick::HTTPStatus::RC_OK)    
end
