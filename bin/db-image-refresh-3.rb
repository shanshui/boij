#!/usr/bin/env ruby
# refresh_image_database_3.rb

=begin

08/11/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Clears the database of images, then loads all images included into a text
file passed to stdin
Second version - all images resized to actual screen height
Third - gets all images in a dir, numbers them in order

$Id: refresh_image_database_3.rb 2626 2010-11-10 14:48:16Z karl $

=end

require 'boij.so'

raise "Usage: #{$0} dirname" if(ARGV.length!=1)

STDERR.printf("Producing for screen size %dX%d (%dX%d)\n",*SCREEN_SIZE,*WINDOW_SIZE)

imgs=[]
cnt=0
Dir::foreach(ARGV[0]) do |entry|
  next if(entry[0,1]=='.')
  fn=ARGV[0]+'/'+entry
  pp File::file?(fn)
  next if(!File::file?(fn))
  imgs.push([fn,cnt])
  cnt+=1
end

raise "No images defined in standard input!" if(imgs.length()<=0)

#
# Clean dir
#

system("rm #{PICTURE_BASE}/*.bo2")
resp=[]
imgs.each do |img|
  newp=Picture::load2(*img)
  resp.push(Picture::new(newp,true))
end
resp.each do |r|
  STDERR.printf("Image #%3.3d: %dX%d (%d)\n",r.prog,*(r.size),r.bottom_offset)
end
