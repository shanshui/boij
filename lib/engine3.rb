#!/usr/bin/env ruby
# engine3.rb

=begin

09/11/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

The engine that runs it all. Second version with resized images
Version using GL
Version getting presence from more modern thermocams
Or from other sources

$Id: engine3.rb 2824 2011-03-15 17:02:12Z karl $

=end

require 'boij'
require 'image_dbase'
require 'movireceptor'

require 'sdl'
require 'opengl'

STARTX_FLAG=false
FULLSCREEN_FLAG=true

EXIT_WITH_ESC=!FULLSCREEN_FLAG
AUDIO_FLAG=true

SOUNDFILE_ATTN=0.8 #0.5

GRIDF=false

class Engine
  GRIDV_STEP=6
  GRIDH_STEP=GRIDV_STEP*4
  GRIDB=2
  GRC=[255,255,40]

  MAGN_PAR=1.0

  #
  # The floating value below directly changes screen occupation
  # (smaller = larger image onscreen)
  #

  COVERED_AREA=3400.0*MAGN_PAR
#  COVERED_AREA=2700.0*MAGN_PAR
#  COVERED_AREA=9000.0*MAGN_PAR

  RXM=(WINDOW_SIZE[0]*MAGN_PAR).to_i
  RYM=[(CUT_BOTTOM*MAGN_PAR).to_i,((WINDOW_SIZE[1]-CUT_TOP)*MAGN_PAR).to_i]
  TR_X=-(RXM*0.5)
  TR_Y=-(WINDOW_SIZE[1]*0.5)
  TR_Z=0.0

  MAX_IMAGES_TO_KEEP=110

  LOOPSLEEP=0.001

  attr_reader(:audiotoy,:sounds,:b_i,:curloudspeaker,:pictures,:imarr_mutex,:orphaned_images)

  def initialize
    ENV['__GL_SYNC_TO_VBLANK']='1'
    ENV['__GL_FSAA_MODE']='12'
    ENV['__GL_LOG_MAX_ANISO']='4'

    @idb=Image_dbase::new()
    @imarr_mutex=Mutex::new()
    reload_folder_tree()

    @mrecep=Movireceptor::fire_up(0,method(:new_positions))

    if(GRIDF)
      @gridr=[]
      sx=WINDOW_SIZE[0].to_f()/GRIDH_STEP
      (GRIDH_STEP+1).times do |x|
	@gridr.push([sx*x-GRIDB,0,GRIDB*2+1,WINDOW_SIZE[1]])
      end
      sy=WINDOW_SIZE[1].to_f()/GRIDV_STEP
      (GRIDV_STEP+1).times do |y|
	@gridr.push([0,sy*y-GRIDB,WINDOW_SIZE[0],GRIDB*2+1])
      end
    else
      @gridr=nil
    end

    if(STARTX_FLAG)
      require 'boij_xserver'
      loggo("Starting screen #{SCREEN_SIZE}")
      @xsv=Boij_xserver::new(*SCREEN_SIZE)
      sleep(3)
      ENV['DISPLAY']=':0'
    else
      @xsv=nil
    end

    loggo("BAAA")
    @b_i=Imager::new(WINDOW_SIZE[0],WINDOW_SIZE[1]-CUT_TOP-CUT_BOTTOM,HORIZON_POS)

    @sounds=[]
    if(AUDIO_FLAG)
      @audiotoy=Audiotoy::new()
      @sounds=[]
      Dir::entries(OPEN_SOUNDDIR).each do |f|
	next if(f[-4,4]!='.wav')
	@sounds.push(@audiotoy.load_file(OPEN_SOUNDDIR+f,SOUNDFILE_ATTN))
      end
    end

    @players=[]
    MAXGRPS.times do |i|
      @players.push(Player::new(self,i))
    end

    @old_positions=nil
    @updatepos_flag=false
    @orphaned_images=[]
    @latest_pixels=nil

    @done=false
    Signal::trap('INT') do
      @done=true
    end
    Signal::trap('TERM') do
      @done=true
    end
    @curloudspeaker=0
#    Signal::trap('USR1') do
#      @curloudspeaker+=1
#      @curloudspeaker%=7
#      @audiotoy.fix_loudspeaker(@curloudspeaker==0 ? nil : @curloudspeaker-1)
#    end
    Signal::trap('USR1') do
      loggo("Request to reload")
      reload_folder_tree()
      true
    end
    Signal::trap('USR2') do
      take_snap()
      true
    end
  end

  def new_positions(pos)
    @new_positions=pos
  end

  def reload_folder_tree
    rootfolders,folders,images=Wfolder::load(@idb)
    np=[]
    rootfolders.each do |k,rf|
      recurse_add_image_from_folder(np,rf)
    end
    @imarr_mutex.synchronize do
      @pictures=np
    end
    loggo("Found #{@pictures.length} pictures")

    @pictures.sort!() do |a,b|
      a.prog<=>b.prog
    end

    build_new_picture_array()
  end

  def build_new_picture_array
    @pictures_to_send=@pictures.sort() do |a,b|
      rand()<=>0.5
    end
  end

  def recurse_add_image_from_folder(arr,folder)
    return if(!folder.used)
    folder.local_images.each do |im|
      arr.push(Picture::new(im.id,true))
    end
    folder.sons.each do |k,son|
      recurse_add_image_from_folder(arr,son)
    end
  end

  def runme
    loggo("BAAA3")
    SDL::init(SDL::INIT_VIDEO)
    if(FULLSCREEN_FLAG)
      loggo("BAAA4 #{WINDOW_SIZE}")
      @screen=SDL::Screen::open(*WINDOW_SIZE,32,SDL::OPENGL|SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME|SDL::FULLSCREEN)
#      @screen=SDL::Screen::open(*WINDOW_SIZE,32,SDL::OPENGL|SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME)
      SDL::Mouse.warp(1,1)
      SDL::Mouse.hide
    else
      @screen=SDL::Screen::open(*WINDOW_SIZE,32,SDL::OPENGL|SDL::HWSURFACE|SDL::DOUBLEBUF|SDL::NOFRAME)
    end

    #
    # Generics
    #

    GL::DepthFunc(GL::LESS)
    GL::ShadeModel(GL::SMOOTH)
    GL::PixelStorei(GL::UNPACK_ALIGNMENT,1)
    GL::ClearColor(0.0,0.0,0.0,0.0)
    GL::ClearDepth(1.0)

    GL::Enable(GL::TEXTURE_2D)
    GL::Enable(GL::DEPTH_TEST)

    @texname=GL.GenTextures(1)[0]
    GL::BindTexture(GL::TEXTURE_2D,@texname)
    @texsize=[firstpowerof2(WINDOW_SIZE[0]),firstpowerof2(WINDOW_SIZE[1])]
    zeroa=Array::new(@texsize[0]*@texsize[1]*3,0).pack('c*')
    GL::TexImage2D(GL::TEXTURE_2D,0,3,*@texsize,0,GL::RGB,GL::UNSIGNED_BYTE,zeroa)
    @tex_x=WINDOW_SIZE[0]/@texsize[0].to_f
    @tex_y=(WINDOW_SIZE[1]-CUT_TOP-CUT_BOTTOM)/@texsize[1].to_f

    GL::MatrixMode(GL::PROJECTION)
    GL::LoadIdentity()
    cov_y=((COVERED_AREA*2.0)*WINDOW_SIZE[1]/WINDOW_SIZE[0])/2.0
#    GL::Ortho(-COVERED_AREA,COVERED_AREA,-COVERED_AREA,COVERED_AREA,-COVERED_AREA,COVERED_AREA)
    GL::Ortho(-COVERED_AREA,COVERED_AREA,-cov_y,cov_y,-COVERED_AREA,COVERED_AREA)
#    GL::Ortho(0,COVERED_AREA*2,0,COVERED_AREA*2,0,COVERED_AREA*2)

    GL::MatrixMode(GL::MODELVIEW)
    GL::LoadIdentity()

    GL::Viewport(0,0,WINDOW_SIZE[0],WINDOW_SIZE[1])

    nxt_refresh=Time::now()
    until(@done)
      event=SDL::Event2::poll()
      if(event)
	case event
        when SDL::Event2::Quit
          @done=true
        when SDL::Event2::KeyDown
	  case event.sym
          when SDL::Key::ESCAPE
	    @done=true if EXIT_WITH_ESC
#	  when 115
#	    take_snap()
	  end
        when SDL::Event2::Active
          @refresh_flg=true
#        else
#          loggo("Event #{event}")
	end
      end

      if(@new_positions)
        update_positions(@new_positions)
        @new_positions=nil
      end

      now=Time::now()
      imgs=[]+@orphaned_images
      @players.each do |p|
        if(p.arrival)
          p.start_new_image_if_needed_2(now)
          imgs+=p.owned_pictures
        end
      end

      imgs.sort!() do |a,b|
        a.start_time<=>b.start_time
      end

      if(imgs.length>MAX_IMAGES_TO_KEEP)
        #        loggo("Must barf #{imgs.length-MAX_IMAGES_TO_KEEP}")
        imgs.slice!(0,imgs.length-MAX_IMAGES_TO_KEEP).each do |thi|
          thi.discard()
        end
      end

      to_send=[]
      imgl=[]
      imgs.each do |img|
        old_width=img.width.to_i
        if(img.start_position-old_width<0 && img.start_position+old_width>=WINDOW_SIZE[0])
          img.discard()
        else
          imgl.push(img.cnt)
          img.update_width(now)
          to_send.push([img.cnt,img.pixels,img.start_position,old_width,img.width.to_i,img.forw_backw])# if(img.width.to_i()>0)
        end
      end

      if(to_send.length()>0)
        @latest_pixels,found_ids=@b_i.screenmaker(to_send.sort do |a,b|
                                                    a[0]<=>b[0]
                                                  end)
        ((imgl.sort())-(found_ids.sort().uniq())).each do |cnt|
          Displayed_picture::discard_with_cnt(self,cnt)
        end
        @refresh_flg=true
      end

      if(@refresh_flg)
        @refresh_flg=false
        refresh_screen()
      end

      sleep(LOOPSLEEP)
    end

    @screen.destroy()
    @xsv.reap() if(@xsv)
  end

  def update_positions(positions)
    #    loggo("New positions #{positions}")
    @players.each_with_index do |p,i|
      if(!p.arrival && positions[i])
        loggo("Player #{p.prog} arrived.")
        p.image=nil
        p.arrival=Time::now()
        p.position=positions[i]
      elsif(p.arrival && !positions[i])
        loggo("Player #{p.prog} left.")
        @orphaned_images+=p.give_owned_pictures_and_leave()
      elsif(p.arrival && positions[i])
        p.position=positions[i]
      end
    end
=begin
    opl=@players.select do |p|
      p.arrival
    end

    while(positions.length>0 && opl.length>0)
      min_dist=99999
      pch=nil
      plch=nil

      positions.each do |ps|
        opl.each do |pl|
          d=(ps[0]-pl.position[0]).abs+(ps[1]-pl.position[1]).abs
          if(d<min_dist)
            pch=ps
            plch=pl
            min_dist=d
          end
        end
      end
      if(pch)
        plch.position=pch
        positions.delete(pch)
        opl.delete(plch)
      end
    end

    opl.each do |pl|
      loggo("Player #{pl.prog} left.")
      @orphaned_images+=pl.give_owned_pictures_and_leave()
    end

    positions.each do |ps|
      @players.each do |pl|
        if(!pl.arrival)
          loggo("Player #{pl.prog} arrived.")
	  pl.image=nil
          pl.arrival=Time::now()
          pl.position=ps
          break
        end
      end
    end
=end
  end

  def refresh_screen
    return if(!@latest_pixels)

    GL::Clear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT)

    GL::BindTexture(GL::TEXTURE_2D,@texname)
    GL::TexParameteri(GL::TEXTURE_2D,GL::TEXTURE_MIN_FILTER,GL::LINEAR)
    GL::TexParameteri(GL::TEXTURE_2D,GL::TEXTURE_MAG_FILTER,GL::LINEAR)

    GL::TexSubImage2D(GL::TEXTURE_2D,0,0,0,WINDOW_SIZE[0],WINDOW_SIZE[1]-CUT_TOP-CUT_BOTTOM,GL::BGR,GL::UNSIGNED_BYTE,@latest_pixels)

    GL::PushMatrix()
    GL::Translate(TR_X,TR_Y,0.0)
    GL::Begin(GL::QUADS)
    GL::TexCoord(0.0,0.0);GL.Vertex(0.0,RYM[1],0.0)
    GL::TexCoord(@tex_x,0.0);GL.Vertex(RXM,RYM[1],0.0)
    GL::TexCoord(@tex_x,@tex_y);GL.Vertex(RXM,RYM[0],0.0)
    GL::TexCoord(0.0,@tex_y);GL.Vertex(0.0,RYM[0],0.0)

    GL::End()
    GL::PopMatrix()
    GL::Flush()

    SDL::GL::swap_buffers()
  end

  def take_snap
    return if(!@latest_pixels)

    cmd='cjpeg > '+SNAPDIR+'s.'+Time::now().stamp()+'.jpg'
    IO::popen(cmd,'w') do |f|
      f.printf("P6\n%d %d\n255\n",*WINDOW_SIZE)
      f.print(Imager::pixel_flip(@latest_pixels))
    end
  end

  def firstpowerof2(v)
    i=2
    loop do
      return i if(i>v)
      i*=2
    end
  end

  def nuke_image(cnt)
    @orphaned_images.each do |oi|
      if(cnt==oi.cnt)
        loggo("WAS ORPHANED!")
        @orphaned_images.delete(oi)
        return
      end
    end
    @players.each do |p|
      p.owned_pictures.each do |oi|
        if(cnt==oi.cnt)
          loggo("WAS FROM PLAYER #{p.prog} (arrival #{p.arrival})!")
          p.owned_pictures.delete(oi)
          return
        end
      end
    end
    loggo("Nuke: not found (#{cnt})")
  end

  def next_picture
    build_new_picture_array() if(@pictures_to_send.length<=0)
    @pictures_to_send.shift()
  end
end

if($0==__FILE__)
#  Fluid_logfile::new('engine')
  Engine::new().runme
end
