# picture.rb

=begin

7/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

One picture, as displayed

$Id: picture.rb 3449 2012-03-15 07:34:52Z karl $

=end

PICTURE_BASE=BASE+'pictures/'
MAX_PICTURES=1024

require 'rmagick'

class Picture
  attr_accessor(:prog)
  attr_reader(:fn,:origpath,:size,:bottom_offset,:pixels,:max_size,:min_size,:diff_size)

  MAX_PERC_TO_USE=0.98
  MIN_PERC_TO_USE=0.85 # 0.1

  def initialize(prog,vers_2=false)
    @prog,@vers_2=prog,vers_2

    @fn=PICTURE_BASE+"#{@prog}.#{vers_2 ? 'bo2' : 'bo'}"
    File::open(@fn,'r') do |f|
      @origpath=f.gets()
      a=f.gets().split(',')
      @size=[a[0].to_i(),a[1].to_i()]
      @bottom_offset=a[2].to_i()
      @pixels=[]
      @size[0].times do
        @pixels.push(f.read(@size[1]*3))
      end
    end

    @min_size=(@size[0]*MIN_PERC_TO_USE).to_i()
    @max_size=(@size[0]*MAX_PERC_TO_USE).to_i()
    @diff_size=@max_size-@min_size
  end

  def convert(blackfill)
    return if(blackfill==Boij_sharedvideoarea::BLACKFILL_NONE)

    @pixels.map!() do |op|
      Boij_sharedvideoarea::extend_line(op,SCREEN_SIZE[1],@bottom_offset,blackfill)
    end

    @size[1]=SCREEN_SIZE[1]
    @bottom_offset=0

    self
  end

  def dump
    File::open(@fn,'w') do |f|
      f.puts(@origpath)
      f.printf("%d,%d,%d\n",*@size,@bottom_offset)
      @size[0].times do |col|
        STDERR.printf("\rCol %4d   ",col)
        f.write(@pixels[col])
      end
    end
    STDERR.puts("\n#{@fn} dumped.")
  end    
  
  def Picture::load(path,horizonpos)
    prog=nil
    fn=nil
    MAX_PICTURES.times do |i|
      fn=PICTURE_BASE+"#{i}.bo"
      if(!File::exist?(fn))
        prog=i
        break
      end
    end
    raise "Too many pictures in data base!" if(prog>=MAX_PICTURES)
    img=Magick::Image::read(path)[0]
    size=[img.columns(),img.rows()]
    ratio=size[0].to_f/size[1]

    #
    # Find where is the horizon
    #

    horizfloat=horizonpos.to_f()/size[1]
    if(horizfloat<HORIZON_POS)
      upp=SCREEN_SIZE[1]-HORIZON_PIX
      downp=(upp*horizfloat/(1.0-horizfloat)).to_i()
    else
      downp=HORIZON_PIX
      upp=(downp*(1.0-horizfloat)/horizfloat).to_i()
    end
    new_size=[((upp+downp)*ratio).to_i(),upp+downp]
    img.resize!(new_size[0],new_size[1])    
    new_size=[img.columns(),img.rows()]

    STDERR.puts("#{path}: resulting image is [#{new_size[0]}X#{new_size[1]}] with botmar #{HORIZON_PIX-downp}.")

    File::open(fn,'w') do |f|
      f.puts(path)
      f.printf("%d,%d,%d\n",*new_size,HORIZON_PIX-downp)
      new_size[0].times do |col|
        STDERR.printf("\rCol %4d   ",col)
        f.write(img.export_pixels_to_str(col,0,1,img.rows,'BGR'))
      end
    end
    STDERR.puts("\n#{fn} dumped.")

    prog
  end

  def update_bottom_offset(val)
    @bottom_offset+=val
    if(@bottom_offset<0)
      @bottom_offset=0
    elsif(@bottom_offset>=@size[1])
      @bottom_offset=@size[1]-1
    end
  end

  def Picture::load2(path,horizonpos)
    prog=nil
    fn=nil
    MAX_PICTURES.times do |i|
      fn=PICTURE_BASE+"#{i}.bo2"
      if(!File::exist?(fn))
        prog=i
        break
      end
    end
    raise "Too many pictures in data base!" if(prog>=MAX_PICTURES)
    img=Magick::Image::read(path)[0]
    size=[img.columns(),img.rows()]
    ratio=SCREEN_SIZE[1]/size[1].to_f()

    #
    # Find where is the horizon
    #

    horiz=(horizonpos*ratio).to_i()
    new_size=[size[0]*ratio,SCREEN_SIZE[1]]
    img.resize!(new_size[0],new_size[1])    
    new_size=[img.columns(),img.rows()]

    loggo("#{path}: resulting image is [#{new_size[0]}X#{new_size[1]}] with hori #{horiz}.")

    File::open(fn,'w') do |f|
      f.puts(path)
      f.printf("%d,%d,%d\n",*new_size,horiz)
      new_size[0].times do |col|
        STDERR.printf("\rCol %4d   ",col)
        f.write(img.export_pixels_to_str(col,0,1,img.rows,'BGR'))
      end
    end
    STDERR.puts("\n#{fn} dumped.")

    prog
  end
end

class Displayed_picture
  MIN_EXPAND_PER_SEC=1.0# 2.0
  PROG_EXPAND_PER_SEC=500.0 #35.0
  #  TAN_USE=(Math::PI/2.0)*0.99

  MAX_CNT=0xffff

  attr_reader(:start_position,:picture,:pixels,:start_time,:expansion_rate,:width,
              :size_to_use,:begf,:endf,:achunk,:forw_backw)
  attr_accessor(:owner,:cnt)

  @@cnt=0
  @@images={}
  @@init_mutex=Mutex::new

  def initialize(engine,owner,start_position,picture,pixels,left_or_right)
    @@init_mutex.synchronize do
      @engine,@owner,@start_position,@picture,@pixels,@forw_backw=
        engine,owner,(start_position*WINDOW_SIZE[0]).to_i(),picture,pixels,left_or_right

      @start_time=Time::now()
      @last_increased=@start_time
      #    @last_rate=1.0-owner.position[1]
      @width=0
      @cnt=@@cnt
      @@images[@cnt]=self

      @@cnt+=1
      if(@@cnt>MAX_CNT)
        ks=@@images.keys.sort
        min_cnt=ks[0]
        max_cnt=ks[-1]
        loggo("Resetting cnt (#{min_cnt}-#{max_cnt})")

        @@cnt=max_cnt-min_cnt

        ([]+@@images.keys).each do |k|
          v=@@images[k]
          v.cnt-=min_cnt
          @@images.delete(k)
          @@images[v.cnt]=v
        end

        @engine.b_i.reset_cnt(min_cnt)
      end
    end

    #    loggo("Pl##{@owner.prog+1} gets new image (#{@picture.prog+1},#{@forw_backw},#{@size_to_use},#{@begf})")
  end

  def update_width(time)
    return if(@width>=MAX_EXPAND)
    timediff=time-@last_increased
    @last_increased=time
    ##    @last_rate=1.0-@owner.position[1] if(@owner)
    #    @width+=((MIN_EXPAND_PER_SEC+@last_rate*DIFF_EXPAND_PER_SEC)*timediff)
    #    rate=1.0+Math::tan(@width/WINDOW_SIZE[0]*TAN_USE)
    #    @width+=MIN_EXPAND_PER_SEC+PROG_EXPAND_PER_SEC*Displayed_picture::tanvalue(@width/WINDOW_SIZE[0])*timediff
    @width+=MIN_EXPAND_PER_SEC+PROG_EXPAND_PER_SEC*Videocapt::tan_propval(@width/WINDOW_SIZE[0])*timediff
  end

  def discard
    #    loggo("Discard #{@cnt}")
    if(!@owner)
      @engine.orphaned_images.delete(self)
    else
      @owner.owned_pictures.delete(self)
    end

    @@images.delete(self.cnt)
  end

  #
  # value is width of painted portion from 0 to 1 (1 is whole screen)
  # returns a number from 0 to 1, growing quickly
  #

  MAX_TANV=0.99
  MAX_TANRES=1.0/Math::tan(MAX_TANV)

  def Displayed_picture::tanvalue(v)
    1.0-(Math::tan((1.0-v)*MAX_TANV)*MAX_TANRES)
  end

  def Displayed_picture::discard_with_cnt(engine,cnt)
    if(!@@images[cnt])
      loggo("Warning! Asked to remove non-existing image #{cnt}")
      engine.nuke_image(cnt)
    else
      @@images[cnt].discard()
    end
  end
end
