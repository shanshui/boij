/* boij_sharedvideoframe.c */

/*
 * 17/1/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * The shared area wih captured frame and object positions
 *
 * $Id: boij_sharedvideoframe.c 2626 2010-11-10 14:48:16Z karl $
 */

#include <ruby.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <linux/videodev2.h>

#include "boij.h"

#define VIDEOMODE VIDEO_MODE_PAL
#define REQBFRS 4

typedef struct
{
  __s16 x,y,ox,oy;
} dot_stc;

typedef struct
{
  dot_stc *d1,*d2;
} line_stc;

typedef struct
{
  dot_stc *s[4];
} square_stc;

typedef struct
{
  struct v4l2_buffer b;
  void *d;
  size_t l;
} allo_buffer_stc;

typedef struct
{
  unsigned int in,out,skip,use;
} direction_para_stc;

union desc
{
  unsigned char d[4];
  __u32 v;
};

typedef struct
{
  direction_para_stc dpx,dpy;
  int unit,u,x,y,npix_in,npix_out,brightness,hue,saturation,contrast,whiteness,n_bfrs,n_flags;
  __u32 fmt;
  __u8 exit_thread,newframe,*frame_park;
  unsigned int *frame_map;
  struct v4l2_capability vcap;
  allo_buffer_stc *bfrs;
  pthread_t read_thr;
  pthread_mutex_t mtx;
} v4l_stc;

typedef struct
{
  v4l_stc v4l;
  shared_stc *ss;
  int key,shm_id,n_client;
  __u8 masterflag;
  void *shareddata,*framedata;
  dot_stc griddots[GRID_Y+1][GRID_X+1];
  square_stc gridsquares[GRID_Y][GRID_X];
  float grid_int_x,grid_int_y;
} boij_sharedvideoarea_stc;

VALUE cls_sharedvideoarea;

static void free_boij_sharedvideoarea(void *p);

static void *background_read(void *arg);
static void recompute_grid(boij_sharedvideoarea_stc *sva);

static int ccw(dot_stc *d1,dot_stc *d2,dot_stc *d3);
static int intersect(line_stc *l1,line_stc *l2);
static int in_square(square_stc *square,dot_stc *dot);
static float point_line_distance(line_stc *l,dot_stc *d);
static float avg_frame_background(__u8 *frame,int size);

VALUE new_boij_sharedvideoarea(VALUE self,VALUE videodata)
{
  boij_sharedvideoarea_stc *sva=ALLOC(boij_sharedvideoarea_stc);
  VALUE va;

  srand48(time(NULL));

  sva->masterflag=videodata==Qnil ? 0 : 1;
  
  if(sva->masterflag)
  {
    int i,j;
  
/*
 * We are masters and we must open the video unit
 */
  
    v4l_stc *v=&sva->v4l;
    
    bzero(v,sizeof(v4l_stc));
    v->unit=FIX2INT(rb_ary_entry(videodata,0));

    va=rb_ary_entry(videodata,3);
    v->dpx.in=FIX2INT(rb_ary_entry(va,0));
    v->dpx.out=FIX2INT(rb_ary_entry(va,1));
    v->dpx.skip=FIX2INT(rb_ary_entry(va,2));
    v->dpx.use=FIX2INT(rb_ary_entry(va,3));
    va=rb_ary_entry(videodata,4);
    v->dpy.in=FIX2INT(rb_ary_entry(va,0));
    v->dpy.out=FIX2INT(rb_ary_entry(va,1));
    v->dpy.skip=FIX2INT(rb_ary_entry(va,2));
    v->dpy.use=FIX2INT(rb_ary_entry(va,3));

    v->npix_in=v->dpx.in*v->dpy.in;
    v->npix_out=v->dpx.out*v->dpy.out;  

    v->frame_park=malloc(v->npix_out);
    v->frame_map=malloc(sizeof(unsigned int)*v->npix_out);

/*
 * First, can the shared area be created?
 */

    int shared_area_size=sizeof(shared_stc)+v->npix_out;

    if((sva->shm_id=shmget(IPC_KEY,shared_area_size,IPC_CREAT|0666))<0)
      rb_raise(rb_eRuntimeError,"%s: Error in shmget (%s)",__func__,strerror(errno));  
    if((sva->shareddata=shmat(sva->shm_id,NULL,0))==NULL)
      rb_raise(rb_eRuntimeError,"%s: Error in shmat (%s)",__func__,strerror(errno));
    sva->ss=(shared_stc *)sva->shareddata;
    sva->framedata=(void *)sva->ss+sizeof(shared_stc);

    sva->ss->x=v->dpx.out;
    sva->ss->y=v->dpy.out;
    sva->ss->framesize=v->npix_out;

    sva->grid_int_x=sva->ss->x/(float)GRID_X;
    sva->grid_int_y=sva->ss->y/(float)GRID_Y;

    bzero(sva->ss->grid_offsets,sizeof(__s16)*GRID_Y*GRID_X*2);

    for(i=0;i<=GRID_Y;i++)
    {
      for(j=0;j<=GRID_X;j++)
      {
	sva->griddots[i][j].ox=(int)(sva->grid_int_x*j);
	sva->griddots[i][j].oy=(int)(sva->grid_int_y*i);
      }
    }
    for(i=0;i<GRID_Y;i++)
    {
      for(j=0;j<GRID_X;j++)
      {
	sva->gridsquares[i][j].s[0]=&sva->griddots[i][j];
	sva->gridsquares[i][j].s[1]=&sva->griddots[i][j+1];
	sva->gridsquares[i][j].s[2]=&sva->griddots[i+1][j+1];
	sva->gridsquares[i][j].s[3]=&sva->griddots[i+1][j];
      }
    }
    
//recompute_grid(sva);    
    
/*
 * Now open the v4l channel
 */

    va=rb_ary_entry(videodata,5);
    v->brightness=FIX2INT(rb_ary_entry(va,0));
    v->hue=FIX2INT(rb_ary_entry(va,1));
    v->saturation=FIX2INT(rb_ary_entry(va,2));
    v->contrast=FIX2INT(rb_ary_entry(va,3));
    v->whiteness=FIX2INT(rb_ary_entry(va,4));
    
    char bfr[256];
    sprintf(bfr,"/dev/video%d",v->unit);
    if((v->u=open(bfr,O_RDWR))<0)
      rb_raise(rb_eRuntimeError,"%s: error opening V4L unit <%s> (%s)",__func__,bfr,strerror(errno));
    
    bzero(&v->vcap,sizeof(struct v4l2_capability));
    if(ioctl(v->u,VIDIOC_QUERYCAP,&v->vcap)<0)
      rb_raise(rb_eRuntimeError,"%s: error reading capabilities for <%s> (%s)",__func__,bfr,strerror(errno));

    fprintf(stderr,"Unit: %d\nDriver: %s\nCard: %s\nBus: %s\nCapabilities: %x\n",v->u,v->vcap.driver,v->vcap.card,
	    v->vcap.bus_info,v->vcap.capabilities);
  
    if(!(v->vcap.capabilities&V4L2_CAP_VIDEO_CAPTURE))
      rb_raise(rb_eRuntimeError,"%s: <%s> cannot capture!",__func__,bfr);

    if(!(v->vcap.capabilities&V4L2_CAP_STREAMING))
      rb_raise(rb_eRuntimeError,"%s: <%s> cannot stream!",__func__,bfr);

    struct v4l2_format vfmt;

    vfmt.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(v->u,VIDIOC_G_FMT,&vfmt)<0)
      rb_raise(rb_eRuntimeError,"%s: error reading capture format for <%s> (%s)",__func__,bfr,strerror(errno));

    union desc ed;
    
    ed.v=vfmt.fmt.pix.pixelformat;
    fprintf(stderr,"Default video capture size: %dX%d (%x%x%x%x)\n",vfmt.fmt.pix.width,vfmt.fmt.pix.height,
	    ed.d[0],ed.d[1],ed.d[2],ed.d[3]);
    
    struct v4l2_fmtdesc vfd;
    __u32 fmt=0;
    char *format=RSTRING_PTR(rb_ary_entry(videodata,2));
    
    for(vfd.type=V4L2_BUF_TYPE_VIDEO_CAPTURE,vfd.index=0;;vfd.index++)
    {
      if(ioctl(v->u,VIDIOC_ENUM_FMT,&vfd)<0)
	break;
      
      ed.v=vfd.pixelformat;
      
      fprintf(stderr,"Fmt #%d: %s (type %d flags %d 4c %c%c%c%c)\n",vfd.index+1,vfd.description,vfd.type,vfd.flags,
	      ed.d[0],ed.d[1],ed.d[2],ed.d[3]);
      if(!strcasecmp(format,(char *)vfd.description))
	fmt=vfd.pixelformat;
    }
    fprintf(stderr,"%d format types\n",vfd.index);
    if(fmt==0)
      rb_raise(rb_eRuntimeError,"%s: requested format <%s> not found in <%s>",__func__,format,bfr);
    
    v->fmt=fmt;

    vfmt.fmt.pix.width=v->dpx.in;
    vfmt.fmt.pix.height=v->dpy.in;
    vfmt.fmt.pix.pixelformat=fmt;
    if(ioctl(v->u,VIDIOC_S_FMT,&vfmt)<0)
      rb_raise(rb_eRuntimeError,"%s: error setting capture format to (%dX%d)/%x for <%s> (%s)",__func__,v->dpx.in,v->dpy.in,fmt,
	       bfr,strerror(errno));
    
/*
 * Search for port
 */
    
    struct v4l2_input vinp;
    int channel=-1;
    char *port=RSTRING_PTR(rb_ary_entry(videodata,1));
    
    for(vinp.index=0;;vinp.index++)
    {
      if(ioctl(v->u,VIDIOC_ENUMINPUT,&vinp)<0)
	break;
      fprintf(stderr,"Input #%d: %s (type %d std %d status %d)\n",vinp.index+1,vinp.name,vinp.type,
	      (int)vinp.std,vinp.status);
      if(!strcasecmp(port,(char *)vinp.name))
	channel=vinp.index;
    }
    fprintf(stderr,"%d channels\n",vinp.index);
    if(channel<0)
      rb_raise(rb_eRuntimeError,"%s: requested channel <%s> not found in <%s>",__func__,port,bfr);

    if(ioctl(v->u,VIDIOC_S_INPUT,&channel)<0)
      rb_raise(rb_eRuntimeError,"%s: error setting to chn#%d on <%s> (%s)",__func__,channel,bfr,strerror(errno));
    
/*
 * The image qualities
 */
    
    struct v4l2_control vctl;
    
    vctl.id=V4L2_CID_BRIGHTNESS;
    ioctl(v->u,VIDIOC_G_CTRL,&vctl);
    if(vctl.value!=v->brightness)
    {
      fprintf(stderr,"Brightness was %d - set to %d\n",vctl.value,v->brightness);
      vctl.value=v->brightness;
      ioctl(v->u,VIDIOC_S_CTRL,&vctl);
    }
    vctl.id=V4L2_CID_HUE;
    ioctl(v->u,VIDIOC_G_CTRL,&vctl);
    if(vctl.value!=v->hue)
    {
      fprintf(stderr,"Hue was %d - set to %d\n",vctl.value,v->hue);
      vctl.value=v->hue;
      ioctl(v->u,VIDIOC_S_CTRL,&vctl);
    }
    vctl.id=V4L2_CID_SATURATION;
    ioctl(v->u,VIDIOC_G_CTRL,&vctl);
    if(vctl.value!=v->saturation)
    {
      fprintf(stderr,"Saturation was %d - set to %d\n",vctl.value,v->saturation);
      vctl.value=v->saturation;
      ioctl(v->u,VIDIOC_S_CTRL,&vctl);
    }
    vctl.id=V4L2_CID_CONTRAST;
    ioctl(v->u,VIDIOC_G_CTRL,&vctl);
    if(vctl.value!=v->contrast)
    {
      fprintf(stderr,"Contrast was %d - set to %d\n",vctl.value,v->contrast);
      vctl.value=v->contrast;
      ioctl(v->u,VIDIOC_S_CTRL,&vctl);
    }
    vctl.id=V4L2_CID_WHITENESS;
    ioctl(v->u,VIDIOC_G_CTRL,&vctl);
    if(vctl.value!=v->whiteness)
    {
      fprintf(stderr,"Whiteness was %d - set to %d\n",vctl.value,v->whiteness);
      vctl.value=v->whiteness;
      ioctl(v->u,VIDIOC_S_CTRL,&vctl);
    }    

/*
 * Allocate the mmap memory
 */

    struct v4l2_requestbuffers vrb;
    
    vrb.count=REQBFRS;
    vrb.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    vrb.memory=V4L2_MEMORY_MMAP;
    if(ioctl(v->u,VIDIOC_REQBUFS,&vrb)<0)
      rb_raise(rb_eRuntimeError,"%s: error requesting buffers for <%s> (%s)",__func__,bfr,strerror(errno));

    fprintf(stderr,"Requested %d buffers. Assigned %d buffers\n",REQBFRS,vrb.count);

    v->n_bfrs=vrb.count;
    v->bfrs=malloc(sizeof(allo_buffer_stc)*v->n_bfrs);
    for(i=0;i<v->n_bfrs;i++)
    {
      v->bfrs[i].b.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
      v->bfrs[i].b.memory=V4L2_MEMORY_MMAP;
      v->bfrs[i].b.index=i;
      if(ioctl(v->u,VIDIOC_QUERYBUF,&v->bfrs[i].b)<0)
	rb_raise(rb_eRuntimeError,"%s: error requesting buffer #%d for <%s> (%s)",__func__,i,bfr,strerror(errno));
      v->bfrs[i].l=v->bfrs[i].b.length;
      v->bfrs[i].d=mmap(NULL,v->bfrs[i].b.length,PROT_READ|PROT_WRITE,MAP_SHARED,v->u,v->bfrs[i].b.m.offset);
      if(v->bfrs[i].d==MAP_FAILED)
	rb_raise(rb_eRuntimeError,"%s: error mapping buffer #%d for <%s> (%s)",__func__,i,bfr,strerror(errno));
      
      fprintf(stderr,"Buffer #%d is %d bytes long, starting at %x\n",i+1,v->bfrs[i].b.length,v->bfrs[i].b.m.offset);

/*
 * Enqueue it
 */
    
      if(ioctl(v->u,VIDIOC_QBUF,&v->bfrs[i].b)<0)
	rb_raise(rb_eRuntimeError,"%s: error enqueuing buffer #%d for <%s> (%s)",__func__,i,bfr,strerror(errno));
    }

    sleep(1);
    
    i=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(v->u,VIDIOC_STREAMON,&i)<0)
    {
      close(v->u);
      rb_raise(rb_eRuntimeError,"%s: error starting streaming for <%s> (%s)",__func__,bfr,strerror(errno));
    }

/*
 * The thread for reading/processing frames.
 */
    
    int ret=pthread_mutex_init(&v->mtx,NULL);
    if(ret)
      rb_raise(rb_eArgError,"%s: error #%d cresting mutex (%s)",__func__,ret,strerror(errno));
    
    v->exit_thread=0;
    ret=pthread_create(&v->read_thr,NULL,background_read,v);
    if(ret)
      rb_raise(rb_eArgError,"%s: error #%d starting background read thread (%s)",__func__,ret,strerror(errno));
  }
  else
  {
      
/*
 * We are slaves. Open the shared memory area and find out the info about the frame
 */

    if((sva->shm_id=shmget(IPC_KEY,0,0))<0)
      rb_raise(rb_eRuntimeError,"%s: Error in shmget (slave) (%s)",__func__,strerror(errno));  
    if((sva->shareddata=shmat(sva->shm_id,NULL,0))==NULL)
      rb_raise(rb_eRuntimeError,"%s: Error in shmat (slave) (%s)",__func__,strerror(errno));
    sva->ss=(shared_stc *)sva->shareddata;
    sva->framedata=(void *)sva->ss+sizeof(shared_stc);

    fprintf(stderr,"%s: Existing area: found x=%d y=%d (%d pixels)\n",__func__,sva->ss->x,sva->ss->y,sva->ss->framesize);
  }
  
  return Data_Wrap_Struct(cls_sharedvideoarea,NULL,free_boij_sharedvideoarea,sva);
}

static void free_boij_sharedvideoarea(void *p)
{
  boij_sharedvideoarea_stc *sva=(boij_sharedvideoarea_stc *)p;
  v4l_stc *v=&sva->v4l;
  int i;

  /**/fprintf(stderr,"%s called!\n",__func__);
  
  if(sva->masterflag)
  {
    sva->v4l.exit_thread=1;
    pthread_join(sva->v4l.read_thr,NULL);

    i=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(sva->v4l.u,VIDIOC_STREAMOFF,&i)<0)
      fprintf(stderr,"%s: error stopping streaming (%s) (unit %d)\n",__func__,
	      strerror(errno),sva->v4l.u);
    
    for(i=0;i<sva->v4l.n_bfrs;i++)
      munmap(sva->v4l.bfrs[i].d,sva->v4l.bfrs[i].l);  
    free(sva->v4l.bfrs);
    
    close(sva->v4l.u);

    free(v->frame_park);
    free(v->frame_map);
  }  

  shmdt(sva->shareddata);
  
  if(sva->masterflag)
    shmctl(sva->shm_id,IPC_RMID,NULL);

  free(p);
}

VALUE sva_latest_framedata(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);
  v4l_stc *v=&sva->v4l;

  if(!v->newframe)
    return Qfalse;

  pthread_mutex_lock(&v->mtx);
  memcpy(sva->framedata,v->frame_park,v->npix_out);
  v->newframe=0;
  pthread_mutex_unlock(&v->mtx);

  return Qtrue;
}

VALUE sva_shp(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);
  
  return Data_Wrap_Struct(rb_cObject,NULL,NULL,sva->ss);
}

VALUE sva_getthresh(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);

  VALUE to_ret=rb_ary_new();
  int i;

  for(i=0;i<N_THRESHS;i++)
    rb_ary_push(to_ret,INT2FIX((int)(sva->ss->threshs[i]*100.0)));
  
  return to_ret;
}

VALUE sva_setthresh(VALUE self,VALUE thresh)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);
  int i;

  for(i=0;i<N_THRESHS;i++)
    sva->ss->threshs[i]=FIX2INT(rb_ary_entry(thresh,i))/100.0;

  return self;
}  

VALUE sva_getclients(VALUE self,VALUE bypass_flags)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);

  if(bypass_flags!=Qtrue && !sva->ss->produced)
    return Qfalse;

  VALUE toret=rb_ary_new(),ele,coord;
  int i;
  client_stc *c;

  for(i=0;i<MAX_MONITORED;i++)
  {
    c=&sva->ss->clients[i];
    
    if(c->present)
    {      
      ele=rb_ary_new();
      rb_ary_push(ele,INT2FIX(i));
      rb_ary_push(ele,c->new_entry ? Qtrue : Qfalse);
      rb_ary_push(ele,rb_float_new((float)c->arrived.time+((float)c->arrived.time/1000.0)));
      coord=rb_ary_new();
      rb_ary_push(coord,INT2FIX(c->blob.cx));
      rb_ary_push(coord,INT2FIX(c->blob.cy));
      rb_ary_push(coord,INT2FIX(c->blob.minx));
      rb_ary_push(coord,INT2FIX(c->blob.miny));
      rb_ary_push(coord,INT2FIX(c->blob.maxx));
      rb_ary_push(coord,INT2FIX(c->blob.maxy));
      rb_ary_push(coord,rb_float_new(c->blob.fweight));
      rb_ary_push(ele,coord);
      rb_ary_push(toret,ele);
    }
  }
  
  if(bypass_flags!=Qtrue)
    sva->ss->produced=0;

  return toret;
}

VALUE sva_getframesize(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);

  VALUE to_ret=rb_ary_new();

  rb_ary_push(to_ret,INT2FIX(sva->ss->x));
  rb_ary_push(to_ret,INT2FIX(sva->ss->y));

  return to_ret;
}

VALUE sva_getgrid(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);
  VALUE grval=rb_ary_new(),col,el;
  int i,j;

  for(i=0;i<=GRID_Y;i++)
  {
    col=rb_ary_new();
    for(j=0;j<=GRID_X;j++)
    {
      el=rb_ary_new();      
      rb_ary_push(el,INT2FIX(sva->ss->grid_offsets[i][j][0]));
      rb_ary_push(el,INT2FIX(sva->ss->grid_offsets[i][j][1]));
      rb_ary_push(col,el);
    }
    rb_ary_push(grval,col);
  }

  VALUE retval=rb_ary_new();
  rb_ary_push(retval,grval);
  rb_ary_push(retval,sva->ss->togx ? Qtrue : Qfalse);
  rb_ary_push(retval,sva->ss->togy ? Qtrue : Qfalse);
  
  return retval;
}

VALUE sva_setgrid(VALUE self,VALUE grid,VALUE togx,VALUE togy)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);
  VALUE col,el;
  int i,j;
  
  for(i=0;i<=GRID_Y;i++)
  {
    col=rb_ary_entry(grid,i);
    for(j=0;j<=GRID_X;j++)
    {
      el=rb_ary_entry(col,j);
      sva->ss->grid_offsets[i][j][0]=FIX2INT(rb_ary_entry(el,0));
      sva->ss->grid_offsets[i][j][1]=FIX2INT(rb_ary_entry(el,1));
    }
  }
  sva->ss->togx=(togx==Qtrue) ? 1 : 0;
  sva->ss->togy=(togy==Qtrue) ? 1 : 0;

  sva->ss->request_grid_recalc=1;

  return self;
}

VALUE sva_relogrid(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);

  if(sva->ss->request_grid_recalc)
  {
    sva->ss->request_grid_recalc=0;
    recompute_grid(sva);
  }

  return self;
}

VALUE sva_getframe(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);
/*fprintf(stderr,"Frame at %p, %d pixels long\n",sva->framedata,sva->ss->framesize);*/
  return rb_str_new((char *)sva->framedata,sva->ss->framesize);
}

VALUE sva_bkglevel(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);

  return rb_float_new(avg_frame_background(sva->framedata,sva->ss->framesize));
}

VALUE sva_fixgreyness(VALUE self)
{
  boij_sharedvideoarea_stc *sva;
  Data_Get_Struct(self,boij_sharedvideoarea_stc,sva);

  float v=avg_frame_background(sva->framedata,sva->ss->framesize);
  float newthresh=0.15+v*0.75;

  sva->ss->threshs[0]=newthresh;
  sva->ss->threshs[1]=newthresh-0.02;

  VALUE toret=rb_ary_new();
  rb_ary_push(toret,rb_float_new(v));
  rb_ary_push(toret,rb_float_new(newthresh));

  return toret;
}

/*
 * gets a column array, returns a flipped single string
 */

extern VALUE sva_fliprotate(VALUE self,VALUE cols)
{
  int n_cols=RARRAY_LEN(cols),n_lines=RSTRING_LEN(rb_ary_entry(cols,0))/3;

  int reslen=n_lines*n_cols*3,i,x,y;
  char *bfr=malloc(reslen),*ptr=bfr,*cptr[n_cols];

  for(i=0;i<n_cols;i++)
    cptr[i]=RSTRING_PTR(rb_ary_entry(cols,i));

  for(y=0;y<n_lines;y++)
  {    
    for(x=0;x<n_cols;x++)
    {
      ptr[0]=cptr[x][2];
      ptr[1]=cptr[x][1];
      ptr[2]=cptr[x][0];
      ptr+=3;
      cptr[x]+=3;
    }
  }
  
  VALUE to_ret=rb_str_new(bfr,reslen);

  free(bfr);

  return to_ret;
}
	
	
  
  
  
  
  
  

VALUE sva_tripl(VALUE self,VALUE frm)
{
  int frl=RSTRING_LEN(frm),i;
  unsigned char *aa=malloc(frl*3),*dptr=aa,*optr=(unsigned char *)RSTRING_PTR(frm);

  for(i=0;i<frl;i++,optr++)
  {
    *dptr++=*optr;
    *dptr++=*optr;
    *dptr++=*optr;
  }

  VALUE retv=rb_str_new((char *)aa,frl*3);
  free(aa);

  return retv;
}

VALUE sva_pixflip(VALUE self,VALUE frm)
{
  int frl=RSTRING_LEN(frm),i;
  __u8 *tmpf=malloc(frl),*curfrom=(__u8 *)RSTRING_PTR(frm),*curto=tmpf;

  for(i=0;i<frl/3;i++,curfrom+=3,curto+=3)
  {
    curto[0]=curfrom[2];
    curto[1]=curfrom[1];
    curto[2]=curfrom[0];
  }

  VALUE toret=rb_str_new((char *)tmpf,frl);
  free(tmpf);

  return toret;
}

VALUE sva_squaret(VALUE self,VALUE vsq,VALUE vdot)
{
  dot_stc d[5];
  square_stc sq;
  int i;
  VALUE v;

  for(i=0;i<4;i++)
  {
    v=rb_ary_entry(vsq,i);
    d[i].x=FIX2INT(rb_ary_entry(v,0));
    d[i].y=FIX2INT(rb_ary_entry(v,1));
    sq.s[i]=&d[i];
  }
  d[4].x=FIX2INT(rb_ary_entry(vdot,0));
  d[4].y=FIX2INT(rb_ary_entry(vdot,1));

  return INT2FIX(in_square(&sq,&d[4]));
}

VALUE sva_extendline(VALUE self,VALUE line,VALUE v_linelen_to,VALUE v_bottom_offset,VALUE v_blackfill)
{
  char *orpixels=RSTRING_PTR(line);
  int linelen_from=RSTRING_LEN(line)/3;
  int linelen_to=FIX2INT(v_linelen_to);
  int bottom_offset=FIX2INT(v_bottom_offset);
  int top_offset=linelen_to-linelen_from-bottom_offset;
  enum blackfill blackfill=FIX2INT(v_blackfill);
  char *destpixels=malloc(linelen_to*3),*ptr=destpixels;
  int i;

  for(i=0;i<bottom_offset;i++,ptr+=3)
    memcpy(ptr,blackfill==BLACKFILL_EXTEND ? orpixels : orpixels+(bottom_offset-i-1)*3,3);

  memcpy(ptr,orpixels,RSTRING_LEN(line));
  ptr+=RSTRING_LEN(line);
  
  for(i=0;i<top_offset;i++,ptr+=3)
    memcpy(ptr,orpixels+(linelen_from-(blackfill==BLACKFILL_EXTEND ? 1 : i+1))*3,3);

  VALUE retval=rb_str_new(destpixels,linelen_to*3);
  free(destpixels);

  return retval;
}

static void *background_read(void *arg)
{
  boij_sharedvideoarea_stc *sva=(boij_sharedvideoarea_stc *)arg;
  v4l_stc *v=&sva->v4l;
  struct v4l2_buffer b;
  __u8 *pin,*pout;
  int i;

  while(!v->exit_thread)
  {
    bzero(&b,sizeof(struct v4l2_buffer));
    
    b.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    b.memory=V4L2_MEMORY_MMAP;
    
    if(ioctl(v->u,VIDIOC_DQBUF,&b)<0)
    {
      perror("VIDIOC_DQBUF");
      return NULL;
    }

/*
 * Map the frame
 */

    pthread_mutex_lock(&v->mtx);
    for(pout=v->frame_park,pin=(__u8 *)v->bfrs[b.index].d,i=0;i<v->npix_out;i++)
      *pout++=pin[v->frame_map[i]];
    
    v->newframe=1;
    pthread_mutex_unlock(&v->mtx);

    if(ioctl(v->u,VIDIOC_QBUF,&b)<0)
    {
      perror("VIDIOC_QBUF");
      return NULL;
    }
  }
  
  return NULL;
}

static void recompute_grid(boij_sharedvideoarea_stc *sva)
{
  v4l_stc *v=&sva->v4l;

  int i,j;

  for(i=0;i<=GRID_Y;i++)
    for(j=0;j<=GRID_X;j++)
    {
      sva->griddots[i][j].x=sva->griddots[i][j].ox+sva->ss->grid_offsets[i][j][0];
      sva->griddots[i][j].y=sva->griddots[i][j].oy+sva->ss->grid_offsets[i][j][1];
    }
  
  dot_stc dot;
  square_stc *sq=NULL;
  
  unsigned int *uiptr=v->frame_map,rowv,colv,x,y;
  double factx=(double)v->dpx.use/(double)v->dpx.out,facty=(double)v->dpy.use/(double)v->dpy.out;
  float dist[4];
  line_stc ln;
  
  for(dot.y=0;dot.y<sva->ss->y;dot.y++)
  {
    for(dot.x=0;dot.x<sva->ss->x;dot.x++)
    {      
      for(sq=NULL,i=0;i<GRID_Y && !sq;i++)
	for(j=0;j<GRID_X && !sq;j++)
	  if(in_square(&sva->gridsquares[i][j],&dot))
	    sq=&sva->gridsquares[i][j];
      if(!sq)
	x=y=0;
      else
      {
/*fprintf(stderr,"{%d,%d} -> [%d,%d]\n",dot.x,dot.y,j,i);*/
	
	for(i=0;i<4;i++)
	{
	  ln.d1=sq->s[i];
	  ln.d2=sq->s[(i+1)%4];
	  dist[i]=point_line_distance(&ln,&dot);
	}
	x=(int)(sq->s[0]->ox+sva->grid_int_x*(dist[3]/(dist[1]+dist[3])));
	if(x<0)
	  x=0;
	else if(x>=sva->ss->x)
	  x=sva->ss->x-1;
	y=(int)(sq->s[0]->oy+sva->grid_int_y*(dist[0]/(dist[0]+dist[2])));
	if(y<0)
	  y=0;
	else if(y>=sva->ss->y)
	  y=sva->ss->y-1;
      }

      if(!sva->ss->togy)
	rowv=(int)(v->dpy.skip+y*facty);
      else
	rowv=(int)(v->dpy.skip+(sva->ss->y-y-1)*facty);
      rowv-=rowv%2;
      
      if(!sva->ss->togx)
	colv=v->dpx.use-(int)(v->dpx.skip+x*factx)-1;
      else
	colv=v->dpx.use-(int)(v->dpx.skip+(sva->ss->x-x-1)*factx)-1;
      
      /*fprintf(stderr,"%d,%d: [%d,%d] [%d,%d] %d (%.2f,%.2f,%.2f,%.2f)\n",dot.x,dot.y,x,y,colv,rowv,rowv*v->dpx.in+colv,
	dist[0],dist[1],dist[2],dist[3]);*/
      *uiptr++=rowv*v->dpx.in+colv;
    }
  }
}

static int ccw(dot_stc *d1,dot_stc *d2,dot_stc *d3)
{
  __s32 dx1,dx2,dy1,dy2,f1,f2;
  int retval;

  dx1=d2->x-d1->x;
  dy1=d2->y-d1->y;
  dx2=d3->x-d1->x;
  dy2=d3->y-d1->y;

  f1=dx1*dy2;
  f2=dy1*dx2;
  
  if(f1>f2)
    retval=1;
  else if(f1<f2)
    retval=-1;
  else
  {
    if(dx1*dx2<0 || dy1*dy2<0)
      retval=-1;
    else if((dx1*dx1+dy1*dy1)>=(dx2*dx2+dy2*dy2))
      retval=0;
    else
      retval=1;
  }
  /*fprintf(stderr,"CCW <%d,%d> <%d,%d> <%d,%d> -> (%d %d %d %d %d %d ) %d\n",
    d1->x,d1->y,d2->x,d2->y,d3->x,d3->y,dx1,dy1,dx2,dy2,f1,f2,retval);*/
  return retval;
}

static int intersect(line_stc *l1,line_stc *l2)
{
  int i1=ccw(l1->d1,l1->d2,l2->d1)*ccw(l1->d1,l1->d2,l2->d2);
  int i2=ccw(l2->d1,l2->d2,l1->d1)*ccw(l2->d1,l2->d2,l1->d2);

  /*fprintf(stderr,"Inters <%d,%d/%d,%d>-<%d,%d/%d,%d> gives %d,%d -> %d\n",
	      l1->d1->x,l1->d1->y,l1->d2->x,l1->d2->y,
	      l2->d1->x,l2->d1->y,l2->d2->x,l2->d2->y,i1,i2,(i1<=0 && i2<=0) ? 1 : 0);*/
  
  return (i1<=0 && i2<=0) ? 1 : 0;
}
  
static int in_square(square_stc *square,dot_stc *dot)
{
  dot_stc ldot={0x7fff,dot->y+1};
  line_stc lp,lt={dot,&ldot};
  int i,j=3,count=0;
  
  for(i=0;i<4;i++)
  {
    lp.d1=lp.d2=square->s[i];
    if(!intersect(&lp,&lt))
    {
      lp.d2=square->s[j];
      j=i;
      if(intersect(&lp,&lt))
	count++;
    }
  }
  /*fprintf(stderr,"(%d)",count);*/
  return count%2;
}

static float point_line_distance(line_stc *l,dot_stc *d)
{
  /*fprintf(stderr,"from {%d,%d-%d,%d} to %d,%d\n",l->d1->x,l->d1->y,l->d2->x,l->d2->y,d->x,d->y);*/
  int dx=l->d2->x-l->d1->x;
  int dy=l->d2->y-l->d1->y;
  float rv=fabs((d->x-l->d1->x)*dy-(d->y-l->d1->y)*dx);

  return rv/sqrtf(dx*dx+dy*dy);
}

static int afb_compare(const void *a, const void *b)
{
  if(*(__u8 *)b>*(__u8 *)a)
    return -1;
  if(*(__u8 *)b<*(__u8 *)a)
    return 1;
  return 0;
}

#define CLIPPOFACT 3

static float avg_frame_background(__u8 *frame,int size)
{
  int qlen=size/CLIPPOFACT,i;
  unsigned long long accum;
  
  __u8 *myf=malloc(size);

  memcpy(myf,frame,size);

  qsort(myf,size,1,afb_compare);

  for(accum=0,i=0;i<qlen;i++)
    accum+=myf[i];
  return (float)accum/(qlen*256.0);
}
