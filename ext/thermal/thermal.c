/* thermal.c */

/*
 * 13/12/2005 c.e. prelz AS FLUIDO <fluido@fluido.as>
 *
 * Serial tribbling is messy with ruby - so we talk with thermaleye via C
 *
 * $Id: thermal.c 1341 2007-08-20 16:36:43Z karl $
 */

#include <ruby.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <sys/poll.h>
#include <linux/types.h>

#define POLL_TIMEOUT 1500
#define BFR_SIZE (1024*64)

typedef struct
{
  int unit;
  struct termios params;
  pthread_t rcv_thr;
  __u8 keepalive,bfr[BFR_SIZE],*wptr,*rptr,overrun;
} thermal_stc;

static VALUE cls;

static VALUE new_thermal(VALUE self,VALUE path);
static void free_thermal(void *p);
static VALUE sendcommand(VALUE self,VALUE vcmd,VALUE vdata);
static VALUE readd(VALUE self);

static void push_short(unsigned char **bfr,unsigned long *cksum,unsigned short val);
static void *recv(void *arg);

void Init_thermal()
{
  cls=rb_define_class("Thermal",rb_cObject);
  rb_define_singleton_method(cls,"new",new_thermal,1);
  rb_define_method(cls,"send_command",sendcommand,2);
  rb_define_method(cls,"read",readd,0);
}

static VALUE new_thermal(VALUE self,VALUE path)
{
  thermal_stc *ts=ALLOC(thermal_stc);
  struct termios params;
  
  ts->unit=open(RSTRING_PTR(path),O_RDWR|O_SYNC|O_NONBLOCK);
//  ts->unit=open(RSTRING_PTR(path),O_RDWR|O_SYNC);
  if(ts->unit<0)
    rb_raise(rb_eRuntimeError,"%s: Error opening %s (%s)",__func__,RSTRING_PTR(path),
	     strerror(errno));

  if(tcgetattr(ts->unit,&ts->params)<0)
  {
    close(ts->unit);
    rb_raise(rb_eRuntimeError,"%s: Error in tcgetaddr on thermal's port (%s)",__func__,strerror(errno));
  }

  params=ts->params;
  
  params.c_lflag=0;
  params.c_iflag=IGNBRK|IGNPAR;
  params.c_oflag=0;
  fprintf(stderr,"Original cflag: %o\n",params.c_cflag);
//  params.c_cflag=B3000000|CS8|CREAD|PARENB;
// fprintf(stderr,"Mod cflag: %o\n",params.c_cflag);
//  params.c_cflag=B3000000|CS8|CREAD|CLOCAL|PARENB;
//  params.c_cflag=B9600|CS8|CREAD|CLOCAL|PARENB;
  params.c_cc[VMIN]='\0';
  params.c_cc[VTIME]='\0';

  if(tcsetattr(ts->unit,TCSANOW,&params)<0)
  {
    close(ts->unit);
    rb_raise(rb_eArgError,"%s: Error in tcsetaddr on thermal's port (%s)",__func__,strerror(errno));
  }

  ts->rptr=ts->wptr=ts->bfr;

/*
 * Start the receiver thread
 */

  ts->keepalive=1;
  int ret=pthread_create(&ts->rcv_thr,NULL,recv,ts);
  if(ret)
    fprintf(stderr,"%s: error #%d starting rcv thread\n",__func__,ret);  

  return Data_Wrap_Struct(cls,NULL,free_thermal,ts);
}

static void free_thermal(void *p)
{
  thermal_stc *t=(thermal_stc *)p;

  t->keepalive=0;
  pthread_join(t->rcv_thr,NULL);
  
  tcsetattr(t->unit,TCSANOW,&t->params);
  close(t->unit);
  free(t);
}

static VALUE sendcommand(VALUE self,VALUE vcmd,VALUE vdata)
{
  thermal_stc *t;
  Data_Get_Struct(self,thermal_stc,t);
  unsigned char bfr[1024],*ptr=bfr;

  int cmd=FIX2INT(vcmd);
  unsigned long cksum=0;

  push_short(&ptr,&cksum,2);
  push_short(&ptr,&cksum,cmd);

  int alen=RARRAY_LEN(vdata),i;

  for(i=0;i<alen;i++)
    push_short(&ptr,&cksum,FIX2INT(rb_ary_entry(vdata,i)));

  push_short(&ptr,&cksum,cksum&0xffff);
  
  {
    int i;
    for(i=0;i<ptr-bfr;i++)
      fprintf(stderr,"<%.2x>",bfr[i]);
    fputc('\n',stderr);
  }
  write(t->unit,bfr,ptr-bfr);
  fsync(t->unit);
  
  return self;
}

static VALUE readd(VALUE self)
{
  thermal_stc *ts;
  Data_Get_Struct(self,thermal_stc,ts);
  int len=((ts->wptr-ts->rptr)+BFR_SIZE)%BFR_SIZE,i;

  if(len<=0)
    return Qnil;

  __u8 rbfr[len],*rptr=rbfr;

  for(i=0;i<len;i++)
  {
    *rptr++=*(ts->rptr);
    ts->rptr++;
    if(ts->rptr>=ts->bfr+BFR_SIZE)
      ts->rptr=ts->bfr;
  }

  return rb_str_new((char *)rbfr,len);
}

static void push_short(unsigned char **bfr,unsigned long *cksum,unsigned short val)
{
  *(*bfr)++=val&0xff;
  *(*bfr)++=(val>>8)&0xff;

  *cksum-=val;
}

static void *recv(void *arg)
{
  thermal_stc *t=(thermal_stc *)arg;
//  struct pollfd pfd={t->unit,POLLIN|POLLERR|POLLHUP,0};
  unsigned char bfr[1024];
  int i,n;
  
  while(t->keepalive)
  {
    n=read(t->unit,bfr,1024);
    if(n<=0)
    {
      if(n<0 && errno!=EAGAIN)
	fprintf(stderr,"Ops: <%s>\n",strerror(errno));
//      else
//	fputc('*',stderr);
      
      usleep(POLL_TIMEOUT);
      continue;
    }
    /**/fprintf(stderr,"!!! %d chars rcvd\n",n);
    for(i=0;i<n;i++)
    {
      *t->wptr=bfr[i];
      t->wptr++;
      if((t->wptr-t->bfr)>=BFR_SIZE)
	t->wptr=t->bfr;
    }
	
//    fprintf(stderr,"[%d]",pfd.revents);
//    if(poll(&pfd,1,POLL_TIMEOUT)<=0)
//      continue;

//    fprintf(stderr,"oGoo %x %x\n",pfd.events,pfd.revents);
//    fputc('<',stderr);
//    for(i=0;i<n;i++)
//      fprintf(stderr,"%.2x",bfr[i]);
//    fputc('>',stderr);
  }
  return NULL;
}

    
    
    
