
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
#require "boij/version"

Gem::Specification.new do |spec|
  spec.name          = "boij"
  spec.version       = "0.1.0"
  spec.authors       = ["fluido", "TG x"]
  #spec.email         = [ ]

  spec.summary       = %q{Shan-Shui & Horizons}
  spec.description   = %q{interactive installation}
  spec.homepage      = "https://geertmul.nl/projects/shan-shui/"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata["homepage_uri"] = spec.homepage
    #spec.metadata["source_code_uri"] = "https:/gitlab.com/shanshui/boij"
    #spec.metadata["changelog_uri"] = "https:/gitlab.com/shanshui/boij"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  #spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
  #  `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  #end
  spec.files         = Dir.glob("{bin,lib,ext}/**/*.{rb,c,h}")
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.extensions    = Dir.glob("ext/**/extconf.rb")

  spec.add_development_dependency "bundler", "~> 1.17"
  #spec.add_development_dependency "rake", "~> 10.0"

  spec.add_dependency "opengl"
  spec.add_dependency "rubysdl"
  spec.add_dependency "rmagick"
  spec.add_dependency "sqlite3"
  spec.add_dependency "base64"

end
