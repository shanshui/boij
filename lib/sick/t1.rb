require 'sdl'
require 'boij'

WX=800
WY=200
MAX=0x1fff
FACT=WY.to_f/MAX

SDL.init(SDL::INIT_VIDEO)
screen=SDL::Screen.open(WX,WY,16,SDL::SWSURFACE)
SDL::WM::set_caption('SICK!','SICK!')

z=Sick::new('/dev/ttyUSB0',Sick::SCANRANGE_100,Sick::ANGRES_0_5)

loop do
  while event = SDL::Event.poll
    case event
    when SDL::Event::KeyDown, SDL::Event::Quit
      exit
    end
  end

  r=z.msg
  if(r)
    if(r==Sick::ACK)
      loggo("ACK")
    elsif(r[2]==0xa0)
      loggo("A0: Rcvd #{r[3].unpack('C*')}")
    elsif(r.length==4)
      vs=r[3].unpack('S*')
      screen.fill_rect(0,0,WX,WY,[0,0,0])
      div=WX.to_f/vs.length
      vs.each_with_index do |v,i|
        x=div*i
        pos=v*FACT
        screen.fill_rect(x,0,div,pos,[50,50,200])
      end
      screen.flip
    end
  else
    #    putc('.')
    sleep(0.1)
  end
end
