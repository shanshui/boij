#!/usr/bin/env ruby
# sensorgrid_visio.rb

=begin

30/12/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

A visual prop for studying sensorgrid effects

$Id: sensorgrid_visio.rb 2690 2011-01-14 07:38:03Z karl $

=end

require 'playground_sensorgrid'
require 'sdl'

class Sensorgrid_visio
  PERSQUARE=105
  SLEEP_TIME=0.06

  MAX_KICK=50.0

  TOP=1.0
  TOP_HALF=TOP/2.0
  COLVAL=255/TOP_HALF
  
  def initialize
    @pg=Playground_sensorgrid::getremote()
    SDL::init(SDL::INIT_VIDEO)
    @screen=SDL::Screen::open(Playground_sensorgrid::NX*PERSQUARE,Playground_sensorgrid::NY*PERSQUARE,
                             32,SDL::HWSURFACE|SDL::DOUBLEBUF)
  end
  
  def runme
    loop do
      loop do
        event=SDL::Event2::poll()
        break if(!event)
        case event
        when SDL::Event2::Quit
          exit(0)
        when SDL::Event2::KeyDown
          case event.sym
          when SDL::Key::ESCAPE
            exit(0)
          end
        when SDL::Event2::MouseButtonDown
          eve(event.x/PERSQUARE,event.y/PERSQUARE)
        end
      end
      
      @pg.cur_values().each do |x,y,v|
        paintsquare(x,y,v)
      end
      @screen.updateRect(0,0,0,0)
      @screen.flip()
      sleep(SLEEP_TIME)
    end
  end

  def eve(x,y)
    @pg.kick(x,y,rand()*MAX_KICK)
  end

  def paintsquare(x,y,v)
    v=TOP if(v>TOP)
    @screen.fillRect(x*PERSQUARE,y*PERSQUARE,PERSQUARE,PERSQUARE,
                     [v<TOP_HALF ? 0 : (v-TOP_HALF)*COLVAL,0,v>TOP_HALF ? 0 : (TOP_HALF-v)*COLVAL])
  end
end

if($0==__FILE__)
  Sensorgrid_visio::new().runme()
end

