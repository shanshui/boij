#!/usr/bin/env ruby
# playground_onlooker.rb

=begin

18/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This is the real playground - monitored by camera

$Id: playground_onlooker.rb 1628 2008-07-13 08:19:02Z karl $

=end

require 'boij'

class Onlooker
  attr_accessor(:chgflg)

  VIDEOCONN_LOOPSLEEP=2.0
  TIMES_TO_WAIT=10
  THRESH_INTERVAL=60*30 # update threshold every half-hour
  
  def initialize
    videodata=[0,'Composite2','8 bpp gray',CAPTURE_PARA[0],CAPTURE_PARA[1],IMG_PARA]
    
    TIMES_TO_WAIT.times do |i|
      begin
	@sva=Boij_sharedvideoarea::new(videodata)
	break
      rescue => err
        loggo("Attempt #{i+1}: refused video port access (#{err.to_s()})")
        @sva=nil
        sleep(VIDEOCONN_LOOPSLEEP)
      end
    end

    raise "Could not open video port" if(!@sva)
    
    @sva.set_threshold(THRESHPARA)
    @latest_thresh_update=Time::now()+4.0

    grid=[]
    togx=false
    togy=false
    File::open(GRIDFILE,'r') do |f|
      (Boij_sharedvideoarea::GRID_Y+1).times do |i|
	grid[i]=[]
	(Boij_sharedvideoarea::GRID_X+1).times do |j|
	  grid[i][j]=f.read(4).unpack('ss')
	end
      end
      togx,togy=f.read(4).unpack('ss')
    end
    @sva.set_grid(grid,togx>0,togy>0)

    @fx,@fy=@sva.get_framesize()

    @regr=Regrouper::new(@sva.sharedarea_pointer())

    @networker=Server_networker::new(self)

    @done=false
    Signal::trap('INT') do
      @done=true
    end
    Signal::trap('TERM') do
      @done=true
    end
  end
  
  def runme
    while(!@done)
      ret=@sva.latest_framedata()
      if(ret)
	@sva.reload_grid_if_needed()
	@regr.regroup()
	nc=@sva.get_clients(false)
	retp=Array::new(Boij_sharedvideoarea::MAX_MONITORED,nil)
        nc.each() do |cl|
	  retp[cl[0]]=[cl[3][0].to_f()/@fx,cl[3][1].to_f()/@fy]
        end
	@networker.send_pkt(retp)
      end
      now=Time::now()
      if(now>=@latest_thresh_update)
	@latest_thresh_update=now+THRESH_INTERVAL
	v,th=@sva.fix_greyness()
	loggo("Background now at #{v}. Thresh set to #{(th*100.0).to_i()}")
      end
      sleep(0.01)
    end
    @networker.done=true
  end
end

if($0==__FILE__)
  lf=Fluid_logfile::new('onlooker')
  
  oo=Onlooker::new()
  begin
    oo.runme()
  rescue
    pp ["Onlooker crash",$!,$!.backtrace()]
  end
end
