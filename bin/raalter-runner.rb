#!/usr/bin/env ruby
# raalte_runner.rb

=begin

17/03/2011 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The logic that runs the process in Raalte

$Id: raalte_runner.rb 2834 2011-03-17 09:09:59Z karl $

=end

require 'boij'
require 'engine4'
require 'playground_sensorlines_3'
require 'sensorline_phidget'

Fluid_logfile::new('runner')

def interr
  $eps.reverse.each do |ep|
    loggo("Expiring pid #{ep.pid} (#{ep.cls.name})")
    ep.expire()
    sleep(2)
  end
  sleep(3)
  loggo('Terminating.')
  exit(0)
end

daemons=[[Engine,nil],5,[Playground_sensorlines,nil],[Sensorline_phidget,nil]]

$eps=[]
daemons.each do |d|
  if(d.is_a?(Numeric))
    sleep(d)
  else
    $eps.push(External_proc::new(d[0],d[1]))
  end
end

Signal::trap('INT') do
  exit(0)
end
Signal::trap('HUP') do
  exit(0)
end

at_exit do
  interr()
end

loop do
  sleep(20)
  $eps.each do |ep|
    v=ep.is_dead?()
    if(v)
      loggo("#{ep.cls.to_s}/#{ep.pid} died! Must die to be reborn.")
      exit(0)
    end
  end
end
