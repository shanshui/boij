#!/usr/bin/env ruby
# manager_webber.rb

=begin

05/07/2010 c.e.prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Web-based manager for maintaining the image/folder dbase

$Id: manager_webber.rb 3959 2013-05-21 13:34:43Z karl $

=end

require 'boij.so'
require 'image_dbase'

require 'webrick'
require 'webrick/httpserver'
require 'webrick/https'
require 'webrick/utils'
require 'digest/md5'
require 'rmagick'

class Wworkimage
  attr_accessor(:image,:reduced_image,:md5,:horpos,:rx,:ry)
  D_AREA=800*800#1024*1024

  DIGFACT=1000
  BWIDTH=5
  REDUCE_FACTOR=0.6

  def destroy!
    @image.destroy!()
    @reduced_image.destroy!()
  end

  def save_ref_image
    rimg=Magick::Image::new(@rx,@ry)
    rimg.alpha(Magick::ActivateAlphaChannel)

    gc=Magick::Draw::new()

    gc.composite(0,0,0,0,@reduced_image)
    gc.fill('red')
    gc.stroke('white')
    gc.stroke_width(2)
    gc.opacity(0.6)
    pos=@ry-(@ry*(@horpos/DIGFACT.to_f))
    gc.polygon(0,pos-BWIDTH,@rx,pos-BWIDTH,@rx,pos+BWIDTH,0,pos+BWIDTH)
    gc.draw(rimg)

    rimg.write(MATBASE+@md5+'.png')
    rimg.destroy!()
  end

  def save_thumbnails(i,size)
    gc=Magick::Draw::new()
    gc.fill('red')
    gc.stroke('white')
    gc.stroke_width(2)
    gc.opacity(0.6)
    pos=size[1]-(size[1]*(@horpos/DIGFACT.to_f))
    gc.polygon(0,pos-BWIDTH,size[0],pos-BWIDTH,size[0],pos+BWIDTH,0,pos+BWIDTH)
    gc.draw(i)
    i.scale!(REDUCE_FACTOR)
    i.write(MATBASE+'/medium/'+@md5+'.png')
    i.scale!(REDUCE_FACTOR)
    i.write(MATBASE+'/small/'+@md5+'.png')

    i.destroy!()
  end

  def rot(ang)
    @image.rotate!(ang)

    ratio=SCREEN_SIZE[1]/@image.rows.to_f()
    new_size=[@image.columns*ratio,SCREEN_SIZE[1]]
    @image.resize!(*new_size)
    @x=@image.columns
    @y=@image.rows
    @ratio=@x.to_f/@y
    loggo("#{@md5}: rotated image is [#{@x}X#{@y}].")

    @rx=(Math::sqrt(Wnewimage::D_AREA*@ratio)).to_i
    @ry=(@rx/@ratio).to_i

    @reduced_image=@image.resize(@rx,@ry)
    save_ref_image()

    @rotated=true
  end
end

class Woldimage < Wworkimage
  attr_reader(:dbimage)

  def initialize(dbimage)
    @dbimage=dbimage
    @md5=@dbimage.md5
    @horpos=@dbimage.horizon

    @image=Magick::Image::read("#{PICTBASE}#{@dbimage.id}.png")[0]

    @x=@image.columns
    @y=@image.rows
    @ratio=@x.to_f/@y

    @rx=(Math::sqrt(Wnewimage::D_AREA*@ratio)).to_i
    @ry=(@rx/@ratio).to_i

    @reduced_image=@image.resize(@rx,@ry)
    save_ref_image()
  end

  def save
    if(@rotated) # must save all!
      @image.write("#{PICTBASE}#{@dbimage.id}.png")
      horiz=(@horpos/DIGFACT.to_f*@ry).to_i
      File::open("#{PICTBASE}#{@dbimage.id}.bo2",'w') do |f|
        f.puts(@md5)
        f.printf("%d,%d,%d\n",@rx,@ry,horiz)
        @rx.times do |col|
          f.write(@image.export_pixels_to_str(col,0,1,@ry,'BGR'))
        end
      end
      save_thumbnails(@image,[@x,@y])
    else

      #
      # First update horizon value in working file
      #

      fn="#{PICTBASE}#{@dbimage.id}.bo2"

      data=nil
      sx=nil
      sy=nil
      File::open(fn,'r') do |f|
        f.gets
        sx,sy,oldhor=f.gets.split(',')
        sx=sx.to_i
        sy=sy.to_i
        data=f.read()
      end
      horiz=(@horpos/DIGFACT.to_f*sy).to_i
      File::open(fn,'w') do |f|
        f.puts(@md5)
        f.printf("%d,%d,%d\n",sx,sy,horiz)
        f.write(data)
      end
      save_thumbnails(@image.resize(sx,sy),[sx,sy])
    end

    loggo("#{@md5} redumped.")
  end
end

class Wnewimage < Wworkimage
  attr_accessor(:created,:folder)

#  BORDER=70
#  LABEL=100
  NSTEPS=11
  FDOTS=30

  def initialize(image,md5,folder)
    @image,@md5,@folder=image,md5,folder
    @horpos=DIGFACT>>1
    @created=Time::now()

    @x=@image.columns
    @y=@image.rows
    @ratio=@x.to_f/@y

    @rx=(Math::sqrt(D_AREA*@ratio)).to_i
    @ry=(@rx/@ratio).to_i

    @reduced_image=@image.resize(@rx,@ry)

    save_ref_image() if(!File::exist?(MATBASE+@md5+'.png'))
  end

  def save(id)
    #
    # First save original resolution
    #

    @image.write("#{PICTBASE}#{id}.png")

    ratio=SCREEN_SIZE[1]/@y.to_f()
    new_size=[@x*ratio,SCREEN_SIZE[1]]
    i=@image.resize(*new_size)
    new_size=[i.columns(),i.rows()]
    horiz=(@horpos/DIGFACT.to_f*new_size[1]).to_i
    loggo("#{@md5}: resulting image is [#{new_size[0]}X#{new_size[1]}] with hori #{horiz}.")

    fn="#{PICTBASE}#{id}.bo2"
    File::open(fn,'w') do |f|
      f.puts(@md5)
      f.printf("%d,%d,%d\n",*new_size,horiz)
      new_size[0].times do |col|
        f.write(reverse_threebythree(i.export_pixels_to_str(col,0,1,i.rows,'RGB')))
      end
    end

    save_thumbnails(i,new_size)

    loggo("#{@md5} dumped.")
  end
end

class Wsession
  attr_reader(:user,:logintime,:md5,:uploaded_images)
  attr_accessor(:message)

  IDLE_TIME=60*5 # 5 minutes

  def initialize(db,user)
    @db,@user=db,user
    @logintime=Time::now
    @accesstime=Time::now
    @md5=Digest::MD5::hexdigest(@logintime.stamp+@user.nick)
    @message=nil
    @uploaded_images={}
  end

  def touch
    @accesstime=Time::now
  end

  def expired?
    (Time::now()-@accesstime)>=IDLE_TIME
  end

  def add_images_to_upload(list)
    list.each do |path,blob|
      if(@uploaded_images[path])
        loggo("Oops: #{path} doubly requested")
      else
        @uploaded_images[path]=blob
      end
    end
  end

  def next_image
    return nil if(@uploaded_images.length()<=0)
    k=@uploaded_images.keys.sort[0]
    im=@uploaded_images[k]
    loggo("Returning #{k} (#{im.length})")
    @uploaded_images.delete(k)
    [k,im]
  end
end

class Manager_webber < WEBrick::HTTPServer
  require 'manager_pages'

  CERT_SUBJECT=[['O','fluido.as'],['OU','nio'],['CN','horizon_manager']]
  CERT_COMMENT='Self-signed certificate for access to horizon image manager'
  CERT_BITS=1024
  WEBSERVE_CERT=BASE+'cert'
  WEBSERVE_KEY=BASE+'key'

  STAMP_AREA=200*200

  def initialize
    @sessions={}
    @loaded_images={}

    @db=Image_dbase::new()
    reload_folder_tree()
    Signal::trap('USR1') do
      loggo("Request to reload")
      reload_folder_tree()
      true
    end

    cert,rsa=get_cert()
    super(:Port=>MANAGER_WEB_PORT,
#          :BindAddress=>"2001:888:30b3:3::4",
          :ServerName=>'Bidibodibu2',
          :ServerSoftware=>'AsFluidoHorizon',
          :SSLEnable=>true,
          :SSLCertificate=>cert,
          :SSLPrivateKey=>rsa)

    mount_proc('/',self.method(:rootpage).to_proc())
    mount_proc('/session',self.method(:sesspage).to_proc())
    mount_proc('/folder',self.method(:folderpage).to_proc())
    mount_proc('/image',self.method(:imagepage).to_proc())
    mount_proc('/digest_images',self.method(:digestpage).to_proc())
    mount_proc('/process_parked',self.method(:processparkedpage).to_proc())
    mount_proc('/sethor_image',self.method(:sethorpage).to_proc())
    mount("/material",WEBrick::HTTPServlet::FileHandler,MATBASE)
  end

  def reload_folder_tree
    @rootfolders,@folders,@images=Wfolder::load(@db)
    @default_folder=@rootfolders.values[0]
  end

  def get_cert
    if(!File::exists?(WEBSERVE_CERT))
      cert,rsa=make_cert(rand(0xffffff),CERT_SUBJECT,CERT_COMMENT)
      File::open(WEBSERVE_CERT,'w') do |f|
        f.write(cert.to_pem)
      end
      File::open(WEBSERVE_KEY,'w') do |f|
        f.write(rsa.to_pem)
      end
      return [cert,rsa]
    end

    txt=nil
    File::open(WEBSERVE_CERT,'r') do |f|
      txt=f.read
    end
    cert=OpenSSL::X509::Certificate::new(txt)
    txt=nil
    File::open(WEBSERVE_KEY,'r') do |f|
      txt=f.read
    end
    rsa=OpenSSL::PKey::RSA::new(txt)

    [cert,rsa]
  end

  def make_cert(serial,subject,comment)
    key=OpenSSL::PKey::RSA::generate(CERT_BITS)
    pub=key.public_key
    ca=OpenSSL::X509::Name::new(subject)
    cert=OpenSSL::X509::Certificate.new()
    cert.version=2
    cert.serial=serial
    cert.subject=ca
    cert.issuer=ca
    cert.public_key=pub
    cert.not_before=Time::now()
    cert.not_after=Time.now+(365*24*60*60*10)
    cert.sign(key,OpenSSL::Digest::SHA1::new())
    [cert,key]
  end

  def maintain
    @sessions.each do |k,ses|
      if(ses.expired?())
        loggo("Session for user id #{ses.user.id} has expired.")
        @sessions.delete(k)
      end
    end
  end

  def parked_images
    pi=[]
    Dir::foreach(PARKINGBASE) do |ent|
      pi.push(ent.split('.')[0]) if(ent.end_with?('.stamp.png'))
    end
    pi.length>0 ? pi : nil
  end

  def ask_engine_to_reload
    begin
      Fluid_logfile::killpid('boij_Engine','USR1')
    rescue
    end
  end
end

if(__FILE__==$0)
  Fluid_logfile::new('manager_webber')

  SLEEP_TIME=1

  $ws=Manager_webber::new()
  $webthread=Thread::new do
    $ws.start()
  end

  def die
    $ws.shutdown()
    $webthread.join()
    exit(0)
  end

  Signal::trap('INT') do
    die()
  end
  Signal::trap('HUP') do
    die()
  end
  Signal::trap('TERM') do
    die()
  end

  loop do
    $ws.maintain()
    sleep(SLEEP_TIME)
  end
end
