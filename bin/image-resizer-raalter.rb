#!/usr/bin/env ruby

require 'boij.so'
require 'image_dbase'
require 'rmagick'

DIGFACT=1000

db=Image_dbase::new()

db.db.execute('select id,md5,horizon from image order by id').each do |row|
  id=row[0]
  image=Magick::Image::read(PICTBASE+id.to_s+'.png')[0]
  x=image.columns()
  y=image.rows()

  ratio=SCREEN_SIZE[1]/y.to_f()
  new_size=[x*ratio,SCREEN_SIZE[1]]
  image.resize!(*new_size)
  new_size=[image.columns(),image.rows()]
  horiz=(row[2]/DIGFACT.to_f*new_size[1]).to_i
  loggo("#{row[1]}: resulting image is [#{new_size[0]}X#{new_size[1]}] with hori #{horiz}.")

  fn="#{PICTBASE}#{id}.bo2"
  File::open(fn,'w') do |f|
    f.puts(row[1])
    f.printf("%d,%d,%d\n",*new_size,horiz)
    new_size[0].times do |col|
      f.write(image.export_pixels_to_str(col,0,1,image.rows,'BGR'))
    end
  end
  image.destroy!()
end
