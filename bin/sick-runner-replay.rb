#!/usr/bin/env ruby
# sick_runner.rb

=begin

14/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

The logic that runs the process with a Sick sensor

$Id: sick_runner.rb 3472 2012-03-21 15:04:22Z karl $

=end

require 'boij'
require 'engine_sick'
require 'playground_sick'
require 'sick/lookaround'
require 'sick/background_saver'

BACKG_DETECT_KW='backg'
LOOKA_DETECT_KW='looka'
CAPTURE_KW='capture'
REPLAY_KW='replay'

Fluid_logfile::new('runner')

def interr
  $eps.reverse.each do |ep|
    loggo("Expiring pid #{ep.pid} (#{ep.cls.name})")
    begin
      ep.expire()
      sleep(2)
    rescue
    end
  end
  sleep(3)
  loggo('Terminating.')
  exit(0)
end

#
# If cmdline contains 'backg', run background detect and reboot
# If cmdline contains 'looka', run lookaround
# Otherwise, normal Sick run
#

replay=false

a=File::read('/proc/cmdline').split
if(a.index(BACKG_DETECT_KW))
  loggo("Backg detect request")
  Backgsa::new(SICK_UNIT,SICK_ANG,SICK_RESOL).runme()
  system('/sbin/shutdown -t1 -a -r now')
  exit()
elsif(a.index(LOOKA_DETECT_KW))
  loggo("Lookaround request")
  Lookaround::new().runme()
  system('/sbin/shutdown -t1 -a -r now')
  exit()
elsif(a.index(REPLAY_KW))
  loggo("Replay request")
  replay=true
end

daemons=[[Engine,nil],2,[Playground_sick,[replay]]]

$eps=[]
daemons.each do |d|
  if(d.is_a?(Numeric))
    sleep(d)
  else
    $eps.push(External_proc::new(d[0],d[1]))
  end
end

Signal::trap('INT') do
  exit(0)
end
Signal::trap('HUP') do
  exit(0)
end

at_exit do
  interr()
end

loop do
  sleep(20)
  $eps.each do |ep|
    v=ep.is_dead?()
    if(v)
      loggo("#{ep.cls.to_s}/#{ep.pid} died! Must die to be reborn.")
      exit(0)
    end
  end
end
