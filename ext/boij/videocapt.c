/* videocapt.c */

/*
 * 09/11/2010 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * The logic for capturing frames from good old thermocam (from laspa)
 *
 * $Id: videocapt.c 3876 2013-01-24 19:22:31Z karl $
 */

#include "boij.h"

#include <pthread.h>
#include <math.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <linux/videodev2.h>

#define VIDEOMODE VIDEO_MODE_PAL
#define REQBFRS 4

union desc
{
  unsigned char d[4];
  __u32 v;
};

typedef struct
{
  struct v4l2_buffer b;
  void *d;
  size_t l;
} allo_buffer_stc;

typedef struct
{
  float x,y;
} dot_stc;
  
typedef struct
{
  dot_stc ul,ur,lr,ll;
  __s32 *map;
} maprect_stc;

typedef struct
{
  struct v4l2_capability vcap;
  int unit,u,x,y,nbytes_in,nbytes_out,brightness,hue,saturation,contrast,whiteness,n_bfrs,xb[4],yb[4],n_maps;
  __u32 fmt;
  __u8 exit_thread,newframe;
  __u8 *frame_park;
  allo_buffer_stc *bfrs;
  pthread_t read_thr;
  pthread_mutex_t mtx;
  maprect_stc *maps;
} videocapt_stc;

VALUE cls_videocapt;

static void free_videocapt(void *p);

static void *background_read(void *arg);

VALUE new_videocapt(VALUE self,VALUE videodata,VALUE v_nmaps)
{
  videocapt_stc *vc=ALLOC(videocapt_stc);
  VALUE va;
  int i;

  bzero(vc,sizeof(videocapt_stc));

  vc->unit=FIX2INT(rb_ary_entry(videodata,0));

  vc->x=FIX2INT(rb_ary_entry(videodata,1));

  if(vc->x%2)
    rb_raise(rb_eRuntimeError,"%s: X dimension must be even! (got %d)",__func__,vc->x);

  vc->y=FIX2INT(rb_ary_entry(videodata,2));
  va=rb_ary_entry(videodata,3);
  vc->xb[0]=FIX2INT(rb_ary_entry(va,0));
  vc->xb[1]=FIX2INT(rb_ary_entry(va,1));
  if(vc->xb[0]%2 || vc->xb[1]%2)
    rb_raise(rb_eRuntimeError,"%s: X bracket values must be even! (got %d, %d)",__func__,vc->xb[0],vc->xb[1]);
  vc->xb[2]=vc->x-vc->xb[1];
  vc->xb[3]=vc->xb[2]-vc->xb[0];
  vc->yb[0]=FIX2INT(rb_ary_entry(va,2));
  vc->yb[1]=FIX2INT(rb_ary_entry(va,3));
  vc->yb[2]=vc->y-vc->yb[1];
  vc->yb[3]=vc->yb[2]-vc->yb[0];
  
  vc->nbytes_in=vc->x*vc->y;
  vc->nbytes_out=vc->xb[3]*vc->yb[3];

  vc->frame_park=malloc(vc->nbytes_out);

/*
 * Now open the v4l channel
 */

  va=rb_ary_entry(videodata,4);
  vc->brightness=FIX2INT(rb_ary_entry(va,0));
  vc->hue=FIX2INT(rb_ary_entry(va,1));
  vc->saturation=FIX2INT(rb_ary_entry(va,2));
  vc->contrast=FIX2INT(rb_ary_entry(va,3));
  vc->whiteness=FIX2INT(rb_ary_entry(va,4));
    
  char bfr[256];
  sprintf(bfr,"/dev/video%d",vc->unit);
  if((vc->u=open(bfr,O_RDWR))<0)
    rb_raise(rb_eRuntimeError,"%s: error opening V4L unit <%s> (%s)",__func__,bfr,strerror(errno));
    
  bzero(&vc->vcap,sizeof(struct v4l2_capability));
  if(ioctl(vc->u,VIDIOC_QUERYCAP,&vc->vcap)<0)
      rb_raise(rb_eRuntimeError,"%s: error reading capabilities for <%s> (%s)",__func__,bfr,strerror(errno));

  fprintf(stderr,"Unit: %d\nDriver: %s\nCard: %s\nBus: %s\nCapabilities: %x\n",vc->u,vc->vcap.driver,vc->vcap.card,
	  vc->vcap.bus_info,vc->vcap.capabilities);
  
  if(!(vc->vcap.capabilities&V4L2_CAP_VIDEO_CAPTURE))
    rb_raise(rb_eRuntimeError,"%s: <%s> cannot capture!",__func__,bfr);

  if(!(vc->vcap.capabilities&V4L2_CAP_STREAMING))
    rb_raise(rb_eRuntimeError,"%s: <%s> cannot stream!",__func__,bfr);

  struct v4l2_format vfmt;

  vfmt.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if(ioctl(vc->u,VIDIOC_G_FMT,&vfmt)<0)
    rb_raise(rb_eRuntimeError,"%s: error reading capture format for <%s> (%s)",__func__,bfr,strerror(errno));

  union desc ed;
    
  ed.v=vfmt.fmt.pix.pixelformat;
  fprintf(stderr,"Default video capture size: %dX%d (%x%x%x%x)\n",vfmt.fmt.pix.width,vfmt.fmt.pix.height,
	  ed.d[0],ed.d[1],ed.d[2],ed.d[3]);
  
  struct v4l2_fmtdesc vfd;
  __u32 fmt=0;
  char *format=RSTRING_PTR(rb_ary_entry(videodata,5));
  
  for(vfd.type=V4L2_BUF_TYPE_VIDEO_CAPTURE,vfd.index=0;;vfd.index++)
  {
    if(ioctl(vc->u,VIDIOC_ENUM_FMT,&vfd)<0)
      break;
    
    ed.v=vfd.pixelformat;
    
    fprintf(stderr,"Fmt #%d: %s (type %d flags %d 4c %c%c%c%c)\n",vfd.index+1,vfd.description,vfd.type,vfd.flags,
	    ed.d[0],ed.d[1],ed.d[2],ed.d[3]);
    if(!strcasecmp(format,(char *)vfd.description))
      fmt=vfd.pixelformat;
  }
  fprintf(stderr,"%d format types\n",vfd.index);
  if(fmt==0)
    rb_raise(rb_eRuntimeError,"%s: requested format <%s> not found in <%s>",__func__,format,bfr);
    
  vc->fmt=fmt;

  vfmt.fmt.pix.width=vc->x;
  vfmt.fmt.pix.height=vc->y;
  vfmt.fmt.pix.pixelformat=fmt;
  if(ioctl(vc->u,VIDIOC_S_FMT,&vfmt)<0)
    rb_raise(rb_eRuntimeError,"%s: error setting capture format to (%dX%d)/%x for <%s> (%s)",__func__,vc->x,vc->y,fmt,
	     bfr,strerror(errno));
  if(vfmt.fmt.pix.width!=vc->x || vfmt.fmt.pix.height!=vc->y)
    rb_raise(rb_eRuntimeError,"%s: Could not set frame size to %dX%d (returned %dX%d)",__func__,
	     vc->x,vc->y,vfmt.fmt.pix.width,vfmt.fmt.pix.height);
    
/*
 * Search for frame sizes
 */

  struct v4l2_frmsizeenum vfe;
  for(vfe.index=0;;vfe.index++)
  {
    if(ioctl(vc->u,VIDIOC_ENUM_FRAMESIZES,&vfe)<0)
      break;
    fprintf(stderr,"Fs #%d: pxf %x type %x\n",vfe.index+1,vfe.pixel_format,vfe.type);
  }
    
/*
 * Search for port
 */
    
  struct v4l2_input vinp;
  int channel=-1;
  char *port=RSTRING_PTR(rb_ary_entry(videodata,6));
    
  for(vinp.index=0;;vinp.index++)
  {
    if(ioctl(vc->u,VIDIOC_ENUMINPUT,&vinp)<0)
      break;
    fprintf(stderr,"Input #%d: %s (type %d std %d status %d)\n",vinp.index+1,vinp.name,vinp.type,
	    (int)vinp.std,vinp.status);
    if(!strcasecmp(port,(char *)vinp.name))
      channel=vinp.index;
  }
  fprintf(stderr,"%d channels\n",vinp.index);
  if(channel<0)
    rb_raise(rb_eRuntimeError,"%s: requested channel <%s> not found in <%s>",__func__,port,bfr);
  
  if(ioctl(vc->u,VIDIOC_S_INPUT,&channel)<0)
    rb_raise(rb_eRuntimeError,"%s: error setting to chn#%d on <%s> (%s)",__func__,channel,bfr,strerror(errno));

/*
 * Search for standard
 */
    
  struct v4l2_standard vstd;
  v4l2_std_id standard=0xffffffff;
  char *std=RSTRING_PTR(rb_ary_entry(videodata,7));

  for(vstd.index=0;;vstd.index++)
  {
    if(ioctl(vc->u,VIDIOC_ENUMSTD,&vstd)<0)
      break;
    fprintf(stderr,"Standard #%d: %s (id %x fract %d/%d lines %d)\n",vstd.index+1,vstd.name,(int)vstd.id,
	    vstd.frameperiod.numerator,vstd.frameperiod.denominator,vstd.framelines);
    if(!strcasecmp(std,(char *)vstd.name))
      standard=vstd.index;
  }
  fprintf(stderr,"%d standards\n",vstd.index);
  if(standard==0xffffffff)
    rb_raise(rb_eRuntimeError,"%s: requested standard <%s> not found in <%s>",__func__,std,bfr);
  
  if(ioctl(vc->u,VIDIOC_S_STD,&standard)<0)
    rb_raise(rb_eRuntimeError,"%s: error setting to std#%d on <%s> (%s)",__func__,(int)standard,bfr,strerror(errno));
  
/*
 * The image qualities
 */
    
  struct v4l2_control vctl;
    
  vctl.id=V4L2_CID_BRIGHTNESS;
  ioctl(vc->u,VIDIOC_G_CTRL,&vctl);
  if(vctl.value!=vc->brightness)
  {
    fprintf(stderr,"Brightness was %d - set to %d\n",vctl.value,vc->brightness);
    vctl.value=vc->brightness;
    ioctl(vc->u,VIDIOC_S_CTRL,&vctl);
  }
  vctl.id=V4L2_CID_HUE;
  ioctl(vc->u,VIDIOC_G_CTRL,&vctl);
  if(vctl.value!=vc->hue)
  {
    fprintf(stderr,"Hue was %d - set to %d\n",vctl.value,vc->hue);
    vctl.value=vc->hue;
    ioctl(vc->u,VIDIOC_S_CTRL,&vctl);
  }
  vctl.id=V4L2_CID_SATURATION;
  ioctl(vc->u,VIDIOC_G_CTRL,&vctl);
  if(vctl.value!=vc->saturation)
  {
    fprintf(stderr,"Saturation was %d - set to %d\n",vctl.value,vc->saturation);
    vctl.value=vc->saturation;
    ioctl(vc->u,VIDIOC_S_CTRL,&vctl);
  }
  vctl.id=V4L2_CID_CONTRAST;
  ioctl(vc->u,VIDIOC_G_CTRL,&vctl);
  if(vctl.value!=vc->contrast)
  {
    fprintf(stderr,"Contrast was %d - set to %d\n",vctl.value,vc->contrast);
    vctl.value=vc->contrast;
    ioctl(vc->u,VIDIOC_S_CTRL,&vctl);
  }
  vctl.id=V4L2_CID_WHITENESS;
  ioctl(vc->u,VIDIOC_G_CTRL,&vctl);
  if(vctl.value!=vc->whiteness)
  {
    fprintf(stderr,"Whiteness was %d - set to %d\n",vctl.value,vc->whiteness);
    vctl.value=vc->whiteness;
    ioctl(vc->u,VIDIOC_S_CTRL,&vctl);
  }    

/*
 * Allocate the mmap memory
 */

  struct v4l2_requestbuffers vrb;
  
  vrb.count=REQBFRS;
  vrb.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
  vrb.memory=V4L2_MEMORY_MMAP;
  if(ioctl(vc->u,VIDIOC_REQBUFS,&vrb)<0)
    rb_raise(rb_eRuntimeError,"%s: error requesting buffers for <%s> (%s)",__func__,bfr,strerror(errno));

  fprintf(stderr,"Requested %d buffers. Assigned %d buffers\n",REQBFRS,vrb.count);

  vc->n_bfrs=vrb.count;
  vc->bfrs=malloc(sizeof(allo_buffer_stc)*vc->n_bfrs);
  for(i=0;i<vc->n_bfrs;i++)
  {
    vc->bfrs[i].b.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    vc->bfrs[i].b.memory=V4L2_MEMORY_MMAP;
    vc->bfrs[i].b.index=i;
    if(ioctl(vc->u,VIDIOC_QUERYBUF,&vc->bfrs[i].b)<0)
      rb_raise(rb_eRuntimeError,"%s: error requesting buffer #%d for <%s> (%s)",__func__,i,bfr,strerror(errno));
    vc->bfrs[i].l=vc->bfrs[i].b.length;
    vc->bfrs[i].d=mmap(NULL,vc->bfrs[i].b.length,PROT_READ|PROT_WRITE,MAP_SHARED,vc->u,vc->bfrs[i].b.m.offset);
    if(vc->bfrs[i].d==MAP_FAILED)
      rb_raise(rb_eRuntimeError,"%s: error mapping buffer #%d for <%s> (%s)",__func__,i,bfr,strerror(errno));
      
    fprintf(stderr,"Buffer #%d is %d bytes long, starting at %x\n",i+1,vc->bfrs[i].b.length,vc->bfrs[i].b.m.offset);

/*
 * Enqueue it
 */
    
    if(ioctl(vc->u,VIDIOC_QBUF,&vc->bfrs[i].b)<0)
      rb_raise(rb_eRuntimeError,"%s: error enqueuing buffer #%d for <%s> (%s)",__func__,i,bfr,strerror(errno));
  }

/*
 * Mapping rects
 */

  vc->n_maps=FIX2INT(v_nmaps);
  vc->maps=malloc(sizeof(maprect_stc)*vc->n_maps);
  bzero(vc->maps,sizeof(maprect_stc)*vc->n_maps);
  
  for(i=0;i<vc->n_maps;i++)
  {
    vc->maps[i].map=malloc(sizeof(__s32)*vc->nbytes_out);  
    bzero(vc->maps[i].map,sizeof(__s32)*vc->nbytes_out);
  }

  usleep(0.3);
    
  i=V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if(ioctl(vc->u,VIDIOC_STREAMON,&i)<0)
  {
    close(vc->u);
    rb_raise(rb_eRuntimeError,"%s: error starting streaming for <%s> (%s)",__func__,bfr,strerror(errno));
  }

/*
 * The thread for reading frames.
 */
    
  int ret=pthread_mutex_init(&vc->mtx,NULL);
  if(ret)
    rb_raise(rb_eArgError,"%s: error #%d cresting mutex (%s)",__func__,ret,strerror(errno));
    
  vc->exit_thread=0;
  ret=pthread_create(&vc->read_thr,NULL,background_read,vc);
  if(ret)
    rb_raise(rb_eArgError,"%s: error #%d starting background read thread (%s)",__func__,ret,strerror(errno));
  
  return Data_Wrap_Struct(cls_videocapt,NULL,free_videocapt,vc);
}

static void free_videocapt(void *p)
{
  videocapt_stc *vc=(videocapt_stc *)p;
  int i;

  /**/fprintf(stderr,"%s called!\n",__func__);
  
  vc->exit_thread=1;
  pthread_join(vc->read_thr,NULL);
  
  i=V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if(ioctl(vc->u,VIDIOC_STREAMOFF,&i)<0)
    fprintf(stderr,"%s: error stopping streaming (%s) (unit %d)\n",__func__,
	    strerror(errno),vc->u);
  
  for(i=0;i<vc->n_bfrs;i++)
    munmap(vc->bfrs[i].d,vc->bfrs[i].l);  
  free(vc->bfrs);
    
  close(vc->u);

  free(vc->frame_park);

  for(i=0;i<vc->n_maps;i++)
    free(vc->maps[i].map);
  free(vc->maps);  

  free(p);
}

VALUE vc_get_latest_frame(VALUE self,VALUE v_nmaps)
{
  videocapt_stc *vc;
  Data_Get_Struct(self,videocapt_stc,vc);

  if(!vc->newframe)
    return Qfalse;
  
  VALUE to_ret;
  
  pthread_mutex_lock(&vc->mtx);
  if(v_nmaps!=Qtrue)
    to_ret=rb_str_new((char *)vc->frame_park,vc->nbytes_out);
  else
  {
    to_ret=rb_ary_new();
    rb_ary_push(to_ret,rb_str_new((char *)vc->frame_park,vc->nbytes_out));

    __u8 *lfrm=malloc(vc->nbytes_out);
    __s32 lp;
    int i,j;
    for(i=0;i<vc->n_maps;i++)
    {
      for(j=0;j<vc->nbytes_out;j++)
      {
	lp=vc->maps[i].map[j];
	lfrm[j]=(lp<0) ? 0 : vc->frame_park[lp];
      }
      rb_ary_push(to_ret,rb_str_new((char *)lfrm,vc->nbytes_out));
    }
    free(lfrm);
  }

  vc->newframe=0;
  pthread_mutex_unlock(&vc->mtx);

  return to_ret;
}

VALUE vc_set_mapping_rect(VALUE self,VALUE v_which,VALUE ul,VALUE ur,VALUE lr,VALUE ll)
{
  videocapt_stc *vc;
  Data_Get_Struct(self,videocapt_stc,vc);

  int which=FIX2INT(v_which);
  float ulx=NUM2DBL(rb_ary_entry(ul,0)),
    uly=NUM2DBL(rb_ary_entry(ul,1)),
    urx=NUM2DBL(rb_ary_entry(ur,0)),
    ury=NUM2DBL(rb_ary_entry(ur,1)),
    lrx=NUM2DBL(rb_ary_entry(lr,0)),
    lry=NUM2DBL(rb_ary_entry(lr,1)),
    llx=NUM2DBL(rb_ary_entry(ll,0)),
    lly=NUM2DBL(rb_ary_entry(ll,1)),
    ldifx=ulx-llx,
    ldify=uly-lly,
    rdifx=urx-lrx,
    rdify=ury-lry,
    inline1x,inline1y,inline2x,inline2y,inlinexd,inlineyd,fx,fy,xr,yr;
  
  int x,y;
  __s32 *ptr=vc->maps[which].map;  

  for(y=0;y<vc->yb[3];y++)
  {
    fy=(float)(vc->yb[3]-y)/vc->yb[3];
    inline1x=llx+ldifx*fy;
    inline1y=lly+ldify*fy;
    inline2x=lrx+rdifx*fy;
    inline2y=lry+rdify*fy;
    inlinexd=inline2x-inline1x;
    inlineyd=inline2y-inline1y;
    
    for(x=0;x<vc->xb[3];x++,ptr++)
    {
      fx=(float)x/vc->xb[3];
      xr=inline1x+inlinexd*fx;
      yr=inline1y+inlineyd*fx;

      if(xr<0.0 || xr>=1.0 || yr<0.0 || yr>=1.0)
	*ptr=-1;
      else
	*ptr=((int)(yr*vc->yb[3]))*vc->xb[3]+(int)(xr*vc->xb[3]);
    }
  }
  return self;  
}

VALUE vc_triplicate(VALUE self,VALUE arr)
{
  int bl=RSTRING_LEN(arr),i;
  __u8 *ptr=(__u8 *)RSTRING_PTR(arr),*rcvr=malloc(bl*3),*rptr=rcvr;
  
  for(i=0;i<bl;i++,ptr++,rptr+=3)
    memset(rptr,*ptr,3);

  VALUE to_ret=rb_str_new((char *)rcvr,bl*3);

  free(rcvr);

  return to_ret;
}
/*
 * gets a column array, returns a flipped or nonflipped single string
 */

extern VALUE vc_fliprotate(VALUE self,VALUE cols,VALUE v_flipro)
{
  int n_cols=RARRAY_LEN(cols),n_lines=RSTRING_LEN(rb_ary_entry(cols,0))/3;

  int reslen=n_lines*n_cols*3,i,x,y;
  char *bfr=malloc(reslen),*ptr=bfr,*cptr[n_cols];

  for(i=0;i<n_cols;i++)
    cptr[i]=RSTRING_PTR(rb_ary_entry(cols,i));

  for(y=0;y<n_lines;y++)
  {    
    for(x=0;x<n_cols;x++)
    {
      ptr[0]=cptr[x][2];
      ptr[1]=cptr[x][1];
      ptr[2]=cptr[x][0];
//      memcpy(ptr,cptr[x],3);
      ptr+=3;
      cptr[x]+=3;
    }
  }
  
  VALUE to_ret=rb_str_new(bfr,reslen);

  free(bfr);

  return to_ret;
}	

static void *background_read(void *arg)
{
  videocapt_stc *vc=(videocapt_stc *)arg;
  struct v4l2_buffer b;
  __u8 *pin,*pout;
  int y;

  while(!vc->exit_thread)
  {
    bzero(&b,sizeof(struct v4l2_buffer));
    
    b.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
    b.memory=V4L2_MEMORY_MMAP;
    
    if(ioctl(vc->u,VIDIOC_DQBUF,&b)<0)
    {
      perror("VIDIOC_DQBUF");
      return NULL;
    }

/*
 * Map the frame
 */

    pthread_mutex_lock(&vc->mtx);

    for(pout=vc->frame_park,
	  pin=((__u8 *)vc->bfrs[b.index].d)+vc->x*vc->yb[0],
	  y=vc->yb[0];
	y<vc->yb[2];
	y++,
	  pout+=vc->xb[3],
	  pin+=vc->x)
      memcpy(pout,pin+vc->xb[0],vc->xb[3]);
    
    vc->newframe=1;
    pthread_mutex_unlock(&vc->mtx);

    if(ioctl(vc->u,VIDIOC_QBUF,&b)<0)
    {
      perror("VIDIOC_QBUF");
      return NULL;
    }
  }
  
  return NULL;
}

