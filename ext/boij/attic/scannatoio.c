/* scannatoio.c */

/*
 * 01/07/2010 c.e. prelz AS FLUIDO <fluido@fluido.as>
 *
 * The stuff for talking to scanners
 *
 * $Id: scannatoio.c 2395 2010-07-02 05:38:47Z karl $
 */

#include <ruby.h>
#include "boij.h"

VALUE cls_scannatoio;
static SANE_Int sane_versioncode=0;

typedef struct
{
  SANE_Int n;
  int type,cap;
  char *name,*title,*desc;
  VALUE val;
} option_stc;

typedef struct
{
  SANE_Handle h;
  int n_options;
  option_stc *options;  
} scannatoio_stc;

static void free_scannatoio(void *p);

static void init_sane();
static VALUE current_option_value(scannatoio_stc *ss,SANE_Int opt_no,SANE_Value_Type type);
static void refresh_option_values(scannatoio_stc *ss);
static void current_option_set(scannatoio_stc *ss,SANE_Int opt_no,SANE_Value_Type type,VALUE val);

VALUE new_scannatoio(VALUE self,VALUE device)
{
  scannatoio_stc *ss=ALLOC(scannatoio_stc);

  init_sane();

  SANE_Status s=sane_open(RSTRING_PTR(device),&ss->h);
  if(s!=SANE_STATUS_GOOD)
    rb_raise(rb_eRuntimeError,"%s: cannot open scanner %s (%s)!",__func__,RSTRING_PTR(device),sane_strstatus(s));

/*
 * Get options
 */

  VALUE v=current_option_value(ss,0,SANE_TYPE_INT);
  int i;
  const SANE_Option_Descriptor *od;
  
  ss->n_options=FIX2INT(v)-1;
  ss->options=malloc(sizeof(option_stc)*ss->n_options);

  for(i=0;i<ss->n_options;i++)
  {
    od=sane_get_option_descriptor(ss->h,i+1);
//    if(od->type==SANE_TYPE_FIXED)
//    {
//      fprintf
//    }
    fprintf(stderr,"!! ctype %d\n",od->constraint_type);
    ss->options[i].n=i+1;
    ss->options[i].type=od->type;
    ss->options[i].cap=od->cap;
    ss->options[i].name=od->name==NULL ? NULL : strdup(od->name);
    ss->options[i].title=od->title==NULL ? NULL : strdup(od->title);
    ss->options[i].desc=od->desc==NULL ? NULL : strdup(od->desc);
  }

  refresh_option_values(ss);
  
  return Data_Wrap_Struct(cls_scannatoio,NULL,free_scannatoio,ss);
}

static void free_scannatoio(void *p)
{
  scannatoio_stc *ss=(scannatoio_stc *)p;
  int i;

  sane_close(ss->h);

  for(i=0;i<ss->n_options;i++)
  {
    free(ss->options[i].name);
    free(ss->options[i].title);
    free(ss->options[i].desc);
  }
  free(ss->options);
  
  free(ss);
}

VALUE scanna_list_devices(VALUE self)
{
  const SANE_Device **d;
  SANE_Status s;
  
  init_sane();
  
  s=sane_get_devices(&d,SANE_FALSE);
  if(s!=SANE_STATUS_GOOD)
    rb_raise(rb_eRuntimeError,"%s: cannot get sane devices (%s)!",__func__,sane_strstatus(s));
  
  VALUE retval=rb_ary_new(),v;
  
  while(*d!=NULL)
  {
    v=rb_ary_new();
    rb_ary_push(v,rb_str_new_cstr((*d)->name));
    rb_ary_push(v,rb_str_new_cstr((*d)->vendor));
    rb_ary_push(v,rb_str_new_cstr((*d)->model));
    rb_ary_push(v,rb_str_new_cstr((*d)->type));
    rb_ary_push(retval,v);
    
    d++;
  }
  
  return retval;
}

VALUE scanna_options(VALUE self)
{
  scannatoio_stc *ss;
  Data_Get_Struct(self,scannatoio_stc,ss);
  int i;
  VALUE to_ret=rb_ary_new(),v;

  for(i=0;i<ss->n_options;i++)
    if(ss->options[i].name)
    {
      v=rb_ary_new();
      rb_ary_push(v,INT2FIX(ss->options[i].n));
      rb_ary_push(v,INT2FIX(ss->options[i].type));
      rb_ary_push(v,INT2FIX(ss->options[i].cap));
      rb_ary_push(v,rb_str_new_cstr(ss->options[i].name));
      rb_ary_push(v,rb_str_new_cstr(ss->options[i].title));
      rb_ary_push(v,rb_str_new_cstr(ss->options[i].desc));
      rb_ary_push(v,ss->options[i].val);
      rb_ary_push(to_ret,v);
    }

  return to_ret;
}

VALUE scanna_set_option(VALUE self,VALUE v_n,VALUE v_val)
{
  scannatoio_stc *ss;
  Data_Get_Struct(self,scannatoio_stc,ss);
  int n=FIX2INT(v_n),i;

  for(i=0;i<ss->n_options;i++)
    if(ss->options[i].n==n)
      break;

  if(i>=ss->n_options)
    rb_raise(rb_eRuntimeError,"%s: Option #%d not found!",__func__,n);

  current_option_set(ss,n,ss->options[i].type,v_val);
  
  refresh_option_values(ss);

  return self;
}

static void init_sane()
{
  if(sane_versioncode==0)
  {
    SANE_Status s=sane_init(&sane_versioncode,NULL);
    if(s!=SANE_STATUS_GOOD)
      rb_raise(rb_eRuntimeError,"%s: cannot init sane (%s)!",__func__,sane_strstatus(s));
    char bfr[256];
    sprintf(bfr,"Sane initialized: version %d",sane_versioncode);
    rb_funcall(Qnil,rb_intern("loggo"),1,rb_str_new_cstr(bfr));
  }
}

static VALUE current_option_value(scannatoio_stc *ss,SANE_Int opt_no,SANE_Value_Type type)
{
  VALUE vret=Qnil;
  SANE_Status s=4;
  SANE_Bool sb;
  SANE_Int si;
  SANE_Fixed sf;
  char sst[256];
  
  switch(type)
  {
  case SANE_TYPE_BOOL:
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_GET_VALUE,&sb,NULL);
    if(s==SANE_STATUS_GOOD)
      vret=sb==SANE_TRUE ? Qtrue : Qfalse;
    break;
  case SANE_TYPE_FIXED:
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_GET_VALUE,&sf,NULL);
    if(s==SANE_STATUS_GOOD)
      vret=rb_float_new(SANE_UNFIX(sf));
    break;
  case SANE_TYPE_INT:
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_GET_VALUE,&si,NULL);
    if(s==SANE_STATUS_GOOD)
      vret=INT2FIX(si);
    break;    
  case SANE_TYPE_STRING:
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_GET_VALUE,(SANE_String)&sst,NULL);
    if(s==SANE_STATUS_GOOD)
      vret=rb_str_new_cstr(sst);
    break;    
  case SANE_TYPE_BUTTON:
    break;
  default:
    fprintf(stderr,"%s: WARNING: Value type %d not supported!!\n",__func__,type);
  }
  
  if(s!=SANE_STATUS_GOOD)
    fprintf(stderr,"%s: error getting option #%d (type %d) (%s/%d)!\n",__func__,opt_no,type,sane_strstatus(s),s);    

  return vret;
}

static void refresh_option_values(scannatoio_stc *ss)
{
  int i;

  for(i=0;i<ss->n_options;i++)
    ss->options[i].val=current_option_value(ss,ss->options[i].n,ss->options[i].type);
}

static void current_option_set(scannatoio_stc *ss,SANE_Int opt_no,SANE_Value_Type type,VALUE val)
{
  SANE_Status s=SANE_STATUS_GOOD;
  SANE_Bool sb;
  SANE_Int si;
  SANE_Fixed sf;
  
  switch(type)
  {
  case SANE_TYPE_BOOL:
    sb=val==Qtrue ? SANE_TRUE : SANE_FALSE;
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_SET_VALUE,&sb,NULL);
    break;
  case SANE_TYPE_FIXED:
    sf=SANE_FIX(NUM2DBL(val));
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_SET_VALUE,&sf,NULL);    

    fprintf(stderr,"ZZZ %f (%d) -> %d\n",NUM2DBL(val),opt_no,s);    
    break;
  case SANE_TYPE_INT:
    si=FIX2INT(val);
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_SET_VALUE,&si,NULL);
    break;
  case SANE_TYPE_STRING:
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_SET_VALUE,(SANE_String)RSTRING_PTR(val),NULL);
    break;
  case SANE_TYPE_BUTTON:
    s=sane_control_option(ss->h,opt_no,SANE_ACTION_SET_VALUE,NULL,NULL);
    break;
  default:
    rb_raise(rb_eRuntimeError,"%s: unknown variable type (%d)!",__func__,type);    
  }    
    
  if(s!=SANE_STATUS_GOOD)
    rb_raise(rb_eRuntimeError,"%s: error setting option #%d (type %d) (%s/%d)!",__func__,opt_no,type,sane_strstatus(s),s);
}
