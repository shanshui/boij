# flirconv.rb

=begin

15/12/2008 c.e prelz AS FLUIDO fluido@fluido.as
project .:laspal:.

Serial comms to the flir thermocam

$Id: flirconv.rb 2626 2010-11-10 14:48:16Z karl $

=end

class String
  def myunpack
    a=[]
    0.step(length()-1,2) do |v|
      a.push(self[v,2].reverse.unpack('s')[0])
    end
    a
  end
end

class IO
  MYWAIT=0.01
  def my_read_nonblock(howmany,loops,firstchar=nil)
    rr=[]
    lh=howmany
    loops.times do |lp| 
      begin
        a=read_nonblock(lh).unpack('C*')
        loggo("#{lp}: Presi #{a}")
        if(firstchar && rr.length()<=0)
          while(a.length()>0 && a[0]!=firstchar)
            loggo("Dropping #{sprintf("%2.2x",a[0])}!")
            a.shift()
          end
        end
        lh-=a.length()
        rr+=a
        break if(rr.length>=howmany)
      rescue Errno::EAGAIN
      end
      sleep(MYWAIT)
    end
    rr.length()==howmany ? rr : nil
  end
end

class Flir
  attr_reader(:unit,:agc_type,:contrast,:brightness,:ittfilter,:plateau,:ittmidpoint,:maxagcgain,:roi)

  ROI_X=80
  ROI_X_2=ROI_X*2.0
  ROI_Y=64
  ROI_Y_2=ROI_Y*2.0

  N_TRIES=5
  
  def initialize(dev,speed)
    @dev=dev
    @unit=File::open(@dev,File::RDWR|File::NONBLOCK)
    @unit.sync=true
    system("stty -F #{@dev} #{speed} raw")
  end

  def crc(stringarr)
    rem=0
    stringarr.each do |v|
      rem^=(v << 8)
      8.times() do |j|
        if(rem&0x8000>0)
          rem=(rem << 1)^0x1021
        else 
          rem <<= 1
        end
      end
      rem&=0xffff
    end
    rem
  end

  def make_cmd(cmd,data)
    string=crc(cmd)+(data ? crc(data) : [0,0].pack('C*'))
    hexdump(string)
    string
  end

  def hexdump(s)
    if(!s)
      loggo("Hexdump: null value!")
      return
    end

    s.each do |v|
      printf("%2.2x ",v)
    end
    putc("\n")
  end

  def cmd(cmd,data)
    datalen=data ? data.length : 0
    ca=[0x6e,0,0,cmd,datalen>>8,datalen&0xff]
    crc=crc(ca)
    ca.push(crc>>8)
    ca.push(crc&0xff)
    if(data)
      ca+=data
      crc=crc(data)
      ca.push(crc>>8)
      ca.push(crc&0xff)
    else
      ca.push(0)
      ca.push(0)
    end
    cs=ca.pack('C*')
    hexdump(cs.unpack('C*'))
    nw=nil
    N_TRIES.times do |i|
      begin
        nw=@unit.syswrite(cs)
        break
      rescue => err
        raise err if(i==N_TRIES-1)
        loggo("#{i+1} Getting (#{err}) (trying to write)")
        sleep(0.2)
      end
      break if(nw)
    end
    loggo("DOPO #{nw}")
    sleep(0.2)
    answ=@unit.my_read_nonblock(8,100,0x6e)
    hexdump(answ)
    raise "No answer" if(!answ)
    raise "Bad answer length (#{answ.length()})" if(answ.length()!=8)
    raise "First byte not 0x6e (#{sprintf("%2.2x",answ[0])})" if(answ[0]!=0x6e)
    raise "Fourth byte not cmd (#{sprintf("%2.2x",answ[3])})" if(answ[3]!=cmd)

    crc=crc(answ[0,6])
    rec_crc=(answ[6]<< 8)|answ[7]
    raise "Bad cmd crc #{sprintf("%4.4x",rec_crc)}(should be #{sprintf("%4.4x",crc)})" if(rec_crc!=crc)
    
    sts=answ[1]
    datalen=(answ[4]<< 8)|answ[5]
    answ=@unit.my_read_nonblock(datalen+2,100)
    hexdump(answ)
    crc=crc(answ[0,datalen])
    rec_crc=(answ[datalen]<< 8)|answ[datalen+1]
    raise "Bad data crc #{sprintf("%4.4x",rec_crc)}(should be #{sprintf("%4.4x",crc)})" if(rec_crc!=crc)

    set_internal_val(cmd,data) if(data)

    [sts,answ[0,datalen]]
  end

  def set_internal_val(cmd,data)
    if(cmd==0x13)
      @agc_type=data
    elsif(cmd==0x14)
      @contrast=data
    elsif(cmd==0x15)
      @brightness=data
    elsif(cmd==0x3e)
      @ittfilter=data
    elsif(cmd==0x3f)
      @plateau=data
    elsif(cmd==0x55)
      @ittmidpoint=data
    elsif(cmd==0x6a)
      @maxagcgain=data
    elsif(cmd==0x4c)
      a=data.pack('C*').myunpack()
      @roi=map_roi_from(a)
    end
  end

  def set_agc_type(ar)
    begin
      cmd(0x13,ar)
    rescue
      loggo("Could not set AGC")
    end
  end
  def set_contrast(ar)
    begin
      cmd(0x14,ar)
    rescue
      loggo("Could not set contrast")
    end
  end
  def set_brightness(ar)
    cmd(0x15,ar)
  end
  def set_ittfilter(ar)
    begin
      cmd(0x3e,ar)
    rescue
      loggo("Could not set ITT filter")
    end
  end
  def set_plateau(ar)
    begin
      cmd(0x3f,ar)
    rescue
      loggo("Could not set plateau")
    end
  end
  def set_ittmidpoint(ar)
    cmd(0x55,ar)
  end
  def set_maxagcgain(ar)
    begin
      cmd(0x6a,ar)
    rescue
      loggo("Could not set max agc gain")
    end
  end
  def set_roi(ar)
    begin
      cmd(0x4c,map_roi_to(ar).pack('nnnn').unpack('C*'))
    rescue
      loggo("Could not set roi")
    end      
  end

  def map_roi_from(shorts)
    [(shorts[0]+ROI_X)/ROI_X_2,(shorts[1]+ROI_Y)/ROI_Y_2,(shorts[2]+ROI_X)/ROI_X_2,(shorts[3]+ROI_Y)/ROI_Y_2]
  end

  def map_roi_to(floats)
    [(floats[0]*ROI_X_2-ROI_X).to_i(),
     (floats[1]*ROI_Y_2-ROI_Y).to_i(),
     (floats[2]*ROI_X_2-ROI_X).to_i(),
     (floats[3]*ROI_Y_2-ROI_Y).to_i()]
  end
end


