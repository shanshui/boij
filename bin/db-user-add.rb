#!/usr/bin/env ruby
# adduser.rb

=begin

05/07/2010 c.e.prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Add a dbase administrator

$Id: adduser.rb 2399 2010-07-05 14:59:06Z karl $

=end

require 'boij'
require 'image_dbase'
require 'digest/md5'

raise "Usage: #{$0} nick passwd" if(ARGV.length!=2)

db=Image_dbase::new()
id=db.create_user(ARGV[0],ARGV[1])
loggo("User <#{ARGV[0]}> gets id #{id}")
