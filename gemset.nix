{
  base64 = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0phh60n6y73jqyrghz0ggjpyqgzhg6dpaa932j2njljqnl0179qw";
      type = "gem";
    };
    version = "0.1.0";
  };
  boij = {
    dependencies = ["base64" "opengl" "rmagick" "rubysdl" "sqlite3"];
    groups = ["default"];
    platforms = [];
    source = {
      path = ./.;
      type = "path";
    };
    version = "0.1.0";
  };
  opengl = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "12as6l2ivbkkb2kqc5v7nsrxlvhmgz2cqnc2hplfrrdg5nlhzg73";
      type = "gem";
    };
    version = "0.10.0";
  };
  rmagick = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "04ahv5gwfwdmwx6b7c0z91rrsfklvnqichgnqk1f9b9n6md3b8yw";
      type = "gem";
    };
    version = "4.2.2";
  };
  rubysdl = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "15g21gsac2jch9gpr72wxrvyqyd7v6nff1yi8y3yl6nsmbjjx2f9";
      type = "gem";
    };
    version = "2.2.0";
  };
  sqlite3 = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0lja01cp9xd5m6vmx99zwn4r7s97r1w5cb76gqd8xhbm1wxyzf78";
      type = "gem";
    };
    version = "1.4.2";
  };
}