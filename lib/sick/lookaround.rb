#!/usr/bin/env ruby
# lookaround.rb

=begin

20/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

Monitors the playground, as detected by the Sick camera

$Id$

=end

require 'sdl'
require 'boij.so'
require 'playground_sick'
require 'sick/sick_config'
require 'sick/background_saver'
require 'player_config'

class Lookaround
  DBG_FLG=(ENV['BOIJ_DBG']!=nil)
  FULLSCREEN_FLAG=!DBG_FLG

  WX=DUMP_SIZE[0]
  WY=DUMP_SIZE[1]
  SQ=20
  SQHALF=SQ>>1
  ZALGS=Player_config::ZOOM_ALGS

  FONT_PATH=ENV['BOIJ_FONT'] || "#{BASE}/fonts/default.ttf"
  #`fc-list 'DejaVu Sans:style=Book' file | grep DejaVuSans.ttf | head -1 | sed 's/:.*//'`

  FONT_COL = [0xEE,0xEE,0xEE]
  LINE_HEIGHT = 25

  CAP_DELAY = 3

  def initialize
    loggo("SDL init")
    SDL::init(SDL::INIT_VIDEO)
    SDL::TTF.init()
    loggo("Loading font [#{FONT_PATH}]")
    @font=SDL::TTF.open(FONT_PATH,15)
    @font.style=SDL::TTF::STYLE_NORMAL

    @screen=SDL::Screen.open(WX,WY,16,SDL::SWSURFACE)
    SDL::WM::set_caption('SICK!','SICK!')

    if(FULLSCREEN_FLAG)
      SDL::Mouse.warp(1,1)
      SDL::Mouse.hide
    end

    @hcfg=Hours_config::load

    @pcfg=Player_config::load
    @zalg = ZALGS.index(@pcfg.zoom_alg)
    @zalg = 0 unless @zalg

    @scfg=Sick_config::load
    if @scfg.backg
      open_sick()
    else
      backgsa()
      save()
    end

    @cap=nil
    @cols=[]
    @scfg.max_groups.times do
      @cols.push([100+rand(150),100+rand(150),100+rand(150)])
    end

    loggo("Lookaround init done")
  end

  def backgsa()
    loggo("Saving background")
    @screen.fill_rect(0,0,WX,WY,[0xDD,0x99,0])
    @font.draw_solid_utf8(@screen,"Starting capture in #{CAP_DELAY} s..",0,0,*FONT_COL)
    @screen.flip
    sleep(CAP_DELAY)
    @screen.fill_rect(0,0,WX,WY,[0xCC,0,0])
    @font.draw_solid_utf8(@screen,"Capturing..",0,0,*FONT_COL)
    @screen.flip
    close_sick()
    Backgsa::new(@scfg, @scfg.get_sick_for_init()).runme()
    open_sick()
  end

  def runme()
    update_sick()
    loop do
      while event = SDL::Event.poll
        case event
        when SDL::Event::KeyDown
          shft = event.mod & SDL::Key::MOD_SHIFT != 0
          ctrl = event.mod & SDL::Key::MOD_CTRL != 0
          step = shft ? 0.01 : 0.02
          zstep = shft ? 0.05 : 0.10
          case(event.sym)
          when SDL::Key::LEFT
            if(!ctrl)
              update_range(step,0,0,0)
            else
              update_range(0,step,0,0)
            end
          when SDL::Key::RIGHT
            if(!ctrl)
              update_range(-step,0,0,0)
            else
              update_range(0,-step,0,0)
            end
          when SDL::Key::UP
            if(!ctrl)
              update_range(0,0,step,0)
            else
              update_range(0,0,0,step)
            end
          when SDL::Key::DOWN
            if(!ctrl)
              update_range(0,0,-step,0)
            else
              update_range(0,0,0,-step)
            end
          when SDL::Key::K1
            if(@scfg.min_weight>0)
              @scfg.min_weight = @scfg.min_weight - 1
              update_sick()
            end
          when SDL::Key::K2
            @scfg.min_weight = @scfg.min_weight + 1
            update_sick()
          when SDL::Key::K4
            if(@scfg.max_dist>0.0)
              @scfg.max_dist = @scfg.max_dist - step
              update_sick()
            end
          when SDL::Key::K5
            @scfg.max_dist = @scfg.max_dist + step
            update_sick()
          when SDL::Key::K7
            if(@scfg.max_disapp>0)
              @scfg.max_disapp = @scfg.max_disapp - 100000
              update_sick()
            end
          when SDL::Key::K8
            @scfg.max_disapp = @scfg.max_disapp + 100000
            update_sick()
          when SDL::Key::F1
            @pcfg.min_zoom = @pcfg.min_zoom - zstep
          when SDL::Key::F2
            @pcfg.min_zoom = @pcfg.min_zoom + zstep
          when SDL::Key::F3
            @pcfg.max_zoom = @pcfg.max_zoom - zstep
          when SDL::Key::F4
            @pcfg.max_zoom = @pcfg.max_zoom + zstep
          when SDL::Key::H
            if ctrl
              save()
              system("xterm -geometry 318x92 -bg black -fg white -e nano #{HOURSCONF}")
              @hcfg = Hours_config::load
            end
          when SDL::Key::P
            if ctrl
              save()
              system("xterm -geometry 318x92 -bg black -fg white -e nano #{PLAYERCONF}")
              @pcfg = Player_config::load
            end
          when SDL::Key::A
            @zalg = @zalg + 1
            @pcfg.zoom_alg = ZALGS[@zalg % ZALGS.size]
          when SDL::Key::S
            if ctrl
              save()
              system("xterm -geometry 318x92 -bg black -fg white -e nano #{SICKCONF}")
              @scfg = Sick_config::load
            else
              @pcfg.zoom_alg_n = @pcfg.zoom_alg_n + 1
            end
          when SDL::Key::D
            if 1 < @pcfg.zoom_alg_n
              @pcfg.zoom_alg_n = @pcfg.zoom_alg_n - 1
            end
          when SDL::Key::B
            if ctrl
              backgsa()
            end
          when SDL::Key::C
            if ctrl
              if @cap
                close_cap()
              else
                open_cap()
              end
            end
          when SDL::Key::M
            if ctrl
              lastrec = `find #{RECDIR} -type f | sort | tail -1`
              if lastrec.length
                loggo("Playing screen recording #{lastrec}")
                system("mpv --quiet --fullscreen #{lastrec}")
              else
                loggo("No screen recording yet")
              end
            end
          when SDL::Key::COMMA
            if 1 < @scfg.replay_n
              @scfg.replay_n = @scfg.replay_n - 1
              open_sick() if @scfg.replay
            end
          when SDL::Key::PERIOD
            @scfg.replay_n = @scfg.replay_n + 1
            open_sick() if @scfg.replay
          when SDL::Key::R
            @scfg.replay = !@scfg.replay
            open_sick()
          when SDL::Key::DELETE
            if ctrl
              @hcfg = Hours_config.new()
              @scfg = Sick_config.new()
              @pcfg = Player_config.new()
              @zalg = ZALGS.index(@pcfg.zoom_alg)
              @zalg = 0 unless @zalg
              update_sick()
              save()
            end
          when SDL::Key::N
            if ctrl
              load = shift
              if load
                msg = "Loading configuration from USB drive.."
              else
                msg = "Saving configuration, screen recordings & snapshots to USB drive.."
              end
              @screen.fill_rect(0,0,WX,WY,[0,0,0x88])
              @font.draw_solid_utf8(@screen,msg,0,0,*FONT_COL)
              @screen.flip

              mntdev=`blkid -t TYPE=vfat | grep -v 'LABEL="\\(EFI\|boot\\)"' | awk -F: '{print $1}' | tail -1`.chomp
              loggo("Syncing to USB drive #{mntdev}")
              if mntdev.match(/^\/dev/)
                mntdir='/tmp/usb'
                FileUtils::mkdir_p(mntdir)
                system("sudo mount #{mntdev} #{mntdir}")
                if load
                  system("sudo rsync -aPvi #{mntdir}/config #{BASE}")
                else
                  system("sudo rsync -aPvi #{BASE}config #{BASE}snap #{BASE}rec #{mntdir}")
                end
                system("sudo umount #{mntdir}")

                @screen.fill_rect(0,0,WX,WY,[0,0x88,0])
                @font.draw_solid_utf8(@screen,"USB sync complete!",0,0,*FONT_COL)
                @screen.flip
                sleep 1
              else
                @screen.fill_rect(0,0,WX,WY,[0x88,0,0])
                @font.draw_solid_utf8(@screen,"No USB drive found!",0,0,*FONT_COL)
                @screen.flip
                sleep 3
              end
            end
          when SDL::Key::U
            if ctrl
              save()
              close()
              @screen.fill_rect(0,0,WX,WY,[0,0,0x88])
              @font.draw_solid_utf8(@screen,"Updating system..",0,0,*FONT_COL)
              @screen.flip
              if system("sudo system-update")
                @screen.fill_rect(0,0,WX,WY,[0,0x88,0])
                @font.draw_solid_utf8(@screen,"Update succesful! Restarting..",0,0,*FONT_COL)
                @screen.flip
                sleep 1
                system("pkill sick-runner.sh")
                return
              else
                @screen.fill_rect(0,0,WX,WY,[0x88,0,0])
                @font.draw_solid_utf8(@screen,"Update failed!",0,0,*FONT_COL)
                @screen.flip
                sleep 3
                open_sick()
              end
            end
          when SDL::Key::ESCAPE, SDL::Event::Quit
            save()
            close()
            return
          end
        end
      end

      res=nil
      res=@s.tracker(true) if @s
      loggo("NO DATA") unless res

      @screen.fill_rect(0,0,WX,WY,[0,0,0])
      line = 0

      s=sprintf("[SICK] BG scan [C+b]  Capture: %s [C+c] #%s [,/.] Replay: %s [C+r]",
                @cap ? 'ON' : 'OFF',
                sprintf('%03d', @scfg.replay_n),
                @scfg.replay ? 'ON' : 'OFF')
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      s=sprintf("[SICK] Range: X: %.2f..%.2f  Y: %.2f..%.2f [(S+)(C+)arrows]", *@scfg.range)
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      s=sprintf("[SICK] Min.weight: %d [1/2]  Max.dist: %.2f [4/5]  Max.disapp: %.2f s [7/8]",
                @scfg.min_weight, @scfg.max_dist, @scfg.max_disapp/1000000.0)
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      s=sprintf("[PLAY] Zoom: %.2f..%.2f [(S+)F1/F2..F3/F4]  Alg: %s [a] #%d [s/d]",
                @pcfg.min_zoom, @pcfg.max_zoom, @pcfg.zoom_alg, @pcfg.zoom_alg_n)
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      s="[CONF] Edit Hours [C+h]  Edit SICK [C+s]  Edit Player [C+p]  Reset config to default [C+Del]";
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      s="Play last recording [C+m]  Save to USB drive [C+n]  Load from USB drive [C+S+n]"
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      s="Exit [Esc]  Software update [C+u]";
      @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
      line = line + 1

      unless (res)
        if @s
          s="[SICK] NO DATA RECEIVED"
        else
          s="[SICK] NO CONNECTION"
        end
        @font.draw_solid_utf8(@screen,s,0,line*LINE_HEIGHT,*FONT_COL)
        line = line + 1
      end

      if(res)
        # background (blue dots)
        if @bgp
          @bgp.each do |a|
            if(a)
              x,y=a
              c=[x*WX-2,y*WY-2]
              @screen.fill_rect(*c,4,4,[50,50,255])
            end
          end
        end

        # objects (yellow dots)
        bb=res[false]
        res.delete(false)
        if bb
          bb.each do |x,y|
            c=[x*WX-2,y*WY-2]
            @screen.fill_rect(*c,4,4,[255,255,0])
          end
        end

        # players (big squares)
        res.each do |k,v|
          c=[v[0]*WX-SQHALF,v[1]*WY-SQHALF]
          @screen.fill_rect(*c,SQ,SQ,@cols[k])
        end

        if @cap
          @cap.write(Marshal::dump(res))
          @cap.flush()
        end
      end

      @screen.flip
      sleep(@scfg.scan_interval)
    end
  end

  def update_range(mnx,mxx,mny,mxy)
    @scfg.range[0]+=mnx
    @scfg.range[1]+=mxx
    @scfg.range[2]+=mny
    @scfg.range[3]+=mxy

    @scfg.range[0] = 0.0 if @scfg.range[0] < 0
    @scfg.range[1] = 1.0 if @scfg.range[1] > 1
    @scfg.range[2] = 0.0 if @scfg.range[2] < 0
    @scfg.range[3] = 1.0 if @scfg.range[3] > 1

    update_sick()
  end

  def update_sick()
    if (@s)
      @s.update_vals(@scfg.range,@scfg.min_weight,@scfg.max_dist,@scfg.max_disapp)
      @bgp=@s.get_mapped_background()
    end
  end

  def save
    @hcfg.save() if @hcfg
    @pcfg.save() if @pcfg
    @scfg.save() if @scfg
  end

  def open_sick(no_replay=false)
    @s.close() if @s
    begin
      @s = @scfg.get_sick(no_replay);
      update_sick()
    rescue
      @s = nil
    end
  end

  def close_sick
    @s.close() if @s
    @s = nil
  end

  def open_cap
    close_cap()
    open_sick(true)
    @cap = File::open(@scfg.replay_file, 'wb')
  end

  def close_cap
    @cap.close if @cap
    @cap = nil
    open_sick()
  end

  def close
    close_cap()
    close_sick()
  end
end

if($0==__FILE__)
  Lookaround::new().runme()
end
