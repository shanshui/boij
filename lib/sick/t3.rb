F=Math::PI/180.0

def s(v)
  Math::sin(v*F)
end
def c(v)
  Math::cos(v*F)
end

0.step(180,10) do |v|
  printf("%3d sin %.2f cos %.2f\n",v,s(v),(1.0-c(v))/2.0)
end
