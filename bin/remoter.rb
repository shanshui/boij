#!/usr/bin/env ruby
# remoter.rb

=begin

8/2/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

The daemon that listens to the remote control and starts/stops

$Id: remoter.rb 1538 2008-04-07 12:38:44Z karl $

=end

require 'boij'

MAIN_COMMAND_START="(cd /home/karl/svn/geert/boij;su -c \"./boij_main.rb\" karl) &"

class Remoter
  EVV=[[1,358,1],[1,207,1],[1,128,1]]

  def initialize(inr)
    @ir=Inputreader::new(inr,EVV)
  end

  def unused_key_func
    loggo("Unused key")
  end

  def on_key_func
    loggo("Requested on")
    pid=Fluid_logfile::getpid('main')
    if(File::directory?("/proc/#{pid}"))
      loggo("Main process already runs, with pid #{pid}")
    else
      system("echo \"nel giardin di frate andrea\" | festival --tts")
      system("echo \"fra simon cfoglion cogliea\" | festival --tts")
      system(MAIN_COMMAND_START)
    end
  end
  def off_key_func
    loggo("Requested off")
    pid=Fluid_logfile::getpid('main')
    if(!File::directory?("/proc/#{pid}"))
      loggo("Main process with pid #{pid} is not running!")
    else
      Process::kill('INT',pid)
    end
  end

  def runme
    Signal::trap('USR1') do
      on_key_func()
    end
    Signal::trap('USR2') do
      off_key_func()
    end

    loop do
      case @ir.event()
      when 0
	unused_key_func()
      when 1
	on_key_func()
      when 2
	off_key_func()
      else
	sleep(0.30)
      end
    end
  end
end

if($0==__FILE__)
  lf=Fluid_logfile::new('remoter')

  re=Remoter::new(REMOTE_INPUT)
  begin
    re.runme()
  rescue
    pp ["Remoter crash",$!,$!.backtrace()]
  end
end
