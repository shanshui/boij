/*
 * Thermal eye driver
 */

//#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/tty.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/usb/serial.h>

/*
 * Version Information
 */
#define DRIVER_VERSION "v0.01"
#define DRIVER_DESC "Thermal eye simple usb driver"

#define TE_VENDOR_ID 0x1553
#define TE_PRODUCT_ID 0x3500

static struct usb_device_id id_table [] =
{
  { USB_DEVICE(TE_VENDOR_ID, TE_PRODUCT_ID) },
  { }					/* Terminating entry */
};

MODULE_DEVICE_TABLE(usb, id_table);

static int te_attach(struct usb_serial *serial);

static struct usb_driver te_driver =
{
  .name="THERMALEYE",
  .probe=usb_serial_probe,
  .disconnect=usb_serial_disconnect,
  .id_table=id_table,
};

static struct usb_serial_driver te_device=
{
  .driver=
  {
    .owner=THIS_MODULE,
    .name="THERMALEYE",
  },
  .id_table=id_table,
  .num_interrupt_in=0,
  .num_bulk_in=1,
  .num_bulk_out=1,
  .num_ports=1,
  .attach=te_attach,
};

static int te_attach(struct usb_serial *serial)
{
//  char *buf;
//  int rst;
//  rst=0;
  printk("Boh?\n");
#if 0
  buf=kmalloc(1,GFP_KERNEL);
  if (!buf)
  {
    dbg("error kmalloc");
    return(-1);
  }
  FXUSB_RCV(0xfe,0xc0,0x0000,0x0003);
  dbg("7 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_RCV(0xfe,0xc0,0x0000,0x0004);
  dbg("12 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0001,0x0004);
  dbg("17 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_RCV(0xfe,0xc0,0x0000,0x0004);
  dbg("18 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0003,0x0004);
  dbg("19 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_RCV(0xfe,0xc0,0x0000,0x0006);
  dbg("20 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0082,0x0003);
  dbg("147 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x000d,0x0000);
  dbg("148 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0000,0x0001);
  dbg("149 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0002,0x0003);
  dbg("150 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_RCV(0xfe,0xc0,0x0000,0x0004);
  dbg("151 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0003,0x0004);
  dbg("152 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_RCV(0xfe,0xc0,0x0000,0x0003);
  dbg("153 control msg return : %d = %0x", rst,buf[0]);
  FXUSB_SND(0xfe,0x40,0x0003,0x0003);
  dbg("154 control msg return : %d = %0x", rst,buf[0]);
  kfree(buf);
#endif  
  return(0);
}

static int __init te_init(void)
{
  int retval;
  retval=usb_serial_register(&te_device);
  if(retval)
    return retval;
  retval=usb_register(&te_driver);
  if (retval)
  {
    usb_serial_deregister(&te_device);
    return retval;
  }

  info(DRIVER_DESC " " DRIVER_VERSION);
  return 0;
}

static void __exit te_exit(void)
{
  usb_deregister(&te_driver);
  usb_serial_deregister(&te_device);
}

module_init(te_init);
module_exit(te_exit);

MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_VERSION(DRIVER_VERSION);
MODULE_LICENSE("GPL");
