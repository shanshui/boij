#!/usr/bin/env ruby
# imageadd_webber.rb

=begin

01/07/2010 c.e.prelz AS FLUIDO <fluido@fluido.as>
project )(horizon)(

Web-based manager for inserting new images

$Id: imageadd_webber.rb 2701 2011-01-20 16:46:05Z karl $

=end

require 'boij.so'
require 'image_dbase'

require 'webrick'
require 'webrick/httpserver'
require 'digest/md5'
require 'rmagick'

class Wnewimage
  attr_accessor(:image,:reduced_image,:md5,:horpos,:created,:msg,:folder,:rx,:ry)

  D_AREA=800*800#1024*1024
#  BORDER=70
#  LABEL=100
  BWIDTH=5

  NSTEPS=11
  FDOTS=30

  DIGFACT=1000

  REDUCE_FACTOR=0.6

  def initialize(image,md5,folder)
    @image,@md5,@folder=image,md5,folder
    @horpos=DIGFACT>>1
    @created=Time::now()

    @x=@image.columns
    @y=@image.rows
    @ratio=@x.to_f/@y

    @rx=(Math::sqrt(D_AREA*@ratio)).to_i
    @ry=(@rx/@ratio).to_i

    @reduced_image=@image.resize(@rx,@ry)

    @msg=nil

    save_ref_image()
  end

  def save_ref_image
=begin
    rimg=Magick::Image::new(@rx+BORDER*2+LABEL,@ry+BORDER*2) do
      self.background_color='yellow'
    end
    rimg.alpha(Magick::ActivateAlphaChannel)

    gc=Magick::Draw::new()
    gc.font=BASE+'font.ttf'
    gc.pointsize(FDOTS)
    gc.font_weight(100)

    gc.fill('black')
    gc.stroke('black')

    NSTEPS.times do |v|
      gc.stroke_width(0)
      vtp=(DIGFACT.to_f/(NSTEPS-1)*v).to_i
      pos=BORDER+@ry-(@ry/(NSTEPS-1).to_f*v).to_i
      gc.text(LABEL/3,pos,vtp.to_s)
      gc.stroke_width(3)
      gc.line(LABEL,pos,@rx+BORDER*2+LABEL,pos)
    end

    gc.composite(BORDER+LABEL,BORDER,0,0,@reduced_image)
    #    gc.fill_opacity(0)
    gc.fill('red')
    gc.stroke('white')
    gc.stroke_width(2)
    gc.opacity(0.6)
    pos=BORDER+@ry-(@ry*(@horpos/DIGFACT.to_f))
    gc.polygon(LABEL,pos-BWIDTH,@rx+BORDER*2+LABEL,pos-BWIDTH,@rx+BORDER*2+LABEL,pos+BWIDTH,LABEL,pos+BWIDTH)
    gc.draw(rimg)
=end
    rimg=Magick::Image::new(@rx,@ry)
    rimg.alpha(Magick::ActivateAlphaChannel)

    gc=Magick::Draw::new()

    gc.composite(0,0,0,0,@reduced_image)
    gc.fill('red')
    gc.stroke('white')
    gc.stroke_width(2)
    gc.opacity(0.6)
    pos=@ry-(@ry*(@horpos/DIGFACT.to_f))
    gc.polygon(0,pos-BWIDTH,@rx,pos-BWIDTH,@rx,pos+BWIDTH,0,pos+BWIDTH)
    gc.draw(rimg)

    rimg.write(MATBASE+@md5+'.png')
    rimg.destroy!()
  end

  def save(id)
    #
    # First save original resolution
    #

    @image.write("#{PICTBASE}#{id}.png")

    ratio=SCREEN_SIZE[1]/@y.to_f()
    new_size=[@x*ratio,SCREEN_SIZE[1]]
    i=@image.resize(*new_size)
    new_size=[i.columns(),i.rows()]
    horiz=(@horpos/DIGFACT.to_f*new_size[1]).to_i
    loggo("#{@md5}: resulting image is [#{new_size[0]}X#{new_size[1]}] with hori #{horiz}.")

    fn="#{PICTBASE}#{id}.bo2"
    File::open(fn,'w') do |f|
      f.puts(@md5)
      f.printf("%d,%d,%d\n",*new_size,horiz)
      new_size[0].times do |col|
        f.write(i.export_pixels_to_str(col,0,1,i.rows,'BGR'))
      end
    end

    #
    # Save thumbnails
    #

    gc=Magick::Draw::new()
    gc.fill('red')
    gc.stroke('white')
    gc.stroke_width(2)
    gc.opacity(0.6)
    pos=new_size[1]-(new_size[1]*(@horpos/DIGFACT.to_f))
    gc.polygon(0,pos-BWIDTH,new_size[0],pos-BWIDTH,new_size[0],pos+BWIDTH,0,pos+BWIDTH)
    gc.draw(i)
    i.scale!(REDUCE_FACTOR)
    i.write(MATBASE+'/medium/'+@md5+'.jpg')
    i.scale!(REDUCE_FACTOR)
    i.write(MATBASE+'/small/'+@md5+'.jpg')

    i.destroy!()
    loggo("#{@md5} dumped.")
  end
end
class Imageadd_webber < WEBrick::HTTPServer
  L_PATH='l1'
  L_VALIDATE='l2'
  L_FORGET='l3'
  L_HORPOS='l4'
  L_FOLDER='l5'
  L_CHANGE_FOLDER='l6'

  def rootpage(req,res)
    cookies,paras=process_req(req)

    header(res,'Insert new image')
    res.body+="<div id=\"zle\"><form id=\"zlo\" enctype=\"multipart/form-data\" action=\"/image?new\" method=\"post\">\
<p><input id=\"zli\" name=\"#{L_PATH}\"type=\"file\" multiple=\"1\" /></form></div></p>"
    res.body+=<<EOF
<script type="text/javascript">
<!--
var form=document.getElementById("zlo");
var inp=document.getElementById("zli");
inp.onchange=zlo;
function zlo(e)
{
  form.submit();
  document.getElementById("zle").innerHTML="<h1>WAIT!</h1>";
}
//-->
</script>
<noscript>
 <b>You don't have JavaScript enabled or your browser does not support JavaScript</b>
</noscript>
EOF
    footer(res,WEBrick::HTTPStatus::RC_OK)
  end

  def imagepage(req,res)
    cookies,paras=process_req(req)
    if(paras['new'])
      begin
        loggo("Il para e` un #{paras[L_PATH].class}")
        loggo("ovvero #{paras[L_PATH].filename}")
        paras[L_PATH].each_data do |bloz|
          loggo("Il bloz e` un #{bloz.class}")
          loggo("ovvero #{bloz.filename}")
        end
        md5=Digest::MD5::hexdigest(paras[L_PATH])
        exv=@db.image_with_md5(md5)
        if(exv)
          header(res,'Already exists!')
          res.body+="<h1>Image #{paras[L_PATH].filename} is already in our database!</h1><p><a href=\"/\">BACK</a></p>"
          footer(res,WEBrick::HTTPStatus::RC_OK)
          return
        end
        img=Wnewimage::new(Magick::Image::from_blob(paras[L_PATH])[0],md5,@default_folder)
      rescue
        header(res,'Bad image')
        res.body+="<h1>Cannot decode #{paras[L_PATH].filename}</h1><p><a href=\"/\">BACK</a></p>"
        footer(res,WEBrick::HTTPStatus::RC_OK)
        return
      end

      @open_images[img.md5]=img

      res.cookies.push(WEBrick::Cookie::new('imid',img.md5))
      res.set_redirect(WEBrick::HTTPStatus::Found,'/image')
      return
    end

    if(!cookies['imid'])
      header(res,'Go away')
      res.body+="<h1>GO AWAY!!</h1>"
      footer(res,WEBrick::HTTPStatus::RC_OK)
      return
    end

    img=@open_images[cookies['imid']]

    if(!img)
      res.set_redirect(WEBrick::HTTPStatus::Found,'/')
      return
    end

    if(paras[L_FORGET])
      drop_img(img)
      res.set_redirect(WEBrick::HTTPStatus::Found,'/')
      return
    end

    if(paras[L_VALIDATE])
      imid=@db.insert_image(img.md5,img.folder.id,img.horpos)
      img.save(imid)
      drop_img(img)
      reload_folder_tree()
      begin
        Fluid_logfile::killpid('manager_webber','USR1')
        Fluid_logfile::killpid('engine','USR1')
      rescue
      end
      header(res,'Saved')
      res.body+="<h1>Your image has been saved!</h1><p>I now have #{how_many_images()} images.</p><p><a href=\"/\">BACK</a></p>"
      footer(res,WEBrick::HTTPStatus::RC_OK)
      return
    end

    if(paras[L_HORPOS])
      v=paras[L_HORPOS].to_i
      if(v!=img.horpos)
        loggo("New hor #{v}")
        if(v<0 || v>Wnewimage::DIGFACT)
          img.msg="#{v}: BAD HORIZON VALUE (must be between 0 and #{Wnewimage::DIGFACT})"
          res.set_redirect(WEBrick::HTTPStatus::Found,'/image')
          return
        end
        img.horpos=v
        img.save_ref_image()
      end
    end

    if(paras[L_CHANGE_FOLDER])
      header(res,'Change folder')

      res.body+="<p>Choose folder:</p><form action=\"/image\" method=\"post\">"
      @rootfolders.values.sort do |a,b|
        a.name<=>b.name
      end.each do |rf|
        recurse_add_folder(res,rf,0,img.folder.id)
      end
      res.body+="<p><button type=\"submit\">SUBMIT CHANGE</button></form></p>"
      footer(res,WEBrick::HTTPStatus::RC_OK)
      return
    end

    if(paras[L_FOLDER])
      img.folder=@folders[paras[L_FOLDER]]
      res.set_redirect(WEBrick::HTTPStatus::Found,'/image')
      return
    end

    header(res,'Tune horizon')
    res.body+="<center>"
    if(img.msg)
      res.body+="<h2>#{img.msg}</h2>"
      img.msg=nil
    end

    res.body+="<p>The size of the image is <strong>#{img.image.columns}</strong>X<strong>#{img.image.rows}</strong></p>\
<p>Horizon is currently at <strong>#{img.horpos}</strong></p>
<div id=\"canvas\" style=\"overflow:hidden;position:relative;height:#{img.ry}px;width:#{img.rx}px;\"></div>\
<p><form id=\"shor\" action=\"/image\" method=\"post\">Set horizon to <input type=\"text\" name=\"#{L_HORPOS}\" \
value=\"#{img.horpos}\" /> (value from 0 to #{Wnewimage::DIGFACT})</form></p>\
<p><form action=\"/image\" method=\"post\">Current folder is #{img.folder.complete_name()}\
<button type=\"submit\" name=\"#{L_CHANGE_FOLDER}\">CHANGE IT</button></form></p>\
<p><form action=\"/image\" method=\"post\"><button type=\"submit\" name=\"#{L_VALIDATE}\">VALIDATE</button></form></p>\
<p><form action=\"/image\" method=\"post\"><button type=\"submit\" name=\"#{L_FORGET}\">FORGET</button></form></p>\
</form></center>"

    res.body+=<<EOF
<center></center>
<script type="text/javascript">
<!--
var canvas_div=document.getElementById("canvas");
var gr=new jsGraphics(canvas_div);
var col=new jsColor("red");
var pen=new jsPen(col,5);
var zeropoint=new jsPoint(0,0);
var mouse_y=0

var form=document.getElementById("shor");
canvas_div.onmousemove=mouo;
canvas_div.onclick=clio;

function mouo(e)
{
  mouse_y=e.pageY;
  if(mouse_y<0){mouse_y=0}
  mouse_y=mouse_y-canvas_div.offsetTop;

  gr.clear()

  gr.drawImage("material/#{img.md5}.jpg",zeropoint,#{img.rx},#{img.ry})
  gr.drawLine(pen,new jsPoint(0,mouse_y),new jsPoint(#{img.rx},mouse_y));

  return true;
}

function clio(e)
{
  form.#{L_HORPOS}.value=(#{img.ry}-mouse_y)/#{img.ry}.0*1000
  form.submit();
//  alert("Val: "+mouse_y);
//  return true;
}

//-->
</script>
<noscript>
 <b>You don't have JavaScript enabled or your browser does not support JavaScript</b>
</noscript>
EOF
    footer(res,WEBrick::HTTPStatus::RC_OK)
  end

  def recurse_add_folder(res,f,indent,checkedvalue)
    res.body+="<p><input type=\"radio\" name=\"#{L_FOLDER}\" value=\"#{f.md5}\" #{checkedvalue==f.id ? 'checked' : ''}>\
#{"&nbsp;"*indent}#{f.name}</input></p>"
    f.sons.values.sort do |a,b|
      a.name<=>b.name
    end.each do |son|
      recurse_add_folder(res,son,indent+4,checkedvalue)
    end
  end

  def initialize
    Fluid_logfile::new('imageadd_webber')

    @db=Image_dbase::new()
    reload_folder_tree()
    Signal::trap('USR1') do
      loggo("Request to reload")
      reload_folder_tree()
      true
    end

    @open_images={}

    super(:Port=>IMAGEADD_WEB_PORT,:ServerName=>'Bidibodibu',:ServerSoftware=>'AsFluidoHorizon')

    mount_proc('/',self.method(:rootpage).to_proc())
    mount_proc('/image',self.method(:imagepage).to_proc())
    mount("/material",WEBrick::HTTPServlet::FileHandler,MATBASE)
  end

  def reload_folder_tree
    @rootfolders,@folders,@images=Wfolder::load(@db)
    @default_folder=@rootfolders.values[0]
  end

  def process_req(req)
    cookies={}
    req.cookies.each do |ck|
      cookies[ck.name]=ck.value
    end
    paras=req.query.update(WEBrick::HTTPUtils::parse_query(req.query_string))

    [cookies,paras]
  end

  def header(res,title)
    res['content-type']='text/html'
    res.body+="<html><head><title>#{title}</title><script type=\"text/javascript\" src=\"material/jsDraw2D.js\"></script></head><body><center><h1>#{title}</h1></center>"
  end

  def footer(res,status)
    res.status=status
    res.body+="</body></html>"
  end

  def drop_img(img)
    img.reduced_image.destroy!()
    img.image.destroy!()
    @open_images.delete(img.md5)
  end

  def how_many_images
    n=0
    @folders.values.each do |f|
      n+=f.local_images.length
    end
    n
  end
end

if(__FILE__==$0)
  SLEEP_TIME=3

  $ws=Imageadd_webber::new()
  $webthread=Thread::new do
    $ws.start()
  end

  def die
    $ws.shutdown()
    $webthread.join()
    exit(0)
  end

  Signal::trap('INT') do
    die()
  end
  Signal::trap('HUP') do
    die()
  end
  Signal::trap('TERM') do
    die()
  end

  loop do
#    $ws.maintain()
    sleep(SLEEP_TIME)
  end
end
