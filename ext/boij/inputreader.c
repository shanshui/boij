/* inputreader.c */

/*
 * 8/2/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * Read dev/input file, firing ruby function when receiving appropriate events
 *
 * $Id: inputreader.c 2666 2010-12-29 16:37:27Z karl $
 */

#include "boij.h"

#include <ruby.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <linux/input.h>

VALUE cls_inputreader;

typedef struct
{
  int unr,unit,n_revents;
  struct input_event *revents;
} inputreader_stc;

static void free_inputreader(void *p);

VALUE new_inputreader(VALUE self,VALUE vunit,VALUE reqevents)
{
  inputreader_stc *ir=ALLOC(inputreader_stc);
  char bfr[256];

  ir->unr=FIX2INT(vunit);
  
  sprintf(bfr,"/dev/input/event%d",ir->unr);
  ir->unit=open(bfr,O_RDONLY|O_NONBLOCK);
  if(ir->unit<0)
    rb_raise(rb_eRuntimeError,"%s: Error opening input device %s (%s)",__func__,bfr,strerror(errno));

  int version;
  if(ioctl(ir->unit,EVIOCGVERSION,&version)<0)
    rb_raise(rb_eRuntimeError,"%s: Error getting version from input device %s (%s)",__func__,bfr,strerror(errno));

  unsigned short id[4];
  if(ioctl(ir->unit,EVIOCGID,id)<0)
    rb_raise(rb_eRuntimeError,"%s: Error getting device ID from input device %s (%s)",__func__,bfr,strerror(errno));

  char name[256]="Unknown";
  if(ioctl(ir->unit,EVIOCGNAME(sizeof(name)),name)<0)
    rb_raise(rb_eRuntimeError,"%s: Error getting device name from input device %s (%s)",__func__,bfr,strerror(errno));

  fprintf(stderr,"Opened event device #%d.\nInput driver version %d.%d.%d.\nDevice ID: bus %x vendor %x product %x version %x\nDevice name: <%s>\n",
	  ir->unr,
	  version>>16,(version>>8)&0xff,version&0xff,
	  id[ID_BUS],id[ID_VENDOR],id[ID_PRODUCT],id[ID_VERSION],
	  name);

  int i;
  VALUE v;
  
  ir->n_revents=RARRAY_LEN(reqevents);
  ir->revents=malloc(sizeof(struct input_event)*ir->n_revents);

  for(i=0;i<ir->n_revents;i++)
  {
    v=rb_ary_entry(reqevents,i);
    ir->revents[i].type=FIX2INT(rb_ary_entry(v,0));
    ir->revents[i].code=FIX2INT(rb_ary_entry(v,1));
    ir->revents[i].value=FIX2INT(rb_ary_entry(v,2));
  }
	  
  return Data_Wrap_Struct(cls_inputreader,NULL,free_inputreader,ir);
}

static void free_inputreader(void *p)
{
  inputreader_stc *ir=(inputreader_stc *)p;

  close(ir->unit);

  free(ir->revents);
  free(ir);
}

VALUE ireader_getevent(VALUE self)
{
  inputreader_stc *ir;
  Data_Get_Struct(self,inputreader_stc,ir);
  struct input_event ev;
  int i;

  while(1)
  {
    int len=read(ir->unit,&ev,sizeof(struct input_event));

    if(len!=sizeof(struct input_event))
      break;

    for(i=0;i<ir->n_revents;i++)
      if(ir->revents[i].type==ev.type &&
	 ir->revents[i].code==ev.code &&
	 ir->revents[i].value==ev.value)
	return INT2FIX(i);
  }  
  return Qfalse;
}



