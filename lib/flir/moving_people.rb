# moving_people.rb

=begin

16/12/2008 c.e prelz AS FLUIDO fluido@fluido.as
project .:laspal:.

The moving people, as a DRb-accessible array. N areas, collected via flir camera

$Id: moving_people.rb 2666 2010-12-29 16:37:27Z karl $

=end

require 'drb/drb'
require 'movireceptor'
require 'flirconv'
require 'quadrilat'

class Mp_onearea
#  THRESHPARA=[34,16,21,4,3]
  THRESHPARA=[34,16,21,4,3]

  attr_reader(:prog,:regrouper,:qlat,:threshpara)
  attr_accessor(:objects,:mrecept)

  def initialize(mp,prog)
    @mp,@prog=mp,prog
    @qlat=Quadrilat::new(prog)
    @regrouper=Regrouper::new(*CAPT.clipped_frame,MAXGRPS)
    @threshpara=THRESHPARA
    @regrouper.set_threshs(@threshpara)
    maprect()
    @objects=nil
  end

  def maprect
    @mp.vc.set_mapping_rect(@prog,@qlat.ul.coords,@qlat.ur.coords,@qlat.lr.coords,@qlat.ll.coords)
  end

  def threshpara=(tp)
    @threshpara=tp
    @regrouper.set_threshs(@threshpara)
  end
end

class Moving_people
  SAVEFILE=BASE+'mp.data'
  TRYTIMES=50
  
  attr_reader(:die,:flirunit,:areas,:vc)
  
  def initialize(n_areas,flirtalk=true)
    @die=false
    
    @n_areas,@flirtalk=n_areas,flirtalk
    @vc=Videocapt::new(CAPT.vcapt_arr,@n_areas)
    if(@flirtalk)
      @flirunit=Flir::new(CAPT.serunit,CAPT.serspeed)
      
      resp=@flirunit.cmd(5,nil)[1]
      loggo("Found FLIR camera on port #{CAPT.serunit}. Firmware #{resp[0]*255+resp[1]}.#{resp[2]*255+resp[3]}. "+
            "Software #{resp[4]*255+resp[5]}.#{resp[6]*255+resp[7]}.")
    else
      @flirunit=false
    end

    @areas=[]
    @n_areas.times do |i|
      @areas.push(Mp_onearea::new(self,i))
    end
    @frames=nil
    @prog_succ=0

    #
    # Processes that receive the people info have their own DRb port open
    # See if they are open
    # Try a few times
    #

    @areas.each do |a|
      a.mrecept=nil
    end
    
    TRYTIMES.times do
      failv=false
      @areas.each do |a|
        next if(a.mrecept)
        a.mrecept=Movireceptor::link_to(a.prog)
        begin
          a.mrecept.respond_to?(:newstuff)
          loggo("Mrecept client ##{a.prog} has connected")
        rescue => err
          loggo("Mrecept client ##{a.prog} is not listening!! (#{err})")
          a.mrecept=nil
          failv=true
        end
      end
      break if(!failv)
      sleep(2)
    end

    load_values()
    #
    # Now send a USR1 to moving_people_master
    #
#    at_exit do
#      save_values()
#    end
  end

  def latest_stuff(cnt,whicharea=nil)
    return false if(cnt==@prog_succ)
    
    return [@prog_succ,@frames,@areas.map do |a|
              a.objects
            end] if(!whicharea)
    [@prog_succ,@areas[whicharea].objects.map do |a|
       a[4,3]
     end]  
  end

  def sync_qlat(qlat)
    @areas[qlat.progr].qlat.update_dots(qlat)
    @areas[qlat.progr].maprect()
  end

  def flircmd(cmd,data)
    @flirunit.cmd(cmd,data) if(@flirunit)
  end

  def update_threshparas(which,tp)
    @areas[which].threshpara=tp
  end

  def roi_rect
    @flirunit.roi
  end

  def looper_loop
    fms=@vc.latest_frame(true)
    if(fms)
      @areas.each_with_index do |a,i|
        a.objects=a.regrouper.regroup(fms[i+1])
        if(a.mrecept)
          if(a.objects.length()>0)
            a.mrecept.newstuff(a.objects.map do |ob|
                                 ob ? [ob[4],1.0-ob[5],ob[6]] : nil
                               end)
          else
            a.mrecept.newstuff(nil)
          end
        end
      end
      @frames=fms
      @prog_succ+=1
    end
  end

  def save_values
    first_avail_back=1
    loop do
      break if(!File.exists?("#{SAVEFILE}.#{first_avail_back}"))
      first_avail_back+=1
    end
    first_avail_back.downto(2) do |v|
      FileUtils.mv("#{SAVEFILE}.#{v-1}","#{SAVEFILE}.#{v}")
    end
    FileUtils.mv(SAVEFILE,"#{SAVEFILE}.1") if(File.exists?(SAVEFILE))
    File::open(SAVEFILE,'w') do |f|
      f.write("# Moving_people savefile\n# Created #{Time::now().to_s()}\n")
      if(@flirunit)
        f.write("#\n# FLIR Camera data\n#\n")        
        f.write("FLIR_AGCTYPE##{@flirunit.agc_type[1]}\n") if(@flirunit.agc_type)
        f.write("FLIR_CONTRAST##{@flirunit.contrast[0]*256+@flirunit.contrast[1]}\n") if(@flirunit.contrast)
        f.write("FLIR_BRIGHTNESS##{@flirunit.brightness[0]*256+@flirunit.brightness[1]}\n") if(@flirunit.brightness)
        f.write("FLIR_ITTFILT##{@flirunit.ittfilter[0]*256+@flirunit.ittfilter[1]}\n") if(@flirunit.ittfilter)
        f.write("FLIR_PLATEAU##{@flirunit.plateau[0]*256+@flirunit.plateau[1]}\n") if(@flirunit.plateau)
        f.write("FLIR_ITTMIDPOINT##{@flirunit.ittmidpoint[0]*256+@flirunit.ittmidpoint[1]}\n") if(@flirunit.ittmidpoint)
        f.write("FLIR_MAXAGCGAIN##{@flirunit.maxagcgain[0]*256+@flirunit.maxagcgain[1]}\n") if(@flirunit.maxagcgain)
        f.write("FLIR_ROI##{@flirunit.roi[0]},#{@flirunit.roi[1]},#{@flirunit.roi[2]},#{@flirunit.roi[3]}\n") if(@flirunit.roi)
      end
      @areas.each do |a|
        f.write("#\n# Area #{a.prog}\n#\n")
        th=a.regrouper.get_threshs()
        f.write("AREA_THRESHOLDS##{a.prog}##{th[0]},#{th[1]},#{th[2]},#{th[3]},#{th[4]}\n")
        f.write("AREA_QUADRILAT##{a.prog}##{a.qlat.to_s()}\n")
      end
    end
  end
  
  def load_values
    return if(!File.exists?(SAVEFILE))
    File::open(SAVEFILE,'r') do |f|
      f.lines.each do |l|
        next if(l[0,1]=='#')
        flds=l.split('#')
        case flds[0]
        when 'FLIR_AGCTYPE'
          @flirunit.set_agc_type([0,flds[1].to_i]) if(@flirunit)
        when 'FLIR_CONTRAST'
          @flirunit.set_contrast([flds[1].to_i()>>8,flds[1].to_i()&0xff]) if(@flirunit)
        when 'FLIR_BRIGHTNESS'
          @flirunit.set_brightness([flds[1].to_i()>>8,flds[1].to_i()&0xff]) if(@flirunit)
        when 'FLIR_ITTFILT'
          @flirunit.set_ittfilter([flds[1].to_i()>>8,flds[1].to_i()&0xff]) if(@flirunit)
        when 'FLIR_PLATEAU'
          @flirunit.set_plateau([flds[1].to_i()>>8,flds[1].to_i()&0xff]) if(@flirunit)
        when 'FLIR_ITTMIDPOINT'
          @flirunit.set_ittmidpoint([flds[1].to_i()>>8,flds[1].to_i()&0xff]) if(@flirunit)
        when 'FLIR_MAXAGCGAIN'
          @flirunit.set_maxagcgain([flds[1].to_i()>>8,flds[1].to_i()&0xff]) if(@flirunit)
        when 'FLIR_ROI'
          a=flds[1].split(',').map do |v|
            v.to_f()
          end
          @flirunit.set_roi(a)
        when 'AREA_THRESHOLDS'
          area=flds[1].to_i()
          vals=flds[2].split(',').map do |v|
            v.to_f()
          end
          @areas[area].threshpara=vals
        when 'AREA_QUADRILAT'
          area=flds[1].to_i()
          vals=flds[2].split(',').map do |v|
            v.to_f()
          end
          @areas[area].qlat.ul.x=vals.shift()
          @areas[area].qlat.ul.y=vals.shift()
          @areas[area].qlat.ur.x=vals.shift()
          @areas[area].qlat.ur.y=vals.shift()
          @areas[area].qlat.lr.x=vals.shift()
          @areas[area].qlat.lr.y=vals.shift()
          @areas[area].qlat.ll.x=vals.shift()
          @areas[area].qlat.ll.y=vals.shift()
          @areas[area].maprect()
        end
      end
    end
  end

  def cur_brightness
    (@flirunit.brightness[0] << 8)|@flirunit.brightness[1]
  end
  
  def set_brightness(val)
    begin
      @flirunit.set_brightness([val >> 8,val&0xff])
    rescue => err
      loggo("Error setting brightness! (#{err})")
    end
  end
end
    
