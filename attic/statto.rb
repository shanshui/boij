require 'pp'

class Period
  attr_reader(:stime,:etime)

  @@mintime=nil
  @@maxtime=nil
  
  def initialize(stime,etime)
    @stime,@etime=stime,etime

    @@mintime=@stime if(!@@mintime || @@mintime>@stime)
    @@maxtime=@etime if(!@@maxtime || @@maxtime<@etime)
  end

  def Period::range
    [@@mintime,@@maxtime]
  end
end

lines=nil
File::open(ARGV[0],'r') do |f|
  lines=f.readlines()
end

arrived_arr=[]
periods=[]

re=/^\[\d{6}.(\d{6})\] Player (\d{1,2}) (arrived|left)/
lines.each do |l|
  m=re.match(l)
  next if(!m)
  a=m.captures()
  pl=a[1].to_i()
  time=a[0][0,2].to_i()*3600+a[0][2,2].to_i()*60+a[0][4,2].to_i()
  arrived=a[2]=='arrived'
  if(arrived)
    arrived_arr[pl]=time
  else
    if(arrived_arr[pl])
      periods.push(Period::new(arrived_arr[pl],time))
      arrived_arr[pl]=nil
    end
  end
end

mf=1.0/60.0
min_tots=Array::new(1440,0)
periods.each() do |p|
  ms=p.stime/60
  me=p.etime/60

  if(ms==me)
    min_tots[ms]+=mf*(p.etime%60-p.stime%60)
  else
    min_tots[ms]+=mf*(60-p.stime%60)
    min_tots[me]+=mf*(p.etime%60)
    if((me-ms)>1)
      (ms+1).upto(me-1) do |m|
	min_tots[m]+=1.0
      end
    end
  end
end

sti,eti=Period::range()
(sti/60).upto(eti/60) do |m|
  printf("%2.2d:%2.2d %f\n",m/60,m%60,min_tots[m])
end
