{
  inputs = {
    lmspkgs.url = "gitlab:shanshui/liblms1xx/master";
  };

  outputs = { self, nixpkgs, lmspkgs }:
    let
      supportedSystems = [ "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor =
        forAllSystems (system:
          import nixpkgs {
            inherit system;
            overlays = [ self.overlay ];
          });
    in
      {
        overlay = final: prev:
          with final;
          {
            boij-app = bundlerApp {
              pname = "boij";
              gemdir = ./.;
              exes = [ "sick-runner" ];

              ruby = ruby_2_7;
              gemConfig = defaultGemConfig // {
                rubysdl = attrs: {
                  nativeBuildInputs = [ pkg-config ];
                  buildInputs = [ SDL SDL_image SDL_ttf ];
                };
              };

              passthru.updateScript = bundlerUpdateScript "boij";
            };

            boij-env = bundlerEnv {
              pname = "boij";
              gemdir = ./.;
              exes = [ "sick-runner" ];

              ruby = ruby_2_7;
              gemConfig = defaultGemConfig // {
                rubysdl = attrs: {
                  nativeBuildInputs = [ pkg-config ];
                  buildInputs = [ libGL SDL SDL_image SDL_ttf ];
                };
              };

              passthru.updateScript = bundlerUpdateScript "boij";
            };

            liblms1xx = lmspkgs.packages.${system}.liblms1xx;

            boij = buildRubyGem rec {
              gemName = "boij";
              version = "0.1.0";
              src = self;

              ruby = ruby_2_7;
              propagatedBuildInputs = [ boij-env ];
              nativeBuildInputs = [ pkg-config ];
              buildInputs = [ alsaLib libGL dejavu_fonts sox liblms1xx ];

              NIX_CFLAGS_COMPILE = toString ([
                "-Wno-error"
                "-I${liblms1xx}/include"
              ]);
              NIX_LDFLAGS = toString ([
                "-L${liblms1xx}/lib"
              ]);

              postInstall = ''
                for f in $out/bin/{engine-sick.rb,sick-runner.rb,raalter-runner.rb}; do
                  wrapProgram "$f" \
                    --prefix LD_LIBRARY_PATH : ${lib.makeLibraryPath [ libGL ]}
                done
                install bin/*.sh $out/bin
              '';
            };
          };

        packages =
          forAllSystems (system:
            nixpkgsFor.${system});

        defaultPackage =
          forAllSystems (system:
            nixpkgsFor.${system}.boij);
      };
}
