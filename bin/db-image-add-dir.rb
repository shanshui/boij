#!/usr/bin/env ruby

raise "Usage: #{$0} dir foldername [height]" if(ARGV.length < 2)

require 'boij.so'
require 'image_dbase'
require 'rmagick'

db=Image_dbase::new()

fld=db.get_folder(ARGV[1],true)
screen_y=ARGV[2].to_i
screen_y=SCREEN_SIZE[1] if screen_y.nil? or screen_y == 0
horpos=0.5

Dir::foreach(ARGV[0]) do |entry|
  next if(entry[0,1]=='.')
  fn=ARGV[0]+'/'+entry
  next if(!(File::file?(fn)))

  md5=Digest::MD5::hexdigest(fn)
  if(db.image_with_md5(md5))
    loggo("#{fn} (#{md5}) already exists")
    next
  end

  im=nil
  begin
    im=Magick::Image::read(fn)[0]
  rescue => err
    loggo("Skipping #{fn} (#{err})")
    next
  end

  ratio=screen_y/im.rows.to_f()
  ssize=[im.columns*ratio,screen_y]
  im.resize!(*ssize)

  new_size=[im.columns(),im.rows()]
  horiz=(new_size[1]*horpos).to_i
  loggo("#{@md5}: resulting image is [#{new_size[0]}X#{new_size[1]}] with hori #{horiz}.")

  imid=db.insert_image(md5,fld,horpos)

  fn="#{PICTBASE}#{imid}.bo2"

  File::open(fn,'w') do |f|
    f.puts(md5)
    f.printf("%d,%d,%d\n",*new_size,horiz)
    f.write(to_roworder(im.export_pixels_to_str,*new_size))
  end
  im.destroy!()
end
