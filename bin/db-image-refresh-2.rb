#!/usr/bin/env ruby
# refresh_image_database_2.rb

=begin

11/2/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Clears the database of images, then loads all images included into a text
file passed to stdin
Second version - all images resized to actual screen height

$Id: refresh_image_database_2.rb 1493 2008-02-12 14:57:49Z karl $

=end

require 'boij.so'

STDERR.printf("Producing for screen size %dX%d (%dX%d)\n",*SCREEN_SIZE,*WINDOW_SIZE)

imgs=[]
STDIN.each_line() do |l|
  next if(l[0,1]=='#')
  a=l.chomp().split('#')
  raise "Bad line #{l.chomp()}!" if(a.length()!=2)
  imgs.push([a[0],a[1].to_i()])
end

raise "No images defined in standard input!" if(imgs.length()<=0)

#
# Clean dir
#

system("rm #{PICTURE_BASE}/*.bo2")
resp=[]
imgs.each do |img|
  newp=Picture::load2(*img)
  resp.push(Picture::new(newp,true))
end
resp.each do |r|
  STDERR.printf("Image #%3.3d: %dX%d (%d)\n",r.prog,*(r.size),r.bottom_offset)
end
                 
            
    
