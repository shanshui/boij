#!/usr/bin/env ruby
# background_saver.rb

=begin

13/03/2012 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

Saves tye currently operative parameters and background data
for the Sick camera

$Id: background_saver.rb 3468 2012-03-20 13:56:50Z karl $

=end

require 'boij.so'
require 'getoptlong'
require 'sick/sick_config'

class Backgsa
  READS=25

  def initialize(scfg = nil, s = nil, dname = nil, srange = nil, angres = nil)
    @s = s if s
    if scfg
      @scfg = scfg
    else
      @scfg = Sick_config::load
      @scfg.dname = dname if dname
      @scfg.srange = srange if srange
      @scfg.angres = angres if angres
    end
  end

  def runme
    unless @s
      @s=@scfg.get_sick_for_init()
      raise "Could not connect to camera!" unless(@s)
    end

    acc=nil
    cnt=0

    loop do
      r=@s.msg
      if(r)
        d=r[0]
        loggo("##{cnt+1} read #{d.length}")
        if(!acc)
          acc=d
        else
          d.each_with_index do |v,i|
            acc[i]=v if(acc[i]>v)
          end
        end
        cnt+=1
        break if(cnt==READS)
      end
    end

    @scfg.backg = acc
    @scfg.save()

    putc("\n")
    loggo("Dumped background")

    @s.close()
  end
end

if($0==__FILE__)
  dname=nil
  srange=nil
  angres=nil

  opts=GetoptLong::new(['--dname','-d',GetoptLong::REQUIRED_ARGUMENT],
                   ['--scanrange','-s',GetoptLong::REQUIRED_ARGUMENT],
                   ['--angres','-a',GetoptLong::REQUIRED_ARGUMENT])

  opts.each do |opt,arg|
    case opt
    when '--dname'
      dname=arg
    when '--scanrange'
      srange=arg.to_i
      raise "Bad scanrange (#{srange}, must be either 100 or 180)" if(srange!=100 && srange!=180)
    when '--angres'
      angres=arg.to_i
      raise "Bad ang res (#{angres}, must be 25 or 50)" if(angres!=25 && angres!=50)
    else
      raise "Usage: #{$0} -d device_path -s scanrange -a angres (#{opts.error_message()})" unless(dname && srange && angres)
    end
  end

  Backgsa::new(nil,nil,dname,srange,angres).runme()
end
