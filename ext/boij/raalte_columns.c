/* raalte_columns.c */

/*
 * 15/3/2011 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * Smoother display generator engine, for Raalte
 *
 * $Id$
 */

#include <ruby.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>

#include "boij.h"

#define EXPFACT (1.0/9000.0)  // (1.0/9000.0) // ACCEL FACT - THE LOWEST THE BIG NBR, THE FASTEST IT ACCELERATES
#define SIZEFACT 300.0 //500.0 // BASIC SPEED

typedef struct
{
  char *path;
  __u32 n_cols,horiz;
  __u8 **cols;
} img_stc;

typedef struct ele
{
  __u32 cnt;
  img_stc *img;
  float scale;
  __u16 col_start,col_from,col_to,used_cols,last_width,*scale_x,*scale_y;
  __u8 backw_flg;
  struct timespec birth;
  struct ele *next_ele;
} ele_stc;

typedef struct
{
  ele_stc *ele;
  __u32 col;
  __u8 new_flg;
} col_stc;

typedef struct
{
  __u32 ele_cnt;
  __u16 w,h,marg_up,marg_down,n_imgs;
  __u8 *dumppark,*ballpark,***bpark_cols,stop_thr;
  int dumpsize,screensize;
  int screen_horiz;
  ele_stc *ele_head;
  pthread_t compute_thr;
  pthread_mutex_t mtx;
  img_stc **imgs;
  col_stc *cols;
} raalte_stc;

static void free_raalte_columns(void *p);
static void *compute(void *arg);
static void ele_free(ele_stc *ele);
static img_stc *add_image(raalte_stc *s,char *path);
static void prepare_scales(raalte_stc *s,ele_stc *ele);
static int timediff(struct timespec *a,struct timespec *b);
static float distance(int tdiff);

VALUE cls_raalte_columns;

VALUE new_raalte_columns(VALUE self,VALUE v_w,VALUE v_h,VALUE v_marg_up,VALUE v_marg_down,VALUE v_screen_horiz)
{
  raalte_stc *s=ALLOC(raalte_stc);
  int i,j;

  s->w=FIX2INT(v_w);
  s->h=FIX2INT(v_h);
  s->marg_up=FIX2INT(v_marg_up);
  s->marg_down=FIX2INT(v_marg_down);
  s->screen_horiz=s->h-FIX2INT(v_screen_horiz);

  s->cols=malloc(sizeof(col_stc)*s->w);
  bzero(s->cols,sizeof(col_stc)*s->w);

  s->dumpsize=s->w*(s->h+s->marg_up+s->marg_down)*3;
  s->screensize=s->w*s->h*3;

  s->dumppark=malloc(s->dumpsize);
  bzero(s->dumppark,s->dumpsize);

  s->ballpark=s->dumppark+s->marg_down*s->w*3;
  s->bpark_cols=malloc(sizeof(__u8 **)*s->w);

  for(i=0;i<s->w;i++)
  {
    s->bpark_cols[i]=malloc(sizeof(__u8 *)*s->h);
    for(j=0;j<s->h;j++)
      s->bpark_cols[i][j]=s->ballpark+((j*s->w+i)*3);
  }

  s->ele_head=NULL;
  s->ele_cnt=0;

  s->n_imgs=0;
  s->imgs=NULL;

  int ret=pthread_mutex_init(&s->mtx,NULL);

  if(ret)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,ret,strerror(errno));

  s->stop_thr=0;
  ret=pthread_create(&s->compute_thr,NULL,compute,s);
  if(ret)
    fprintf(stderr,"%s: error #%d starting compute thread\n",__func__,ret);

  return Data_Wrap_Struct(cls_raalte_columns,NULL,free_raalte_columns,s);
}

static void free_raalte_columns(void *p)
{
  raalte_stc *s=(raalte_stc *)p;
  int i,j;
  ele_stc *ele,*nele;
  img_stc *img;

  s->stop_thr=1;

  ele=s->ele_head;
  while(ele)
  {
    nele=ele->next_ele;
    ele_free(ele);
    ele=nele;
  }

  for(i=0;i<s->w;i++)
    free(s->bpark_cols[i]);
  free(s->bpark_cols);
  free(s->dumppark);

  for(i=0;i<s->n_imgs;i++)
  {
    img=s->imgs[i];

    for(j=0;j<img->n_cols;j++)
      free(img->cols[j]);
    free(img->cols);
    free(img->path);
    free(img);
  }

  free(s->imgs);

  free(s->cols);

  free(s);
}

VALUE raalte_add_ele(VALUE self,VALUE v_path,VALUE v_scale,
		     VALUE v_colstart,VALUE v_colfrom,VALUE v_colto,
		     VALUE v_backw)
{
  raalte_stc *s;
  Data_Get_Struct(self,raalte_stc,s);

  float scale=NUM2DBL(v_scale);

  if(scale<=0.0)
  {
    loggo("%s: Warning: scale is 0 or negative (%f)",__func__,scale);
    return Qfalse;
  }

  ele_stc *ele=malloc(sizeof(ele_stc)),*elep;
  char *path=RSTRING_PTR(v_path);
  int i;

  ele->cnt=s->ele_cnt;
  s->ele_cnt++;

  ele->img=NULL;

  for(i=0;i<s->n_imgs;i++)
    if(!strcmp(s->imgs[i]->path,path))
    {
      ele->img=s->imgs[i];
      break;
    }

  if(!ele->img)
  {
    ele->img=add_image(s,path);
    loggo("Loaded img#%d (%s)",s->n_imgs,path);
  }

  ele->last_width=0;

  ele->scale=scale;
  ele->col_start=(int)(NUM2DBL(v_colstart)*s->w);
  ele->col_from=FIX2INT(v_colfrom);
  ele->col_to=FIX2INT(v_colto);
  ele->used_cols=(int)(ele->col_to-ele->col_from+1)/ele->scale;
  ele->backw_flg=(v_backw==Qtrue) ? 1 : 0;
//  loggo("Added %s/%f/%d/%d-%d(%d)",path,ele->scale,ele->col_start,ele->col_from,ele->col_to,ele->used_cols);

  clock_gettime(CLOCK_MONOTONIC,&ele->birth);

  prepare_scales(s,ele);

  pthread_mutex_lock(&s->mtx);
  ele->next_ele=NULL;
  if(!s->ele_head)
    s->ele_head=ele;
  else
  {
    for(elep=s->ele_head;elep->next_ele;elep=elep->next_ele)
      ;
    elep->next_ele=ele;
  }
  pthread_mutex_unlock(&s->mtx);

  return self;
}

VALUE raalte_curframe(VALUE self)
{
  raalte_stc *s;
  Data_Get_Struct(self,raalte_stc,s);
  VALUE to_ret;

  pthread_mutex_lock(&s->mtx);
  to_ret=rb_str_new((char *)s->dumppark,s->dumpsize);
  pthread_mutex_unlock(&s->mtx);

  return to_ret;
}

static void *compute(void *arg)
{
  raalte_stc *s=(raalte_stc *)arg;
  struct timespec now;
  ele_stc *prelep,*elep,*elep2;
  __u16 d,cnt,locp,*sptr;
  __u8 *icols,found_flg;
  int col,y;

  while(!s->stop_thr)
  {
    clock_gettime(CLOCK_MONOTONIC,&now);
    for(elep=s->ele_head;elep;elep=elep->next_ele)
    {
      d=(__u16)distance(timediff(&now,&elep->birth));
      if(d!=elep->last_width)
      {
//	fprintf(stderr,"%s/%.2f from %d to %d\n",elep->img->path,elep->scale,elep->last_width,d);
	for(cnt=elep->last_width;cnt<d;cnt++)
	{
	  locp=cnt%elep->used_cols;
	  if(((cnt/elep->used_cols)%2)!=0)
	    locp=elep->used_cols-locp-1;
	  if((elep->col_start-cnt)>=0 && (s->cols[elep->col_start-cnt].ele==NULL ||
					  s->cols[elep->col_start-cnt].ele->cnt<elep->cnt))
	  {
	    s->cols[elep->col_start-cnt].ele=elep;
	    s->cols[elep->col_start-cnt].col=locp;
	    s->cols[elep->col_start-cnt].new_flg=1;
	  }
	  if(cnt>0 && (elep->col_start+cnt)<s->w && (s->cols[elep->col_start+cnt].ele==NULL ||
						     s->cols[elep->col_start+cnt].ele->cnt<elep->cnt))
	  {
	    s->cols[elep->col_start+cnt].ele=elep;
	    s->cols[elep->col_start+cnt].col=locp;
	    s->cols[elep->col_start+cnt].new_flg=1;
	  }
	}
	elep->last_width=d;
      }
    }

/*
 * now copy the image parts
 */

    pthread_mutex_lock(&s->mtx);
    for(col=0;col<s->w;col++)
      if(s->cols[col].new_flg)
      {
	elep=s->cols[col].ele;
	icols=elep->img->cols[elep->scale_x[s->cols[col].col]];

	for(sptr=elep->scale_y,y=0;y<s->h;y++,sptr++)
	  memcpy(s->bpark_cols[col][y],icols+(*sptr)*3,3);

	s->cols[col].new_flg=0;
      }

/*
 * reap the completed ones
 */

    for(prelep=NULL,elep=s->ele_head;elep;prelep=elep,elep=elep->next_ele)
      if(elep->col_start-elep->last_width<0 && elep->col_start+elep->last_width>=s->w)
      {
	for(found_flg=0,col=0;col<s->w;col++)
	  if(s->cols[col].ele==elep)
	  {
	    found_flg=1;
	    break;
	  }
	if(!found_flg) // reaping!
	{
	  elep2=elep->next_ele;
	  ele_free(elep);
	  elep=elep2;

	  if(!prelep)
	    s->ele_head=elep;
	  else
	    prelep->next_ele=elep;
	}
      }
//    fputc('.',stderr);
    pthread_mutex_unlock(&s->mtx);

    usleep(1000);
  }

  return NULL;
}

static void ele_free(ele_stc *ele)
{
  free(ele->scale_x);
  free(ele->scale_y);
  free(ele);
}

static img_stc *add_image(raalte_stc *s,char *path)
{
  FILE *funit=fopen(path,"r");

  if(!funit)
    rb_raise(rb_eArgError,"%s: error opening %s (%s)",__func__,path,strerror(errno));

  char bfr[256];
  int i;

  if (fgets(bfr,256,funit) == NULL) /* md5*/
      rb_raise(rb_eArgError,"%s: error fgets returned NULL",__func__);
  if (fgets(bfr,256,funit) == NULL)
      rb_raise(rb_eArgError,"%s: error fgets returned NULL",__func__);

  s->imgs=realloc(s->imgs,sizeof(img_stc *)*(s->n_imgs+1));
  s->imgs[s->n_imgs]=malloc(sizeof(img_stc));

  img_stc *img=s->imgs[s->n_imgs];

  s->n_imgs++;

  img->path=strdup(path);

  sscanf(bfr,"%d,%d,%d",&img->n_cols,&i,&img->horiz);
//  img->horiz=s->h-img->horiz;

  if(i!=s->h)
    rb_raise(rb_eArgError,"%s: %s: bad height (%d, should be %d)",__func__,path,i,s->h);

  img->cols=malloc(sizeof(char *)*img->n_cols);
  for(i=0;i<img->n_cols;i++)
  {
    img->cols[i]=malloc(s->h*3);
    if (fread(img->cols[i],s->h,3,funit) == 0)
      rb_raise(rb_eArgError,"%s: error fread returned 0",__func__);
  }

  fclose(funit);

  return img;
}

static void prepare_scales(raalte_stc *s,ele_stc *ele)
{
  int x,y,np;

//  loggo("Preparo orizz %d: %d\n",ele->cnt,ele->img->horiz);

  ele->scale_y=malloc(sizeof(__u16)*s->h);

  for(y=0;y<s->h;y++)
  {
    np=(int)((y-s->screen_horiz)*ele->scale+ele->img->horiz);

//   loggo("%d -> %d (%.2f, %d)",y,np,ele->scale,ele->img->horiz);
    if(np<0)
      ele->scale_y[y]=0;
    else if(np>=s->h)
      ele->scale_y[y]=s->h-1;
    else
      ele->scale_y[y]=np;
  }

  ele->scale_x=malloc(sizeof(__u16)*ele->used_cols);

  for(x=0;x<ele->used_cols;x++)
  {
    np=(int)(ele->col_from+x*ele->scale);
    if(ele->backw_flg)
      ele->scale_x[ele->used_cols-x-1]=np;
    else
      ele->scale_x[x]=np;
  }
}

static int timediff(struct timespec *a,struct timespec *b)
{
  return (a->tv_sec - b->tv_sec) * 1000 + (a->tv_nsec - b->tv_nsec) / 1000000;
}

/*
 * Receives time diff in msecs
 * returns (float) distance in columns
 */

static float distance(int tdiff)
{
  return (expf(tdiff*EXPFACT)-1.0)*SIZEFACT;
}
