#!/usr/bin/env ruby
# playground_faker_drb.rb

=begin

07/07/2010 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This is the faker of the playground - a gtk window where we may have persons
appearing or disappearing, moving, etc
Version that talks to the engine via drb

$Id: playground_faker_drb.rb 2626 2010-11-10 14:48:16Z karl $

=end


require 'boij'

require 'sdl'

class Ball
  RADIUS=10
  BALLC=[[255,0,0],[255,128,0],[255,255,0],[0,255,0],[80,80,255],[40,0,128],[200,0,200]]
  TIMEFACTOR=5.0
  @@cnt=0
  MAXRANDMOVE=0.7
  MAXREDUCE=0.3
  REDUCE_POS=0.01

  attr_accessor(:pos)
  attr_reader(:col)

  def initialize(faker,pos)
    @faker,@pos=faker,pos
    @col=BALLC[@@cnt%BALLC.length()]
    @moveto=nil
    @randmove=[0.0,0.0]

    @@cnt+=1
  end

  def fire_moveto(pos,time)
    @movefrom=@pos
    @moveto=pos
    @movedist=[@moveto[0]-@movefrom[0],@moveto[1]-@movefrom[1]]
    @movefrom_time=Time::now()
    @moveto_time=@movefrom_time+time*TIMEFACTOR
    @move_len=@moveto_time-@movefrom_time
    @randmove=[0.0,0.0]
  end

  def move
    if(@moveto)
      now=Time::now()
      if(now>=@moveto_time)
        @pos=@moveto
        @moveto=nil
      else
        fact=(now-@movefrom_time)/@move_len
        @pos=[@movefrom[0]+@movedist[0]*fact,@movefrom[1]+@movedist[1]*fact]
      end
    else
      2.times do |i|
        if(rand()<REDUCE_POS)
          @randmove[i]*=rand()*MAXREDUCE
        else
          @randmove[i]+=rand()*(MAXRANDMOVE*2)-MAXRANDMOVE
          @pos[i]+=@randmove[i]
          if(@pos[i]<0.0)
            @randmove[i]=-@randmove[i]
            @pos[i]=0.0
          elsif(@pos[i]>@faker.localsize[i])
            @randmove[i]=-@randmove[i]
            @pos[i]=@faker.localsize[i]
          end
        end
      end
    end
  end
end

class Faker
  attr_accessor(:chgflg,:localsize)

  MM_PER_PIXEL=10
  BACKG=[0,0,0]
  MINDIST=Ball::RADIUS*2

  def initialize
    SDL::init(SDL::INIT_VIDEO)

    @localsize=PLAYGROUND_SIZE.collect do |v|
      v/MM_PER_PIXEL
    end
    @w=SDL::Screen::open(*@localsize,32,SDL::HWSURFACE|SDL::DOUBLEBUF)
    @balls=Array::new(Boij_sharedvideoarea::MAX_MONITORED)

    DRb::start_service()
    @eng=DRbObject::new(nil,drb_uri(DRB_PORT_IMG))

    @chgflg=false
  end

  def runme
    @selected=nil
    @done=false
    while(!@done)
      @chgflg=false
      loop do
        event=SDL::Event2::poll()
	break if(!event)
	case event
	  #      when MouseMotionEvent
	  #        if(@selected && @selected[0])
	  #          @selected[0].pos=event.pos
	  #          @chgflg=true
	  #        end
	when SDL::Event2::MouseButtonDown
          pos=[event.x,event.y]
	  case event.button
	  when 1
	    @selected=find_closest(pos).push(Time::now())
	    @chgflg=true
	  when 2
	    Boij_sharedvideoarea::MAX_MONITORED.times do |b|
	      if(!@balls[b])
		@balls[b]=Ball::new(self,pos)
		@chgflg=true
		break
	      end
	    end
	  when 3
	    t=find_closest(pos)
	    if(t[0])
	      @balls[@balls.index(t[0])]=nil
	      @chgflg=true
	    end
	  end
	when SDL::Event2::MouseButtonUp
          pos=[event.x,event.y]
	  if(event.button==1 && @selected && @selected[0])
	    @selected[0].fire_moveto(pos,Time::now()-@selected[2])
	    @selected=nil
	    @chgflg=true
	  end
	when SDL::Event2::KeyDown
	  @done=true if(event.key==K_ESCAPE)
	when SDL::Event2::Quit
	  @done=true
	end
      end
      @balls.each() do |b|
        if(b)
          b.move()
          @chgflg=true
        end
      end

      if(@chgflg)
        paint_screen()
        pkt=@balls.collect do |b|
          b ? [b.pos[0].to_f()/@localsize[0],
               b.pos[1].to_f()/@localsize[1]] : nil
        end
        @eng.new_positions(pkt)
      end
      sleep(0.01)
    end
    @w.destroy()
  end

  def find_closest(pos)
    mindist=MINDIST
    clb=nil
    @balls.each do |b|
      next if(!b)
      xd=b.pos[0]-pos[0]
      yd=b.pos[1]-pos[1]
      dist=Math::sqrt(xd*xd+yd*yd)
      if(dist<mindist)
        clb=b
        mindist=dist
      end
    end
    [clb,mindist]
  end

  def paint_screen
    @w.fill_rect(0,0,0,0,BACKG)
    @balls.each do |b|
      next if(!b)
      pp b.pos
      @w.fill_rect(b.pos[0]-Ball::RADIUS,b.pos[1]-Ball::RADIUS,
                   Ball::RADIUS*2+1,Ball::RADIUS*2+1,[255,255,255]) if(@selected && @selected[0]==b)

      @w.draw_circle(*b.pos,Ball::RADIUS,b.col)
    end
    @w.flip
  end
end

lf=Fluid_logfile::new('faker')

fo=Faker::new()
begin
  fo.runme()
rescue
  pp ["Faker crash",$!,$!.backtrace()]
end
