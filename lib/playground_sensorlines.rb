#!/usr/bin/env ruby
# playground_sensorlines.rb

=begin

14/01/2011 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

This playground version is constituted by two lines of N sensors

$Id: playground_sensorlines.rb 2691 2011-01-14 16:20:29Z karl $

=end

require 'boij'
require 'movireceptor'

class Sensor
  ATT_FACTOR=0.03

  attr_reader(:line,:pos,:energy)

  def initialize(line,pos)
    @line,@pos=line,pos
    @energy=0.0
    @last_att=Time::now()
  end

  def kick(val)
    @energy+=val
  end

  def attenuate
    @energy*=1.0-rand()*ATT_FACTOR
  end
end

class Sensorline
  MIN_ENERGY=0.03
  EXPFACTOR=7.0
  MAX_MOTION_PER_SECOND=0.15
  
  attr_reader(:line_no,:sensors,:curpos,:curwgt)

  def initialize(line_no,nsensors)
    @line_no=line_no
    @sensors=[]
    nsensors.times do |i|
      @sensors.push(Sensor::new(self,i))
    end
    @curpos=nil
    @curwgt=nil
    @last_indupos=nil
    @last_indupos_time=nil
  end

  def energies
    @sensors.map do |s|
      s.energy
    end
  end

  def refresh_linepos
    curpos=nil
    curwgt=nil
    @sensors.each_with_index do |s,i|
      next if(s.energy<MIN_ENERGY)
      en=Math::exp(s.energy*EXPFACTOR)
      #      loggo("#{i}: #{s.energy}/#{en}")
      if(!curpos)
        curpos=Playground_sensorlines::SENSORPOS[i]
        curwgt=en
      else
        newpos=curwgt*curpos+en*Playground_sensorlines::SENSORPOS[i]
        curwgt+=en
        curpos=newpos/curwgt
      end
    end
    @curpos=curpos ? (curpos+1.0)/2.0 : nil

    if(curwgt && !@curwgt)
      @last_indupos=@curpos
      @last_indupos_time=Time::now
    end

    @curwgt=curwgt
  end

  def latest_indupos
    return nil if(!@curwgt)
    now=Time::now
    maxmot=(now-@last_indupos_time)*MAX_MOTION_PER_SECOND
    @last_indupos_time=now

    dist=@curpos-@last_indupos
    if(dist<-maxmot)
      dist=-maxmot
    elsif(dist>maxmot)
      dist=maxmot
    end
    @last_indupos+=dist
    [@last_indupos,@curwgt]
  end
end

class Playground_sensorlines
  NSENSORS=16
  SENSORPOS=[]
  NSENSORS.times do |i|
    SENSORPOS.push(i.to_f/(NSENSORS-1)*2.0-1.0)
  end

  LOOP_SLEEP=0.06
  REFR_POS=2 # every N cycles
  MAX_ENERGY=100.0

  def initialize
    @mrecept=Movireceptor::link_to(0)

    @vertline=Sensorline::new(0,NSENSORS)
    @horline=Sensorline::new(1,NSENSORS)
    @lines=[@vertline,@horline]
    @mtx=Mutex::new()

    DRb.start_service(SL_URI,self)
  end

  def runme
    @ncycle=0
    loop do
      cycle()
      sleep(LOOP_SLEEP)
    end
  end

  def cycle
    @lines.each do |l|
      l.sensors.each do |s|
        s.attenuate
      end
      l.refresh_linepos()
    end
    @ncycle+=1
    if(@ncycle%REFR_POS==0)
      @mrecept.newstuff(@lines.map do |l|
                          x,y=l.latest_indupos()
                          x ? [x,1.0-([y,MAX_ENERGY].min/MAX_ENERGY)] : nil
                        end)
    end
  end

  def energies
    @lines.map do |l|
      l.energies
    end
  end

  def lineaves
    @lines.map do |l|
      l.curpos ? [l.curpos,l.curwgt] : nil      
    end
  end

  def kick(x,y,val)
    if(x>=0 && y>=0 && x<NSENSORS && y<2)
      @mtx.synchronize do
        @lines[y].sensors[x].kick(val)
      end
    end
  end

  def Playground_sensorlines::getremote
    DRb.start_service()
    DRbObject::new(nil,SL_URI)
  end
end

if($0==__FILE__)
#  Fluid_logfile::new('sensorlines') 
  Playground_sensorlines::new().runme()
end
