#!/usr/bin/env ruby
# boij_main.rb

=begin

29/1/2008 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

Starts the whole process

$Id: boij_main.rb 2400 2010-07-06 09:41:49Z karl $

=end

require 'boij'
require 'playground_onlooker.rb'
require 'engine2.rb'

class Boij_main
  def initialize
    @procs=[]
    @procs.push(External_proc::new(Onlooker))
    sleep(2)
    @procs.push(External_proc::new(Engine))
    STDERR.printf("[%s] Onlooker pid: %d. Engine pid: %d\n",Time::now().stamp(),@procs[0].pid,@procs[1].pid)
  end

  def runme
    Signal::trap('INT') do
      expire()
    end
    Signal::trap('TERM') do
      expire()
    end
    loop do
      sleep(20)
    end
  end

  def expire
    STDERR.printf("[%s] Expiring.\n",Time::now().stamp())
    @procs.each do |p|
      p.expire()
    end
    sleep(2)
    exit(0)
  end
end

if($0==__FILE__)
  lf=Fluid_logfile::new('main')
  
  ma=Boij_main::new()
  begin
    ma.runme()
  rescue
    pp ["Main crash",$!,$!.backtrace()]
  end
end
