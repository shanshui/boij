#!/usr/bin/env ruby
# sensorline_phidget.rb

=begin

14/03/2011 c.e prelz AS FLUIDO fluido@fluido.as
project )(horizon)(

The phidget-based sensor for Raalte

$Id$

=end

require 'playground_sensorlines_3'

class Sens
  attr_reader(:pos,:cond)

  def initialize(pos)
    @pos=pos
    @cond=false
  end

  def set(val)
    return nil if(val==@cond)
    @cond=val
    #    loggo("#{pos} goes #{@cond}")
    @cond
  end
end

class Sensorline_phidget
  SLEEP_TIME=0.06
  MAPPING=[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,nil],
           [15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,nil]]
  FREEBAND=1

  def initialize
    @pg=Playground_sensorlines::getremote()
    @ph=Phidget::new()
    @mapele={}
    @mapping=MAPPING.map do |line|
      line.map do |ele|
        if(ele)
          @mapele[ele]=Sens::new(ele)
          @mapele[ele]
        else
          nil
        end
      end
    end
    Signal::trap('USR1') do
      randomkick()
    end
  end

  def runme
    randomkick()
    loop do
      2.times do |i|
        @ph.cvalue(i).each_with_index do |v,j|
          if(@mapping[i][j])
            m=@mapping[i][j]
            res=m.set(v==1)
            if(res)
              dokick=true
              FREEBAND.times do |v|
                if((@mapele[m.pos-v-1] && @mapele[m.pos-v-1].cond) ||
                   (@mapele[m.pos+v+1] && @mapele[m.pos+v+1].cond))
                  dokick=false
                  break
                end
              end
              if(dokick)
                loggo("Kicco #{m.pos}")
                @pg.kick(m.pos)
              end
            end
          end
        end
      end
      sleep(SLEEP_TIME)
    end
  end

  def randomkick
    m=@mapping[rand(2)][rand(15)]
    if(m)
      loggo("Random kick: #{m.pos}")
      @pg.kick(m.pos)
    end
  end
end

if($0==__FILE__)
  Sensorline_phidget::new.runme
end
