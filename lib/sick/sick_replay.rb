=begin

Replay previously captured SICK sensor data stream from file.

=end

require 'boij'

class Sick_replay
  def initialize(capfile)
    @cap = File::open(capfile, 'rb')
  end

  def close()
    @cap.close()
  end

  def tracker(gensum=false)
    @cap.seek(0, IO::SEEK_SET) if @cap.eof?
    Marshal::restore(@cap)
  end

  def msg()
    false
  end

  def get_mapped_background()
    []
  end

  def update_vals(ranges, minwgt, maxdist, maxdisapp)
  end

end
