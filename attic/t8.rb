require 'boij.rb'
require 'activity_handler'

MAX_SLEEP=0.5
MAX_TRAVEL=0.8

DRb::start_service()
ah=DRbObject::new(nil,drb_uri(DRB_PORT_ACTIVITY))
x,y=ah.dimensions
cur_pos=[rand()*x,rand()*y]

loop do
  sleep(rand()*MAX_SLEEP)
  cur_pos[0]+=rand()*(MAX_TRAVEL*2)-MAX_TRAVEL
  cur_pos[0]-=x if(cur_pos[0]>=x)
  cur_pos[0]+=x if(cur_pos[0]<0)
  cur_pos[1]+=rand()*(MAX_TRAVEL*2)-MAX_TRAVEL
  cur_pos[1]-=y if(cur_pos[1]>=y) 
  cur_pos[1]+=y if(cur_pos[1]<0) 
  ah.tickle(cur_pos[0].to_i,cur_pos[1].to_i,rand()*2.0)
end

  
