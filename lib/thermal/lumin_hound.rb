#!/usr/bin/env ruby
# lumin_hound.rb

=begin

20/01/2009 c.e. prelz AS FLUIDO <fluido@fluido.as>
project .:laspal:.

Tracks the grey level of given pixels from the thermocam to remain at wished-for bracket

$Id: lumin_hound.rb 2626 2010-11-10 14:48:16Z karl $

=end

require 'boij'

class Lumin_hound
  MASK_IMAGE=BASE+'mask.jpg'
  THRESH=200
  LOOP_TIME=20
  LOW_GREY=0.24
  HIGH_GREY=0.26
  CHANGE_VALUE=6

  def initialize()
    img=Magick::Image::read(MASK_IMAGE)[0]
    ix=img.columns()
    iy=img.rows()
    xfact=CAPT.clipped_frame[0].to_f()/ix
    yfact=CAPT.clipped_frame[1].to_f()/iy
    pix=img.export_pixels_to_str().unpack('C*')
    cnt=0
    mask=[]

    iy.times do |y|
      yeq=(y*yfact).to_i()
      ix.times do |x|
        dotn=yeq*CAPT.clipped_frame[0]+(x*xfact).to_i()

        mask.push(dotn) if(pix[cnt*3]>THRESH)
        cnt+=1
      end
    end

    @mask=mask.sort().uniq()

    DRb.start_service()
    @mp=DRbObject::new(nil,MP_URI)
    @latest_cnt=0
  end

  def runme
    loop do
      curgrey=getgrey()
      if(!curgrey)
        loggo("Current value is still null!")
      else
        loggo("Current value: #{curgrey}")
        if(curgrey<LOW_GREY)
          adapt_lumi(-CHANGE_VALUE)
        elsif(curgrey>HIGH_GREY)
          adapt_lumi(CHANGE_VALUE)
        end
      end
     sleep(LOOP_TIME)
    end
  end

  def getgrey
    arr=nil
    begin
      arr=@mp.latest_stuff(@latest_cnt)
    rescue => err
      loggo("Latest_stuff failing (#{err})")
    end
    return nil if(!arr)
    @latest_cnt=arr[0]
    px=arr[1][0].unpack('C*')
    totgrey=0
    @mask.each do |v|
      totgrey+=px[v]
    end
    totgrey/(@mask.length*256.0)
  end

  def adapt_lumi(val)
    begin
      cl=@mp.cur_brightness()
      loggo("Brightness from #{cl} to #{cl+val}")
      @mp.set_brightness(cl+val)
    rescue => err
      loggo("Was trying to adapt lumi  (#{err})")
    end
  end
end

if($0==__FILE__)
  Fluid_logfile::new('lhound')
  Lumin_hound::new().runme()
end
