/* imager.c */

/*
 * 11/2/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project )(horizon)(
 *
 * Handling image resize operations
 *
 * $Id: imager.c 3880 2013-01-26 11:08:12Z karl $
 */

#include "boij.h"

#include <ruby.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

VALUE cls_imager;

typedef struct
{
  int oldclient,client,relcol,prog;
} colval_stc;

typedef struct
{
  int x,y,horizon,*col_array;
  char *col,*screen,***screen_pointers;
  colval_stc *rows;
} imager_stc;

static void free_imager(void *p);

VALUE new_imager(VALUE self,VALUE x,VALUE y,VALUE horizon)
{
  imager_stc *ir=ALLOC(imager_stc);
  int i,j;

  ir->x=FIX2INT(x);
  ir->y=FIX2INT(y);
  ir->horizon=(int)(ir->y*NUM2DBL(horizon));

  ir->col_array=malloc(sizeof(int)*ir->y);
  ir->col=malloc(ir->y*3);
  ir->screen=malloc(ir->x*ir->y*3);

  ir->screen_pointers=malloc(sizeof(char **)*ir->y);
  for(i=0;i<ir->y;i++)
  {
    ir->screen_pointers[i]=malloc(sizeof(char *)*ir->x);
    for(j=0;j<ir->x;j++)
      ir->screen_pointers[i][j]=ir->screen+(ir->x*i+j)*3;
  }
  
  ir->rows=malloc(sizeof(colval_stc)*ir->x);
  for(i=0;i<ir->x;i++)
    ir->rows[i].client=0;

  return Data_Wrap_Struct(cls_imager,NULL,free_imager,ir);
}

static void free_imager(void *p)
{
  imager_stc *ir=(imager_stc *)p;
  int i;

  free(ir->col_array);
  free(ir->col);
  free(ir->screen);

  for(i=0;i<ir->y;i++)
    free(ir->screen_pointers[i]);
  free(ir->screen_pointers);
  
  free(ir->rows);
  free(ir);
}

#define ZOOMFACT_MIN -4.0
#define ZOOMFACT_MAX 7.0
#define ZOOMFACT_DIFF (ZOOMFACT_MAX-ZOOMFACT_MIN)

VALUE imager_resize(VALUE self,VALUE pixels,VALUE v_horizon_pos,VALUE v_xfrom,VALUE v_xto,VALUE v_zoom)
{
  imager_stc *ir;
  Data_Get_Struct(self,imager_stc,ir);
  
  int horizon_pos=FIX2INT(v_horizon_pos);
  int xfrom=FIX2INT(v_xfrom);
  int xto=FIX2INT(v_xto);
  int i,j,v;
  float zoom=NUM2DBL(v_zoom);

  zoom=expf(ZOOMFACT_MIN+ZOOMFACT_DIFF*zoom);

  float rv;

  for(i=ir->horizon,rv=(float)horizon_pos;i>=0;i--,rv-=zoom)
  {
#if 0
    if(rv<0.0)
      rv+=ir->y*2;
    v=(int)rv;
    ir->col_array[i]=v%ir->y;
    if((v/ir->y)%2)
      ir->col_array[i]=ir->y-ir->col_array[i]-1;
#else
    v=(int)rv;
    if(v>=0)
      ir->col_array[i]=v;
    else
      ir->col_array[i]=0;    
#endif
  }
  for(i=ir->horizon,rv=(float)horizon_pos;i<ir->y;i++,rv+=zoom)
  {
#if 0
    v=(int)rv;
    ir->col_array[i]=v%ir->y;
    if((v/ir->y)%2)
      ir->col_array[i]=ir->y-ir->col_array[i]-1;
#else
    v=(int)rv;
    if(v<ir->y)
      ir->col_array[i]=v;
    else
      ir->col_array[i]=ir->y-1;
#endif
  }
  
  VALUE to_ret=rb_ary_new();
  char *ptr1,*ptr2;
  /*loggo("horpos %d pct %f enl %f cols %d colstouse %d diff %d firstcol %d oc %d\n",horizon_pos,pct_to_use,zoom,
    cols,cols_to_use,cols-cols_to_use,first_col,output_cols);*/
  for(i=0,rv=(float)xfrom;rv<xto;i++,rv+=zoom)
  {
    ptr1=RSTRING_PTR(rb_ary_entry(pixels,(int)rv));
    for(j=0,ptr2=ir->col;j<ir->y;j++,ptr2+=3)
      memcpy(ptr2,ptr1+ir->col_array[j]*3,3);
    rb_ary_push(to_ret,rb_str_new(ir->col,ir->y*3));
  }    
    
  return to_ret;
}

VALUE imager_screenmaker(VALUE self,VALUE curclients)
{
  imager_stc *ir;
  Data_Get_Struct(self,imager_stc,ir);
  
/*
 * cur clients is an array of all cur valid pictures, with index, pixels, centercol, breadth and leftright.
 * Will return frame and array of clients that have disappeared
 */

  int n_cli=RARRAY_LEN(curclients),i,j,ncols,centercol,old_breadth,breadth,v,id;
  char leftright,*ptr;
  VALUE vv;

  for(i=0;i<ir->x;i++)
    ir->rows[i].oldclient=ir->rows[i].client;  

  for(i=0;i<n_cli;i++)
  {
    vv=rb_ary_entry(curclients,i);
    id=FIX2INT(rb_ary_entry(vv,0));
    ncols=RARRAY_LEN(rb_ary_entry(vv,1));
    centercol=FIX2INT(rb_ary_entry(vv,2));
    old_breadth=FIX2INT(rb_ary_entry(vv,3));
    breadth=FIX2INT(rb_ary_entry(vv,4));
    leftright=(rb_ary_entry(vv,5)==Qtrue) ? 1 : 0;

    for(j=old_breadth;j<=breadth;j++)
    {
      v=j%ncols;
      if((j/ncols)%2!=leftright)
	v=ncols-v-1;
/*fprintf(stderr,"(%d,%d)",j,v);*/
      if(centercol+j<ir->x && ir->rows[centercol+j].client<id)
      {
	ir->rows[centercol+j].client=id;
	ir->rows[centercol+j].relcol=v;
	ir->rows[centercol+j].prog=i;
      }
      if(centercol-j>=0 && ir->rows[centercol-j].client<id)
      {
	ir->rows[centercol-j].client=id;
	ir->rows[centercol-j].relcol=v;
	ir->rows[centercol-j].prog=i;
      }
    }
  }

  vv=rb_ary_new();

  for(i=0;i<ir->x;i++)
    if(ir->rows[i].client!=ir->rows[i].oldclient)
    {
      rb_ary_push(vv,INT2FIX(ir->rows[i].client));
      
      ptr=RSTRING_PTR(rb_ary_entry(rb_ary_entry(rb_ary_entry(curclients,ir->rows[i].prog),1),ir->rows[i].relcol));
      for(j=0;j<ir->y;j++,ptr+=3)
	memcpy(ir->screen_pointers[j][i],ptr,3);
    }

  VALUE to_ret=rb_ary_new();

  rb_ary_push(to_ret,rb_str_new(ir->screen,ir->x*ir->y*3));
  rb_ary_push(to_ret,vv);
  
  return to_ret;
}

VALUE imager_resetcnt(VALUE self,VALUE v_v)
{
  imager_stc *ir;
  Data_Get_Struct(self,imager_stc,ir);
  int v=FIX2INT(v_v),i;

  for(i=0;i<ir->x;i++)
    ir->rows[i].client-=v;

  return self;
}

VALUE imager_pixflip(VALUE self,VALUE frm)
{
  int frl=RSTRING_LEN(frm),i;
  __u8 *tmpf=malloc(frl),*curfrom=(__u8 *)RSTRING_PTR(frm),*curto=tmpf;

  for(i=0;i<frl/3;i++,curfrom+=3,curto+=3)
  {
    memcpy(curto,curfrom,3);
    /* curto[0]=curfrom[2]; */
    /* curto[1]=curfrom[1]; */
    /* curto[2]=curfrom[0]; */
  }

  VALUE toret=rb_str_new((char *)tmpf,frl);
  free(tmpf);

  return toret;
}
