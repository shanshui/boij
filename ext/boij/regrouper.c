/* regrouper.c */

/*
 * 20/12/2008 c.e. prelz AS FLUIDO <fluido@fluido.as>
 * project .:laspal:.
 *
 * regrouping, from boij
 *
 * $Id: regrouper.c 3451 2012-03-16 08:15:02Z karl $
 */

#include "boij.h"
#include <ruby.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <linux/types.h>

#define OVERSAMPLE_FACTOR 1.5
#define MAX_ACCEPTABLE_DISTANCE 80.0
#define MSEC_WAIT_BEFORE_LEAVING 800
#define MAX_RECURSE_LEVEL 1500

VALUE cls_regr;

const char *thresh_labels[N_THRESHS]={"When contrib to eval","When clipping eval",
				"When grouping (direct)","When grouping (recurse)","Group acceptance"};

typedef struct
{
  __u8 occupied;
  __u16 minx,maxx,miny,maxy,cx,cy,weight;
  struct client *associated;
  float fweight;
} blob_stc;

typedef struct client
{
  __u8 present,new_entry;
  struct timespec arrived,left;
  blob_stc blob,*newblob;
} client_stc;

typedef struct
{
  unsigned char fact;
  struct grid_pos *pos;
} ele_accum_stc;

typedef struct grid_pos
{
  __u16 x,y;
  unsigned int loc;
  ele_accum_stc **eles;
  int n_eles;
  float weight,eval;
  blob_stc *blob;
} grid_pos_stc;

typedef struct
{
  int x,y,npix,maxgrps;
  __u32 framesize;
  __u8 maxblobs,produced;
  float *myframe,myfe_max,thresh[N_THRESHS];
  grid_pos_stc *gridnodes;
  client_stc *clients;
  blob_stc *lblobs;
} regr_stc;

static void free_regr(void *p);

static void get_grid(regr_stc *r);
static void addgrid(grid_pos_stc *ep,regr_stc *r,int xc,int yc,int xo,int yo,int w);
static void freegrid(regr_stc *r);
static void gather(regr_stc *r);
static blob_stc *available_blob(regr_stc *r);
static void assign_blob(regr_stc *v,grid_pos_stc *gp,blob_stc *grp,unsigned short recurse_level);
static int time_diff(struct timespec *a,struct timespec *b);

VALUE regr_new(VALUE self,VALUE x,VALUE y,VALUE maxgrps)
{
  regr_stc *r=ALLOC(regr_stc);

  r->x=FIX2INT(x);
  r->y=FIX2INT(y);
  r->framesize=r->x*r->y;

  r->maxgrps=FIX2INT(maxgrps);
  r->maxblobs=(int)(r->maxgrps*OVERSAMPLE_FACTOR);

  r->clients=malloc(sizeof(client_stc)*r->maxgrps);
  bzero(r->clients,sizeof(client_stc)*r->maxgrps);

  r->lblobs=malloc(sizeof(blob_stc)*r->maxblobs);

  r->myframe=malloc(sizeof(float)*r->framesize);

  get_grid(r);

  return Data_Wrap_Struct(cls_regr,NULL,free_regr,r);
}

static void free_regr(void *p)
{
  regr_stc *r=(regr_stc *)p;

  free(r->myframe);
  free(r->lblobs);
  free(r->clients);
  freegrid(r);

  free(p);
}

VALUE regr_set_threshs(VALUE self,VALUE v_t)
{
  regr_stc *r;
  Data_Get_Struct(self,regr_stc,r);
  int i;

  for(i=0;i<N_THRESHS;i++)
  {
    r->thresh[i]=FIX2INT(rb_ary_entry(v_t,i))/100.0;
    fprintf(stderr,"Thr#%d set to %.2f (%d)\n",i+1,r->thresh[i],FIX2INT(rb_ary_entry(v_t,i)));
  }

  return self;
}

VALUE regr_get_threshs(VALUE self)
{
  regr_stc *r;
  Data_Get_Struct(self,regr_stc,r);
  int i;
  VALUE to_ret=rb_ary_new();

  for(i=0;i<N_THRESHS;i++)
    rb_ary_push(to_ret,INT2FIX((int)(r->thresh[i]*100.0)));

  return to_ret;
}

VALUE regr_regroup(VALUE self,VALUE vframe)
{
  regr_stc *r;
  Data_Get_Struct(self,regr_stc,r);
  int i,j;
  float fact=M_PI_2/(1.0-r->thresh[0]),fv,ang;
  __u8 *frame=(__u8 *)RSTRING_PTR(vframe);

  for(i=0;i<r->framesize;i++)
  {
    fv=((float)frame[i]/255.0);
    if(fv<r->thresh[0])
      r->myframe[i]=0.0;
    else
    {
      ang=(fv-r->thresh[0])*fact-M_PI_2;
      r->myframe[i]=sin(ang)+1.0;
/*      fprintf(stderr,"<%.2f %.2f %.2f>",fv,ang,r->myframe[i]);*/
    }
  }

  gather(r);

/*
 * Now associate to old ones if possible
 */

  for(i=0;i<r->maxblobs;i++)
    r->lblobs[i].associated=NULL;
  for(i=0;i<r->maxgrps;i++)
    r->clients[i].newblob=NULL;

  blob_stc *bf;
  client_stc *cf;
  float distance,min_distance;
  __s16 dx,dy;

  while(1)
  {
    bf=NULL;
    cf=NULL;
    min_distance=MAX_ACCEPTABLE_DISTANCE;

    for(i=0;i<r->maxblobs;i++)
      if(r->lblobs[i].occupied && r->lblobs[i].associated==NULL)
	for(j=0;j<r->maxgrps;j++)
	  if(r->clients[j].present && r->clients[j].newblob==NULL)
	  {
	    dx=r->lblobs[i].cx-r->clients[j].blob.cx;
	    dy=r->lblobs[i].cy-r->clients[j].blob.cy;

	    distance=sqrtf(dx*dx+dy*dy);
	    if(distance<min_distance)
	    {
	      bf=&r->lblobs[i];
	      cf=&r->clients[j];
	      min_distance=distance;
	    }
	  }

    if(bf==NULL)
      break;

/*fprintf(stderr,"Getto at %f\n",min_distance);*/

    bf->associated=cf;
    cf->newblob=bf;
  }

/*
 * The associations that were found
 */

  struct timespec tb;
  clock_gettime(CLOCK_MONOTONIC,&tb);

  for(i=0;i<r->maxgrps;i++)
  {
    if(r->clients[i].newblob!=NULL)
      r->clients[i].blob=*r->clients[i].newblob;
    else if(r->clients[i].present)
    {
      if(r->clients[i].left.tv_sec==0)
	r->clients[i].left=tb;
      else
      {
	int td=time_diff(&tb,&r->clients[i].left);
/*fprintf(stderr,"{%d/%d}",i,td);*/
	if(td>=MSEC_WAIT_BEFORE_LEAVING)
	  r->clients[i].present=0;
      }
    }
  }

/*
 * Some new ones?
 */

  for(i=0;i<r->maxblobs;i++)
    if(r->lblobs[i].occupied && r->lblobs[i].associated==NULL)
    {
      for(j=0;j<r->maxgrps;j++)
	if(!r->clients[j].present)
	{
	  r->clients[j].present=1;
	  r->clients[j].new_entry=1;
	  clock_gettime(CLOCK_MONOTONIC,&r->clients[j].arrived);
	  r->clients[j].left.tv_sec=0;
	  r->clients[j].blob=r->lblobs[i];
	  break;
	}
    }

  r->produced=1;

/*
 * Eventually, prepare the return array
 */

  VALUE to_ret=rb_ary_new(),v;

  for(i=0;i<r->maxgrps;i++)
    if(r->clients[i].present)
    {
      v=rb_ary_new();
      rb_ary_push(v,rb_float_new(r->clients[i].blob.minx/(float)r->x));
      rb_ary_push(v,rb_float_new(r->clients[i].blob.maxx/(float)r->x));
      rb_ary_push(v,rb_float_new(r->clients[i].blob.miny/(float)r->y));
      rb_ary_push(v,rb_float_new(r->clients[i].blob.maxy/(float)r->y));
      rb_ary_push(v,rb_float_new(r->clients[i].blob.cx/(float)r->x));
      rb_ary_push(v,rb_float_new(r->clients[i].blob.cy/(float)r->y));
      rb_ary_push(v,rb_float_new(r->clients[i].blob.fweight));
      rb_ary_push(v,rb_float_new(r->clients[i].arrived.tv_sec+
				 r->clients[i].arrived.tv_nsec/1000000.0));
      rb_ary_push(to_ret,v);
    }

  return to_ret;
}

/*
 * Stuff for finding existing pixel blobs.
 */

static void get_grid(regr_stc *r)
{
  int x,y;

  r->gridnodes=malloc(sizeof(grid_pos_stc)*r->framesize);
  grid_pos_stc *app=r->gridnodes;

  for(y=0;y<r->y;y++)
  {
    for(x=0;x<r->x;x++,app++)
    {
      bzero(app,sizeof(grid_pos_stc));
      app->loc=app-r->gridnodes;
      app->x=x;
      app->y=y;

      addgrid(app,r,x,y,0,0,8);
      addgrid(app,r,x,y,-1,0,4);
      addgrid(app,r,x,y,1,0,4);
      addgrid(app,r,x,y,0,-1,4);
      addgrid(app,r,x,y,0,1,4);
      addgrid(app,r,x,y,-1,-1,3);
      addgrid(app,r,x,y,-1,1,3);
      addgrid(app,r,x,y,1,-1,3);
      addgrid(app,r,x,y,1,1,3);
      addgrid(app,r,x,y,-2,0,2);
      addgrid(app,r,x,y,2,0,2);
      addgrid(app,r,x,y,0,-2,2);
      addgrid(app,r,x,y,0,2,2);
      addgrid(app,r,x,y,-1,-2,1);
      addgrid(app,r,x,y,-2,-1,1);
      addgrid(app,r,x,y,1,-2,1);
      addgrid(app,r,x,y,2,-1,1);
      addgrid(app,r,x,y,1,2,1);
      addgrid(app,r,x,y,2,1,1);
      addgrid(app,r,x,y,-1,2,1);
      addgrid(app,r,x,y,-2,1,1);
    }
  }
}

static void addgrid(grid_pos_stc *ep,regr_stc *r,int xc,int yc,int xo,int yo,int w)
{
  int nx=xc+xo;
  int ny=yc+yo;

  if(nx>=r->x || nx<0 || ny>=r->y || ny<0)
    return;

  ep->eles=realloc(ep->eles,sizeof(ele_accum_stc *)*(ep->n_eles+1));
  ep->eles[ep->n_eles]=malloc(sizeof(ele_accum_stc));
  ep->eles[ep->n_eles]->pos=r->gridnodes+(ny*r->x+nx);
  ep->eles[ep->n_eles]->fact=w;
  ep->n_eles++;

  ep->weight+=(float)w;
}

static void freegrid(regr_stc *v)
{
  int i,j;

  for(i=0;i<v->framesize;i++)
  {
    for(j=0;j<v->gridnodes[i].n_eles;j++)
      free(v->gridnodes[i].eles[j]);
    free(v->gridnodes[i].eles);
  }
  free(v->gridnodes);
}

static void gather(regr_stc *r)
{
  int i,j;

  r->myfe_max=0.0;

  for(i=0;i<r->framesize;i++)
  {
    r->gridnodes[i].blob=NULL;
    r->gridnodes[i].eval=0.0;

    for(j=0;j<r->gridnodes[i].n_eles;j++)
    {
//      if(i==500)
//	/**/fprintf(stderr,"[%d %d,%d,%.2f}",j,r->gridnodes[i].eles[j]->fact,r->gridnodes[i].eles[j]->pos->loc,r->myframe[r->gridnodes[i].eles[j]->pos->loc]);
      r->gridnodes[i].eval+=r->gridnodes[i].eles[j]->fact*
	r->myframe[r->gridnodes[i].eles[j]->pos->loc];
    }

    r->gridnodes[i].eval/=r->gridnodes[i].weight;
/*if(r->gridnodes[i].eval>=r->thresh[1])
  fprintf(stderr,"{%d: %.2f,%.2f,%d )(%.2f)}",i,r->gridnodes[i].eval,r->gridnodes[i].weight,r->gridnodes[i].n_eles,r->thresh[1]);*/
    if(r->gridnodes[i].eval<r->thresh[1])
      r->gridnodes[i].eval=0;
    else
    {
      r->gridnodes[i].eval-=r->thresh[1];
      if(r->myfe_max<r->gridnodes[i].eval)
	r->myfe_max=r->gridnodes[i].eval;
    }
  }

/*
 * Now the blobs
 */

  for(i=0;i<r->maxblobs;i++)
    r->lblobs[i].occupied=0;

  grid_pos_stc *cur_gp;
  blob_stc *found_blob;
  int maxwgt=0;

  for(i=0;i<r->framesize;i++)
  {
    cur_gp=&(r->gridnodes[i]);

    if(cur_gp->blob!=NULL)
      continue;

    if(cur_gp->eval<(float)r->thresh[2])
      continue;

/*
 * First search for the heaviest blob that we can connect to
 */

    for(found_blob=NULL,j=0;j<cur_gp->n_eles;j++)
    {
      if(cur_gp->eles[j]->pos->blob!=NULL && cur_gp->eles[j]->pos->blob->weight>maxwgt)
      {
	found_blob=cur_gp->eles[j]->pos->blob;
	maxwgt=found_blob->weight;
      }
    }
    if(found_blob==NULL)
      found_blob=available_blob(r);
    if(found_blob==NULL)
      continue;
    assign_blob(r,cur_gp,found_blob,0);
  }

  for(i=0;i<r->maxblobs;i++)
    r->lblobs[i].fweight=0.0;

  for(i=0;i<r->framesize;i++)
    if(r->gridnodes[i].blob)
      r->gridnodes[i].blob->fweight+=r->myframe[i];

  int nblobs=0;
  float cmpv=r->thresh[4]*r->framesize*0.05;

  for(i=0;i<r->maxblobs;i++)
    if(r->lblobs[i].occupied>0)
    {
/*      fprintf(stderr,"{%d: %.2f,%.2f,%.2f}",i,r->lblobs[i].fweight,r->thresh[4],cmpv);*/

      if(r->lblobs[i].weight<cmpv)
      {
/*fprintf(stderr,"<%d,%.2f>",r->lblobs[i].weight,cmpv);*/
	r->lblobs[i].occupied=0;
      }
      else
      {
	r->lblobs[i].cx=r->lblobs[i].minx+((r->lblobs[i].maxx-r->lblobs[i].minx)>>1);
	r->lblobs[i].cy=r->lblobs[i].miny+((r->lblobs[i].maxy-r->lblobs[i].miny)>>1);
	nblobs++;
/*fprintf(stderr,"[%2.2d] %d,%d (%d,%.2f)\n",nblobs,r->lblobs[i].cx,r->lblobs[i].cy,r->lblobs[i].weight,r->lblobs[i].fweight);*/
      }
    }

/*  fputc('\n',stderr);*/
}

static blob_stc *available_blob(regr_stc *r)
{
  int i;

  for(i=0;i<r->maxblobs;i++)
    if(r->lblobs[i].occupied<=0)
    {
      r->lblobs[i].occupied=1;
      r->lblobs[i].maxx=r->lblobs[i].maxy=0;
      r->lblobs[i].minx=r->x;
      r->lblobs[i].miny=r->y;
      r->lblobs[i].weight=0;
      return &r->lblobs[i];
    }

  return NULL;
}

static void assign_blob(regr_stc *r,grid_pos_stc *gp,blob_stc *blob,unsigned short recurse_level)
{
  if(gp->blob)
  {
    if(gp->blob->weight<=1)
      gp->blob->occupied=0;
    else
      gp->blob->weight-=1;
  }

  gp->blob=blob;
  if(blob->minx>gp->x)
    blob->minx=gp->x;
  else if(blob->maxx<gp->x)
    blob->maxx=gp->x;
  if(blob->miny>gp->y)
    blob->miny=gp->y;
  else if(blob->maxy<gp->y)
    blob->maxy=gp->y;
  blob->weight++;

/*
 * Now see if I can assign blob to some other remote locations
 */

  if(recurse_level>=MAX_RECURSE_LEVEL)
    return;

  int i;

  for(i=0;i<gp->n_eles;i++)
    if(gp->eles[i]->pos->blob==NULL && gp->eles[i]->pos->eval>=(float)r->thresh[3])
      assign_blob(r,gp->eles[i]->pos,blob,recurse_level+1);
}

static int time_diff(struct timespec *a,struct timespec *b)
{
  /*fprintf(stderr,"(a %ld.%hd b %ld.%hd)",a->time,a->millitm,b->time,b->millitm);*/
  return (a->tv_sec - b->tv_sec) * 1000 + (a->tv_nsec - b->tv_nsec) / 1000000;
}
